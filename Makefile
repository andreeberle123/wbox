
MKDIR=mkdir
CP=cp
CCADMIN=CCadmin
CC = g++
SOURCES := $(wildcard *.cpp)
CSOURCES := $(wildcard *.c)
GLSOURCES = $(wildcard gl/*.cpp)
LOGSOURCES := $(wildcard log/*.c)

OBJS = $(SOURCES:.cpp=.o) $(GLSOURCES:.cpp=.o) $(CSOURCES:.c=.o)  $(LOGSOURCES:.c=.o) trap.o

LIBS = -lpthread -lGL -lGLU -lGLEW -lglut -lfreetype -lxcb-glx -lX11-xcb -lX11 -lxcb -ljson-c -lyaml -L/usr/lib/gcc/i686-linux-gnu/5/ -ldl -L. -lSDL2 -lxcb-randr -lxcb-cursor -lxcb-icccm
TARGET = wb

INCLUDEDIR = -Iinclude -I/usr/include/freetype2 -Isdl/include
CFLAGS = $(INCLUDEDIR) -fno-stack-protector -m32 -ggdb -rdynamic -fPIC -Wno-write-strings -Wno-format -Wno-int-to-pointer-cast -std=c++11 -fshort-wchar -DBITS16_SUPPORT
CXXFLAGS = $(INCLUDEDIR) -fno-stack-protector -m32 -ggdb -rdynamic -fPIC -Wno-write-strings -Wno-format -Wno-int-to-pointer-cast -std=c++11 -fshort-wchar -fmax-errors=10 -DBITS16_SUPPORT
CFLAGS_SHARED = $(CFLAGS) -shared
SIP_LIBS = $(LIBS) -losipparser2 
SLIBS = 

all: wb 

$(TARGET): asm $(OBJS) $(COBJS) $(GLOBJS) $(GEOOBJS) 
	$(CC) $(CFLAGS) $(M32_CFLAGS) -o $(TARGET) $(OBJS)  $(SLIBS)  $(LIBS) 

shared: $(OBJS) $(COBJS) $(TOBJS) $(GLOBJS) $(GEOOBJS)
	$(CC) $(CFLAGS) $(M32_CFLAGS) -fPIC -shared -o /usr/lib/libsm.so $(OBJS) $(LIBS)

asm:
	nasm -f elf -o trap.o trap.S

clean:
	-find -name "*.o" -delete
    
	
java:
	javac

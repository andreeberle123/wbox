#include "common.h"
#include <stdarg.h>
#include <string.h>


FILE * fp = NULL;

void log_data(char *file, unsigned int line, int level, char *category,
              const char *data, ...) {
    
    if (!fp) {
        fp = fopen("wb.log","w");
    }
    va_list va;
    va_start(va,data);
    char dbl[1024];    

    vsprintf(dbl,data,va);
    
    int len = strlen(dbl);
    if (dbl[len-1] == '\n') {
        dbl[len-1] = 0;
    }
    
    fprintf(fp,"%s.[%i] [%s] %s\n",file,line,category,dbl);
    fflush(fp);
    
    va_end(va);
}
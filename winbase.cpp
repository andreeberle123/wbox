#include "common.h"
#include <wchar.h>
#include <dlfcn.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>

#include <SDL.h>

#include "fs.h"
#include "gl.h"
#include <pthread.h>
#include "winm.h"
#include "dx.h"


DWORD _lastError = 0;

dword werrno = 0;



extern "C" {
    
    
char * process_ini(FILE * fp,char * lpAppName,char * lpKeyName, char * dest) {
    char line [1024];
    int state = 0;
    while (fgets(line,1024,fp)) {
        //printf("%s\n",line);
        if (line[0] == '[') {
            if (!strncmp(lpAppName,line+1,strlen(lpAppName))) {
                state = 1;
            }
            else {
                state = 0;
            }
        }
        else {
            if (state == 1) {
                if (!strncmp(line,lpKeyName,strlen(lpKeyName))) {
                    char * t = line+strlen(lpKeyName)+1;
                    t[strlen(t)-1] = 0;
                    strcpy(dest,t);
                    return dest;
                }
            }
        }
    }
    
    return dest;
}

typedef struct _STARTUPINFOA {
    DWORD   cb;
    LPSTR   lpReserved;
    LPSTR   lpDesktop;
    LPSTR   lpTitle;
    DWORD   dwX;
    DWORD   dwY;
    DWORD   dwXSize;
    DWORD   dwYSize;
    DWORD   dwXCountChars;
    DWORD   dwYCountChars;
    DWORD   dwFillAttribute;
    DWORD   dwFlags;
    WORD    wShowWindow;
    WORD    cbReserved2;
    LPBYTE  lpReserved2;
    HANDLE  hStdInput;
    HANDLE  hStdOutput;
    HANDLE  hStdError;
} STARTUPINFOA, *LPSTARTUPINFOA;

typedef struct _PROCESS_INFORMATION {
    HANDLE hProcess;
    HANDLE hThread;
    DWORD dwProcessId;
    DWORD dwThreadId;
} PROCESS_INFORMATION, *PPROCESS_INFORMATION, *LPPROCESS_INFORMATION;

BOOL WINAPI CreateProcessA(
    LPCSTR                lpApplicationName,
    LPSTR                 lpCommandLine,
    LPSECURITY_ATTRIBUTES lpProcessAttributes,
    LPSECURITY_ATTRIBUTES lpThreadAttributes,
    BOOL                  bInheritHandles,
    DWORD                 dwCreationFlags,
    LPVOID                lpEnvironment,
    LPCSTR                lpCurrentDirectory,
    LPSTARTUPINFOA        lpStartupInfo,
    LPPROCESS_INFORMATION lpProcessInformation
    ) {
    printf("Requested process creation %s %s\n",lpApplicationName,lpCommandLine);
    return 0;
}

BOOL WINAPI SetForegroundWindow(
    HWND hWnd
  ) {
    return 1;
}

VOID WINAPI Sleep(
    DWORD dwMilliseconds
    ) {
    printf("sleep %u\n",dwMilliseconds);
    usleep(dwMilliseconds*1000);
}

typedef struct _SYSTEMTIME {
    WORD wYear;
    WORD wMonth;
    WORD wDayOfWeek;
    WORD wDay;
    WORD wHour;
    WORD wMinute;
    WORD wSecond;
    WORD wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME, *LPSYSTEMTIME;

void WINAPI GetLocalTime(
    LPSYSTEMTIME lpSystemTime
  ) {
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);
    time_t ttime = (time_t)spec.tv_sec;
    
    struct tm *info;
    long int millis = spec.tv_nsec;
    millis /= 1000000;

    info = localtime( &ttime );
    
    lpSystemTime->wDay = info->tm_mday;
    lpSystemTime->wDayOfWeek = info->tm_wday;
    lpSystemTime->wHour = info->tm_hour;
    lpSystemTime->wMilliseconds = millis;
    lpSystemTime->wMinute = info->tm_min;
    lpSystemTime->wMonth = info->tm_mon;
    lpSystemTime->wSecond = info->tm_sec;
    lpSystemTime->wYear =  info->tm_year;
}

HMODULE WINAPI GetModuleHandle(LPCTSTR lpModuleName) {
    if (!lpModuleName) {
        return dlopen(NULL, 0);
    }

    *(int*) 0 = 0;
}

HMODULE WINAPI GetModuleHandleA(LPCTSTR lpModuleName) {
    if (!lpModuleName) {
        return dlopen(NULL, 0);
    }

    *(int*) 0 = 0;
}

HANDLE WINAPI GetStdHandle(DWORD nStdHandle) {
    switch (nStdHandle) {
        case -10:
            return stdin;
        case -11:
            return stdout;
        case -12:
            return stderr;
    }
    return (HANDLE) - 1;
}

DWORD WINAPI GetCurrentThreadId(void) {
    return (DWORD) pthread_self();
}

typedef void *LPTCH;

static char * _lenv = NULL;

LPTCH WINAPI GetEnvironmentStrings(void) {
    if (!_lenv) {
        _lenv = (char*)malloc(10*1024);
        char * tmp = (char*)malloc(20*1024);
        FILE * fp = fopen("env","rb");
        int rt = fread(tmp,1,20*1024,fp);
        
        int i;
        for(i=0;i<rt;i+=2) {
            _lenv[i/2] = tmp[i];
        }
        fclose(fp);

        free(tmp);
        
    }
    //char ** e = environ;
    return _lenv;
}

DWORD WINAPI GetVersion(void) {
    return 5 << 16;
}

DWORD WINAPI GetModuleFileNameA(
        HMODULE hModule,
        char * lpFilename,
        DWORD nSize
        ) {

    if (!hModule) {
        strcpy(lpFilename,global_config_data.cmdline_remapped);
        return strlen(lpFilename);
    }
    printf("GetModuleFileNameA failed %X\n",hModule);
    exit(0);
}

DWORD WINAPI GetModuleFileNameW(
        HMODULE hModule,
        wchar_t * lpFilename,
        DWORD nSize
        ) {
    if (!hModule) {
        swprintf(lpFilename, nSize, L"C:\\SUNS\\EFS.EXE");
        return wcslen(lpFilename);
    }
    printf("failed %X\n", hModule);
    exit(0);
}

LPSTR WINAPI GetCommandLineA(void) {
    
    return global_config_data.cmdline_remapped_q;
}

wchar_t * WINAPI GetCommandLineW(void) {
    wchar_t * p = global_config_data.cmdline_w_q;
  //  printf("%X %ls %X\n", p, &global_config_data.cmdline_w, &global_config_data.cmdline);
    byte * px = (byte*) p;
    int i;

    return p;
}

LPTOP_LEVEL_EXCEPTION_FILTER WINAPI SetUnhandledExceptionFilter(
        LPTOP_LEVEL_EXCEPTION_FILTER lpTopLevelExceptionFilter
        ) {
    //sigaction(); // all signals?
}

typedef struct _cpinfo {
    UINT MaxCharSize;
    BYTE DefaultChar[2];
    BYTE LeadByte[12];
} CPINFO, *LPCPINFO;

BOOL WINAPI GetCPInfo( UINT CodePage,LPCPINFO lpCPInfo) {
    memset(lpCPInfo,0,sizeof(struct _cpinfo));
    lpCPInfo->MaxCharSize = 1;
    lpCPInfo->DefaultChar[0] = '?'; //?
    return 1;
    
}

#define IBM850_OEM_CODE 0x352
UINT WINAPI GetOEMCP() {
    return IBM850_OEM_CODE; // is it enough?
}

DWORD WINAPI CharUpperBuffA(
    char * lpsz,
    DWORD  cchLength
  ) {
    int i;
    for(i=0;i<cchLength && lpsz[i];i++) {
        lpsz[i] = toupper(lpsz[i]);
    }
    
    return i;
}

#define HKEY void *

typedef DWORD REGSAM;

#define HKEY_CLASSES_ROOT                   ( (ULONG_PTR)((LONG)0x80000000) )
#define HKEY_CURRENT_USER                   ( (ULONG_PTR)((LONG)0x80000001) )
#define HKEY_LOCAL_MACHINE                  ( (ULONG_PTR)((LONG)0x80000002) )
#define HKEY_USERS                          ( (ULONG_PTR)((LONG)0x80000003) )
#define HKEY_PERFORMANCE_DATA               ( (ULONG_PTR)((LONG)0x80000004) )
#define HKEY_PERFORMANCE_TEXT               ( (ULONG_PTR)((LONG)0x80000050) )
#define HKEY_PERFORMANCE_NLSTEXT            ( (ULONG_PTR)((LONG)0x80000060) )
#define HKEY_CURRENT_CONFIG                 ( (ULONG_PTR)((LONG)0x80000005) )
#define HKEY_DYN_DATA                       ( (ULONG_PTR)((LONG)0x80000006) )
#define HKEY_CURRENT_USER_LOCAL_SETTINGS    ( (ULONG_PTR)((LONG)0x80000007) )

#define ERROR_INVALID_FUNCTION 1
#define ERROR_FILE_NOT_FOUND 2
#define ERROR_MORE_DATA                  234L    // dderror

LONG WINAPI RegOpenKeyExA(
        HKEY    hKey,
        LPCTSTR lpSubKey,
        DWORD   ulOptions,
        REGSAM  samDesired,
        void **  phkResult
    ) {
    char base[256];
    switch ((dword)hKey) {
        case HKEY_CLASSES_ROOT:
            strcpy(base,"Computer\\HKEY_CLASSES_ROOT");
            break;
        case HKEY_CURRENT_USER:
            strcpy(base,"Computer\\HKEY_CURRENT_USER");
            break;
        case HKEY_LOCAL_MACHINE:
            strcpy(base,"Computer\\HKEY_LOCAL_MACHINE");
            break;
        case HKEY_USERS:
            strcpy(base,"Computer\\HKEY_USERS");
            break;
        case HKEY_PERFORMANCE_DATA:
            strcpy(base,"Computer\\HKEY_PERFORMANCE_DATA");
            break;
        case HKEY_PERFORMANCE_TEXT:
            strcpy(base,"Computer\\HKEY_PERFORMANCE_TEXT");
            break;
        case HKEY_PERFORMANCE_NLSTEXT:
            strcpy(base,"Computer\\HKEY_PERFORMANCE_NLSTEXT");
            break;
        case HKEY_CURRENT_CONFIG:
            strcpy(base,"Computer\\HKEY_CURRENT_CONFIG");
            break;
        case HKEY_DYN_DATA:
            strcpy(base,"Computer\\HKEY_DYN_DATA");
            break;
        case HKEY_CURRENT_USER_LOCAL_SETTINGS:
            strcpy(base,"Computer\\HKEY_CURRENT_USER_LOCAL_SETTINGS");
            break;    
        default:
            // key is preopen?
            tree_node * tn = (tree_node*)hKey;
            *phkResult = tn;
            return 0;
            
    }
    
    if (lpSubKey) {
        int len = strlen(base);
        *(base+len) = '\\';
        
        strcpy(base+len+1,lpSubKey);
        tree_node * tn = localRegistry->meta_search(base);
        if (!tn) {
            return ERROR_FILE_NOT_FOUND;
        }
        printf("Reg %X\n",tn);
        *phkResult = tn;
        return 0;
    }
    printf("RegOpenKeyExA %X %s %s\n",hKey,lpSubKey,base);
    exit(0);
    
}
void draw_test();
int WINAPI MessageBoxA(
    HWND    hWnd,
    LPCTSTR lpText,
    LPCTSTR lpCaption,
    UINT    uType
) {
    printf("messbox %s %s %u\n",lpText,lpCaption,uType);
   // SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR ,lpCaption,lpText,NULL);
}



LONG WINAPI RegQueryValueExA(
    HKEY    hKey,
    LPCTSTR lpValueName,
    LPDWORD lpReserved,
    LPDWORD lpType,
    LPBYTE  lpData,
    LPDWORD lpcbData
    ) {
    
    if (!lpValueName) {
        printf("reg query fail\n");
        // return default, not supported yet
        exit(0);
    }
    tree_node * tn = (tree_node*)hKey;
    if (tn->ltype == 1) {
        // leaf
        printf("reg query fail\n");
        exit(0);
    }
    
    tree_node * tnc = localRegistry->node_look_up(tn,lpValueName);
    
    if (!tnc) {
        return ERROR_FILE_NOT_FOUND;
    }
    
    if (tnc->ltype == 0) {
        // subpath
        printf("reg query fail\n");
        exit(0);
    }
    
    tree_leaf_node * tf = (tree_leaf_node*) tnc;
        
    if (lpType) {
        *lpType = tf->type;
    }
    
    void * data;

    int rt = localRegistry->leaf_get_data(tf,&data);
    printf("RegQueryValueExA %i %i\n",rt,*lpcbData);
    if (rt >= *lpcbData) {
        return ERROR_MORE_DATA;
    }
    memcpy(lpData,data,rt+1);
    
    printf("Got key OK \n");
    return 0;
    
}

typedef struct _FILETIME {
    DWORD dwLowDateTime;
    DWORD dwHighDateTime;
} FILETIME, *PFILETIME, *LPFILETIME;

typedef char            TCHAR;

typedef struct _WIN32_FIND_DATA {
    DWORD    dwFileAttributes;
    FILETIME ftCreationTime;
    FILETIME ftLastAccessTime;
    FILETIME ftLastWriteTime;
    DWORD    nFileSizeHigh;
    DWORD    nFileSizeLow;
    DWORD    dwReserved0;
    DWORD    dwReserved1;
    TCHAR    cFileName[260];
    TCHAR    cAlternateFileName[14];
} WIN32_FIND_DATA, *PWIN32_FIND_DATA, *LPWIN32_FIND_DATA;

void tv2FILETIME(struct timespec * tv, FILETIME * f1) {
    unsigned long long l = tv->tv_sec;
    l *= 10000000;
    l += tv->tv_nsec/100;
    
    f1->dwLowDateTime = l &0xFFFFFFFF;
    f1->dwHighDateTime = l>>32;
}



HANDLE WINAPI FindFirstFileA(
    LPCTSTR           lpFileName,
    LPWIN32_FIND_DATA lpFindFileData
) {
    struct stat st;
    
    local_find_file * wff = (local_find_file*)malloc(sizeof(local_find_file));
    strcpy(wff->base,lpFileName);
    wff->count = 0;
    //vfmap->find_file();
    printf("find %s\n",lpFileName);
    tvfshandle * th = fs_look_up(lpFileName,"r+b", &st,wff);
    
    if (!th) {
        _lastError = ERROR_FILE_NOT_FOUND;
        return INVALID_HANDLE_VALUE;
    }
    printf("%i\n",st.st_size);
    vfmap->get_emulated_address(th,lpFindFileData->cFileName);

    lpFindFileData->nFileSizeHigh = 0;
    lpFindFileData->nFileSizeLow = st.st_size;
    
   // strcpy(lpFindFileData->cFileName,lpFileName);
    l_substring(lpFindFileData->cFileName,l_lastindexof(lpFindFileData->cFileName,'\\')+1,strlen(lpFindFileData->cFileName));
    
    tv2FILETIME(&st.st_mtim,&lpFindFileData->ftLastWriteTime);
    tv2FILETIME(&st.st_atim,&lpFindFileData->ftLastAccessTime);
    tv2FILETIME(&st.st_ctim,&lpFindFileData->ftCreationTime);
    printf("find ret %s\n",lpFindFileData->cFileName);
   
    
    
    return wff;
}

#define ERROR_NO_MORE_FILES              18L

BOOL WINAPI FindNextFileA(
    HANDLE             hFindFile,
    LPWIN32_FIND_DATA lpFindFileData
) {
    struct stat st;
    local_find_file * wff = (local_find_file*)hFindFile;
    printf("find next %s\n",wff->base);
    tvfshandle * th = fs_look_up(wff->base,"r+b", &st,wff);
    
    if (!th) {
        _lastError = ERROR_NO_MORE_FILES;
        return 0;
    }
    
    vfmap->get_emulated_address(th,lpFindFileData->cFileName);

    lpFindFileData->nFileSizeHigh = 0;
    lpFindFileData->nFileSizeLow = st.st_size;
    
   // strcpy(lpFindFileData->cFileName,lpFileName);
    l_substring(lpFindFileData->cFileName,l_lastindexof(lpFindFileData->cFileName,'\\')+1,strlen(lpFindFileData->cFileName));
    
    tv2FILETIME(&st.st_mtim,&lpFindFileData->ftLastWriteTime);
    tv2FILETIME(&st.st_atim,&lpFindFileData->ftLastAccessTime);
    tv2FILETIME(&st.st_ctim,&lpFindFileData->ftCreationTime);
    printf("find ret %s\n",lpFindFileData->cFileName);
    
    return 1;
    
}


BOOL WINAPI FileTimeToLocalFileTime(
    const FILETIME   *lpFileTime,
    LPFILETIME lpLocalFileTime
) {
    memcpy(lpLocalFileTime,lpFileTime,sizeof(FILETIME));
    return 1;
}

typedef word WORD;
typedef WORD* LPWORD;

BOOL WINAPI FileTimeToDosDateTime(
    const FILETIME *lpFileTime,
    LPWORD   lpFatDate,
    LPWORD   lpFatTime
) {
    unsigned long long l = lpFileTime->dwHighDateTime;
    l = l << 32;
    l += lpFileTime->dwLowDateTime;
    l /= 10000000;
    //sprintf("%llu %X %X\n",l,lpFileTime->dwHighDateTime,lpFileTime->dwLowDateTime);
    time_t t;
    t = (long)l;
    
    struct tm tmx = *localtime(&t);
    *lpFatDate = tmx.tm_mday + ((tmx.tm_mon+1) << 5) + ((tmx.tm_year-80)<<9);
    *lpFatTime = tmx.tm_sec/2 + (tmx.tm_min<<5) + (tmx.tm_hour << 10);
    return 1;
}

HWND WINAPI FindWindowA(
    LPCTSTR lpClassName,
    LPCTSTR lpWindowName
    ) {
    
    printf("%s %s\n",lpClassName,lpWindowName);
    return NULL;
}

BOOL WINAPI SetCurrentDirectoryA(
    LPCTSTR lpPathName
) {
    printf("%s\n",lpPathName);
    // not now
    return 1;
}

/*******************************************************************************
 * UI
 */






#define GENERIC_READ                     (0x80000000L)
#define GENERIC_WRITE                    (0x40000000L)
#define GENERIC_EXECUTE                  (0x20000000L)
#define GENERIC_ALL                      (0x10000000L)

#define CREATE_NEW          1
#define CREATE_ALWAYS       2
#define OPEN_EXISTING       3
#define OPEN_ALWAYS         4
#define TRUNCATE_EXISTING   5



HANDLE WINAPI CreateFileA(
    LPCTSTR               lpFileName,
    DWORD                 dwDesiredAccess,
    DWORD                 dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD                 dwCreationDisposition,
    DWORD                 dwFlagsAndAttributes,
    HANDLE                hTemplateFile
    ) {
    if (hTemplateFile) {
        printf("Unsupported CreateFile\n");
        exit(0);
    }
    
    int read = 0;
    int write = 0;
    if (dwDesiredAccess & GENERIC_READ) {
        read = 1;
    }
    if (dwDesiredAccess & GENERIC_WRITE) {
        write = 1;
    }
    
    char * mode;
    int excl = 0;
    if (!read && write && (dwCreationDisposition == CREATE_NEW)) {
        mode = "wb";
        excl = 1;
    }
    else if (read && write && (dwCreationDisposition == CREATE_NEW)) {
        mode = "w+b";
        excl = 1;
    }    
    else if (!read && write && (dwCreationDisposition == CREATE_ALWAYS)) {
        mode = "wb";
    }
    else if (read && write && (dwCreationDisposition == CREATE_ALWAYS)) {
        mode = "w+b";
    }
    else if (read && !write && (dwCreationDisposition == OPEN_EXISTING)) {
        mode = "rb";
    }
    else {
        return INVALID_HANDLE_VALUE;
    }
   // printf("CREATEFILE %s %X %s %X\n",lpFileName,dwCreationDisposition,mode,excl);
    
    tvfshandle * fp = fs_open_bpath(lpFileName,mode,excl);
    if (!fp) {
        _lastError = ERROR_FILE_NOT_FOUND;
        return INVALID_HANDLE_VALUE;
    }
    
    //printf("file handle %X %s\n",fp,lpFileName);
    
    return fp;
    
}

DWORD WINAPI GetFileType(
    HANDLE hFile
    ) {
    return 2;
}

typedef struct _OVERLAPPED {
  ULONG_PTR Internal;
  ULONG_PTR InternalHigh;
  union {
    struct {
      DWORD Offset;
      DWORD OffsetHigh;
    };
    PVOID  Pointer;
  };
  HANDLE    hEvent;
} OVERLAPPED, *LPOVERLAPPED;
int cr = 0;
BOOL WINAPI ReadFile(
    HANDLE       hFile,
    LPVOID       lpBuffer,
    DWORD        nNumberOfBytesToRead,
    LPDWORD      lpNumberOfBytesRead,
    LPOVERLAPPED lpOverlapped
) {
    tvfshandle * fh = (tvfshandle*)hFile;
   // printf("read at %X\n",ftell(fh->ph));
//    printf("read %X %i\n",fh->ph,nNumberOfBytesToRead);
    dword rt = fread(lpBuffer,1,nNumberOfBytesToRead,fh->ph);
    *lpNumberOfBytesRead = rt;
   
  //  printf("read %i %X %i\n",rt,hFile, 0);
    
    return rt>=0;
}

DWORD WINAPI SetFilePointer(
    HANDLE hFile,
    LONG   lDistanceToMove,
    LONG*  lpDistanceToMoveHigh,
    DWORD  dwMoveMethod
) {
    if (lpDistanceToMoveHigh) {
        printf("Unsupported SetFilePointer\n");
        exit(0);
    }
    tvfshandle * fh = (tvfshandle*)hFile;
    
    
    //;;printf("fseek %X %X %X %X\n",hFile,lDistanceToMove,dwMoveMethod,&_IO_2_1_stderr_);
    if (fh == (HANDLE)&_IO_2_1_stdout_ || fh == (HANDLE)&_IO_2_1_stdin_ || fh == (HANDLE)&_IO_2_1_stderr_) {
        return 0;
    }
    
    DWORD pos = fseek(fh->ph,lDistanceToMove,dwMoveMethod);
    if (pos != 0) {
        return -1;
    }
    return ftell(fh->ph);
    
}

BOOL WINAPI WriteFile(
    HANDLE       hFile,
    LPCVOID      lpBuffer,
    DWORD        nNumberOfBytesToWrite,
    LPDWORD      lpNumberOfBytesWritten,
    LPOVERLAPPED lpOverlapped
) {
    tvfshandle * fh = (tvfshandle*)hFile;
    *lpNumberOfBytesWritten = fwrite(lpBuffer,1,nNumberOfBytesToWrite,fh->ph);
    if (*lpNumberOfBytesWritten < 0) {
        *lpNumberOfBytesWritten = 0;
        return 0;
    }
    return 1;
}

BOOL WINAPI CloseHandle(
    HANDLE hObject
) {
    // what are we
}

#define DRIVERVERSION 0     /* Device driver version                    */
#define TECHNOLOGY    2     /* Device classification                    */
#define HORZSIZE      4     /* Horizontal size in millimeters           */
#define VERTSIZE      6     /* Vertical size in millimeters             */
#define HORZRES       8     /* Horizontal width in pixels               */
#define VERTRES       10    /* Vertical height in pixels                */
#define BITSPIXEL     12    /* Number of bits per pixel                 */
#define PLANES        14    /* Number of planes                         */
#define NUMBRUSHES    16    /* Number of brushes the device has         */
#define NUMPENS       18    /* Number of pens the device has            */
#define NUMMARKERS    20    /* Number of markers the device has         */
#define NUMFONTS      22    /* Number of fonts the device has           */
#define NUMCOLORS     24    /* Number of colors the device supports     */
#define PDEVICESIZE   26    /* Size required for device descriptor      */
#define CURVECAPS     28    /* Curve capabilities                       */
#define LINECAPS      30    /* Line capabilities                        */
#define POLYGONALCAPS 32    /* Polygonal capabilities                   */
#define TEXTCAPS      34    /* Text capabilities                        */
#define CLIPCAPS      36    /* Clipping capabilities                    */
#define RASTERCAPS    38    /* Bitblt capabilities                      */
#define ASPECTX       40    /* Length of the X leg                      */
#define ASPECTY       42    /* Length of the Y leg                      */
#define ASPECTXY      44    /* Length of the hypotenuse                 */

#define LOGPIXELSX    88    /* Logical pixels/inch in X                 */
#define LOGPIXELSY    90    /* Logical pixels/inch in Y                 */

#define SIZEPALETTE  104    /* Number of entries in physical palette    */
#define NUMRESERVED  106    /* Number of reserved entries in palette    */
#define COLORRES     108    /* Actual color resolution                  */

typedef void * HDC;

HDC WINAPI GetDC(
    HWND hWnd
    ) {
    return (void*)1;
}

byte default_palette[] = {
    0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x00, 0x80, 0x80, 0x00, 0xC0, 0xC0, 0xC0, 0x00, 0xAC, 0xA8, 0x99, 0x00, 0xEC, 0xE9, 0xD8, 0x00, 0x99, 0x33, 0x00, 0x00, 0xCC, 0x33, 0x00, 0x00, 0xFF, 0x33, 0x00, 0x00, 0x00, 0x66, 0x00, 0x00, 0x33, 0x66, 0x00, 0x00, 0x66, 0x66, 0x00, 0x00, 0x99, 0x66, 0x00, 0x00, 0xCC, 0x66, 0x00, 0x00, 0xFF, 0x66, 0x00, 0x00, 0x00, 0x99, 0x00, 0x00, 0x33, 0x99, 0x00, 0x00, 0x66, 0x99, 0x00, 0x00, 0x99, 0x99, 0x00, 0x00, 0xCC, 0x99, 0x00, 0x00, 0xFF, 0x99, 0x00, 0x00, 0x00, 0xCC, 0x00, 0x00, 0x33, 0xCC, 0x00, 0x00, 0x66, 0xCC, 0x00, 0x00, 0x99, 0xCC, 0x00, 0x00, 0xCC, 0xCC, 0x00, 0x00, 0xFF, 0xCC, 0x00, 0x00, 0x66, 0xFF, 0x00, 0x00, 0x99, 0xFF, 0x00, 0x00, 0xCC, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x33, 0x00, 0x33, 0x00, 0xFF, 0x00, 0x66, 0x00, 0x33, 0x00, 0x99, 0x00, 0x33, 0x00, 0xCC, 0x00, 0x33, 0x00, 0xFF, 0x00, 0x33, 0x00, 0x00, 0x33, 0xFF, 0x00, 0x33, 0x33, 0x33, 0x00, 0x66, 0x33, 0x33, 0x00, 0x99, 0x33, 0x33, 0x00, 0xCC, 0x33, 0x33, 0x00, 0xFF, 0x33, 0x33, 0x00, 0x00, 0x66, 0x33, 0x00, 0x33, 0x66, 0x33, 0x00, 0x66, 0x66, 0x33, 0x00, 0x99, 0x66, 0x33, 0x00, 0xCC, 0x66, 0x33, 0x00, 0xFF, 0x66, 0x33, 0x00, 0x00, 0x99, 0x33, 0x00, 0x33, 0x99, 0x33, 0x00, 0x66, 0x99, 0x33, 0x00, 0x99, 0x99, 0x33, 0x00, 0xCC, 0x99, 0x33, 0x00, 0xFF, 0x99, 0x33, 0x00, 0x00, 0xCC, 0x33, 0x00, 0x33, 0xCC, 0x33, 0x00, 0x66, 0xCC, 0x33, 0x00, 0x99, 0xCC, 0x33, 0x00, 0xCC, 0xCC, 0x33, 0x00, 0xFF, 0xCC, 0x33, 0x00, 0x33, 0xFF, 0x33, 0x00, 0x66, 0xFF, 0x33, 0x00, 0x99, 0xFF, 0x33, 0x00, 0xCC, 0xFF, 0x33, 0x00, 0xFF, 0xFF, 0x33, 0x00, 0x00, 0x00, 0x66, 0x00, 0x33, 0x00, 0x66, 0x00, 0x66, 0x00, 0x66, 0x00, 0x99, 0x00, 0x66, 0x00, 0xCC, 0x00, 0x66, 0x00, 0xFF, 0x00, 0x66, 0x00, 0x00, 0x33, 0x66, 0x00, 0x33, 0x33, 0x66, 0x00, 0x66, 0x33, 0x66, 0x00, 0x99, 0x33, 0x66, 0x00, 0xCC, 0x33, 0x66, 0x00, 0xFF, 0x33, 0x66, 0x00, 0x00, 0x66, 0x66, 0x00, 0x33, 0x66, 0x66, 0x00, 0x66, 0x66, 0x66, 0x00, 0x99, 0x66, 0x66, 0x00, 0xCC, 0x66, 0x66, 0x00, 0x00, 0x99, 0x66, 0x00, 0x33, 0x99, 0x66, 0x00, 0x66, 0x99, 0x66, 0x00, 0x99, 0x99, 0x66, 0x00, 0xCC, 0x99, 0x66, 0x00, 0xFF, 0x99, 0x66, 0x00, 0x00, 0xCC, 0x66, 0x00, 0x33, 0xCC, 0x66, 0x00, 0x99, 0xCC, 0x66, 0x00, 0xCC, 0xCC, 0x66, 0x00, 0xFF, 0xCC, 0x66, 0x00, 0x00, 0xFF, 0x66, 0x00, 0x33, 0xFF, 0x66, 0x00, 0x99, 0xFF, 0x66, 0x00, 0xCC, 0xFF, 0x66, 0x00, 0xFF, 0x00, 0xCC, 0x00, 0xCC, 0x00, 0xFF, 0x00, 0x00, 0x99, 0x99, 0x00, 0x99, 0x33, 0x99, 0x00, 0x99, 0x00, 0x99, 0x00, 0xCC, 0x00, 0x99, 0x00, 0x00, 0x00, 0x99, 0x00, 0x33, 0x33, 0x99, 0x00, 0x66, 0x00, 0x99, 0x00, 0xCC, 0x33, 0x99, 0x00, 0xFF, 0x00, 0x99, 0x00, 0x00, 0x66, 0x99, 0x00, 0x33, 0x66, 0x99, 0x00, 0x66, 0x33, 0x99, 0x00, 0x99, 0x66, 0x99, 0x00, 0xCC, 0x66, 0x99, 0x00, 0xFF, 0x33, 0x99, 0x00, 0x33, 0x99, 0x99, 0x00, 0x66, 0x99, 0x99, 0x00, 0x99, 0x99, 0x99, 0x00, 0xCC, 0x99, 0x99, 0x00, 0xFF, 0x99, 0x99, 0x00, 0x00, 0xCC, 0x99, 0x00, 0x33, 0xCC, 0x99, 0x00, 0x66, 0xCC, 0x66, 0x00, 0x99, 0xCC, 0x99, 0x00, 0xCC, 0xCC, 0x99, 0x00, 0xFF, 0xCC, 0x99, 0x00, 0x00, 0xFF, 0x99, 0x00, 0x33, 0xFF, 0x99, 0x00, 0x66, 0xCC, 0x99, 0x00, 0x99, 0xFF, 0x99, 0x00, 0xCC, 0xFF, 0x99, 0x00, 0xFF, 0xFF, 0x99, 0x00, 0x00, 0x00, 0xCC, 0x00, 0x33, 0x00, 0x99, 0x00, 0x66, 0x00, 0xCC, 0x00, 0x99, 0x00, 0xCC, 0x00, 0xCC, 0x00, 0xCC, 0x00, 0x00, 0x33, 0x99, 0x00, 0x33, 0x33, 0xCC, 0x00, 0x66, 0x33, 0xCC, 0x00, 0x99, 0x33, 0xCC, 0x00, 0xCC, 0x33, 0xCC, 0x00, 0xFF, 0x33, 0xCC, 0x00, 0x00, 0x66, 0xCC, 0x00, 0x33, 0x66, 0xCC, 0x00, 0x66, 0x66, 0x99, 0x00, 0x99, 0x66, 0xCC, 0x00, 0xCC, 0x66, 0xCC, 0x00, 0xFF, 0x66, 0x99, 0x00, 0x00, 0x99, 0xCC, 0x00, 0x33, 0x99, 0xCC, 0x00, 0x66, 0x99, 0xCC, 0x00, 0x99, 0x99, 0xCC, 0x00, 0xCC, 0x99, 0xCC, 0x00, 0xFF, 0x99, 0xCC, 0x00, 0x00, 0xCC, 0xCC, 0x00, 0x33, 0xCC, 0xCC, 0x00, 0x66, 0xCC, 0xCC, 0x00, 0x99, 0xCC, 0xCC, 0x00, 0xCC, 0xCC, 0xCC, 0x00, 0xFF, 0xCC, 0xCC, 0x00, 0x00, 0xFF, 0xCC, 0x00, 0x33, 0xFF, 0xCC, 0x00, 0x66, 0xFF, 0x99, 0x00, 0x99, 0xFF, 0xCC, 0x00, 0xCC, 0xFF, 0xCC, 0x00, 0xFF, 0xFF, 0xCC, 0x00, 0x33, 0x00, 0xCC, 0x00, 0x66, 0x00, 0xFF, 0x00, 0x99, 0x00, 0xFF, 0x00, 0x00, 0x33, 0xCC, 0x00, 0x33, 0x33, 0xFF, 0x00, 0x66, 0x33, 0xFF, 0x00, 0x99, 0x33, 0xFF, 0x00, 0xCC, 0x33, 0xFF, 0x00, 0xFF, 0x33, 0xFF, 0x00, 0x00, 0x66, 0xFF, 0x00, 0x33, 0x66, 0xFF, 0x00, 0x66, 0x66, 0xCC, 0x00, 0x99, 0x66, 0xFF, 0x00, 0xCC, 0x66, 0xFF, 0x00, 0xFF, 0x66, 0xCC, 0x00, 0x00, 0x99, 0xFF, 0x00, 0x33, 0x99, 0xFF, 0x00, 0x66, 0x99, 0xFF, 0x00, 0x99, 0x99, 0xFF, 0x00, 0xCC, 0x99, 0xFF, 0x00, 0xFF, 0x99, 0xFF, 0x00, 0x00, 0xCC, 0xFF, 0x00, 0x33, 0xCC, 0xFF, 0x00, 0x66, 0xCC, 0xFF, 0x00, 0x99, 0xCC, 0xFF, 0x00, 0xCC, 0xCC, 0xFF, 0x00, 0xFF, 0xCC, 0xFF, 0x00, 0x33, 0xFF, 0xFF, 0x00, 0x66, 0xFF, 0xCC, 0x00, 0x99, 0xFF, 0xFF, 0x00, 0xCC, 0xFF, 0xFF, 0x00, 0xFF, 0x66, 0x66, 0x00, 0x66, 0xFF, 0x66, 0x00, 0xFF, 0xFF, 0x66, 0x00, 0x66, 0x66, 0xFF, 0x00, 0xFF, 0x66, 0xFF, 0x00, 0x66, 0xFF, 0xFF, 0x00, 0xA5, 0x00, 0x21, 0x00, 0x5F, 0x5F, 0x5F, 0x00, 0x77, 0x77, 0x77, 0x00, 0x86, 0x86, 0x86, 0x00, 0x96, 0x96, 0x96, 0x00, 0xCB, 0xCB, 0xCB, 0x00, 0xB2, 0xB2, 0xB2, 0x00, 0xD7, 0xD7, 0xD7, 0x00, 0xDD, 0xDD, 0xDD, 0x00, 0xE3, 0xE3, 0xE3, 0x00, 0xEA, 0xEA, 0xEA, 0x00, 0xC0, 0xDC, 0xC0, 0x00, 0xA6, 0xCA, 0xF0, 0x00, 0x00, 0x00, 0x33, 0x00, 0x33, 0x00, 0x00, 0x00, 0x33, 0x00, 0x33, 0x00, 0x00, 0x33, 0x33, 0x00, 0x16, 0x16, 0x16, 0x00, 0x1C, 0x1C, 0x1C, 0x00, 0x22, 0x22, 0x22, 0x00, 0x29, 0x29, 0x29, 0x00, 0x55, 0x55, 0x55, 0x00, 0x4D, 0x4D, 0x4D, 0x00, 0x42, 0x42, 0x42, 0x00, 0x39, 0x39, 0x39, 0x00, 0xFF, 0x7C, 0x80, 0x00, 0xFF, 0x50, 0x50, 0x00, 0xD6, 0x00, 0x93, 0x00, 0xCC, 0xEC, 0xFF, 0x00, 0xEF, 0xD6, 0xC6, 0x00, 0xE7, 0xE7, 0xD6, 0x00, 0xAD, 0xA9, 0x90, 0x00, 0x33, 0xFF, 0x00, 0x00, 0x66, 0x00, 0x00, 0x00, 0x99, 0x00, 0x00, 0x00, 0xCC, 0x00, 0x00, 0x00, 0x00, 0x33, 0x00, 0x00, 0x33, 0x33, 0x00, 0x00, 0x66, 0x33, 0x00, 0x00, 0xFF, 0xFB, 0xF0, 0x00, 0x00, 0x4E, 0x98, 0x00, 0x80, 0x80, 0x80, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0x00
};


UINT WINAPI GetSystemPaletteEntries(
    HDC            hdc,
    UINT           iStart,
    UINT           cEntries,
    LPPALETTEENTRY pPalEntries
    ) {
    
    memcpy(pPalEntries,default_palette+(iStart*4),cEntries*4);
        
    return 1;
}

UINT WINAPI GetPrivateProfileIntA(
    LPCTSTR lpAppName,
    LPCTSTR lpKeyName,
    UINT     nDefault,
    LPCTSTR lpFileName
    ) {
   // printf("%s %s %s\n",lpAppName,lpKeyName,lpFileName);
    
    tvfshandle * fp = fs_open_bpath(lpFileName,"rb",0);
    char out[1024];
    process_ini(fp->ph,lpAppName,lpKeyName,out);
    
    fclose(fp->ph);
    free(fp);
    
    if (out[0] == 0) {
        return nDefault;
    }
    
    return atoi(out);
}

int WINAPI GetDeviceCaps(
    HDC hdc,
    int nIndex
) {
    switch (nIndex) {
        case BITSPIXEL:
            return 32; // 32 bits color depth
        case RASTERCAPS:
            return 0xFFFF; // can do anything
    }
    printf("getdevicecap %i\n",nIndex);
    exit(0);
}

int WINAPI ReleaseDC(
    HWND hWnd,
    HDC  hDC
) {
    return 1;
}

int WINAPI ShowCursor(
    BOOL bShow
) {
    return 1;
}

int (*_mkeys[256])();

int _mkey14() {
    return 0;
}

int _mkey90() {
    return 1;
}




void init_keys() {
    memset(_mkeys,0,sizeof(_mkeys));
    _mkeys[VK_CAPITAL] = _mkey14;
    _mkeys[VK_NUMLOCK] = _mkey90;
    
    
}

SHORT WINAPI GetKeyState(
    int nVirtKey
) {
    
    printf("get key state %X\n",nVirtKey);
    
    
    return _mkeys[nVirtKey]();
    
}

UINT WINAPI timeBeginPeriod(
   UINT uPeriod
) {
    return 0;
}


BOOL WINAPI PeekMessageA(
    LPMSG lpMsg,
    HWND  hWnd,
    UINT  wMsgFilterMin,
    UINT  wMsgFilterMax,
    UINT  wRemoveMsg
) {
    usleep(1000);
    if (!hWnd) {
        
    }
    if (wMsgFilterMin || wMsgFilterMax) {
        printf("Unsupported PeekMessageA\n ");
        exit(0);
    }
        
    int rt = process_msg_queue(lpMsg,wRemoveMsg & 1,hWnd);
    
    return rt;
}


BOOL WINAPI FreeEnvironmentStringsA(
    LPTCH lpszEnvironmentBlock
) {
    return 1;
}

DWORD WINAPI GetLastError(void) {
  //  printf("GetLastError\n");
    return _lastError;
}

VOID WINAPI ExitProcess(
    UINT uExitCode
    ) {
    printf("ExitProcess called (%i)\n",uExitCode);
    exit(0);
}

DWORD WINAPI timeGetTime() {
    struct timespec spec;
    clock_gettime(CLOCK_REALTIME, &spec);
     
    long int millis = spec.tv_nsec;
    millis /= 1000000;
    return spec.tv_sec*1000 + millis;

}

BOOL WINAPI DeleteFileA(
    LPCSTR lpFileName
) {
    printf("delete file %s\n",lpFileName);
    return 1;
}



}

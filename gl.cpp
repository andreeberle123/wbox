#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "common.h"
#include "TextRender.h"

#include <xcb/xcb.h>
#include <xcb/randr.h>
#include <X11/Xlib-xcb.h> 
#include <xcb/xproto.h>

#include "gl.h"
#include "dx.h"

PFNGLCREATESHADERPROC glCreateShader;
void (*glShaderSource)(GLuint shader,GLsizei count,const GLchar **string,const GLint *length);
PFNGLCOMPILESHADERPROC glCompileShader;    
PFNGLCREATEPROGRAMPROC glCreateProgram;    
PFNGLATTACHSHADERPROC glAttachShader;
PFNGLLINKPROGRAMPROC glLinkProgram;
PFNGLUSEPROGRAMPROC glUseProgram;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
PFNGLVALIDATEPROGRAMPROC glValidateProgram;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
PFNGLGETPROGRAMIVPROC glGetProgramiv;
PFNGLGETSHADERIVPROC glGetShaderiv;
PFNGLUNIFORM1IPROC glUniform1i;
PFNGLUNIFORM1IVPROC glUniform1iv;

static GLuint local_frag;
static GLint tex_loc;
static GLint color_table_loc;
static GLint color_sample_loc;

typedef struct _local_color_table {
    int internal_idx;
    byte * raw;
    int size;
    int inited;
    int r,g,b;
} local_color_table;

static byte color_maps_internal[256*3*16] = {255,};

static local_color_table local_tables[16] = {{0,},};
static int active_color = -1;
static int pushed_color = -1;

GLuint tex[10];

static const GLfloat light_ambient[] = {0.1f, 0.1f, 0.1f, 1.0f};
static const GLfloat light_diffuse[] = {0.8f, 0.8f, 0.8f, 1.0f};
static const GLfloat light_specular[] = {0.9f, 0.9f, 0.9f, 1.0f};
static GLfloat light_position[] = {0, 50, 0, 1.0f};

static const GLfloat mat_ambient[] = {0.7f, 0.7f, 0.7f, 1.0f};
static const GLfloat mat_diffuse[] = {0.8f, 0.8f, 0.8f, 1.0f};
static const GLfloat mat_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
static const GLfloat high_shininess[] = {100.0f};

TextRender * textRender;

static int www, hhh;

void _lock_gl() {

}

void _unlock_gl() {

}

void idle() {
    usleep(1);
}

int rtst = 0;

void reqRedisplay() {
    rtst = 1;
    if (rtst) {
        glutPostRedisplay();
    }
}

void cube_inv_init();


static double ar;

static double hmap[8192 * 16];
static double rmap[8192 * 16];
static int hmapw = 0;
static int hmaph = 0;

static double hlines[8192 * 16];
static int hlinessz = 0;

static int dmode = 0;

void dlines() {
    int i;

    glBegin(GL_LINES);
    for (i = 0; i < hlinessz; i++) {
        int idx = i * 2;

        if (hlines[idx] == 0 && hlines[idx + 1] == 0) {
            glEnd();
            glBegin(GL_LINES);
            continue;
        }
        glVertex3f(hlines[idx], 0, hlines[idx + 1]);

    }
    glEnd();

}

void drawAxes() {
    glColor4f(0, 1, 0, 1);

    glBegin(GL_LINES);
    glVertex3f(-100, 0, 0);
    glVertex3f(100, 0, 0);
    glEnd();

    glColor4f(0, 0, 1, 1);
    glBegin(GL_LINES);
    glVertex3f(0, -100, 0);
    glVertex3f(0, 100, 0);
    glEnd();

    glColor4f(1, 0, 0, 1);
    glBegin(GL_LINES);
    glVertex3f(0, 0, -100);
    glVertex3f(0, 0, 100);
    glEnd();
}

void disp() {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glDisable(GL_TEXTURE_2D);

    glClearColor(1, 1, 1, 1);
    //glClearColor(0,0,0,1);

    gluPerspective(45.0, ar, 1.0, -1);


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glDisable(GL_LIGHTING);

    // drawAxes();

    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);



}

void display2() {

    _lock_gl();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glClearColor(1, 1, 1, 1);

    disp(); // display code    

    glutSwapBuffers();
    _unlock_gl();


}


int lvv = 0;

void _WRAP_key(unsigned char k, int x, int y) {
    _lock_gl();

    switch (k) {

    }

    reqRedisplay();
    _unlock_gl();
}

void _WRAP_keyUp(unsigned char k, int x, int y) {
    _lock_gl();
    reqRedisplay();
    _unlock_gl();
}

void _WRAP_mouse(int b, int s, int x, int y) {
    _lock_gl();

    reqRedisplay();
    _unlock_gl();
}

void _WRAP_special(int b, int x, int y) {
    _lock_gl();

    //display();
    reqRedisplay();
    _unlock_gl();
}

void _WRAP_specialup(int b, int x, int y) {
    _lock_gl();

    //display();
    reqRedisplay();
    _unlock_gl();
}

void _WRAP_mouse_motion(int x, int y) {
    _lock_gl();

    reqRedisplay();
    _unlock_gl();
}

void _WRAP_mouse_passive(int x, int y) {
    _lock_gl();

    reqRedisplay();
    _unlock_gl();
}

int xc();

int init_gl(int w, int h, int stx, int sty, char * title) {
    www = w;
    hhh = h;
    ar = (double) w / (double) h;

    textRender = new TextRender();
    glutInitWindowSize(w, h);

    glutInitWindowPosition(stx, sty);
    int argc = 0;
    char ** argv = 0;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE | GLUT_STENCIL);

    glutCreateWindow(title);

    //glutReshapeFunc(resize);
    glutDisplayFunc(display2);
    glutKeyboardFunc(_WRAP_key);
    glutKeyboardUpFunc(_WRAP_keyUp);
    glutSpecialFunc(_WRAP_special);
    glutSpecialUpFunc(_WRAP_specialup);
    glutIdleFunc(idle);


    glutMouseFunc(_WRAP_mouse);
    glutMotionFunc(_WRAP_mouse_motion);
    glutPassiveMotionFunc(_WRAP_mouse_passive);

    //glutSetOption ( GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION ) ;

    //glClearColor(0,0,0,1);
    //glClearColor(1,1,1,1);


    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    //glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    //glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    //glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    //glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

    rtst = 1;
    glutHideWindow();
    // int ix = glewInit();  

    const GLubyte * st = glGetString(GL_VERSION);

    //textRender->createDisplayLists();
    printf("Main init finished\n");

    // glutMainLoop();
}

static Display * _disp = NULL;
static GLXDrawable _drawable;
extern xcb_connection_t *connection;
GLXContext context;

byte cbuf[1024 * 1024];

static pthread_mutex_t init_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t init_cond = PTHREAD_COND_INITIALIZER;
static int init_state = 0;

typedef struct _render_data {
    byte * raw_data;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int state;
    int x,y;
    Display * display;
    xcb_window_t window; 
    GLXFBConfig fb_config;
} render_data;

render_data * local_render_data = NULL;

void init_color(int idx);



GLuint load_shader(char * name, GLenum mode) {
    GLuint v;
    char *vs = NULL;
    v = glCreateShader(mode);
    FILE * fp = fopen(name,"rb");
    fseek(fp,0,SEEK_END);
    int size = ftell(fp);
    fseek(fp,0,SEEK_SET);
    vs = (char*)malloc(size+1);
    int rt = fread(vs,1,size,fp);
    vs[rt] = 0;
    
    const char * vv = vs;
    glShaderSource(v, 1, &vv, NULL);     
    
    int oo = 0;    

    glCompileShader(v);
    
    GLenum errCode;
    const GLubyte *errString;
    if ((errCode = glGetError()) != GL_NO_ERROR) {
        errString = gluErrorString(errCode);
        printf("compile: %s\n", errString);
        
        
    }
    
    glGetShaderiv(v,GL_COMPILE_STATUS,&oo);
    printf("compile status %i \n",oo);
    return v;
}

void load_shaders() {
    glCreateShader = (PFNGLCREATESHADERPROC)glXGetProcAddress((unsigned char*)"glCreateShader");
    glShaderSource = (void (*)(GLuint ,GLsizei ,const GLchar **,const GLint *))
            glXGetProcAddress((unsigned char*)"glShaderSource");
    glCompileShader = (PFNGLCOMPILESHADERPROC)glXGetProcAddress((unsigned char*)"glCompileShader");
    glCreateProgram = (PFNGLCREATEPROGRAMPROC)glXGetProcAddress((unsigned char*)"glCreateProgram");
    glAttachShader = (PFNGLATTACHSHADERPROC)glXGetProcAddress((unsigned char*)"glAttachShader");
    glLinkProgram = (PFNGLLINKPROGRAMPROC)glXGetProcAddress((unsigned char*)"glLinkProgram");
    glUseProgram = (PFNGLUSEPROGRAMPROC)glXGetProcAddress((unsigned char*)"glUseProgram");
    glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)glXGetProcAddress((unsigned char*)"glGetShaderInfoLog");
    glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)glXGetProcAddress((unsigned char*)"glGetUniformLocation");
    glValidateProgram = (PFNGLVALIDATEPROGRAMPROC)glXGetProcAddress((unsigned char*)"glValidateProgram");
    glGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC)glXGetProcAddress((unsigned char*)"glGetProgramInfoLog");  
    glGetProgramiv = (PFNGLGETPROGRAMIVPROC)glXGetProcAddress((unsigned char*)"glGetProgramiv");  
    glGetShaderiv = (PFNGLGETSHADERIVPROC)glXGetProcAddress((unsigned char*)"glGetShaderiv"); 
    glUniform1i = (PFNGLUNIFORM1IPROC)glXGetProcAddress((unsigned char*)"glUniform1i"); 
    glUniform1iv = (PFNGLUNIFORM1IVPROC)glXGetProcAddress((unsigned char*)"glUniform1iv"); 
    
    GLenum errCode;
    const GLubyte *errString;

    GLuint v = load_shader("t1.vert",GL_VERTEX_SHADER);
    GLuint v2 = load_shader("t1.frag",GL_FRAGMENT_SHADER);
    //glCompileShader(f2);

    local_frag = glCreateProgram();
   // printf("program %i\n",ppx);
    glAttachShader(local_frag, v);
    glAttachShader(local_frag, v2);

    glLinkProgram(local_frag);
    
    int oo = 0;  
    
    glGetProgramiv(local_frag,GL_LINK_STATUS,&oo);
    printf("link status %i %s \n",oo,glGetString(GL_VERSION));
    
    //glValidateProgram(ppx);
    
    glUseProgram(local_frag);

    if ((errCode = glGetError()) != GL_NO_ERROR) {
        errString = gluErrorString(errCode);
        printf("%s\n", errString);
        int bufflen = 1024;
        GLchar* log_string = new char[bufflen + 1];
        int ol = 0;
        glGetShaderInfoLog(v2, bufflen, &ol, log_string);
        
        int i;
        //for(i=0;i<ol;i++) printf("%s\n",log_string[i]);
        printf("%s\n", log_string);

        delete log_string;
    }
    
    tex_loc = glGetUniformLocation(local_frag,"image_tex");
    color_table_loc = glGetUniformLocation(local_frag,"color_mapx");
    color_sample_loc = glGetUniformLocation(local_frag,"color_sample");
    printf("loc %i %i %i\n",tex_loc,color_table_loc,color_sample_loc);
    if (tex_loc < 0 || color_table_loc < 0) {
    //    exit(0);
    }    
    
    if (!oo) {
        exit(0);
    }
    //exit(0);
}

static byte * pushed_buf = NULL;

void gl_push_buffer(byte * buf, int x, int y) {
    LOCK_MUTEX(local_render_data->mutex);
    pushed_buf = buf;
    printf("pushing buffer %X\n",buf);
    local_render_data->raw_data = buf;
    SIGNAL(local_render_data->cond);
    UNLOCK_MUTEX(local_render_data->mutex);
}

static pthread_mutex_t _gen_mutex = PTHREAD_MUTEX_INITIALIZER;

static int visual_attribs[] = {
    GLX_X_RENDERABLE, True,
    GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
    GLX_RENDER_TYPE, GLX_RGBA_BIT,
    GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
    GLX_RED_SIZE, 8,
    GLX_GREEN_SIZE, 8,
    GLX_BLUE_SIZE, 8,
    GLX_ALPHA_SIZE, 8,
    GLX_DEPTH_SIZE, 24,
    GLX_STENCIL_SIZE, 8,
    GLX_DOUBLEBUFFER, True,
    //GLX_SAMPLE_BUFFERS  , 1,
    //GLX_SAMPLES         , 4,
    None
};

GLXDrawable drawable;

void * render_thread2(void * arg) {
    render_data * rd = (render_data*)arg;
    Display * display = rd->display;
    xcb_window_t window = rd->window;
    
    
     GLXFBConfig *fb_configs = 0;
    int num_fb_configs = 0;
   // XSetEventQueueOwner(display, XCBOwnsEventQueue);
    
    int default_screen = DefaultScreen(display);
    fb_configs = glXChooseFBConfig(display, default_screen, visual_attribs, &num_fb_configs);

    int visualID;
    GLXFBConfig fb_config = fb_configs[0];
    glXGetFBConfigAttrib(display, fb_config, GLX_VISUAL_ID , &visualID);
    
    //xcb(visualID);
    
    GLXContext context = glXCreateNewContext(display, fb_config, GLX_RGBA_TYPE, 0, True);
    GLXWindow glxwindow = glXCreateWindow(display, fb_config, window, 0);
    drawable = glxwindow;
        
    int rt = glXMakeContextCurrent(display, drawable, drawable, context);
    printf("glxwin %X %X %i\n", glxwindow, context, rt);

    LOCK_MUTEX(init_mutex);
    init_state = 1;
    SIGNAL(init_cond);
    UNLOCK_MUTEX(init_mutex);
    int at = 0;
    for(;;) {
        glClearColor(0, at ? 0.3 : 0, 0.5, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        usleep(1000000);
        glXSwapBuffers(display, drawable);
        at = !at;
    }
}
    
static void * render_thread(void * arg) {
    render_data * rd = (render_data*)arg;
    byte * local_buf = (byte*)malloc(640*480);
    
    Display * display = rd->display;
    xcb_window_t window = rd->window;
    GLXFBConfig fb_config = rd->fb_config;
    
    GLXFBConfig *fb_configs = 0;
    int num_fb_configs = 0;
    //XSetEventQueueOwner(display, XCBOwnsEventQueue);
    int default_screen = DefaultScreen(display);
    fb_configs = glXChooseFBConfig(display, default_screen, visual_attribs, &num_fb_configs);

    fb_config = fb_configs[0];
    
    context = glXCreateNewContext(display, fb_config, GLX_RGBA_TYPE, 0, True);
    GLXWindow glxwindow = glXCreateWindow(display, fb_config, window, 0);
    GLXDrawable drawable = glxwindow;
    _disp = display;
    _drawable = drawable;

    int rt = glXMakeContextCurrent(display, drawable, drawable, context);
    printf("glxwin %X %X %i\n", glxwindow, context, rt);

    glGenTextures(10, tex);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex[0]);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, 640, 480, 0, GL_RED_INTEGER, GL_UNSIGNED_BYTE, cbuf);
    
    
    
    glActiveTexture(GL_TEXTURE0+1);    
    glBindTexture(GL_TEXTURE_2D, tex[1]);   
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);   
    
    load_shaders();
    
    glUniform1i(tex_loc, 0);
    
    glUniform1i(color_table_loc,1);
    
    LOCK_MUTEX(init_mutex);
    init_state = 1;
    SIGNAL(init_cond);
    UNLOCK_MUTEX(init_mutex);
    for(;;) {
        int x,y;
        LOCK_MUTEX(rd->mutex);
        while (!rd->state || !rd->raw_data) {
            WAIT_CONDITION(rd->cond,rd->mutex);
        }        
        
        int act = 0;
        LOCK_MUTEX(_gen_mutex);
        act = active_color;
        UNLOCK_MUTEX(_gen_mutex);
        
        init_color(act);
        x = rd->x;
        y = rd->y;
        memcpy(local_buf,rd->raw_data,x*y);
       // memset(local_buf,rand(),x*y);
        rd->state = 0;
        UNLOCK_MUTEX(rd->mutex);
        
       // printf("rendering\n");
        
            
        glDisable(GL_LIGHTING);
        glEnable(GL_TEXTURE_2D);
    //    glEnable(GL_TEXTURE_1D);
        glDisable(GL_COLOR_MATERIAL);

        glClearColor(0, 0, 0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
//              
//        FILE * fp = fopen("pic","rb");
//        fread(local_buf,1,640*480,fp);
//        fclose(fp);
//        int j;
//        for(j=0;j<36;j++) {
//            memset(local_buf+640*(100+j)+72,152,156);
//            int w;
//            for(w=0;w<156;w++) {
//                printf("(%2X) ",*(local_buf+640*(100+j)+72+w));
//            }
//            printf("---\n");
//        }
//        exit(0);
        //72 100 
          //      228 136
//        //memset(local_buf+640*400,0,640*40);
//      //  memset(local_buf+640*400+260,0,40);
//        int i;
//        for(i=640*400+260;i<640*400+260+40;i++) {
//            printf("(%X) ",local_buf[i]);
//        }
//        printf("\n");
//        exit(0);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, x, y, GL_RED_INTEGER, GL_UNSIGNED_BYTE, local_buf);
        //glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, x, y, 0, GL_RED_INTEGER, GL_UNSIGNED_BYTE, buf);

        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, 1);
        //glNormal3f( 0.0f, 0.0f, 0.0f);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-1.0f, -1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, -1.0f, 0.0f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, 1.0f, 0.0f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-1.0f, 1.0f, 0.0f);

        glEnd();

        glXSwapBuffers(_disp, _drawable);
       // printf("render\n");
        //sleep(1);
    }
}

void set_active_color(int id) {
    LOCK_MUTEX(_gen_mutex);
    active_color = id;
    UNLOCK_MUTEX(_gen_mutex);
}

void gl_from_x(Display * display, xcb_window_t window, GLXFBConfig fb_config) {
    pthread_t xx;
    render_data * rd = (render_data*)malloc(sizeof(render_data));
    //rd->raw_data = (byte*)malloc(640*480*3);
    
    rd->raw_data = pushed_buf;
    rd->display = display;
    rd->window = window;
    rd->fb_config = fb_config;
    
    pthread_mutex_init(&rd->mutex,0);
    pthread_cond_init(&rd->cond,0);
    rd->state = 0;
    rd->x = 640;
    rd->y = 480;
    local_render_data = rd;
    
    pthread_create(&xx,0,render_thread,rd);
    
    LOCK_MUTEX(init_mutex);
    while (!init_state) {
        WAIT_CONDITION(init_cond,init_mutex);
    }
    UNLOCK_MUTEX(init_mutex);

}


void xgl();

int color_id(int r, int g, int b) {
    
    int i;
    int found = -1;
    for(i=0;i<16;i++) {
        if (local_tables[i].internal_idx == 0) {
            found = i;
            local_tables[i].size = 0;
            local_tables[i].inited = 2;
            local_tables[i].internal_idx = i;
            local_tables[i].r = r;
            local_tables[i].g = g;
            local_tables[i].b = b;
            
            break;
        }
    }
    if (found < 0) {
        printf("too many color tables\n");
        exit(0);
        return -1;
    }
    
    return found;
}

int set_colors(LPPALETTEENTRY entries, int start, int size, int id) {

    LOCK_MUTEX(_gen_mutex);
    
    local_color_table * lt = local_tables+id;
    int i;    
    int delta = start * 3;    
    
    lt->raw = color_maps_internal + (256*3*id);
    byte * ctable = lt->raw;
    if (lt->inited == 2) {
        memset(ctable,0,256*3);        
        lt->inited = 0;
        ctable[255*3] = 255;
        ctable[255*3+1] = 255;
        ctable[255*3+2] = 255;
        
    }
        
    for (i = 0; i < size; i++) {
        ctable[delta + i * 3] = entries[i].peRed;
        ctable[delta + i * 3 + 1] = entries[i].peGreen;
        ctable[delta + i * 3 + 2] = entries[i].peBlue;
      //  printf("(%i %i %i) ",entries[i].peRed,entries[i].peGreen,entries[i].peBlue);
    }

   // printf("\n");

    //printf("adjusted colors on %i\n",id);
    lt->inited = 0;
    UNLOCK_MUTEX(_gen_mutex);
    
   // active_color = id;
   
}

void flush_palette(int idx) {
    local_tables[idx].internal_idx = -1;
}

void init_color(int idx) {
    
    if (idx != pushed_color) {
        glUniform1i(color_sample_loc,idx);
        printf("set loc %i\n",color_sample_loc);
        pushed_color = idx;
    }
    if (idx < 0) return;
    LOCK_MUTEX(_gen_mutex);
    if (local_tables[idx].inited) {
        UNLOCK_MUTEX(_gen_mutex);
        return;
    }
    int pos = local_tables[idx].internal_idx;
    
    glActiveTexture(GL_TEXTURE0+1);    
    glBindTexture(GL_TEXTURE_2D, tex[1]);
    int err = glGetError();
    if (err) {
        printf("Failure setting colors (%X)\n",err);
        exit(0);
    }
    
    //memset(color_maps_internal,200,sizeof(color_maps_internal));
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, 256, 16, 0, GL_RGB, GL_UNSIGNED_BYTE, color_maps_internal);
    err = glGetError();
    glUniform1i(color_table_loc,1);
    
  //  printf("Setting color on position %i\n",pos);
    
    if (err) {
        printf("Failure setting colors (%X)\n",err);
        exit(0);
    }
    
    local_tables[idx].inited = 1;
    UNLOCK_MUTEX(_gen_mutex);
}

GLfloat stuff[256*3];

void gl_lock() {
   // printf("lock\n");  
    LOCK_MUTEX(local_render_data->mutex);
}

void gl_unlock() {
   // printf("unlock\n")    ;
    UNLOCK_MUTEX(local_render_data->mutex);
}

// must enter here locked
void draw_from_buffer_and_unlock(byte * buf, int x, int y) {
    local_render_data->state = 1;
    local_render_data->x = x;
    local_render_data->y = y;
    SIGNAL(local_render_data->cond);
    
    UNLOCK_MUTEX(local_render_data->mutex);
}

void draw_test() {
    if (!_disp) {
        xgl();
    }

    //    printf("blue\n");

    glClearColor(0.2, 0.4, 0.9, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    glXSwapBuffers(_disp, _drawable);


    sleep(3242344);
}
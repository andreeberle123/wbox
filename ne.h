#ifndef NE_H
#define NE_H
#include <stdint.h>
#include "common.h"

typedef struct NE_header {
    uint8_t sig[2];                 //"NE"
    uint8_t MajLinkerVersion;    //The major linker version
    uint8_t MinLinkerVersion;    //The minor linker version
    uint16_t EntryTableOffset;   //Offset of entry table, see below
    uint16_t EntryTableLength;   //Length of entry table in bytes
    uint32_t FileLoadCRC;        //32-bit CRC of entire contents of file
    uint8_t ProgFlags;           //Program flags, bitmapped
    uint8_t ApplFlags;           //Application flags, bitmapped
    uint8_t AutoDataSegIndex;    //The automatic data segment index
    uint8_t unk;
    uint16_t InitHeapSize;       //The intial local heap size
    uint16_t InitStackSize;      //The inital stack size
    uint32_t EntryPoint;         //CS:IP entry point, CS is index into segment table
    uint32_t InitStack;          //SS:SP inital stack pointer, SS is index into segment table
    uint16_t SegCount;           //Number of segments in segment table
    uint16_t ModRefs;            //Number of module references (DLLs)
    uint16_t NoResNamesTabSiz;   //Size of non-resident names table, in bytes (Please clarify non-resident names table)
    uint16_t SegTableOffset;     //Offset of Segment table
    uint16_t ResTableOffset;     //Offset of resources table
    uint16_t ResidNamTable;      //Offset of resident names table
    uint16_t ModRefTable;        //Offset of module reference table
    uint16_t ImportNameTable;    //Offset of imported names table (array of counted strings, terminated with string of length 00h)
    uint32_t OffStartNonResTab;  //Offset from start of file to non-resident names table
    uint16_t MovEntryCount;      //Count of moveable entry point listed in entry table
    uint16_t FileAlnSzShftCnt;   //File alligbment size shift count (0=9(default 512 byte pages))
    uint16_t nResTabEntries;     //Number of resource table entries
    uint8_t targOS;              //Target OS
    uint8_t OS2EXEFlags;         //Other OS/2 flags
    uint16_t retThunkOffset;     //Offset to return thunks or start of gangload area - what is gangload?
    uint16_t segrefthunksoff;    //Offset to segment reference thunks or size of gangload area
    uint16_t mincodeswap;        //Minimum code swap area size
    uint8_t expctwinver[2];      //Expected windows version (minor first)
} __attribute__((packed)) NE_header;
 
//Program flags
//DGroup type (what is this?)
enum dgrouptype {
    none,         //None
    sinshared,    //single shared
    multiple,     //Multiple
    null          //(null)
};
 
#define GLOBINIT 1<<2     //global initialization
#define PMODEONLY 1<<3    //Protetced mode only
#define INSTRUC86 1<<4    //8086 instructions
#define INSTRU286 1<<5    //80286 instructions
#define INSTRU386 1<<6    //80386 instructions
#define INSTRUx87 1<<7    //80x87 (FPU) instructions
 
//Application flags
//Application type
enum apptype {
    nonea,
    fullscreeen,    //fullscreen (not aware of Windows/P.M. API)
    winpmcompat,    //compatible with Windows/P.M. API
    winpmuses       //uses Windows/P.M. API
};
#define OS2APP 1<<3    //OS/2 family application
//bit 4 reserved?
#define IMAGEERROR 1<<5    //errors in image/executable
#define NONCONFORM 1<<6    //non-conforming program?
#define DLL        1<<7    //DLL or driver (SS:SP invalid, CS:IP->Far INIT routine AX=HMODULE,returns AX==0 success, AX!=0 fail)
 
//Target Operating System
enum targetos {
    unknown,    //Obvious ;)
    os2,        //OS/2 (as if you hadn't worked that out!)
    win,        //Windows (Win16)
    dos4,       //European DOS  4.x
    win386,     //Windows for the 80386 (Win32s). 32 bit code.
    BOSS        //The boss, a.k.a Borland Operating System Services
};
//Other OS/2 flags
#define LFN 1        //OS/2 Long File Names (finally, no more 8.3 conversion :) )
#define PMODE 1<<1   //OS/2 2.x Protected Mode executable
#define PFONT 1<<2   //OS/2 2.x Proportional Fonts
#define GANGL 1<<3   //OS/2 Gangload area

typedef struct _segment {
    WORD offset;
    WORD length;
    WORD flags;
    WORD minsize;
} __attribute__((packed)) segment;


typedef struct _ne_import_info {
    char module[64];
    //char function[64];
    //char remapped[64];
} ne_import_info;

#define RELOC_TYPE_ORD 0x100
#define RELOC_TYPE_INTERNAL 0x200

typedef struct _ne_import_reloc {
    int idx;
    int ord;
    int offset;
    int type;
} ne_import_reloc;

typedef struct _ne_file_info {
    segment* segments[32];
    int seg_num;
    byte * data;
    int dsize;
    int vashift;
    ne_import_info * import_table;
    int import_table_size;
    ne_import_reloc * relocs;
    int relocs_size;
    dword entry;
    dword cs;
    dword ds;
    byte * exec_base;
    byte * mm_base;
} ne_file_info;


typedef struct _NE_entry {
    unsigned char entries;
    unsigned char type;
    char data[];
}__attribute__((packed)) NE_entry;

typedef struct _NE_movable_segment {
    unsigned char flags;
    unsigned short inst;
    unsigned char seg_num;
    unsigned short seg_off;
}__attribute__((packed)) NE_movable_segment;

typedef struct _NE_fixed_segment {
    unsigned char flags;
    unsigned short offset;
} __attribute__((packed)) NE_fixed_segment;

enum PRsrcName {
  rt_Cursor      = 0x8001,
  rt_Bitmap      = 0x8002,
  rt_Icon        = 0x8003,
  rt_Menu        = 0x8004,
  rt_Dialog      = 0x8005,
  rt_String      = 0x8006,
  rt_FontDir     = 0x8007,
  rt_Font        = 0x8008,
  rt_Accelerator = 0x8009,
  rt_RCData      = 0x800A,
  rt_Group_Cursor= 0x800C,
  rt_Group_Icon  = 0x800E,
  rt_Version = 0x8010
};

typedef struct _NAMEINFO {
    WORD rnOffset;
    WORD rnLength;
    WORD rnFlags;
    WORD rnID;
    WORD rnHandle;
    WORD rnUsage;
} __attribute__((packed)) NAMEINFO;

typedef struct _TYPEINFO {
    WORD rtTypeID;
    WORD rtResourceCount;
    DWORD rtReserved;
    NAMEINFO rtNameInfo[];
} __attribute__((packed)) TYPEINFO;

typedef struct _res_table {
    WORD align;
    TYPEINFO infos[];
} __attribute__((packed)) res_table;


typedef struct _ne_exp_info {
    char name[64];
    int nsize;
    int ord;
} ne_exp_info;

int process_file_ne(char * path, ne_file_info * ne);
int exp_parse(char * path, ne_exp_info ** infos, int *i_size, char * mod_name);
int get_ne_symbol(char * module, int ordinal, char * fn);
int get_global_fn_idx(char * module, int ord);
void finish_exports();

#define MEM16_STACK_BASE 0xFC00

#define CS_ENTRY_ID 0x66
#define DS_ENTRY_ID 0x67
#define STUBS_ENTRY_ID 0x68
#define ES_ENTRY_ID 0x69

extern "C" {
void mem16_setup(void * addr, uint32 len, uint32 stack_base);
void setup_16(dword entry, void * base, int ct, int is32);
}
#endif /* NE_H */


#include <xcb/randr.h>

#include "common.h"
#include "winm.h"
#include "xw.h"
#include "dx.h"
#include "sink.h"
#include "gl.h"
extern "C" {




typedef struct _DDSCAPS2 {
  DWORD dwCaps;
  DWORD dwCaps2;
  DWORD dwCaps3;
  union {
    DWORD dwCaps4;
    DWORD dwVolumeDepth;
  } DUMMYUNIONNAMEN(1);
} DDSCAPS2, *LPDDSCAPS2;

typedef struct _DDSURFACEDESC2 {
  DWORD      dwSize;
  DWORD      dwFlags;
  DWORD      dwHeight;
  DWORD      dwWidth;
  union {
    LONG  lPitch;
    DWORD dwLinearSize;
  } DUMMYUNIONNAMEN(1);
  union {
    DWORD dwBackBufferCount;
    DWORD dwDepth;
  } DUMMYUNIONNAMEN(5);
  union {
    DWORD dwMipMapCount;
    DWORD dwRefreshRate;
    DWORD dwSrcVBHandle;
  } DUMMYUNIONNAMEN(2);
  DWORD      dwAlphaBitDepth;
  DWORD      dwReserved;
  LPVOID     lpSurface;
  union {
    DDCOLORKEY ddckCKDestOverlay;
    DWORD      dwEmptyFaceColor;
  } DUMMYUNIONNAMEN(3);
  DDCOLORKEY ddckCKDestBlt;
  DDCOLORKEY ddckCKSrcOverlay;
  DDCOLORKEY ddckCKSrcBlt;
  union {
    DDPIXELFORMAT ddpfPixelFormat;
    DWORD         dwFVF;
  } DUMMYUNIONNAMEN(4);
  DDSCAPS2   ddsCaps;
  DWORD      dwTextureStage;
} DDSURFACEDESC2, *LPDDSURFACEDESC2;


/*HRESULT*/ int WINAPI QueryInterface(IDirectDraw *_this, /*REFIID*/ int riid, void* ppvObj) {
    printf("QueryInterface\n");
exit(0);
}

/*ULONG*/ int WINAPI AddRef(IDirectDraw *_this) {
    printf("AddRef\n");
exit(0);
}

/*ULONG*/ int WINAPI Release(IDirectDraw *_this) {
  //  printf("Release\n");
    return DD_OK;
}

/*HRESULT*/ int WINAPI Compact(struct IDirectDraw *_this, int i) {
    printf("Compact\n");
exit(0);
}

/*HRESULT*/ int WINAPI CreateClipper(struct IDirectDraw *_this, int i) {
    printf("CreateClipper\n");
exit(0);
}

void dummy() {
    printf("dummy fn\n");exit(0);
}

int _p_GetCaps(struct _IDirectDrawPalette * _this, LPDWORD) {
    printf("_p_GetCaps\n");exit(0);
}

int _p_GetEntries(struct _IDirectDrawPalette * _this, DWORD,DWORD,DWORD,LPPALETTEENTRY) {
    printf("_p_GetEntries\n");exit(0);
}

int _p_Initialize(struct _IDirectDrawPalette * _this, LPDIRECTDRAW, DWORD, LPPALETTEENTRY) {
    printf("_p_Initialize\n");exit(0);
}

int _p_SetEntries(struct _IDirectDrawPalette * _this, DWORD dwFlags,
                    DWORD dwStartingEntry,DWORD dwCount,LPPALETTEENTRY e) {
    //printf("SetEntries %X %i %i\n",dwFlags,dwStartingEntry,dwCount);
//    int i;
//    for(i=0;i<dwCount;i++) {
//        printf("bit depth %i - %i %i %i\n",e[i].peFlags,e[i].peRed,e[i].peGreen,e[i].peBlue);
//    }
    
    IDirectDrawPalette * p = *(IDirectDrawPalette**)_this;
    set_colors(e,dwStartingEntry,dwCount,p->internal_id);
   // printf("Alter palette %i\n",p->internal_id);

    return DD_OK;
}

/*ULONG*/ int WINAPI _p_Release(IDirectDraw *_this) {
    printf("Release palette\n");
    IDirectDrawPalette * p = *(IDirectDrawPalette**)_this;
    flush_palette(p->internal_id);
    return DD_OK;
}

IDirectDrawPalette basePalette = {
    &QueryInterface,&AddRef,&_p_Release,_p_GetCaps,
    &_p_GetEntries,
    &_p_Initialize,
    &_p_SetEntries
};

/*HRESULT*/ int WINAPI CreatePalette(struct IDirectDraw *_this, DWORD i, LPPALETTEENTRY e,
        LPDIRECTDRAWPALETTE * dp, IUnknown * n) {
    if (!(i & 0x48)) {
        printf("Unsupported createpalette\n");
        exit(0);
    }

    IDirectDrawPalette **ap = (IDirectDrawPalette** )malloc(sizeof(IDirectDrawPalette*));
    *ap = &basePalette;
    *dp = (LPDIRECTDRAWPALETTE)ap;
    (*ap)->internal_id = color_id(0,0,0);
    set_colors(e,0,255,(*ap)->internal_id);
    return DD_OK;
}

/*HRESULT*/ int WINAPI CreateSurface(struct IDirectDraw *_this, LPDDSURFACEDESC desc, 
        LPDIRECTDRAWSURFACE *ds, IUnknown * unk) {

    printf("CreateSurface %i %i %X\n",desc->dwWidth,desc->dwHeight,desc->ddsCaps);

    IDirectDrawSurface * surf= (IDirectDrawSurface*)malloc(sizeof(IDirectDrawSurface));
    dxs_fill_surface(surf);
    surf->ddraw = _this;
    surf->sink = new ImageSink();
    pthread_mutex_init(&surf->mutex,0);
    surf->locked = 0;
    
    
    wmm_dimension(surf->sink,_this->dcontext.hwnd);    
    
    // c++ indirection
    IDirectDrawSurface **ap = (IDirectDrawSurface** )malloc(sizeof(IDirectDrawSurface*));
    *ap = surf;
    *ds = (LPDIRECTDRAWSURFACE)ap;
    
    return DD_OK;
}

/*HRESULT*/ int WINAPI DuplicateSurface(struct IDirectDraw *_this, int i) {
    printf("DuplicateSurface\n");
exit(0);
}

extern DDSURFACEDESC local_dx_display_modes[];
extern int local_dx_display_modes_sz;

/*HRESULT*/ int WINAPI EnumDisplayModes(struct IDirectDraw *_this,DWORD dwFlags, 
        HRESULT* lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK lpEnumModesCallback) {
    DDSURFACEDESC desc;
    printf("EnumDisplayModes\n");
    int i;
    //for (i = 0; i < size; i++) {
    for (i = 0; i < local_dx_display_modes_sz; i++) {
        HRESULT r = lpEnumModesCallback(local_dx_display_modes+i,lpContext);
        if (!r) {
            break;
        }
    }
    
        
    return DD_OK;
}

/*HRESULT*/ int WINAPI EnumSurfaces(struct IDirectDraw *_this, int i) {
    printf("EnumSurfaces\n");
exit(0);
}

/*HRESULT*/ int WINAPI FlipToGDISurface(struct IDirectDraw *_this, int i) {
    printf("FlipToGDISurface\n");
exit(0);
}

/*HRESULT*/ int WINAPI GetCaps(struct IDirectDraw *_this, int a, int b) {
    printf("GetCaps\n");
exit(0);
}

/*HRESULT*/ int WINAPI GetDisplayMode(struct IDirectDraw *_this, int i) {
    printf("GetDisplayMode\n");
exit(0);
}

/*HRESULT*/ int WINAPI GetFourCCCodes(struct IDirectDraw *_this, int i) {
    printf("GetFourCCCodes\n");
exit(0);
}

/*HRESULT*/ int WINAPI GetGDISurface(struct IDirectDraw *_this, int i) {
    printf("GetGDISurface\n");
exit(0);
}

/*HRESULT*/ int WINAPI GetMonitorFrequency(struct IDirectDraw *_this, int i) {
    printf("GetMonitorFrequency\n");
exit(0);
}

/*HRESULT*/ int WINAPI GetScanLine(struct IDirectDraw *_this, int i) {
    printf("GetScanLine\n");
exit(0);
}

/*HRESULT*/ int WINAPI GetVerticalBlankStatus(struct IDirectDraw *_this, int i) {
    printf("GetVerticalBlankStatus\n");
exit(0);
}

/*HRESULT*/ int WINAPI Initialize(struct IDirectDraw *_this, int i) {
    printf("Initialize\n");
exit(0);
}

/*HRESULT*/ int WINAPI RestoreDisplayMode(struct IDirectDraw *_this) {
    return DD_OK;
}

/*HRESULT*/ int WINAPI SetCooperativeLevel(struct IDirectDraw *_this, HWND hWnd, DWORD dwFlags) {
    printf("Cooperative level %X %X\n", hWnd, dwFlags);
    _this->dcontext.hwnd = hWnd;
    
    return DD_OK;

}

/*HRESULT*/ int WINAPI SetDisplayMode(struct IDirectDraw *_this, int w, int h, int br) {
    printf("Set Display Mode %i %i %i %X\n",w,h,br,_this->dcontext.hwnd);
    wmm_set_desired_res(w,h, _this->dcontext.hwnd);
    //wmm_resize_window(w,h);    
    
    return DD_OK;
}

/*HRESULT*/ int WINAPI WaitForVerticalBlank(struct IDirectDraw *_this, int i) {
    printf("WaitForVerticalBlank\n");
exit(0);
}

IDirectDrawl localIDirectDrawl = {
    &QueryInterface, &AddRef, &Release, &Compact, &CreateClipper, &CreatePalette,
    &CreateSurface, &DuplicateSurface, &EnumDisplayModes, &EnumSurfaces, &FlipToGDISurface, &GetCaps, &GetDisplayMode,
    &GetFourCCCodes, &GetGDISurface, &GetMonitorFrequency, &GetScanLine, &GetVerticalBlankStatus,
    &Initialize, &RestoreDisplayMode, &SetCooperativeLevel, &SetDisplayMode, &WaitForVerticalBlank
};

IDirectDraw localIDirectDraw;

HRESULT WINAPI DirectDrawCreate(
        GUID *lpGUID,
        LPDIRECTDRAW *lplpDD,
        IUnknown *pUnkOuter
        ) {

    //printf("pfunc base %X %X %X\n",&localIDirectDraw,localIDirectDraw.QueryInterface,QueryInterface);
    *lplpDD = &localIDirectDraw;

    return 0;
}

void init_dx_subsystem() {
    localIDirectDraw.QueryInterface = &localIDirectDrawl.QueryInterface;
    localIDirectDraw.AddRef = &localIDirectDrawl.AddRef;
    localIDirectDraw.Release = &localIDirectDrawl.Release;
    localIDirectDraw.Compact = &localIDirectDrawl.Compact;
    localIDirectDraw.CreateClipper = &localIDirectDrawl.CreateClipper;
    localIDirectDraw.CreatePalette = &localIDirectDrawl.CreatePalette;
    localIDirectDraw.CreateSurface = &localIDirectDrawl.CreateSurface;
    localIDirectDraw.DuplicateSurface = &localIDirectDrawl.DuplicateSurface;
    localIDirectDraw.EnumDisplayModes = &localIDirectDrawl.EnumDisplayModes;
    localIDirectDraw.EnumSurfaces = &localIDirectDrawl.EnumSurfaces;
    localIDirectDraw.FlipToGDISurface = &localIDirectDrawl.FlipToGDISurface;
    localIDirectDraw.GetDisplayMode = &localIDirectDrawl.GetDisplayMode;
    localIDirectDraw.GetFourCCCodes = &localIDirectDrawl.GetFourCCCodes;
    localIDirectDraw.GetGDISurface = &localIDirectDrawl.GetGDISurface;
    localIDirectDraw.GetMonitorFrequency = &localIDirectDrawl.GetMonitorFrequency;
    localIDirectDraw.GetScanLine = &localIDirectDrawl.GetScanLine;
    localIDirectDraw.GetVerticalBlankStatus = &localIDirectDrawl.GetVerticalBlankStatus;
    localIDirectDraw.Initialize = &localIDirectDrawl.Initialize;
    localIDirectDraw.RestoreDisplayMode = &localIDirectDrawl.RestoreDisplayMode;
    localIDirectDraw.SetCooperativeLevel = &localIDirectDrawl.SetCooperativeLevel;
    localIDirectDraw.SetDisplayMode = &localIDirectDrawl.SetDisplayMode;
    localIDirectDraw.WaitForVerticalBlank = &localIDirectDrawl.WaitForVerticalBlank;
    

}

#define DSBSTATUS_PLAYING           0x00000001
#define DSBSTATUS_BUFFERLOST        0x00000002
#define DSBSTATUS_LOOPING           0x00000004
#define DSBSTATUS_LOCHARDWARE       0x00000008
#define DSBSTATUS_LOCSOFTWARE       0x00000010
#define DSBSTATUS_TERMINATED        0x00000020

int WINAPI _dsb_tGetCaps3              (struct IDirectSoundBuffer * _this, LPDSBCAPS)  {
    printf("_dsb_tGetCaps3\n");exit(0);
}
int WINAPI _dsb_tGetCurrentPosition   (struct IDirectSoundBuffer * _this, LPDWORD, LPDWORD)  {
    printf("_dsb_tGetCurrentPosition\n");exit(0);
}
int WINAPI _dsb_tGetFormat            (struct IDirectSoundBuffer * _this, LPWAVEFORMATEX, DWORD, LPDWORD)  {
    printf("_dsb_tGetFormat\n");exit(0);
}
int WINAPI _dsb_tGetVolume            (struct IDirectSoundBuffer * _this, LPLONG)  {
    printf("_dsb_tGetVolume\n");exit(0);
}
int WINAPI _dsb_tGetPan               (struct IDirectSoundBuffer * _this, LPLONG)  {
    printf("_dsb_tGetPan\n");exit(0);
}
int WINAPI _dsb_tGetFrequency         (struct IDirectSoundBuffer * _this, LPDWORD)  {
    printf("_dsb_tGetFrequency\n");exit(0);
}
int WINAPI _dsb_tGetStatus            (struct IDirectSoundBuffer * _this, LPDWORD status)  {
    *status = DSBSTATUS_TERMINATED ;
    return 0;
}
int WINAPI _dsb_tInitialize2           (struct IDirectSoundBuffer * _this, LPDIRECTSOUND, LPCDSBUFFERDESC) {
    printf("_dsb_tInitialize2\n");exit(0);
}
int WINAPI _dsb_tLock(struct IDirectSoundBuffer * _this, DWORD dwWriteCursor,DWORD dwWriteBytes,
        LPVOID *lplpvAudioPtr1,LPDWORD lpdwAudioBytes1,LPVOID *lplpvAudioPtr2,LPDWORD lpdwAudioBytes2,DWORD dwFlags )  {
    *lplpvAudioPtr1 = malloc(dwWriteBytes);
    *lpdwAudioBytes1 = dwWriteBytes;
    *lplpvAudioPtr2 = NULL;
    printf("%X %i %X\n",dwWriteCursor,dwWriteBytes,dwFlags);
    //printf("_dsb_tLock\n");exit(0);
    return 0;
}

int WINAPI _dsb_tPlay                 (struct IDirectSoundBuffer * _this, DWORD, DWORD, DWORD)  {
    return 0;
}

int WINAPI _dsb_tSetCurrentPosition   (struct IDirectSoundBuffer * _this, DWORD)  {
    return 0;
}
int WINAPI _dsb_tSetFormat           (struct IDirectSoundBuffer * _this, LPCWAVEFORMATEX)  {
    printf("_dsb_tSetFormat\n");exit(0);
}
int WINAPI _dsb_tSetVolume           (struct IDirectSoundBuffer * _this, LONG volume) {
    return 0;
}
int WINAPI _dsb_tSetPan               (struct IDirectSoundBuffer * _this, LONG pan) {
    return 0;
}
int WINAPI _dsb_tSetFrequency         (struct IDirectSoundBuffer * _this, DWORD) {
    printf("_dsb_tSetFrequency\n");exit(0);
}
int WINAPI _dsb_tStop                 (struct IDirectSoundBuffer * _this) {
    printf("_dsb_tStop\n");exit(0);
}
int WINAPI _dsb_tUnlock               (struct IDirectSoundBuffer * _this, LPVOID, DWORD, LPVOID, DWORD) {
    return 0;
}
int WINAPI _dsb_tRestore              (struct IDirectSoundBuffer * _this) {
    printf("_dsb_tRestore\n");exit(0);
}

IDirectSoundBuffer localDirectSoundBuffer = {
    &QueryInterface,&AddRef,&Release,
    &_dsb_tGetCaps3,&_dsb_tGetCurrentPosition,&_dsb_tGetFormat,&_dsb_tGetVolume,&_dsb_tGetPan,&_dsb_tGetFrequency,&_dsb_tGetStatus,&_dsb_tInitialize2,&_dsb_tLock,&_dsb_tPlay,&_dsb_tSetCurrentPosition,&_dsb_tSetFormat,&_dsb_tSetVolume,&_dsb_tSetPan,&_dsb_tSetFrequency,&_dsb_tStop,&_dsb_tUnlock,&_dsb_tRestore
    
};

int WINAPI _ds_CreateSoundBuffer(struct IDirectSound *_this, LPCDSBUFFERDESC desc,
        LPDIRECTSOUNDBUFFER * sbuffer, void * unk) {
    // workaround c++ linking
    IDirectSoundBuffer ** delim = (IDirectSoundBuffer**)malloc(sizeof(IDirectSoundBuffer*));
    *delim = &localDirectSoundBuffer;
    *sbuffer = (LPDIRECTSOUNDBUFFER)delim;
    
   // printf("_ds_CreateSoundBuffer\n");exit(0);
    
    return 0;
}
int WINAPI _ds_GetCaps              (struct IDirectSound *_this, LPDSCAPS) {
    printf("_ds_GetCaps\n");exit(0);
}
int WINAPI _ds_DuplicateSoundBuffer (struct IDirectSound *_this, LPDIRECTSOUNDBUFFER, LPDIRECTSOUNDBUFFER *) {
    printf("_ds_DuplicateSoundBuffer\n");exit(0);
}
int WINAPI _ds_SetCooperativeLevel  (struct IDirectSound *_this, HWND, DWORD) {
    printf("_ds_SetCooperativeLevel\n");
    return 0;
}
int WINAPI _ds_Compact              (struct IDirectSound *_this) {
    printf("_ds_Compact\n");exit(0);
}
int WINAPI _ds_GetSpeakerConfig     (struct IDirectSound *_this, LPDWORD) {
    printf("_ds_GetSpeakerConfig\n");exit(0);
}
int WINAPI _ds_SetSpeakerConfig     (struct IDirectSound *_this, DWORD) {
    printf("_ds_SetSpeakerConfig\n");exit(0);
}
int WINAPI _ds_Initialize           (struct IDirectSound *_this, LPCGUID) {
    printf("_ds_Initialize\n");exit(0);
}

IDirectSound localIDirectSound = {
    &QueryInterface, &AddRef, &Release, &_ds_CreateSoundBuffer, &_ds_GetCaps,
    &_ds_DuplicateSoundBuffer, &_ds_SetCooperativeLevel, &_ds_Compact, &_ds_GetSpeakerConfig,
    &_ds_SetSpeakerConfig, &_ds_Initialize
};


HRESULT WINAPI DirectSoundCreate(
    LPCGUID lpGuid, 
    LPDIRECTSOUND * ppDS, 
    void * pUnkOuter ) {
    // workaround c++ linking
    IDirectSound ** delim = (IDirectSound**)malloc(sizeof(IDirectSound*));
    *delim = &localIDirectSound;
    *ppDS = (LPDIRECTSOUND)delim;
    
    return 0;
}

}
#include "common.h"    
#include <pthread.h>
#include "ne.h"

extern "C" {
    
/*******************************************************************************
 * Memory functions
 */

void *ds_base;
byte * es_base;
static int ds_brk;
static int ds_lim;
static pthread_mutex_t mm16_mutex = PTHREAD_MUTEX_INITIALIZER;

void setup_16(dword entry, void * base, int ct, int is32) {
    struct user_desc udx;
    dword idx;
    int rt;
    
    memset(&udx,0,sizeof(udx));
    udx.entry_number = entry;
    
    udx.limit = 16;    
    
    udx.limit_in_pages = 1;
    udx.useable = 1;
    udx.seg_32bit = is32;
    udx.contents = ct;

    printf("segm ct %X\n",ct);
    
    udx.base_addr = (dword)base;
    
    rt = syscall(__NR_modify_ldt,0x11,&udx,sizeof(udx));
    
    if (rt<0) {
        printf("Failed setting up 16bits ldt, maybe you need to enable ldt16?\n");
        exit(0);
    }
    
    printf("setting up %X %X\n",entry,udx.entry_number);
    
}

void mem16_setup(void * addr, uint32 len, uint32 stack_base) {
    ds_base = addr;
    ds_brk = len;
    ds_lim = stack_base;
}

DWORD WINAPI localalloc16(UINT   uFlags, SIZE_T uBytes) {
    DWORD res = ds_brk;
    ds_brk += uBytes;
    return res;
}

void WINAPI locksegment16(uint32 arg1) {
    LOCK_MUTEX(mm16_mutex);
}

void WINAPI unlocksegment16(uint32 arg1) {
    UNLOCK_MUTEX(mm16_mutex);
}

void * ds_unmap(void * addr) {
    uint32 laddr = (uint32)addr - (uint32)ds_base;
    return (void*)((0x33F<<16)+laddr);
}

void * ds_map(dword ref) {
    return (void*)((ref&0xFFFF)+(dword)ds_base);
}

void * mm_alloc_16(dword size) {
    dword poff = localalloc16(0,size);
    return (void*)(poff+(dword)ds_base);
}

/*******************************************************************************
 * Misc
 */

dword WINAPI getdosenvironment16() {

    *(dword*)es_base = 0;
    
    dword eseg = (ES_ENTRY_ID<<3)+0x7;
    return (eseg<<16);
}

uint32 WINAPI initapp16(uint32 a1) {
    
}

uint32 WINAPI inittask16(uint32 bp) {
    
}

void WINAPI exitwindows16() {
    exit(0);
}



uint32 WINAPI waitevent16(uint32 a1) {
    
}

uint32 WINAPI getcurrenttask16() {
    
}

uint32 WINAPI _wsprintf16(char * dest, char * fmt,...) {
}

HWND WINAPI CreateWindowExA(
    DWORD     dwExStyle,
    LPCTSTR   lpClassName,
    LPCTSTR   lpWindowName,
    DWORD     dwStyle,
    int       x,
    int       y,
    int       nWidth,
    int       nHeight,
    HWND      hWndParent,
    HMENU     hMenu,
    HINSTANCE hInstance,
    LPVOID    lpParam
    );

uint32 WINAPI createwindow16(LPCTSTR lpClassName, LPCTSTR lpWindowName, 
        DWORD dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent, 
        HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam) {
    
    uint32 ret = (uint32)CreateWindowExA(0,lpClassName, lpWindowName, dwStyle, x, y,\
        nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
    printf("ret win %X\n",ret);
    exit(0);
}

DWORD WINAPI GetTempPathA(
  DWORD nBufferLength,
  LPSTR lpBuffer
);

typedef struct tagTASKENTRY
{
    DWORD dwSize;
    WORD hTask;
    WORD hTaskParent;
    WORD hInst;
    WORD hModule;
    WORD wSS;
    WORD wSP;
    WORD wStackTop;
    WORD wStackMinimum;
    WORD wStackBottom;
    WORD wcEvents;
    WORD hQueue;
    char szModule[9 + 1];
    WORD wPSPOffset;
    WORD hNext;
} __attribute__((packed)) TASKENTRY;


uint32 WINAPI gettempdrive16(uint32 drive) {
    
}

uint32 WINAPI taskfindhandle16(TASKENTRY task_info, uint32 task_handle) { // ret 1 success, 0 failure
    return 1;
}

uint32 WINAPI dos3call16(uint32 arg) {
    uint32 command = (arg>>8)&0xFF;
    printf("dos3call %X\n",command);
    
    switch(command) {
        case 0x30: //get dos version
            asm __volatile (
                "movw $0,%%cx;"
                "movw $0,%%bx;"
            :::
            );
            
            return 0x700;
        case 0x35: { //interrupt vector on es:bx
            uint32 vecnum = arg&0xFF;
            printf("vec num %i\n",vecnum);
            return 0;
        }   
        default:
            printf("Not implemented doscall\n");
            exit(0);
    }
    

}

uint32 WINAPI createdialog16(uint32 hinstance, uint32 lpstr, uint32 hwnd, uint32 proc) {
    
}

void WINAPI setdlgitemtext16(uint32 hwnd, int i1, uint32 lpstr) {
    
}

typedef struct tagWNDCLASS16 {
    word      style;
    WNDPROC   lpfnWndProc;
    short       cbClsExtra;
    short       cbWndExtra;
    word        hInstance;
    word     hIcon;
    word   hCursor;
    word    hbrBackground;
    LPCTSTR   lpszMenuName;
    LPCTSTR   lpszClassName;
} __attribute__ ((packed)) WNDCLASS16, *PWNDCLASS16;

void _setclass16(int idx);

uint32 WINAPI registerclass16(
    const WNDCLASS16 *lpWndClass
) {
    printf("reg16 %X\n",lpWndClass->lpfnWndProc);
    WNDCLASS w;
    w.style = lpWndClass->style;
    w.lpfnWndProc = lpWndClass->lpfnWndProc;
    w.cbClsExtra = lpWndClass->cbClsExtra;
    w.cbWndExtra = lpWndClass->cbWndExtra;
    w.hInstance = (HINSTANCE)lpWndClass->hInstance;
    w.hIcon = (HICON)lpWndClass->hIcon;
    w.hCursor = (HCURSOR)lpWndClass->hCursor;
    w.hbrBackground = (HBRUSH) lpWndClass->hbrBackground;
    w.lpszClassName = (char*)ds_map((dword)lpWndClass->lpszClassName);
    w.lpszMenuName = (char*)ds_map((dword)lpWndClass->lpszMenuName);
    int idx = RegisterClassA(&w);
    _setclass16(idx);
    return idx;
    
}


/*******************************************************************************
 * Main gateway
 */

uint32 tmpdata1;
extern uint32 ne_exp_symbols_table[4096];

/*
* NOTE: these variables arent made for multi threading usage
*/
uint32 aux_asm_addr;
unsigned short int aux_asm_seg;
uint32 aux_asm_stack;

uint32 esp_guard;
uint32 ss_guard;
byte pstack[8192*2];
char * err_msg = "Bad segment on remap -- %X\n";

#define ST_INNER_BASE 0x1C
#define STACK_BACK 0x8

dword def_route_16(dword target, dword stentry, int count, char * mask,dword st_size);

dword global_16_route(dword idx) {
    dword target = ne_exp_symbols_table[idx];
    dword sbase = (dword)&idx;
    if (!target) {
        printf("No symbol for %X\n",idx);
        exit(0);
    }
    printf("Target %X %X\n",target,idx);
    
    printf("%X\n",*(uint32*)(sbase+4));
    aux_asm_stack = STACK_BACK;
    dword arg1;
    dword arg2;
    dword arg3;
    dword res;
    char * mmask;
    int ncount;
    uint32 st_size =  0;
    
    switch (idx) {
        case 0x1D9: // CREATEWINDOW
        {
            //W(LPCSTR, LPCSTR, DWORD, int, int, int, int, HWND, HMENU, HINSTANCE, void far *)
            st_size = 30;
            ncount = 11;
            mmask = "44422222224";
            break;
        }
        // no param
        case 0x9f: // GETVERSION
        case 0x6E: // GETDOSENVIRONMENT
            __asm volatile(
                "call %1;"
                "movl %%eax,%0;"
            :"=r"(res):"r"(target)
            :"eax"
            );
            return res;
        // one 2 bytes param (short int)
        case 0x400:
        case 0x48: // WAITEVENT
        case 0x272: // INITAPP
        case 0xC0: // LOCKSEGMENT
        case 0xEB: // UNLOCKSEGMENT
            st_size = 2;
            ncount = 1;
            mmask = "2";
            break;
        // two 2 bytes params
        case 0x113:
            mmask = "22";
            st_size = 4;
            ncount = 2;
            
            break; 
        // 2 args, 2nd is expanded
        case 0x172: // LOADCURSOR
        case 0x2e7: // LOADICON
        {
            mmask = "24";
            st_size = 6;
            ncount = 2;            
            break; 
        }
        // 3 args, 2nd is expanded
        case 0x74: { // GETMODULENAME 
            mmask = "242";
            st_size = 8;
            ncount = 3;   
//            aux_asm_stack = 8+8;
//            arg1 = *(word*)(sbase+ST_INNER_BASE);
//            arg2 = *(dword*)(sbase+ST_INNER_BASE+0x2);
//            arg3 = *(word*)(sbase+ST_INNER_BASE+0x6);
//            dword seg = arg2>>16;
//            void * remapped_addr;
//            if (seg == 0x33F) { 
//                //ds
//                remapped_addr = (void*)((uint32)ds_base+(arg2&0xFFFF));                
//            }
//            printf("args %X %X %X %X\n",arg1,arg2,arg3,remapped_addr);
//            __asm volatile(
//                "pushl %1;"
//                "pushl %3;"
//                "pushl %4;"
//                "call %2;"
//                "movl %%eax,%0;"
//            :"=r"(res):"r"(arg1),"r"(target),"r"(remapped_addr),"r"(arg3)
//            :"eax"
//            );
            break; 
        }
        // one 4 bytes param
        case 0x2ED:
        {
            st_size = 4;
            ncount = 1;
            mmask = "4";
            break;
        }
        default:
            printf("Not implemented global -- %X\n",idx);
            exit(0);
    }
    
    aux_asm_stack += st_size;
    return def_route_16(target,sbase+ST_INNER_BASE,ncount,mmask,st_size);
    
    //dword res = ((dword(*)(void))target)();
    
    return res;
}

}

#include <stdio.h>

#include "common.h"
#include <sys/stat.h>
#include <dirent.h>
#include "c_hash.h"
#include "lstring.h"
#include "fs.h"

VirtualFileMap * vfmap;

class FSMap {
};

char fs_map_base[1024];

void _fs_replace(char * logical) {
    char * t = logical;

    while (*t) {
        if (*t == '\\') {
            *t = '/';
        }
        t++;
    }
}

FILE * fs_open(char * logical, char *mode) {
    char pmap[1024];
    _fs_replace(logical);
    sprintf(pmap, "%s/%s", fs_map_base, logical + 2);
    return fopen(pmap, mode);
}

void fs_rename(char * path, char * out) {

}

void replace_slash(char * path) {
    char * t = path;
    while (*t) {
        if (*t == '\\') {
            *t = '/';
        }
        t++;
    }
}

static c_hash * vpaths;

typedef struct _pfile {
    char name[128];    
} pfile;

typedef struct _v_path {
    pfile * files;
    int file_count;
} v_path;

void remap_base(char * pmap, char * path) {
    memcpy(pmap, global_config_data.base_path, global_config_data.base_path_sz);
    memcpy(pmap + global_config_data.base_path_sz, path, strlen(path) + 1);
    replace_slash(pmap);
    
}

static void create_vpath(char * path) {
    int k = l_lastindexof(path,'/');
    
    *(path+k) = 0;
//    remap_base(pmap,path);
    printf("vpath %s\n",path);
    int len = strlen(path);
    DIR *d;
    struct dirent *dir;
    d = opendir(path);
    
    while ((dir = readdir(d)) != NULL) {
        if (dir->d_type != DT_REG) continue;
        
        char * vf = (char*)malloc(len+strlen(dir->d_name)+2);
        pfile * pf = (pfile*)malloc(sizeof(pfile));
        strcpy(vf,path);   
        *(vf+len) = '/';
        strcpy(vf+len+1,dir->d_name);
        strcpy(pf->name,path);
        *(pf->name+len) = '/';
        strcpy(pf->name+len+1,dir->d_name);
        toupper_str(vf);
        c_hash_put_k(DEF_STRING_HASH(vf),vf,pf,vpaths);
    }
    closedir(d);
    
    char * vf = (char*)malloc(strlen(path)+1);
    pfile * pf = (pfile*)malloc(sizeof(pfile));
    strcpy(pf->name,path);
    strcpy(vf,path);
    toupper_str(vf);
    c_hash_put_k(DEF_STRING_HASH(vf),vf,pf,vpaths);
    
    *(path+k) = '/';
    
}

/**
 * Converts a path into a vpath. Essentially looks for an abstraction over a
 * file to circumvent case
 * @param 
 * @return 
 */
static char * vpath(char * path) {
    char vp_map[1024];
    strcpy(vp_map,path);
    toupper_str(vp_map);
    
    pfile * v = (pfile*)c_hash_get_k(DEF_STRING_HASH(vp_map),vp_map,vpaths);
    
    if (!v) {
        create_vpath(path);
        v = (pfile*)c_hash_get_k(DEF_STRING_HASH(vp_map),vp_map,vpaths);
    }
    
    return v->name;
}

tvfshandle * fs_look_up(char * expr, char * mode, struct stat * st, local_find_file * info) {
    tvfshandle * th = vfmap->lookup_first(expr,info);
    if (!th) {
        return NULL;
    }
    stat(th->vc->physical_path,st); 
    //FILE * fp = vfmap->get_phys_handle(th,mode);
    stat(th->vc->physical_path,st);
    return th;
}

FILE * fs_open_bpath_stat(char * path, char * mode, struct stat * st, int excl) {
    
    char pmap[1024];
    memcpy(pmap, global_config_data.base_path, global_config_data.base_path_sz);
    memcpy(pmap + global_config_data.base_path_sz, path, strlen(path) + 1);
    
    replace_slash(pmap);
    
    char * rname = vpath(pmap);
    printf("rname %s %X\n",rname,0);
    
    if (excl) {
        if (access( rname, F_OK ) != -1) {
            return NULL;
        }
    }
    
    stat(rname,st);    

    return fopen(rname, mode);    
    
}

tvfshandle * fs_open_bpath(char * path, char * mode, int excl) {
    //char pmap[1024];
    
//    printf("bpath %s %s\n",path,mode);
    tvfshandle * f = vfmap->virtual_handle(path);
    if (!f) {
        if (!strcmp(mode,"rb")) {
            return NULL;
        }
        if (!strcmp(mode,"wb")) {
            f = vfmap->createNewFile(path);
        }
    }
    
    
    vfmap->get_phys_handle(f,mode);
    return f;
//    memcpy(pmap, global_config_data.base_path, global_config_data.base_path_sz);
//    memcpy(pmap + global_config_data.base_path_sz, path, strlen(path) + 1);
//    replace_slash(pmap);
//
//    char * rname = vpath(pmap);
//
//    if (excl) {
//        if (access( rname, F_OK ) != -1) {
//            return NULL;
//        }
//    }
//    return fopen(rname, mode);
}

FILE * fs_open_base(char * path, char * mode) {
    char pmap[1024];
    
    //sprintf(pmap,"%s/%s",global_config_data.emu_base_dir,path);
    memcpy(pmap, global_config_data.emu_base_dir, global_config_data.emu_base_dir_sz);
    memcpy(pmap + global_config_data.emu_base_dir_sz, path, strlen(path) + 1);
    
    return fopen(pmap, mode);
}



void init_fs() {
    vfmap = new VirtualFileMap ();
    vpaths = c_hash_create(16,16,0);
    vpaths->cmp_func = str_cmp;
}
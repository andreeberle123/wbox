
#ifndef EXEC_H
#define EXEC_H
#include "common.h"
#include "peloader.h"
#include "ne.h"

int load_exec(pe_info * info);
int load_exec_ne(ne_file_info * info);
int loader_init();
pe_info * lload(char * libname);
pe_info * lload2(pe_info * info);

#endif /* EXEC_H */


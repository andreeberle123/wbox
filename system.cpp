#include "common.h"

LocalRegistry * localRegistry;

void init_winm_system() {
    init_dx_subsystem();
    localRegistry = new LocalRegistry();
    init_keys();
    
    log_info("WINM subsystem initialized");
}
#version 330

uniform usampler2D image_tex;
uniform sampler2D color_mapx;
uniform int color_sample;

out vec4 fragColor;
 
in vec2 texCoords;
 
void main() {

    uint value = texture(image_tex, texCoords).r;
    vec4 color = texelFetch(color_mapx,ivec2(int(value),color_sample), 0);

    fragColor = color;
    
}
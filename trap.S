[BITS 32]

extern __isyscall_trap

global __syscall_trap
__syscall_trap:
;pop edx
pop ecx
pop edx
push eax
;push esp
push edx
push ecx
call __isyscall_trap
pop ecx
pop edx
;pop ecx
add esp,4

;add esp,eax
push edx
push ecx
retn

extern global_16_route

;general 16->32 bits gateway
global __32gateway
extern tmpdata1

extern valid_cs
extern valid_ds
extern valid_es
extern valid_fs
extern valid_gs
extern valid_ss

extern aux_asm_addr
extern aux_asm_seg
extern aux_asm_stack
extern pstack
extern esp_guard
extern ss_guard

;; MM_DS_BASE 0xFD00
extern global_ds_base
__setupstack:
mov [ss_guard],ss
mov ss,eax
mov [esp_guard],esp
add dword [esp_guard],4
mov edi,pstack
add edi,(8192*2)-1024
mov eax,esp
add eax, [global_ds_base]
mov esi,eax
mov ecx,1024
rep movsb

mov esp,pstack
add esp,(8192*2)-1024
ret

__32gateway:
pop edx
cmp dx,0x12 ; dos syscall, eax has the arg
jz __dossyscall
; adjust stack ret
pop ecx
mov eax,ecx
and eax,0xFFFF
shr ecx,16
push ecx
push eax

; after this point stack top is ready for retf
cmp edx,0xA0
jz __inittask16

; now save the remaining
push ebx
push edi
push esi



push ds
mov [0xFD00+0x20],es ; global_es
push dword [0xFD00+0x8] ; valid_es
mov dword [0xFD00+0x30],eax ; ret addr
mov dword [0xFD00+0x34],ecx ; ret segment

mov ds,[0xFD00+0x4] ; valid_ds


pop eax ; valid_es
mov es,eax

call __setupstack

push edx
call global_16_route
;add esp,4
mov edx,eax
shr edx,16
mov ecx,[ss_guard]
mov ss,ecx
mov esp,[esp_guard]

mov ecx,[aux_asm_stack]
pop ds

pop esi
pop edi
pop ebx

add esp, ecx
mov ecx,[0xFD00+0x20]
mov es,ecx

;retf
jmp far [0xFD00+0x30] ; ret addr


;;;;;;;;;;;;;;;;;;;;;;;;;;
extern dos3call16

global __dossyscall
__dossyscall:
pop ecx
mov [esp-8],ecx
and dword [esp-8],0xFFFF
shr ecx,16
push ecx
mov ecx, [esp-4]
push dword ecx
cmp ah,0x25 ; interupt set
jz __dossyscall25

push ds
push eax
mov [0xFD00+0x20],es ; global_es
mov eax,[0xFD00+0x8]
mov es,eax
mov ds, [0xFD00+0x4]

call __setupstack
call dos3call16

mov eax,[ss_guard]
mov ss,eax
mov esp,[esp_guard]
add esp,4 ; clear args
pop ds
push eax
mov eax,[0xFD00+0x20]
mov es,eax
pop eax
retf

__dossyscall25:

retf

extern global_es
;di si es:bx dx

global __inittask16
__inittask16:
mov cx,0xEFFF
mov di,0x00
mov si,0
mov eax,[0xFD00+0x20]
mov es,eax
mov bx,0
mov dx,5
mov eax,1
retf

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 

extern exit
extern ds_base
extern err_msg
extern printf

error:
push eax
push dword [err_msg]
call printf
call exit

remap:
push eax
push ecx
mov eax,edx
shr eax,16
cmp eax,0x33F
jz remap_cont
;cmp eax,0 ; if the segment is zero we let the function handle it
;jnz error ; if not, its an error..
pop ecx
pop eax
ret

remap_cont:
mov ecx,[ds_base]
mov eax,edx
and eax,0xFFFF
add ecx,eax
mov edx,ecx
pop ecx
pop eax
ret

global def_route_16
; dword def_route_16 (dword target, dword stentry, int count, char * mask, dword st_size)
def_route_16:
push esi
push edi
push ebx
push ebp
mov ebp, [esp+0x24]
mov eax, [esp+0x20]
mov ecx, [esp+0x1C]
mov esi, [esp+0x18] ; stentry
lea ebx, [esp+0x14]

xor edi,edi
xor edx,edx

;add esi,ebp

lstart:
cmp ecx,0
jz lend

mov edx,[eax+ecx-1]
cmp dl,0x32
jz _2arg
cmp dl,0x34
jz _4arg
jmp loopcont
_2arg:

lea edx,[esi]
add esi,0x2
mov edx,[edx]
and edx,0xFFFF
push dword edx
jmp loopcont
_4arg:
lea edx,[esi]
add esi,0x4
mov edx,[edx]
call remap
push edx
jmp loopcont
loopcont:
;inc eax
dec ecx
jmp lstart

lend:

call [ebx]
pop ebp
pop ebx
pop edi
pop esi
ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

extern ds_global_entry

global __16_gate
__16_gate:
push esi
push edi
push ebx
mov eax,[esp+0x10]
mov ecx,[ds_global_entry]
mov ss,[ecx+0x2c]
mov es,[ecx+0x1c]
mov ecx,[ecx+0x1c]
mov ds,ecx

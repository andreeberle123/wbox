#include "common.h"
#include "winm.h"
#include "gl.h"
#include "sink.h"
#include "xw.h"
#include <pthread.h>

/*
 * Window Messages
 */

extern "C" {
    
#define WPROC(iw,msg,wparam,lparam) \
    iw->wclass->lpfnWndProc(iw->hwnd,msg,(WPARAM)(wparam),(LPARAM)(lparam));

int do_create_window(innerwin * iw);

static dword lfind_class(LPCTSTR lpClassName);

__thread innerwin * local_hwnd = NULL;

//static wmsg_queue * wmsg_queue_h = NULL;
//static wmsg_queue * wmsg_queue_t = NULL;

//static pthread_mutex_t wmsg_queue_mutex = PTHREAD_MUTEX_INITIALIZER;
//static pthread_cond_t wmsg_queue_cond = PTHREAD_COND_INITIALIZER;

void push_queue_msg(UINT msg, WPARAM wparam, LPARAM lparam, int processable, innerwin * iw) {
    wmsg_queue * t = (wmsg_queue*)malloc(sizeof(wmsg_queue));
    
    t->next = NULL;
    t->msg.message = msg;
    t->msg.lParam = lparam;
    t->msg.wParam = wparam;
    t->processable = processable;
    struct timespec tx;
    clock_gettime(CLOCK_MONOTONIC, &tx);
    t->time = tx;
    unsigned long millis = tx.tv_nsec;
    millis /= 1000000;
    t->msg.time = tx.tv_sec*1000+millis;
    t->iw = iw;
    LOCK_MUTEX(iw->wmsg_queue_mutex);
    
    if (iw->wmsg_queue_t) {
        iw->wmsg_queue_t->next = t;
    }
    else {
        iw->wmsg_queue_h = t;
    }
    iw->wmsg_queue_t = t;
    SIGNAL(iw->wmsg_queue_cond);
    UNLOCK_MUTEX(iw->wmsg_queue_mutex);
}

void time_diff(struct timespec *then, struct timespec *now, struct timespec *diff) {
    diff->tv_sec = now->tv_sec - then->tv_sec;
    if (now->tv_nsec < then->tv_nsec) {
        diff->tv_nsec = 1000000000 + now->tv_nsec;
        diff->tv_nsec -= then->tv_nsec;
        diff->tv_sec--;
    }
    else {
        diff->tv_nsec = now->tv_nsec - then->tv_nsec;
    }
    
}

#define MSG_MAX_WAIT 20000000

int process_msg_queue(MSG * msg, int excl, void * win) { 

    innerwin * iw = (innerwin *)win;
    if (!iw) {
        iw = local_hwnd;
    }
    for(;;) {
        wmsg_queue * t = NULL;

        LOCK_MUTEX(iw->wmsg_queue_mutex);
        if (iw->wmsg_queue_h) {
            t = iw->wmsg_queue_h;
            if (excl) {
                iw->wmsg_queue_h = iw->wmsg_queue_h->next;
                if (!iw->wmsg_queue_h) {
                    iw->wmsg_queue_t = NULL;
                }
            }
        }
        UNLOCK_MUTEX(iw->wmsg_queue_mutex);
        struct timespec now;
        clock_gettime(CLOCK_MONOTONIC, &now);
        if (t) {
            if (t->processable) {
                if (t->msg.message == WM_MOUSEMOVE) {
                    struct timespec age;
                    time_diff(&t->time,&now,&age);
                    if (age.tv_sec > 0 || age.tv_nsec > MSG_MAX_WAIT) {
                        continue;
                    }
                }       
//                if (t->msg.message == 0x200) {
//                    //printf("Dispatching processable %X %i %i\n",t->msg.message,
//                    //        (int)t->msg.lParam&0xFF,((int)t->msg.lParam>>16)&0xFF);
//                }
//                else {
//                    printf("Dispatching processable %X\n",t->msg.message);
//                }
                *msg = t->msg;
                free(t);
                return 1;
            }
            else {
                printf("Dispatching message %X\n",t->msg.message);
                LRESULT result = WPROC(t->iw,t->msg.message,t->msg.wParam,t->msg.lParam);
                
                printf("result %i\n",result);
                free(t);
                continue;
            }
        }
        return 0;
    }
    
}

HCURSOR cursors[32];

HCURSOR WINAPI LoadCursorA(
    HINSTANCE hInstance,
    LPCTSTR   lpCursorName
    ) {
    
    if (hInstance) {
        printf("Unsupported LoadCursor\n");
        exit(0);
    }
    if ((dword)lpCursorName & 0xFFFF0000) {
        printf("cursor\n");
    }
    else {
        return cursors+0;
    }
    
    exit(0);
}

LRESULT WINAPI DefWindowProcA(
    HWND   hWnd,
    UINT   Msg,
    WPARAM wParam,
    LPARAM lParam
    ) {
   // printf("default processor %X\n",Msg);
    return 0;
}


HICON WINAPI LoadIconA(
    HINSTANCE hInstance,
    LPCTSTR   lpIconName
    ) {
    printf("icon %X %X\n",hInstance,lpIconName);
    return NULL;
}



#define CS_VREDRAW          0x0001
#define CS_HREDRAW          0x0002
#define CS_DBLCLKS          0x0008
#define CS_OWNDC            0x0020
#define CS_CLASSDC          0x0040
#define CS_PARENTDC         0x0080
#define CS_NOCLOSE          0x0200
#define CS_SAVEBITS         0x0800
#define CS_BYTEALIGNCLIENT  0x1000
#define CS_BYTEALIGNWINDOW  0x2000
#define CS_GLOBALCLASS      0x4000

#define CS_IME              0x00010000



static WNDCLASS win_classes[32];
static int win_classes_sz = 1;
static innerwin windows_[32];
static int windows_sz = 0;



ATOM WINAPI RegisterClassA(
    const WNDCLASS *lpWndClass
) {
    win_classes[win_classes_sz] = *lpWndClass;
    win_classes[win_classes_sz].reserved = 0;
    if (lpWndClass->cbClsExtra || lpWndClass->cbWndExtra) {
        printf("Unsupported RegisterClass");
        exit(0);
    }
    
    
    return win_classes_sz++;
}

void _setclass16(int idx) {
    win_classes[idx].reserved = 1;
}

#define WS_EX_TOPMOST 0x8

HWND WINAPI CreateWindowExA(
    DWORD     dwExStyle,
    LPCTSTR   lpClassName,
    LPCTSTR   lpWindowName,
    DWORD     dwStyle,
    int       x,
    int       y,
    int       nWidth,
    int       nHeight,
    HWND      hWndParent,
    HMENU     hMenu,
    HINSTANCE hInstance,
    LPVOID    lpParam
    ) {
    
    
    // find the class
    dword cidx = lfind_class(lpClassName);
    if (!cidx) {
        printf("Error creating window - invalid class\n");
        exit(0);
    }
    
    int ix = windows_sz++;
    
    innerwin* iw = &windows_[ix];
    local_hwnd = iw;
    memset(iw,0,sizeof(innerwin));
    iw->wclass = &win_classes[cidx];
    iw->x = x;
    iw->y = y;
    iw->h = nHeight;
    iw->w = nWidth;
    iw->lparam = lpParam;
    iw->hwnd = (void*)(windows_+ix);
    strcpy(iw->wname,lpWindowName);
    
    pthread_mutex_init(&iw->wmsg_queue_mutex,0);
    pthread_cond_init(&iw->wmsg_queue_cond,0);
    printf("coords %i %i %i %i\n",iw->x,iw->y,iw->h,iw->w);
    
    printf("create win at %X\n",windows_+ix);
    
    int ret = do_create_window(iw);
    if (ret < 0) {
        return NULL;
    }
    return windows_+ix;
    
}

//========================

void wwm_create(innerwin *iw, int porigin, CREATESTRUCT * cst) {
    if (porigin == 2) {
        printf("Unsupported create\n");
        exit(0);
    }
    
    cst->x = iw->x;
    cst->y = iw->y;
    cst->cx = iw->w;
    cst->cy = iw->h;   
    cst->lpCreateParams = iw->lparam;
    cst->lpszName = iw->wclass->lpszClassName;
    cst->lpszName = iw->wname;
    
    cst->hInstance = (HINSTANCE)1;

}



void fill_wpos(WINDOWPOS *wp,innerwin * iw) {
    WINDOWPOS * nig = (WINDOWPOS*)0;
    wp->cx = 0;
    wp->cx = iw->w;    
    wp->cy = iw->h;
    wp->x = iw->x;
    wp->y = iw->y;
}

#ifdef BITS16_SUPPORT

typedef struct lcall {
    dword addr;
    word seg;
} __attribute__ ((packed)) lcall;

LRESULT lproc_gate(innerwin *iw, UINT msg,WPARAM wparam,LPARAM lparam) {
    if (iw->wclass->reserved) {
        lcall ll;
        ll.seg = (dword)iw->wclass->lpfnWndProc>>16;
        ll.addr = (dword)iw->wclass->lpfnWndProc&0xFFFF;
        __asm volatile(
            "pushl %0;"
            "pushw %1;"
            "pushw %2;"
            "pushw %3;"
            "movl %4,%%eax;"
            "lcall *(%%eax);"
        ::"r"(lparam),"r"(wparam),"r"(msg),"r"(iw->hwnd),"r"(&ll)
        : "eax"
        );
    }
    else {
        return WPROC(iw,msg,wparam,lparam);
    }
}
#endif

int do_create_window(innerwin * iw) {
    LRESULT lresult;
    CREATESTRUCT cst;
    struct tagWINDOWPOS wp;
    
    wwm_create(iw,0,&cst);
#ifdef BITS16_SUPPORT    
    lresult = lproc_gate(iw,WM_CREATE,NULL,(LPARAM)&cst);
#else
    lresult = WPROC(iw,WM_CREATE,NULL,&cst);
#endif
    if (lresult < 0) {
        printf("Window creation aborted by user request\n");
        return -1;
    }
    
    lresult = WPROC(iw,WM_SHOWWINDOW,1,0);
    
    fill_wpos(&wp,iw);
    
    wp.hwnd = iw->hwnd;
    wp.flags = SWP_NOMOVE | SWP_NOSIZE;
    
    create_window(iw->x,iw->y,iw->w,iw->h,iw);
    printf("window created\n");
    lresult = WPROC(iw,WM_ACTIVATEAPP, (WPARAM) 1, (LPARAM)1);
    
    // 'resized' event
    lresult = WPROC(iw,WM_SIZE,0,((iw->h<<16) + iw->w));
    
    return 0;
}

void w_expose_event(void * arg) {
    push_queue_msg(WM_PAINT, NULL,NULL, 0,(innerwin*)arg);
}

void w_focus_in(void * arg) {
    push_queue_msg(WM_ACTIVATEAPP, (WPARAM) 1, (LPARAM)1, 0,(innerwin*)arg);
    push_queue_msg(WM_PAINT, NULL,NULL, 0,(innerwin*)arg);
}

int wmm_set_desired_res(int w, int h, HWND wd) {
    innerwin * iw = (innerwin*)wd;
    iw->desired_h = h;
    iw->desired_w = w;
}

int wmm_resize_window(int w, int h) {
    xc_resize_window(w,h);
}

int wmm_dimension(ImageSink * sink, HWND hwnd) {
    innerwin * iw = (innerwin*)hwnd;
    sink->setDimensions(iw->desired_w,iw->desired_h);
}

void w_mouse_move(int x, int y, int rx, int ry, void * arg) {
    int mods = 0; // pressed stuff
    push_queue_msg(WM_MOUSEMOVE, (WPARAM)mods,(LPARAM)((y<<16)+x), 1,(innerwin*)arg);
}



int ascii_to_scan_table[] = {0,0,0,0,0,0,0,0,14,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        ,0,0,0,0,0,0,0,0,0,40,0,0,0,0,40,0,0,0,13,51,12,52,53,11,2,3,4,5,6,7,8,9,
        10,39,39,51,13,52,53,0,30,48,46,32,18,33,34,35,23,36,37,38,50,49,24,25,16,19,
        31,20,22,47,17,45,21,44,26,43,27,0,12,41,30,48,46,32,18,33,34,35,23,36,37,38,
        50,49,24,25,16,19,31,20,22,47,17,45,21,44,26,43,27,41,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


int key_to_scan(int key) {
    return ascii_to_scan_table[key];
}

int xkey_to_scan(int key, int * exten) {
    if (key >= 0x43 && key <= 0x4c) {
        //F1-F10
        return key-8;
    }
    switch (key) {
        case 0x6c: //ralt
        case 0x40: // lalt
            return 0x38;
        case 0x32: // lshift
            return 0x2a;
        case 0x3e: // rshift
            return 0x36;
        case 0x25: // lctrl
        case 0x69: // rctrl
            return 0x1d;
        case 0x6F: // up
            *exten = 1;
            return 0x48;
        case 0x71: // left
            *exten = 1;
            return 0x4b;
        case 0x74: //down
            *exten = 1;
            return 0x50;
        case 0x72: //right
            *exten = 1;
            return 0x4d;
        default:
            return 0;
    }
}


void w_key_event(int key, int ascii, int state, int x, int y, int mods, void * arg) {
    DWORD scan = 0;
    int exten = 0;
    
    if (key == 0x20) {
        scan = 0x39;
    }
    else {
        if (ascii) {
            scan = key_to_scan(key);
        }
        else {
            scan = xkey_to_scan(key,&exten);

        }
    }
    if (!scan) {
        printf("could not convert to scan\n");
    }
    if (scan == 0x50) key = 0x28;
    
    if (state == STATE_DOWN) {
        DWORD lparam = 1;
        lparam |= (scan<<16);
        lparam |= (exten<<24);
        innerwin * iw = (innerwin *)arg;
        int ex = 0;
        LOCK_MUTEX(iw->wmsg_queue_mutex);
        if (iw->wmsg_queue_t && iw->wmsg_queue_t->msg.message == WM_KEYDOWN &&
                iw->wmsg_queue_t->msg.wParam == (WPARAM)key) {
            // increase frequency
            lparam = (DWORD)iw->wmsg_queue_t->msg.lParam;
            lparam++;
            iw->wmsg_queue_t->msg.lParam = (LPARAM)lparam;
            ex = 1;
        }
        UNLOCK_MUTEX(iw->wmsg_queue_mutex);
        
        if (!ex) {
            //lparam |= 0x100000;
            printf("key %X %i %X\n",key,ascii,scan);
            
            push_queue_msg(WM_KEYDOWN,(WPARAM)key,(LPARAM)lparam,1,(innerwin*)arg);
        }
    }
    else {
        DWORD lparam = 1+(1<<30);
        lparam |= (scan<<16);
        lparam |= (exten<<24);
        push_queue_msg(WM_KEYUP,(WPARAM)key,(LPARAM)lparam,1,(innerwin*)arg);
    }
}

void w_mouse_button(int b, int state, int x, int y, int rx, int ry, void * arg) {
    int mods = 0;
    if (state == STATE_DOWN) {
        //printf("mouse down event\n");
        if (b == 1) {
            mods |= MK_LBUTTON;
            push_queue_msg(WM_LBUTTONDOWN, (WPARAM)mods,(LPARAM)((y<<16)+x), 1,(innerwin*)arg);
        }
        else if (b == 3) {
            mods |= MK_RBUTTON;
            push_queue_msg(WM_RBUTTONDOWN, (WPARAM)mods,(LPARAM)((y<<16)+x), 1,(innerwin*)arg);
        }
            
    }
    else {
       // printf("mouse up event\n");
        if (b == 1) {
            push_queue_msg(WM_LBUTTONUP, (WPARAM)mods,(LPARAM)((y<<16)+x), 1,(innerwin*)arg);
        }
        else if (b == 3) {
            push_queue_msg(WM_RBUTTONUP, (WPARAM)mods,(LPARAM)((y<<16)+x), 1,(innerwin*)arg);
        }
    }
    
}

static dword lfind_class(LPCTSTR lpClassName) {
    dword cidx = 0;
    if ((dword)lpClassName & 0xFFFF0000) {
        int i;
        for(i=1;i<win_classes_sz;i++) {
            
            if (!strcmp(lpClassName,win_classes[i].lpszClassName)) {
                cidx = i;
                break;
            }
        }
        if (!cidx) {
            printf("Class not found %s\n",lpClassName);
            return 0;    
        }
        
    }
    else {
        cidx = (dword)lpClassName;
    }
    return cidx;
}


void process_messages() {
    
}

BOOL WINAPI GetCursorPos(
    LPPOINT lpPoint
) {
    int rx,ry,wx,wy;
    int rt = xc_get_mouse_pos(&rx,&ry,&wx,&wy);
    if (!rt) {
        return 0;
    }
    lpPoint->x = rx;
    lpPoint->y = ry;
    return 1;
}

BOOL WINAPI ScreenToClient(
    HWND    hWnd,
    LPPOINT lpPoint
) {
    int x,y;
    
    int rt = xc_root_to_window(lpPoint->x,lpPoint->y,&x,&y);
    
    if (!rt) {
        return 0;
    }
    
    lpPoint->x = x;
    lpPoint->y = y;
 //   printf("end %i %i\n",x,y);
    
    return 1;
}

BOOL WINAPI GetUpdateRect(
  HWND   hWnd,
  LPRECT lpRect,
  BOOL   bErase
) {
    innerwin * iw = (innerwin*)hWnd;
    lpRect->left = iw->x;
    lpRect->top = iw->y;
    lpRect->right = iw->w;
    lpRect->bottom = iw->h;
    
    return 1;
}

BOOL WINAPI ValidateRect(
  HWND       hWnd,
  RECT *lpRect
) {
    return 1;
}
HWND WINAPI SetCapture(
  HWND hWnd
) {
    return hWnd;
}

BOOL WINAPI ReleaseCapture(

) {
    return 1;
}

}

#include "common.h"
#include <unistd.h>
#include <stdio.h>

int get_current_res(int * w, int * h, char * output) {
    FILE *fp;
    char data[256];

    /* Open the command for reading. */
    fp = popen("xrandr", "r");
    if (fp == NULL) {
        printf("Failed xrandr\n" );
        exit(0);
    }

    
    while (fgets(data, 255, fp) != NULL) {
        if (!strncmp(data,"Screen ",7)) {
            char * t = data;
            while (*t) {
                if (!strncmp(t, "current ",8)) {
                    t+= 8;
                    char * v = t;
                    while (*v && *v != ' ') v++;
                    *v = 0;
                    int r1 = atoi(t);
                    
                    t = v+1;
                    
                    if (*t != 'x') {
                        return 0;
                    }
                    t+= 2;
                    v = t;
                    while (*v && *v != ' ') v++;
                    *v = 0;
                    int r2 = atoi(t);
                    //printf("%i %i\n",r1,r2);
                    *w = r1;
                    *h = r2;
                    if (!fgets(data, 255, fp)) return 0;
                    v = data;
                    while (*v && *v != ' ') {
                        v++;
                    }
                    *v = 0;
                    strcpy(output,data);
                    return 1;
                    
                }
                t++;
            }
            //printf("%s", data);
            break;
        }
    }
    
    

    
    pclose(fp);
    return 0;
}


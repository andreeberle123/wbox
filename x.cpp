#include "common.h"
#include "xw.h"

#include <xcb/xcb.h>
#include <xcb/randr.h>
#include <xcb/xcb_cursor.h>
#include <X11/Xlib-xcb.h> 
#include <xcb/xproto.h>
#include <xcb/xcb_icccm.h>

xcb_connection_t *connection;
xcb_screen_t *screen;
xcb_window_t window;
GLXFBConfig fb_config;
xcb_window_t      root_window = { 0 };
static Display * display = NULL;
extern "C" {
void w_expose_event(void * arg);
void w_focus_in(void * arg);
}
 
xcb_atom_t atoms[4];

xcb_atom_t *x_get_atom_value(xcb_atom_t atom) {
    xcb_get_property_cookie_t ck =  xcb_get_property(connection, false, window, atom, XCB_ATOM_ATOM, 0, 32);
    xcb_generic_error_t*         err;
    xcb_get_property_reply_t * reply = xcb_get_property_reply(connection, ck, &err);
    
    
    xcb_atom_t*  value = (xcb_atom_t*)(xcb_get_property_value(reply));
    return value;
}

static int x_key_table[] = {
    0,0,0,0,0,0,0,0,0,0,49,50,51,52,53,54,55,56,57,48,45,61,0,0,113,119,101,114,116,121,117,105,111,112,32,91,0,0,97,115,100,102,103,104,106,107,108,63,32,0,0,93,122,120,99,118,98,110,109,44,46,59,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,47,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

int xkey_to_ascii(int key, int mod) {
    return x_key_table[key];
}

void * xevent_cycle(void * arg) {
    char buf[16];
    
    while(1) {
        xcb_generic_event_t * t  = xcb_wait_for_event (connection);

        if (!t) {
            exit(0);
        }
            
        byte event = t->response_type & ~0x80;
        
        //printf("event %i\n",t->response_type & ~0x80);
        switch (event) {
            case XCB_FOCUS_IN: {
                printf("focus in\n");
                w_focus_in(arg);
                break;
            }                
            case XCB_CREATE_NOTIFY: {
                xcb_create_notify_event_t * xcn = (xcb_create_notify_event_t*)t;
                printf("Created %i %i\n",xcn->x,xcn->y);
                break;
            }
            case XCB_MOTION_NOTIFY: {
                xcb_motion_notify_event_t * xmn = (xcb_motion_notify_event_t*)t;
                int x = (xmn->event_x*640)/1440;
                int y = (xmn->event_y*480)/810;
                w_mouse_move(x,y,xmn->root_x,xmn->root_y,arg);
                //printf("xmn %i %i %i %i %i\n",xmn->detail,xmn->event_x,xmn->event_y, xmn->root_x,xmn->root_y);
                break;
            }
            case XCB_EXPOSE: {
                xcb_expose_event_t *expose = (xcb_expose_event_t *)event;
                printf("Expose event %X\n",arg);
                
                w_expose_event(arg);
                
                //exit(0);
                break;
            }
            case XCB_BUTTON_RELEASE: {
                xcb_button_release_event_t * release = (xcb_button_release_event_t*)t;
                
                w_mouse_button(release->detail,2,release->event_x,release->event_y,release->root_x,release->root_y,arg);
                //printf("Button event release %i %i %i\n",release->event_x,release->event_y,release->detail);
                break;
            }
            case XCB_BUTTON_PRESS: {
                xcb_button_press_event_t *press = (xcb_button_press_event_t *)t;
                w_mouse_button(press->detail,1,press->event_x,press->event_y,press->root_x,press->root_y,arg);
               // printf("Button event press %i %i %i\n",press->event_x,press->event_y,press->detail);
                break;
            }
            case XCB_REPARENT_NOTIFY: {
                struct xcb_reparent_notify_event_t * xrn = (xcb_reparent_notify_event_t*)t;
                printf("Reparent notify \n");
                
                break;
            }
            case XCB_CLIENT_MESSAGE : {
                struct xcb_client_message_event_t *  xcm = (struct xcb_client_message_event_t*)t;
                printf("Client message %X\n",xcm->format);
                break;
            }
            case XCB_PROPERTY_NOTIFY: {
                struct xcb_property_notify_event_t * xpn = (struct xcb_property_notify_event_t *)t;
                if (xpn->atom == atoms[0]) {
                    xcb_atom_t * val = x_get_atom_value(atoms[0]);
                   // printf("%X\n",*val);
                }
             //   printf("Property notify %X %X\n",xpn->state,xpn->atom);
                break;
            }
            
            case XCB_KEY_PRESS: {
                xcb_key_press_event_t * kevent = (xcb_key_press_event_t*)t;
                printf("%i %i %i %X\n",kevent->event_x,kevent->event_y,kevent->detail,kevent->state);

                int key = xkey_to_ascii(kevent->detail,kevent->state);
               // printf("akey %s\n",buf);
                w_key_event(key ? key : kevent->detail, key, STATE_DOWN, kevent->event_x,kevent->event_y, kevent->state,arg);
                break;
            }
            case XCB_KEY_RELEASE: {
                xcb_key_release_event_t * kevent = (xcb_key_release_event_t*)t;
                int key = xkey_to_ascii(kevent->detail,kevent->state);
                
                w_key_event(key ? key : kevent->detail, key, STATE_UP, kevent->event_x,kevent->event_y, kevent->state,arg);
                break;
            }
            default:
              //  printf("other event %i\n",event);
                ;
        }
    }
}
        
static char * x_names[] = {    
    "_NET_WM_STATE",
    "_NET_WM_STATE_HIDDEN",
    "_NET_WM_STATE_MAXIMIZED_VERT",
    "_NET_WM_STATE_MAXIMIZED_HORZ"
};

static int visual_attribs[] = {
    GLX_X_RENDERABLE, True,
    GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
    GLX_RENDER_TYPE, GLX_RGBA_BIT,
    GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
    GLX_RED_SIZE, 8,
    GLX_GREEN_SIZE, 8,
    GLX_BLUE_SIZE, 8,
    GLX_ALPHA_SIZE, 8,
    GLX_DEPTH_SIZE, 24,
    GLX_STENCIL_SIZE, 8,
    GLX_DOUBLEBUFFER, True,
    //GLX_SAMPLE_BUFFERS  , 1,
    //GLX_SAMPLES         , 4,
    None
};

int xerrorhandler(Display *dsp, XErrorEvent *error) {
    char errorstring[128];
    XGetErrorText(dsp, error->error_code, errorstring, 128);

    printf("ack!fatal: X error-- %s\n",  errorstring);
    exit(-1);
}


void xgl() {
    gl_from_x(display,window,fb_config);
}

static void setBlankCursor() {
    xcb_cursor_t cursor = xcb_generate_id(connection);
    xcb_pixmap_t pix = xcb_generate_id(connection);

    xcb_create_pixmap(connection, 1, pix, screen->root, 1, 1);
    xcb_create_cursor(connection, cursor, pix, pix, 0, 0, 0, 0, 0, 0, 1, 1);
    
    xcb_change_window_attributes(connection,window, XCB_CW_CURSOR,&cursor);
}

static Cursor bcurs(void) {
    static char data[1] = {0};
    Cursor cursor;
    Pixmap blank;
    XColor dummy;

    blank = XCreateBitmapFromData(display, window,  data, 1, 1);
    printf("blank %X\n",blank);
    cursor = XCreatePixmapCursor(display, blank, blank, &dummy, &dummy, 0, 0);
    XFreePixmap(display, blank);

    return cursor;
}

#define MWM_HINTS_DECORATIONS   (1L << 1)



void create_window(int x, int y, int w, int h, void * arg) {

   // XInitThreads();
  //  XSetErrorHandler(xerrorhandler);
    display = XOpenDisplay(NULL);
 
    printf("X window create\n");
    
    connection = XGetXCBConnection(display);
    
    screen = xcb_setup_roots_iterator(xcb_get_setup(connection)).data;
    root_window = screen->root;
    uint32_t mask;
    uint32_t values[3];
    
    xcb_void_cookie_t cookie;

    //mask =  XCB_CW_EVENT_MASK | XCB_CW_COLORMAP ;
    mask =  XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    //values[0] = None;
    //values[1] = XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_KEY_PRESS;
    values[1] =  XCB_EVENT_MASK_KEY_PRESS  | XCB_EVENT_MASK_BUTTON_RELEASE;
    //values[1] &= (~XCB_EVENT_MASK_RESIZE_REDIRECT);
    values[1] |= XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY;
    values[1] |= XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_FOCUS_CHANGE;
    values[1] |= XCB_EVENT_MASK_KEY_RELEASE | XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_PROPERTY_CHANGE | XCB_EVENT_MASK_POINTER_MOTION;
        printf("mask %X\n",values[1]); 
    //values[1] = 0;
    //values[1] = XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_POINTER_MOTION | XCB_EVENT_MASK_VISIBILITY_CHANGE |
      //      XCB_EVENT_MASK_RESIZE_REDIRECT | XCB_EVENT_MASK_OWNER_GRAB_BUTTON | XCB_EVENT_MASK_STRUCTURE_NOTIFY
        //    | XCB_EVENT_MASK_BUTTON_1_MOTION | XCB_EVENT_MASK_BUTTON_2_MOTION | XCB_EVENT_MASK_BUTTON_3_MOTION
          //  | XCB_EVENT_MASK_BUTTON_4_MOTION | XCB_EVENT_MASK_BUTTON_5_MOTION | XCB_EVENT_MASK_BUTTON_MOTION;

    
    int visualID = 0;
    
    
    //glXGetFBConfigAttrib(display, fb_config, GLX_VISUAL_ID , &visualID);
    //xcb_colormap_t colormap = xcb_generate_id(connection);

    window = xcb_generate_id(connection);
    //values[1] = colormap;
    values[0] = screen->white_pixel;
    /* Create colormap */
  //  xcb_create_colormap(connection,XCB_COLORMAP_ALLOC_NONE,colormap,screen->root,visualID);
    

    cookie = xcb_create_window(connection,
            XCB_COPY_FROM_PARENT, window, screen->root,
            x, y, w, h,
            0,
            XCB_WINDOW_CLASS_INPUT_OUTPUT,
            screen->root_visual,
            mask, values);

    
    unsigned long mhints[3];
    mhints[0] = MWM_HINTS_DECORATIONS;
    mhints[2] = 0;
    
    xcb_intern_atom_cookie_t cookie3 = xcb_intern_atom(connection, 0, strlen("_MOTIF_WM_HINTS"),"_MOTIF_WM_HINTS");
    xcb_intern_atom_reply_t* reply = xcb_intern_atom_reply(connection, cookie3, 0);
    xcb_atom_t motif = reply->atom;
    printf("motif %X\n",motif);
    
//    xcb_change_property(connection,XCB_PROP_MODE_REPLACE,window,motif,motif,32,4,mhints);
    
   // xc_fullscreen();
    xcb_intern_atom_cookie_t cookiea = xcb_intern_atom(connection, 0, 
            strlen("_NET_WM_STATE_FULLSCREEN"),"_NET_WM_STATE_FULLSCREEN");
    xcb_intern_atom_reply_t* at = xcb_intern_atom_reply(connection, cookiea, 0);
    xcb_atom_t wmfull = at->atom;
    
      
    cookiea = xcb_intern_atom(connection, 0, strlen("_NET_WM_STATE"),"_NET_WM_STATE");    
    xcb_intern_atom_reply_t *at2 = xcb_intern_atom_reply(connection, cookiea, 0);
    
    xcb_atom_t netwm = at2->atom;
    printf("at %i %i\n",wmfull,netwm);
    xcb_change_property(connection,XCB_PROP_MODE_REPLACE,window,netwm,XCB_ATOM_ATOM,32,1,&at->atom);   
    
    xcb_map_window(connection, window);
    
    xcb_flush(connection);
        
    pthread_t t;    
    
//    Cursor c = bcurs();
//    XDefineCursor(display,window,c);    
    
    setBlankCursor();
    xcb_flush(connection);
    
    gl_from_x(display,window,fb_config);

    pthread_create(&t,0,xevent_cycle,arg);
    
    w_expose_event(arg);

//    xcb_cursor_context_t *ctx;
//    if (xcb_cursor_context_new(connection, screen, &ctx) >= 0) {
//        xcb_cursor_t cursor = xcb_cursor_load_cursor(ctx, "pointing_hand");
//        if (cursor == XCB_CURSOR_NONE) // they come with various names ...
//           cursor = xcb_cursor_load_cursor(ctx, "hand2");
//        if (cursor == XCB_CURSOR_NONE)
//           cursor = xcb_cursor_load_cursor(ctx, "hand");
//        if (cursor == XCB_CURSOR_NONE)
//           cursor = xcb_cursor_load_cursor(ctx, "hand1");
//        if (cursor == XCB_CURSOR_NONE)
//           cursor = xcb_cursor_load_cursor(ctx, "pointer");
//        if (cursor == XCB_CURSOR_NONE)
//           cursor = xcb_cursor_load_cursor(ctx, "e29285e634086352946a0e7090d73106");
//        if (cursor == XCB_CURSOR_NONE)
//           cursor = xcb_cursor_load_cursor(ctx, "9d800788f1b08800ae810202380a0822");
//        if (cursor != XCB_CURSOR_NONE) {
//           xcb_change_window_attributes(connection, window, XCB_CW_CURSOR, &cursor);
//        }
//        xcb_cursor_context_free(ctx);
    //}    
    
    //xc_fullscreen();
}

void xc_fullscreen() {
    
    xcb_intern_atom_cookie_t cookiea = xcb_intern_atom(connection, 0, 
            strlen("_NET_WM_STATE_FULLSCREEN"),"_NET_WM_STATE_FULLSCREEN");
    xcb_intern_atom_reply_t* at = xcb_intern_atom_reply(connection, cookiea, 0);
    xcb_atom_t wmfull = at->atom;
    
    xcb_atom_t atomsx[2] = {wmfull, None};
    
    cookiea = xcb_intern_atom(connection, 0, strlen("_NET_WM_STATE"),"_NET_WM_STATE");    
    xcb_intern_atom_reply_t *at2 = xcb_intern_atom_reply(connection, cookiea, 0);
    
    xcb_atom_t netwm = at2->atom;
    printf("at %i %i\n",wmfull,netwm);
    xcb_change_property(connection,XCB_PROP_MODE_REPLACE,window,netwm,XCB_ATOM_ATOM,32,1,&at->atom);
   
    xcb_flush(connection);
    
    xcb_randr_screen_size_t *sizes;
    int sizes_length;
    xcb_randr_get_screen_info_cookie_t screen_info;
    xcb_randr_get_screen_info_reply_t *reply;
    printf("rots %X\n",reply->nInfo);
    screen_info = xcb_randr_get_screen_info_unchecked(connection, screen->root);
    //printf("screen_info.sequence = %d\n", screen_info.sequence);
    reply = xcb_randr_get_screen_info_reply(connection, screen_info,
            NULL);
    
    sizes = xcb_randr_get_screen_info_sizes(reply);
    //printf("sizes = %p\n", sizes);
    sizes_length = xcb_randr_get_screen_info_sizes_length(reply);
    
    int i;
    printf("%i\n",reply->sizeID);
    for(i=0;i<sizes_length;i++) {
        printf("%d %d\n",sizes[i].width,sizes[i].height);
    }
    
    //static uint32_t values2[] = { 0, 0, sizes[reply->sizeID].width, sizes[reply->sizeID].height };
    static uint32_t values2[] = { 0, 0, 1920,1080 };

    xcb_configure_window (connection,window,XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT,
                            values2);
    xcb_flush(connection);
    
    xcb_randr_get_screen_resources_cookie_t xxx = xcb_randr_get_screen_resources (connection,window);
    
    xcb_randr_get_screen_resources_reply_t * r1 = 
        xcb_randr_get_screen_resources_reply (connection,xxx,0);
    xcb_randr_mode_info_t * cx = xcb_randr_get_screen_resources_modes (r1);
    int ll = xcb_randr_get_screen_resources_modes_length (r1);
    for(i=0;i<ll;i++) {
        printf("r1 %i\n",cx[i].width);
    }

                             
    exit(0);
}

/**
 * Resize only, no scaling here
 */
int xc_resize_window(int w, int h) {
    printf("resize\n");exit(0);
    uint32_t values[] = { (uint32)w, (uint32)h };
    xcb_configure_window(connection, window, XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, values);
}

void xc_test() {
    display = XOpenDisplay(NULL);
    //xcb(0);
    printf("d %X %X\n",display,window);
    gl_from_x(display,window,fb_config);
   // create_window(0,0,640,480,NULL);
}

void screen_from_Xlib_Display(Display * const display, xcb_connection_t *connection, int * const out_screen_num, xcb_screen_t * * const out_screen) {
    xcb_screen_iterator_t screen_iter = xcb_setup_roots_iterator(xcb_get_setup(connection));
    int screen_num = DefaultScreen(display);
    while (screen_iter.rem && screen_num > 0) {
        xcb_screen_next(&screen_iter);
        --screen_num;
    }
    *out_screen_num = screen_num;
    *out_screen = screen_iter.data;
}

xcb_randr_screen_size_t * x_enum_screen_types(int * size) {
    Display *display;
  //  xcb_connection_t *connection;
    xcb_window_t win;
    const int GLX_TRUE = True;
    int attrib_list[] = {GLX_X_RENDERABLE, GLX_TRUE,
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_CONFIG_CAVEAT, GLX_NONE,
        GLX_DOUBLEBUFFER, GLX_TRUE,
        GLX_BUFFER_SIZE, 32,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        0};
    GLXFBConfig *FBConfigs;
    int nelements;
    GLXFBConfig fb_config;
    XVisualInfo *visual;
    int visualID;
    GLXContext context;
    xcb_colormap_t colormap;
    xcb_void_cookie_t create_color;
    xcb_void_cookie_t create_win;
    const uint32_t eventmask = XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS;
    const uint32_t valuemask = XCB_CW_EVENT_MASK | XCB_CW_COLORMAP;
    uint32_t valuelist[] = {eventmask, colormap};
    xcb_randr_get_screen_info_cookie_t screen_info;
    xcb_randr_get_screen_info_reply_t *reply;
    int screen_num;
    xcb_screen_t *screen;
    xcb_generic_error_t *error;
    xcb_randr_screen_size_t *sizes;
    int sizes_length;
    xcb_randr_refresh_rates_iterator_t rates_iter;
    uint16_t *rates;
    int rates_length;
    int i;
    
    *size = 1;

    /* Open Xlib Display */
    display = XOpenDisplay(NULL);
    //printf("display = %p\n", display);
    //connection = XGetXCBConnection(display);
    //printf("connection = %p\n", connection);
    //XSetEventQueueOwner(display, XCBOwnsEventQueue);
    win = xcb_generate_id(connection);
    //printf("win = %d\n", win);
    screen_from_Xlib_Display(display, connection, &screen_num, &screen);
    //printf("screen_num = %d\n", screen_num);
    //printf("screen->root = %d\n", screen->root);
    
    FBConfigs = glXChooseFBConfig(display, screen_num, attrib_list,
            &nelements);
    //printf("a\n");
    //printf("FBConfig = %p\n", FBConfigs);
    //printf("nelements = %d\n", nelements);
    fb_config = FBConfigs[0];
    visual = glXGetVisualFromFBConfig(display, fb_config);
    //printf("visual = %p\n", visual);
    visualID = visual->visualid;
    //printf("visualID = %d\n", visualID);
    context = glXCreateNewContext(display, fb_config, GLX_RGBA_TYPE,
            0, True);
    //printf("context = %p\n", context);
    colormap = xcb_generate_id(connection);
    //printf("colormap = %d\n", colormap);
    create_color = xcb_create_colormap_checked(connection,
            XCB_COLORMAP_ALLOC_NONE, colormap, screen->root, visualID);
    //printf("create_color.sequence = %d\n", create_color.sequence);
    error = xcb_request_check(connection, create_color);
    //printf("error = %p\n", error);
    create_win = xcb_create_window_checked(connection,
            XCB_COPY_FROM_PARENT, win, screen->root, 0, 0, 640, 480, 2,
            XCB_WINDOW_CLASS_INPUT_OUTPUT, visualID, valuemask, valuelist);
    //printf("create_win.sequence = %d\n", create_win.sequence);
    error = xcb_request_check(connection, create_win);
    //printf("error = %p\n", error);
    screen_info = xcb_randr_get_screen_info_unchecked(connection, screen->root);
    //printf("screen_info.sequence = %d\n", screen_info.sequence);
    reply = xcb_randr_get_screen_info_reply(connection, screen_info,
            NULL);
    //printf("reply = %p\n", reply);
    //printf("reply->response_type = %d\n", reply->response_type);
    //printf("reply->rotations = %d\n", reply->rotations);
    //printf("reply->sequence = %d\n", reply->sequence);
    //printf("reply->length = %d\n", reply->length);
    //printf("reply->nSizes = %d\n", reply->nSizes);
    //printf("reply->sizeID = %d\n", reply->sizeID);
    //printf("reply->rotation = %d\n", reply->rotation);
    //printf("reply->rate = %d\n", reply->rate);
    //printf("reply->nInfo = %d\n", reply->nInfo);
    //printf("reply+1 = %p\n", reply + 1);
    sizes = xcb_randr_get_screen_info_sizes(reply);
    //printf("sizes = %p\n", sizes);
    sizes_length = xcb_randr_get_screen_info_sizes_length(reply);
    //printf("sizes_length = %d\n", sizes_length);
    rates_iter = xcb_randr_get_screen_info_rates_iterator(reply);
    //printf("rates_iter.data = %p\n", rates_iter.data);
    //printf("rates_iter.rem = %d\n", rates_iter.rem);
    //printf("rates_iter.index = %d\n", rates_iter.index);
    for (; rates_iter.rem; xcb_randr_refresh_rates_next(&rates_iter)) {
        rates = xcb_randr_refresh_rates_rates(rates_iter.data);
        //printf("rates = %p\n", rates);
        rates_length =
                xcb_randr_refresh_rates_rates_length(rates_iter.data);
        //printf("rates_length = %d\n", rates_length);
        //printf("rates[0] = %d\n", rates[0]);
        /*
              for(i = 0; i < rates_length; i++)
              {
                 //printf("%d%c",rates[i],(i==rates_length-1)?'\n':' ');
              }
         */
    }
    
    *size = sizes_length;
    for (i = 0; i < sizes_length; i++) {
        //printf("%d %d %d %d %d\n", i, sizes[i].width, sizes[i].height, sizes[i].mwidth, sizes[i].mheight);
    }
    
    printf("enum %i\n",sizes);
    return sizes;
}

int xc_get_mouse_pos(int * rx, int * ry, int * wx, int * wy) {
    unsigned int mask;
    
    xcb_query_pointer_cookie_t xct = xcb_query_pointer (connection,  window);
    xcb_query_pointer_reply_t * rp = (xcb_query_pointer_reply_t*)xcb_query_pointer_reply(connection,xct,NULL);
    
//    if (!rp->same_screen) {
//        return 0;
//    }
    *rx = rp->root_x;
    *ry = rp->root_y;
    *wx = rp->win_x;
    *wy = rp->win_y;
    
    return 1;
}

int xc_root_to_window(int x, int y, int * wx, int * wy) {

    xcb_generic_error_t * e;
    //xcb_get_geometry_cookie_t geom = xcb_get_geometry(connection, root_window);
    xcb_translate_coordinates_cookie_t offset = xcb_translate_coordinates(
            connection, root_window,window , x, y);
    
    xcb_translate_coordinates_reply_t * reply = xcb_translate_coordinates_reply(connection,
              offset,  &e);

    //printf("conv %i %i\n",reply->dst_x,reply->dst_y);
    
    
    *wx = reply->dst_x;
    *wy = reply->dst_y;
    
    return 1;
}
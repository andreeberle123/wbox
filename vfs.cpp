
#include <dirent.h>
#include <sys/stat.h>
#include <ctype.h>

#include "lstring.h"
#include "common.h"

#include "fs.h"
#include <regex.h>   


char * VirtualFileMap::remap_from_absolute(char * path, char * dest) {
    if (path[0] == 'c' || path[0] == 'C') {
        strcpy(dest,cbase);
        char * t = dest+strlen(dest);
        char * t2 = path+2;
        while (*t2) {
            if (*t2 == '\\') {
                *t++ = '/';
            }
            else {
                *t++ = tolower(*t2);
            }
            t2++;
        }
        *t = 0;
    }
    
    return dest;
}


char * VirtualFileMap::remap_from_rel(char * path, char * dest) {
    
    strcpy(dest,cbase_full);
    *(dest+cbase_full_sz) = '/';
    strcpy(dest+cbase_full_sz+1,path);
    tolower_str(dest);
    
    return dest;
}

char * VirtualFileMap::remap_from_current(char * path, char * dest) {
    
    char * t = path+global_config_data.base_path_sz;
    
    strcpy(dest,cbase);
    
    strcpy(dest+cbase_sz,t);
    tolower_str(dest);
    
    return dest;
}

void VirtualFileMap::process_dir(char * path) {
    DIR *d;
    byte cbuf[8192];
    struct dirent *dir;
    d = opendir(path);
   // printf("processing %s\n",path);
    char fpath[512];
    strcpy(fpath,path);
    char * pbase = fpath+strlen(fpath);
    *pbase++ = '/';
    while ((dir = readdir(d)) != NULL) {
        if (!strcmp(dir->d_name,".") || !strcmp(dir->d_name,"..")) continue;
        strcpy(pbase,dir->d_name);
       // printf("real path %s\n",fpath);
        char rmap[512];
        remap_from_current(fpath,rmap);
        
        if (dir->d_type == DT_REG) {
            
            FILE * fr = fopen(fpath,"rb");
            FILE * fw = fopen(rmap,"wb");
            int rt;
            while ((rt = fread(cbuf,1,8192,fr)) > 0) {
                fwrite(cbuf,1,rt,fw);
            }
            fclose(fw);
            fclose(fr);
            addFile(rmap, fpath,DT_REG);
            
        }
        else if (dir->d_type == DT_DIR) {
            printf("mkdir %s\n",rmap);
            mkdir(rmap,__S_IREAD|__S_IWRITE);
            process_dir(fpath);
            addFile(rmap,fpath,DT_DIR);
        }


    }
}

char * base_maps[] = {"windows"};
#define VIRTUAL_PURE 0x100

void VirtualFileMap::make_base() {
    char path[512];
    int i;
    strcpy(path,cbase);
    char * t = path+cbase_sz;
    *t++ = '/';
    for(i=0;i<sizeof(base_maps)/sizeof(char*);i++) {
        
        char * base = base_maps[i];
        strcpy(t,base);
        
        mkdir(path,__S_IREAD|__S_IWRITE);
        addFile(path,"",VIRTUAL_PURE);
    }
    
}

void VirtualFileMap::add_fixed_virtuals() {
    char pmap[512];
    sprintf(pmap,"%s/winmm.dll",SHELF);
    addFile("/etc/ntbox/c/windows/system32/winmm.dll",pmap,DT_VIRT);
}

VirtualFileMap::VirtualFileMap() {
    paths = c_hash_create(16,256,0);
    paths->cmp_func = str_cmp;
    
    if (global_config_data.use_full_vmap) {
        char lpath[512];
        strcpy(lpath,global_config_data.emu_base_dir);
        strcpy(lpath+global_config_data.emu_base_dir_sz,"/c");
        strcpy(cbase,lpath);
        cbase_sz = strlen(cbase);
        strcpy(cbase_full,cbase);
        
        
        
        make_base();
        
        mkdir(lpath,__S_IREAD|__S_IWRITE);
        char * fname = global_config_data.cur_dir + l_lastindexof(global_config_data.cur_dir,'/');
        strcpy(cbase_full+cbase_sz,fname);
        tolower_str(cbase_full);
        cbase_full_sz = strlen(cbase_full);
        
        strcpy(lpath+strlen(lpath),fname);
        tolower_str(lpath);
        //printf("mkdir %s\n",lpath);
        
        mkdir(lpath,__S_IREAD|__S_IWRITE);
        addFile(lpath,global_config_data.cur_dir,DT_DIR);
        
        process_dir(global_config_data.cur_dir);
        
        add_fixed_virtuals();
        

    }
    
}

void VirtualFileMap::addPath(char * path) {
    
}

void VirtualFileMap::matchFile(char * path) {
    
    DIR *d;
    struct dirent *dir;
    d = opendir(path);
    
    while ((dir = readdir(d)) != NULL) {
        if (dir->d_type != DT_REG) continue;
        
        //dir->d_name
        
    }
    closedir(d);
    
}

void replace_slash(char * path);

int regex_match(char * str, char * regex) {
    // simple regex, just wildcards
    while (*str && *regex) {
        if (*regex == '*') {
            regex++;
            char * t = regex;
            if (!*t) return 1;
            while (*t && *t != '*') {
                t++;
            }       
            
            while (strncmp(str,regex,(int)(t-regex))) {
                str++;
                if (!*str) {
                    return 0;
                }
            }
        }
        else {
            if (*str != *regex) {
                return 0;
            }
            str++;
            regex++;
        }
    }
    return 1;
}

tvfshandle * VirtualFileMap::lookup_first(char * expr, local_find_file * info) {
    printf("lookup %s\n",expr);
    char pmap[1024];
    char rmap[1024];
    strcpy(pmap,expr);
    replace_slash(pmap);
    if ((pmap[0] == 'C' || pmap[0] == 'c') && pmap[1] == ':') {
        // abs path
        remap_from_absolute(pmap,rmap);
    }
    else {
        remap_from_rel(pmap,rmap);
    }
    
    printf("remap to %s\n",rmap);
    int len = strlen(rmap);
    int i;
    char * slash = rmap;
    int regex = 0;
    for(i=0;i<len;i++) {
        if (rmap[i] == '/') {
            slash = rmap+i;;
        }
        else if (rmap[i] == '*') {
            regex = 1;
            break;
        }
        
    }
    
    
    vfs_container * vc = NULL;
    
    if (!regex) {
        // regular search
        printf("look %s\n",rmap);
        vc = (vfs_container*)c_hash_get_k(DEF_STRING_HASH(rmap),rmap,paths);
               
    }
    else {
        *slash = 0;
        struct dirent *dir;
        DIR *d;
        d = opendir(rmap);
        int cc = 0;
        if (d) {
            printf("processing %s \n",rmap);
            while ((dir = readdir(d)) != NULL) {
                if (dir->d_type == DT_REG) {
                    //printf("potential %s\n",dir->d_name);
                    if (regex_match(dir->d_name,slash+1)) {
                        cc++;
                        if (cc <= info->count) {
                            continue;
                        }
                        strcpy(pmap,rmap);
                        *(pmap+strlen(rmap)) = '/';
                        strcpy(pmap+strlen(rmap)+1,dir->d_name);
                        printf("found %s %s\n",dir->d_name,pmap);
                        vc = (vfs_container*)c_hash_get_k(DEF_STRING_HASH(pmap),pmap,paths);
                        break;
                    }
                }
            }
        }
    }
    
    if (!vc) {
        // not found
        printf("not found\n");
        return NULL;
    } 
    
    printf("found file %s\n",vc->path);
    tvfshandle * h = (tvfshandle*)malloc(sizeof(tvfshandle));
    h->type = vc->type;
    h->vc = vc;
    h->ph = 0;
    info->count++;
    return h;
}


tvfshandle * VirtualFileMap::virtual_handle_local(char * opath) {
    char rmap[512];
    remap_from_rel(opath,rmap);
    
    vfs_container * vc = (vfs_container*)c_hash_get_k(DEF_STRING_HASH(rmap),rmap,paths);
    printf("local remap %s %X\n",rmap,vc);
    
    if (!vc) {
        printf("failure in recovering virtual file (%s)\n",opath);
        return NULL;
    }
    
    tvfshandle * h = (tvfshandle*)malloc(sizeof(tvfshandle));
    h->type = vc->type;
    h->vc = vc;
    h->ph = 0;
    return h;
}

tvfshandle * VirtualFileMap::virtual_handle(char * opath) {
    char rmap[512];
    remap_from_absolute(opath,rmap);
    
    vfs_container * vc = (vfs_container*)c_hash_get_k(DEF_STRING_HASH(rmap),rmap,paths);
 //   printf("abs remap %s %X\n",rmap,vc);
    
    if (!vc) {
      //  printf("failure in recovering virtual file (%s)\n",opath);
        return NULL;
    }
    
    tvfshandle * h = (tvfshandle*)malloc(sizeof(tvfshandle));
    h->type = vc->type;
    h->vc = vc;
    h->ph = 0;
    return h;
}

void VirtualFileMap::free_handle(void * handle) {
    //if (handle->ph)
    free(handle);
}

void win_slash(char * str) {
    char * t = str;
    while (*t) {
        if (*t == '/') {
            *t = '\\';
        } 
        t++;
    }
}

void VirtualFileMap::get_emulated_address(tvfshandle * handle, char * dest) {
    
    strcpy(dest,"c:");
    strcpy(dest+2,handle->vc->path+cbase_sz);
    
    win_slash(dest);
    printf("%s %s\n",handle->vc->path+cbase_sz,dest);
    
}

FILE * VirtualFileMap::get_phys_handle(tvfshandle * handle, char * mode) {
    if (handle->ph) {
        return handle->ph;
    }
   // printf("ph %s\n",mode);
    handle->ph = fopen(handle->vc->physical_path,mode);
    return handle->ph;
}

int VirtualFileMap::get_real_path(tvfshandle * handle, char * path) {
    tvfshandle * h = (tvfshandle*)handle;
    strcpy(path,h->vc->physical_path);
}

tvfshandle * VirtualFileMap::createNewFile(char * path) {
    printf("createNewFile\n");
    char pmap[1024];
    char rmap[1024];
    char vpath[1024];
    strcpy(pmap,path);
    replace_slash(pmap);
    
    //strcpy(vpath,global_config_data.base_path);
    
    if ((pmap[0] == 'C' || pmap[0] == 'c') && pmap[1] == ':') {
        // abs path
        remap_from_absolute(pmap,rmap);
        
        //strcpy(vpath+global_config_data.base_path_sz,path+2);
    }
    else {
        remap_from_rel(pmap,rmap);
    }
    
    char * slash = 0;
    char * t = rmap;
    while (*t) {
        if (*t == '/') {
            slash = t;
        }
        t++;
    }
    
    if (slash) {
        *slash = 0;
        vfs_container * vc = (vfs_container*)c_hash_get_k(DEF_STRING_HASH(rmap),rmap,paths);

        printf("vc %X %s\n",vc,rmap);
        *slash = '/';
        strcpy(vpath,vc->physical_path);
        *(vpath+strlen(vc->physical_path)) = '/';
        strcpy(vpath+strlen(vc->physical_path)+1,slash+1);
    }
    else {
        printf("Unsupported relative creation\n");
        exit(0);
    }
    
    printf("create virt %s %s\n",rmap,vpath);
    
    vfs_container * vc = addFile(rmap,vpath,DT_REG);
    
    tvfshandle * h = (tvfshandle*)malloc(sizeof(tvfshandle));
    h->type = vc->type;
    h->vc = vc;
    h->ph = 0;
    
    
    return h;
}

vfs_container * VirtualFileMap::addFile(char * path, char * orig, int type) {
    char tmp[512];
    strcpy(tmp,path);
    tolower_str(tmp);
    vfs_container * vc;
    if (vc = (vfs_container *)c_hash_get_k(DEF_STRING_HASH(path),path,paths)) {
        return vc;
    }
    
    vc = (vfs_container*)malloc(sizeof(vfs_container));
    strcpy(vc->path,tmp);
    strcpy(vc->physical_path,orig);
    vc->type = type;
    
//    printf("put %s\n",vc->path);
    c_hash_put_k(DEF_STRING_HASH(vc->path),&vc->path,vc,paths);
    
    return vc;   
    
}


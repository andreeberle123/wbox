#include "TextRender.h"

#include <ft2build.h>
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>
#include <freetype/ftoutln.h>
#include <freetype/fttrigon.h>
#include "common.h"

TextRender * TR_instance = 0;

TextRender * TR_getInstance() {
    if (TR_instance) {
        return TR_instance;
    }
    TR_instance = new TextRender();

    return TR_instance;
}

#define FT_PATH "fonts/"

FT_Error TextRender::loadFont(char * name) {
    char p[256];
    sprintf(p,"%s%s",FT_PATH,name);
    return FT_New_Face(library, p, 0, &face);
}

TextRender::TextRender() {
    int error = FT_Init_FreeType(&library);

    if (error) {
        printf("FreeType load error\n");
    }

    error = FT_Init_FreeType(&library);
    if (error) {
        printf("FreeType load error2\n");
    }
    //error = loadFont("DroidSansMono.ttf"); 
    error = loadFont("JustinFont12Bold.ttf");
    //error = loadFont("calibrib.ttf"); 
    if (error) {
        printf("FreeType load error %i\n", error);
    }

    base_sizes[0] = 8;
    base_sizes[1] = 12;
    base_sizes[2] = 14;

    font_heights[0] = TEX_SIZE_8_H;
    font_heights[1] = TEX_SIZE_10_H;
    font_heights[2] = TEX_SIZE_14_H;

}

int TextRender::getFontHeight(int id) {
    return font_heights[id];
}

inline int next_p2(int a) {
    int rval = 1;
    while (rval < a) rval <<= 1;
    return rval;
}

void TextRender::createDisplayLists() {

    int i;
    for (i = 0; i < 3; i++) {
        int ssz = base_sizes[i];

        int error = FT_Set_Char_Size(face, 64 * ssz, 64 * ssz, 96, 96);
        if (error) {
            printf("FreeType load error %i\n", error);
        }
        list_base[i] = glGenLists(128);
        glGenTextures(128, font_tex[i]);

        int ch;
        for (ch = 0; ch < 128; ch++) {

            if (FT_Load_Glyph(face, FT_Get_Char_Index(face, ch), FT_LOAD_DEFAULT))
                printf("ERROR\n");

            if (FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL)) {
                printf("ER\n");
            }

            FT_Glyph glyph;
            if (FT_Get_Glyph(face->glyph, &glyph))
                printf("ERROR\n");

            int rt = FT_Glyph_To_Bitmap(&glyph, ft_render_mode_normal, 0, 1);
            
            FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph) glyph;

            FT_Bitmap& bitmap = bitmap_glyph->bitmap;
            int width = next_p2(bitmap.width);
            int height = next_p2(bitmap.rows);

            GLubyte* expanded_data = new GLubyte[ 2 * width * height];

            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    expanded_data[2 * (i + j * width)] = expanded_data[2 * (i + j * width) + 1] =
                            (i >= bitmap.width || j >= bitmap.rows) ?
                            0 : bitmap.buffer[i + bitmap.width * j];
                }
            }

            glBindTexture(GL_TEXTURE_2D, font_tex[i][ch]);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            //glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
                    GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data);

            delete [] expanded_data;
            //printf("rw %i %i %i %c\n",bitmap_glyph->top,bitmap.width,bitmap.rows,ch);

            glNewList(list_base[i] + ch, GL_COMPILE);

            glBindTexture(GL_TEXTURE_2D, font_tex[i][ch]);
            glPushMatrix();
            //glRotatef(180,0,0,1);
            glTranslatef(bitmap_glyph->left, 0, 0);
            glTranslatef(0, -(bitmap_glyph->top - bitmap.rows), 0);

            float x = (float) bitmap.width / (float) width,
                    y = (float) bitmap.rows / (float) height;

            glBegin(GL_QUADS);

            glTexCoord2d(0, 0);
            glVertex2f(0, -(signed int)bitmap.rows);
            glTexCoord2d(0, y);
            glVertex2f(0, 0);
            glTexCoord2d(x, y);
            glVertex2f(bitmap.width, 0);
            glTexCoord2d(x, 0);
            glVertex2f(bitmap.width, -(signed int)bitmap.rows);
            

            glEnd();

//            glDisable(GL_BLEND);
//            glDisable(GL_TEXTURE_2D);
//            glBegin(GL_QUADS);
//
//            glEnd();
            glEnable(GL_BLEND);
            glEnable(GL_TEXTURE_2D);
            glPopMatrix();
            glTranslatef((face->glyph->advance.x >> 6) + 1, 0, 0);

            glEndList();
            
        }
        
    }
    

}

void TextRender::renderText(char * text, int size) {

    glPushMatrix();
    glLoadIdentity();
    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glListBase(list_base[size]);

    glTranslatef(px, py, depth);

    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glColor3fv(color);
    glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);
    glPopMatrix();

    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);


}

void TextRender::setPos(int x, int y) {
    px = x;
    py = y;
    depth = -1;
}

void TextRender::setPos(int x, int y, float z) {
    px = x;
    py = y;
    depth = z;

}

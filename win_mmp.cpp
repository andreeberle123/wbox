#include "common.h"
#include "winm.h"
extern "C" {

SIZE_T WINAPI VirtualQuery(
        LPCVOID lpAddress,
        PMEMORY_BASIC_INFORMATION lpBuffer,
        SIZE_T dwLength
        ) {
    FILE * fp = fopen("/proc/self/maps", "r");
    char line[1024];
    if (!lpAddress) {
        return 0;
    }
    while (fgets(line, 1024, fp)) {
        line[8] = 0;
        line[17] = 0;
        dword p1 = strtoul(line, NULL, 16);
        dword p2 = strtoul(line + 9, NULL, 16);
        //printf("%X %X %X %s %s\n",lpAddress,p1,p2,line,line+9);
        if ((dword) lpAddress >= p1 && (dword) lpAddress < p2) {
            //            printf("found query %s\n",line+18);
            lpBuffer->BaseAddress = (void*) p1;
            lpBuffer->AllocationBase = (void*) p1;
            lpBuffer->AllocationProtect = 0;
            if (line[19] == 'r' && line[20] == 'w') {
                lpBuffer->AllocationProtect |= 0x04;
            }
            lpBuffer->RegionSize = (dword) (p2 - p1);
            lpBuffer->Type = 0x20000;
            lpBuffer->State = 0x1000;
            return sizeof (MEMORY_BASIC_INFORMATION);
        }
    }
    printf("VirtualQuery failed\n");
    exit(0);
    //lpBuffer->AllocationBase
}

LPVOID WINAPI VirtualAlloc(LPVOID lpAddress, DWORD dwSize, DWORD flAllocationType, DWORD flProtect) {
    int prot = 0;
    if (flProtect & PAGE_READONLY || flProtect & PAGE_READWRITE || flProtect & PAGE_EXECUTE_READWRITE) {
        prot |= PROT_READ;
    }

    if (flProtect & PAGE_READWRITE || flProtect & PAGE_EXECUTE_READWRITE) {
        prot |= PROT_WRITE;
    }

    if (flProtect & PAGE_EXECUTE || flProtect & PAGE_EXECUTE_READWRITE || flProtect & PAGE_EXECUTE) {
        prot |= PROT_EXEC;
    }

    void * addr =  mmap(lpAddress, dwSize, prot, MAP_ANONYMOUS | MAP_PRIVATE, 0, 0);
    
    //printf("Allocated on %X\n",addr);
    return addr;
}

BOOL WINAPI VirtualFree(
    LPVOID lpAddress,
    SIZE_T dwSize,
    DWORD  dwFreeType
) {
    if (dwFreeType == 0x4000) {
        printf("Not supported VirtualFree\n");
        exit(0);
    }
    munmap(lpAddress,dwSize);
    return 1;
}

DWORD LocalAlloc(
    UINT   uFlags,
    SIZE_T uBytes
) {
    printf("locallaloc %X\n",uBytes);
    exit(0);
}

}

#include "ne.h"
#include <signal.h>
#include "common.h"
#include <sys/wait.h>
#include <unistd.h>
#include <sys/mman.h>
#include "exec.h"
#include "lstring.h"
#include <dlfcn.h>
#include <stdlib.h>
#include "winm.h"
#include "c_hash.h"


/*******************************************************************************
 * Global data
 */

#define MM_DS_BASE 0xFD00

typedef struct _ds_global_data {
    uint32 valid_cs;
    uint32 valid_ds; // 0x4
    uint32 valid_es; // 0x8
    uint32 valid_fs; // 0xc
    uint32 valid_gs; // 0x10
    uint32 valid_ss; // 0x14

    uint32 global_cs; // 0x18
    uint32 global_ds; // 0x1c
    uint32 global_es; // 0x20
    uint32 global_fs; // 0x24
    uint32 global_gs; // 0x28
    uint32 global_ss; // 0x2c
    
    uint32 aux_asm_addr; // 0x30
    unsigned short int aux_asm_seg; //0x34
    
    
} __attribute__((packed)) ds_global_data;

uint32 global_ds_base;


ds_global_data * ds_global_entry;

extern byte * es_base;

typedef struct _es_fixed_data {
    char appname[256];
} es_fixed_data;



void * sym_lookup(char * name) {
    void * h = dlopen(NULL, RTLD_LAZY | RTLD_LOCAL);
    if (h != NULL) {
        void * sym = dlsym(h, name);
      //  printf("sym %X %s %X\n",sym,name,h);
        return sym;        
        
    }
    
    return NULL;
}

typedef struct _pmap {
    char libname[128];
    dword map;
} pmap;
static pmap pmaps[2048];
static int pmaps_sz = 0;

static byte _pfunc[] = {
    0x68, 0x23, 0x01, 0x00, 0x00, 0xb8, 0x78, 0x56, 0x34, 0x12, 0xff, 0xd0
};

static byte * f_addr = 0;




void _no_func(int idx) {
    dword ret,rt;
    
    asm volatile (
        "movl 4(%%ebp), %0;"
        "movl 12(%%ebp), %1;"
    :"=r"(ret),"=r"(rt) ::
    );
    printf("No func for %X (%X)\n",ret,rt);
    printf("At %s\n",pmaps[idx].libname);
    
    exit(0);
}

void * build_func(int idx) {
    if (!f_addr) {
        f_addr = (byte*)mmap(0,4096*16,PROT_EXEC | PROT_READ | PROT_WRITE,MAP_ANONYMOUS | MAP_PRIVATE,0,0);
    }
    byte * addr = f_addr;
    memcpy(addr,_pfunc,sizeof(_pfunc));
    
    *(int*)(addr+1) = idx;
    f_addr += sizeof(_pfunc);
    
    
    *(dword*)(addr+6) = (dword)_no_func;
    return addr;
}

void __no_func() {
    _no_func(0x123);
}

typedef struct _llib {
    char name[512];
    pe_info pi;
    dword base;
    LDR_DATA_TABLE_ENTRY le;
} llib;

static llib llibs[128];
static int llib_sz = 0;

int load_exec(pe_info * info);

pe_info * lload2(pe_info * info) {
    char c1[512];
    if (!strncmp(info->name,"shelf/",6)) {
        strcpy(c1,info->name+6);   
    }
    else {
        strcpy(c1,info->name);
    }
    tolower_str(c1);
    char cmap[512];
    
    int i;
    for(i=0;i<llib_sz;i++) {
        if (!strcmp(llibs[i].name,c1)) {
           // printf("already loaded\n");
            return &llibs[i].pi;
            
        }
    }
    
    printf("Loading %s\n",c1);
    
    int idx = llib_sz++;
    strcpy(llibs[idx].name,c1);
    llibs[idx].pi = *info;
    
    printf("File processed, loading in memory\n");
    //printf("ch %X\n",llibs[idx].pi.ex_header->characteristics);
    load_exec(&llibs[idx].pi);
    return &llibs[idx].pi;
    
}

pe_info * lload(char * libname) {
    char c1[512];
    strcpy(c1,libname);
    tolower_str(c1);
    char cmap[512];
    libname = c1;
    int i;
    for(i=0;i<llib_sz;i++) {
        if (!strcmp(llibs[i].name,libname)) {
           // printf("already loaded\n");
            return &llibs[i].pi;
            
        }
    }
    
    printf("Loading %s\n",libname);
    
    sprintf(cmap,"%s/%s",SHELF,libname);
    
    tolower_str(cmap);
    int idx = llib_sz++;
    
    strcpy(llibs[idx].name,libname);
    process_file(cmap,&llibs[idx].pi);
    
    printf("File processed, loading in memory\n");
    //printf("ch %X\n",llibs[idx].pi.ex_header->characteristics);
    load_exec(&llibs[idx].pi);
    return &llibs[idx].pi;
}

struct _PEB * peb = NULL;

void assign_string_offset(char * str, UNICODE_STRING * where, void * offset_base) {
    int len = strlen(str);
    
    int tlen = (len+1)*2;
    if (tlen < 0x208) tlen = 0x208;
    WORD * pstr = (WORD*)malloc(tlen);
    wide_str(str,(wchar_t*)pstr);
    
    dword off = (dword)pstr - (dword)offset_base;
    where->Buffer = (WORD*)off;
    where->Length = len*2;
    where->MaximumLength = tlen;
}


 
void build_peb(pe_info * info) {
    strcpy(llibs[0].name,info->name);
    llib_sz = 1;
    peb = (struct _PEB *)calloc(1,sizeof(struct _PEB));
    
    log_debug("PEB at %X",peb);
    peb->Ldr = NULL;
    peb->ProcessParameters = (RTL_USER_PROCESS_PARAMETERS*)calloc(1,sizeof(RTL_USER_PROCESS_PARAMETERS));
    peb->ImageBaseAddress = (void*)info->ex_opt_header->image_base;
    peb->HeapDeCommitFreeBlockThreshold = 0x1000;
    peb->SystemDefaultActivationContextData = (ACTIVATION_CONTEXT_DATA*)mmap((void*)0x70000,0x2498, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_ANONYMOUS | MAP_PRIVATE,0,0);
    
   /* peb->SystemDefaultActivationContextData->HeaderSize = sizeof(ACTIVATION_CONTEXT_DATA);
    peb->SystemDefaultActivationContextData->TotalSize = 0x2498;
    peb->SystemDefaultActivationContextData->DefaultTocOffset = 0xc4;
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0xc4) = 0x10;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0xc8) = 0x4;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0xcc) = 0xd4;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0xd0) = 0x2;
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0xe4) = 0x2;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0xe8) = 0x10a0;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0xec) = 0x32c;
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x10a0) = 0x64487353; // magic
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x10a8) = 0x1;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x10b0) = 0x1;    
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x10b4) = 0x8;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x10c0) = 0x2ec;
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x138c) = 0x3;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x1390) = 0x2f4;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x139c) = 0x2;
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x13a0) = 0x314;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x13b4) = 0x32c;
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x13a4) = 0x4;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x13a8) = 0x31c;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x13ac) = 0x2c;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x13b0) = 0xa4;
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x1394) = 0x4;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x1398) = 0x30c;
    
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x13ac) = 0x2c;
    *(dword*)((dword)peb->SystemDefaultActivationContextData+0x13b0) = 0xa4;
    */
    FILE * lmem = fopen("lmem","rb");
  //  printf("%X\n",lmem);
    fread(peb->SystemDefaultActivationContextData,1,0x2498,lmem);
    fclose(lmem);
    
    
    peb->HotpatchInformation = malloc(16);
    
    
    peb->HeapDeCommitTotalFreeThreshold = 0x10000;
    
    char dllpath[1024];
    sprintf(dllpath,"%s;%s",global_config_data.base_path_remapped,"C:\\WINDOWS\\system32;C:\\WINDOWS\\system;C:\\WINDOWS;.;C:\\WINDOWS\\system32;C:\\WINDOWS;C:\\WINDOWS\\System32\\Wbem");

    
    assign_string_offset(dllpath,&peb->ProcessParameters->DllPath,peb->ProcessParameters);
   /* peb->ProcessParameters->DllPath.Buffer = (WORD*)malloc(512*sizeof(WORD));
    
    int len = _wcslen(L"c:\\windows\\system32");
    peb->ProcessParameters->DllPath.Length = len*2;
    peb->ProcessParameters->DllPath.MaximumLength = 1024;
    memcpy(peb->ProcessParameters->DllPath.Buffer,L"c:\\windows\\system32",(len+1)*2);*/
    
    
    
    //len = wide_str(global_config_data.base_path_remapped,peb->ProcessParameters->CurrentDirectory.DosPath->Buffer);
    
    
    printf("PEB at %X, ProcessParameters at %X, DllPath at %X\n",peb,peb->ProcessParameters,&peb->ProcessParameters->DllPath);
    peb->SomeValues1 = (BYTE*)calloc(1,0x1000);
    *(dword*)peb->SomeValues1 = (dword)0x4e4000d;
    dword aoff = (dword)peb->SomeValues1+2*0xd;
    
    *(word*)aoff = 0x103; // ??
    aoff +=2;
    
    int i;
    for(i=0;i<0x80;i++) {
        *(word*)(aoff+i*2) = i;
    }
    
    
    *(word*)(peb->SomeValues1+4)  = 1;
    *(word*)(peb->SomeValues1+6)  = 0x3f;
    *(word*)(peb->SomeValues1+8)  = 0x3f;
    *(word*)(peb->SomeValues1+10)  = 0x3f;
    *(word*)(peb->SomeValues1+12)  = 0x3f;
    
    peb->SomeValues2 = (BYTE*)calloc(1,0x1000);
    *(dword*)peb->SomeValues2 = (dword)0x1b5000d;
    aoff = (dword)peb->SomeValues2+2*0xd;
    
    
    *(word*)aoff = 0x203; // ??    
    *(dword*)(aoff+2) = 0x10000; // ??
    *(dword*)(aoff+0x202) = 0x100; // ??
    
    *(word*)(peb->SomeValues2+4)  = 1;
    *(word*)(peb->SomeValues2+6)  = 0x3f;
    *(word*)(peb->SomeValues2+8)  = 0x3f;
    *(word*)(peb->SomeValues2+10)  = 0x3f;
    *(word*)(peb->SomeValues2+12)  = 0x3f;
    
    peb->SomeValues3 = (BYTE*)malloc(1024);
    *(dword*)peb->SomeValues3= (dword)0x6f10001;
    
    peb->unk2 = 0xFFFFE86D;
    peb->HeapSegmentReserve = 0x100000;
    
    assign_string_offset(global_config_data.base_path_remapped,&peb->ProcessParameters->CurrentDirectory.DosPath,peb->ProcessParameters);
    
    char vtmp[256];
    sprintf(vtmp,"\"%s\"",global_config_data.cmdline_remapped);
    assign_string_offset(vtmp,&peb->ProcessParameters->CommandLine,peb->ProcessParameters);

    assign_string_offset(global_config_data.cmdline_remapped,&peb->ProcessParameters->WindowTitle,peb->ProcessParameters);
   
    
    assign_string_offset("WinSta0\\Default",&peb->ProcessParameters->DesktopInfo,peb->ProcessParameters);
    assign_string_offset("",&peb->ProcessParameters->ShellInfo,peb->ProcessParameters);
    
    peb->ProcessParameters->CurrentDirectores[0].DosPath.Buffer = (char*)malloc(512);
    
    memcpy(peb->ProcessParameters->CurrentDirectores[0].DosPath.Buffer,L"c:\\",2*wcslen(L"c:\\"));
    
    w_join((wchar_t*)peb->ProcessParameters->CurrentDirectores[0].DosPath.Buffer,L"",global_config_data.cmdline_remapped);
    
    dword off = (dword)peb->ProcessParameters->CurrentDirectores[0].DosPath.Buffer - (dword)peb->ProcessParameters;
    peb->ProcessParameters->ImagePathOff = off;
    peb->ProcessParameters->unk1 = _wcslen((wchar_t*)peb->ProcessParameters->CurrentDirectores[0].DosPath.Buffer)*2;
    
    peb->GdiSharedHandleTable = build_gdi_table();
    
    peb->ProcessParameters->Environment = (void*) malloc(0x1000);
    FILE * fp = fopen("env","rb");
    fread(peb->ProcessParameters->Environment,1,0x1000,fp);
    fclose(fp);
    
    
}

void finish_lib_load(pe_info * info) {
    //
}

int loader_init() {
//    int i;
//    
//    log_debug("Initializing DLLs");
//    
//    setup_tls();
//    
//    for(i=0;i<llib_sz;i++) {
//        dword lep = llibs[i].pi.ex_opt_header->entry_point + llibs[i].pi.ex_opt_header->image_base;
//        int rt = ((int(*)(dword,dword,dword))lep)(llibs[i].pi.ex_opt_header->image_base,1,0);
//        printf("load %i\n",rt);
//        if (!rt) {
//            exit(0);
//        }
//        
//    }
}

typedef struct _mprot {
    void * address;
    dword size;
    int flags;
} mprot;


//#ifdef _DBG

#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/types.h>

#define DR_OFFSET(x) (((struct user *)0)->u_debugreg + x)

void  ssbg(int sig, siginfo_t * info, void * ucontext) {
    if (sig != SIGTRAP) {
        printf("unexpected signal\n");
        exit(0);
    }
    ucontext_t * t = (ucontext_t*)ucontext;
    printf("sigtrap %X %X\n",t->uc_mcontext.gregs[REG_EIP],info->si_addr);
    long l = ptrace(PTRACE_POKEUSER, syscall(__NR_gettid), DR_OFFSET(0), 0);
    
    printf("o %X %s\n",l,strerror(errno));
    t->uc_mcontext.gregs[REG_EIP]--;
    *(byte*)(t->uc_mcontext.gregs[REG_EIP]) = 0x8B;
    
    printf("handled %X \n",*(byte*)0x7C912C28);
}

int fork_child = 0;

void dbg_start(dword addr) {
    
    int pid = fork();
    if (!pid) {
//        fork_child = 1;
//        struct sigaction sa;
//
//        memset(&sa, 0, sizeof (struct sigaction));
//        sigemptyset(&sa.sa_mask);
//        sa.sa_sigaction = ssbg;
//        sa.sa_flags = SA_SIGINFO;
//
//        sigaction(SIGTRAP, &sa, NULL);
        return;        
    }
    
    
    long l = ptrace(PT_ATTACH,pid,0,0);
    printf("start dbg\n");
    int status;
    int rt = waitpid(pid,&status,0);
    printf("waitpid %X %X\n",rt,status);
    FILE * fp = fopen("ldump","wb");
    int log  = 0;
    int cc = 0;
    dword sysmin = (dword)__isyscall_trap-0x100000;
    dword sysmax = (dword)__isyscall_trap+0x100000;
    printf("syscalls estimated between %X and %X\n",sysmin,sysmax);
    
    FILE * fdiff = fopen("dst","wb");
    for(;;) {
        unsigned int dr;
        struct user_regs_struct regs;
        //l = ptrace(PTRACE_PEEKUSER, pid, DR_OFFSET(7), &dr);
        l = ptrace(PTRACE_GETREGS, pid, NULL, &regs);
        
       // printf("getregs %X %i %X\n",dr,l,regs.eip);
        if (l<0) {
            printf("getregs %X %i %X %s\n",dr,l,regs.eip,strerror(errno));
            break;
        }
        
        if (regs.eip >= 0x400000 && regs.eip < 0x500000) {
            log = 1;            
        }
        else {
            log = 0;
        }
        
        if (log) {
            
            if (0&&(dword)regs.eip == 0x7C90FE40) {
                
                int j;
                fwrite(&regs.eip,1,4,fdiff);
                for(j=0;j<20;j++) {
                    DWORD dd = ptrace(PT_READ_D, pid, (dword)regs.esp +(j*4));
                
                    fwrite(&dd,1,4,fdiff);
                }
                dword zero = 0;
                fwrite(&zero,1,4,fdiff);
                fflush(fdiff);
                
            }
            //if (regs.eip<sysmin || regs.eip > sysmax) {            
            
           // if ((dword)regs.eip>=0x70800000 && (dword)regs.eip < 0x8CA00000) {
                cc++;
                fwrite(&regs.eip,1,4,fp);
                fflush(fp);
            //}
          //  printf("log %X\n",regs.eip);
            if (0&&(dword)regs.eip == 0x7C90D50A) {
                printf("deferring\n");
                dword oldp;
                
                oldp = ptrace(PT_READ_D, pid, 0x7C90D50C);
                if (oldp == 0) {
                    printf("error %X %s\n",l,strerror(errno));
                    exit(0);
                }
                printf("read %X %X\n",l,oldp);
                l = ptrace(PT_WRITE_D, pid, 0x7C90D50C, 0xCCcc);
                printf("wrote %X \n",l);
                l = ptrace(PT_CONTINUE,pid,0,0);
                
                rt = waitpid(pid,&status,0);
                if (rt && WIFEXITED(status)){
                    printf("Error - child aborted prematurely\n");
                    exit(0);
                }
                printf("continued %X %i %i\n",status,rt,WIFSIGNALED(status));
                l = ptrace(PTRACE_GETREGS, pid, NULL, &regs);
                printf("at %X\n",regs.eip);
                l = ptrace(PT_WRITE_D, pid, 0x7C90D50C, oldp);
                
                regs.eip = 0x7C90D50C;
                l = ptrace(PTRACE_SETREGS, pid, NULL, &regs);
                
                cc++;
                fwrite(&regs.eip,1,4,fp);
                fflush(fp);
                
//                 l = ptrace(PT_STEP,pid,0,0);
//                rt = waitpid(pid,&status,0);
//                l = ptrace(PTRACE_GETREGS, pid, NULL, &regs);
//                printf("ret at %X\n",regs.eip);
//                exit(0);
                
            }            
            
        }

        l = ptrace(PT_STEP,pid,0,0);
//        if (l) {
//            printf("%i %s\n",l,strerror(errno));
//        }

        rt = waitpid(pid,&status,0);
        if (WIFSTOPPED(status)&& WSTOPSIG(status) != SIGTRAP) printf("signal\n");
        if (WIFSTOPPED(status) && WSTOPSIG(status) == SIGSEGV) {
            printf("segmentation fault received\n");
            break;
        }
//        if (rt < 0) {
//            printf("waitpid %X %X %X %s\n",rt,status,pid,strerror(errno));
//        }
       // if (cc == 10) break;
    }
    printf("dbg finished %i\n",cc);
    sleep(22222);
}
//#endif


void setup_syscalls(dword base) {
    *(dword*)base = (dword)__syscall_trap;
}

int WINAPI __internal_nt_dbg(dword arg1, dword arg2, dword arg3, dword arg4, dword arg5) {
    printf("%X %s\n",arg1,arg2);
    return 0;
}

byte _nt_dbg_asm[] = {
    0xb8, 0x78, 0x56, 0x34, 0x12, 0xff, 0xe0
};

void init_ntdll(pe_info * info) {
    printf("init ntdll\n");
    int i;
    for(i=0;i<info->export_table_sz;i++) {
        
        if (!strcmp("LdrInitializeThunk",info->export_table[i].name)) {
            printf("%s %X\n",info->export_table[i].name,info->export_table[i].vaddr);
#ifdef _DBG
            dbg_start(0x7C901166);
#endif    
            void WINAPI (*ep)(DWORD,DWORD,DWORD,DWORD) = (void WINAPI (*)(DWORD,DWORD,DWORD,DWORD)) (info->export_table[i].vaddr+info->ex_opt_header->image_base);
            char *tmp = (char*)malloc(1024);
            wide_str(global_config_data.base_path_remapped,(wchar_t*)tmp);
            
            if (global_config_data.use_nt_debug) {
                // dbgpoint
                *(byte*)0x7C97B1C1 = 1;
                
                // nt kernel dbg
                dword dbg_base = 0x7C92FCC4;
                *(dword*)(_nt_dbg_asm+1) = (dword)__internal_nt_dbg;
                memcpy((void*)dbg_base,_nt_dbg_asm,sizeof(_nt_dbg_asm));
            }
            
            ep(0,info->ex_opt_header->image_base,0,(DWORD)tmp);
            printf("end\n");
            break;
        }
    }
    //RtlUserThreadStart
    
}


int ld_imports(pe_info * info) {
    if (!strcmp(info->name,"ntdll.dll")) {
        return 1;
    }
    return 0;
}



static byte stub_16[] = {
    0x68, 0x78, 0x56, 0x34, 0x12, 0x68, 0x78, 0x56, 0x34, 0x12, 0x68, 0x78, 0x56, 0x34, 0x12, 0xcb
};

extern "C" {
void __32gateway(); // asm trap for 16 bits fns
}

#define NE_TYPE_MASK 7

int create_16_stub(byte * pos, uint32 idx) {
    uint32 c_cs;
    __asm volatile (
        "movl %%cs,%0;"
    :"=r"(c_cs)
    ::
    );
    
    memcpy(pos,stub_16,sizeof(stub_16));
    *(uint32*)(pos+1) = idx;
    *(uint32*)(pos+6) = c_cs;
    *(uint32*)(pos+11) = (uint32)__32gateway;
    
    return sizeof(stub_16);
}

void setup_internal_segments() {
    es_base = (byte*)malloc(0x10000);
    setup_16(ES_ENTRY_ID,es_base,0,0);
    ds_global_entry->global_es = (ES_ENTRY_ID<<3)+0x7;
    
    es_fixed_data * esfixed = (es_fixed_data*)es_base;
    strcpy(esfixed->appname,"TEST");
}

uint32 ne_exp_symbols_table[4096];

int load_exec_ne(ne_file_info * info) {
    
    
    c_hash * stubs = c_hash_create(8,1024,0);
    
    int i;
    byte * stub_area = (byte*)malloc(0x10000); // is it enough?
    byte * t = stub_area;
    char fn[256];
    
    // create a 32 bits ldt intermediate segment
    setup_16(STUBS_ENTRY_ID,stub_area,2,1);
    
    
    
    byte * cs_end = NULL;
        
    for(i=0;i<info->seg_num;i++) {
        
        if ((info->segments[i]->flags & NE_TYPE_MASK) == 0) {
            
            void * addr = mmap(NULL,info->segments[i]->length+0x1000, // extra 0x1000 for extra data
                    PROT_READ | PROT_WRITE | PROT_EXEC, MAP_FIXED | MAP_ANONYMOUS | MAP_PRIVATE,0,0);
            byte * cbase = info->data+info->segments[i]->offset*info->vashift;
            memcpy(addr,cbase,info->segments[i]->length);
            cs_end = (byte*)addr+0x1000;
            printf("ne exec segment at %X. segment size %X\n",addr,info->segments[i]->length);
            info->exec_base = (byte*)addr;            

            setup_16(0x66,addr,2,0);
            info->cs = 0x66;
        }
        else {
            // data segment
            
            void * addr = mmap(NULL,0x10000,//info->segments[i]->length,
                    PROT_READ | PROT_WRITE | PROT_EXEC, MAP_ANONYMOUS | MAP_PRIVATE,0,0);
            byte * cbase = info->data+info->segments[i]->offset*info->vashift;
            memcpy(addr,cbase,info->segments[i]->length);
        
            mem16_setup(addr,info->segments[i]->length,MEM16_STACK_BASE);
            printf("ne data segment at %X\n",addr);
            setup_16(0x67,addr,0,0);
            info->ds = 0x67;
            info->mm_base = (byte*)addr;
            
            global_ds_base = (uint32)addr;
            ds_global_entry = (ds_global_data*)((byte*)addr+MM_DS_BASE); // reserve some space at ds end for control data
            
        }
    }
    
    ds_global_entry->global_cs = 0x66;
    ds_global_entry->global_ds = 0x67;
    
    __asm volatile (
        "movl %%cs,%0;"
        "movl %%ds,%1;"
        "movl %%es,%2;"
        "movl %%fs,%3;"
        "movl %%gs,%4;"
        "movl %%ss,%5;"
    :"=r"(ds_global_entry->valid_cs),
            "=r"(ds_global_entry->valid_ds),
            "=r"(ds_global_entry->valid_es),
            "=r"(ds_global_entry->valid_fs),
            "=r"(ds_global_entry->valid_gs),
            "=r"(ds_global_entry->valid_ss)
    ::
    );
    
    setup_internal_segments();
    
    for(i=0;i<info->relocs_size;i++) {
        if (info->relocs[i].type & RELOC_TYPE_ORD) {
            char * name = info->import_table[info->relocs[i].idx].module;
            if ((info->relocs[i].type & 0xFF) == 3) {
                
                uint32 bidx = (info->relocs[i].idx << 16) + info->relocs[i].ord;
                void * sst = c_hash_get_k(bidx,(void*)bidx,stubs);
                byte * stub;
                if (sst) {
                    stub = (byte*) sst;
                }
                else {
                    
                    int rt = get_ne_symbol(name, info->relocs[i].ord, fn);
                  //  printf("Associated fn %s\n",fn);
                    int glix = get_global_fn_idx(name,info->relocs[i].ord);
                    int sz = create_16_stub(t,glix);
                    stub = t;
                    c_hash_put_k(bidx,(void*)bidx,t,stubs);
                    t += sz;
                    
                    void * symbol = sym_lookup(fn);

                    ne_exp_symbols_table[glix] = (uint32)symbol;
                    //printf("%s %X %X %X\n",fn,(stub-stub_area),glix,info->relocs[i].offset);
                }
                
                uint32 chain = 0;
                uint32 offset = info->relocs[i].offset;
                do {
                    chain = *(uint32*)(info->exec_base+offset);
                  //  printf("local offset %X %X\n",offset,chain);

                    uint32 soff = (stub-stub_area); // offset in the segment
                    uint32 seg = (STUBS_ENTRY_ID<<3) + 0x7;
                    soff += (seg<<16);

                    memcpy(info->exec_base+offset,&soff,sizeof(uint32));
                    offset = chain;
                } while (chain != 0xFFFF);
                
            }
            else {
                int rt = get_ne_symbol(name, info->relocs[i].ord, fn);
                if (rt != 3) {
                    printf("Bad symbol\n");
                    exit(0);
                }
                
                if (!strcmp(fn,"__winflags")) {
                    *(unsigned short int*)cs_end = 0x01;
                    word csoff = cs_end-info->exec_base;
                    memcpy(info->exec_base+info->relocs[i].offset,cs_end,sizeof(word));
                    printf("wfl %X\n",info->relocs[i].offset);
                    
                }
                else {
                    printf("Bad symbol\n");
                    exit(0);
                }
            }
        }
        else if (info->relocs[i].type & RELOC_TYPE_INTERNAL) {
            uint32 chain = 0;
            uint32 offset = info->relocs[i].offset;
            uint32 xseg = 0;
            /* FIXME get segment index from segment table */
            if (info->relocs[i].ord == 1) {
                xseg = (CS_ENTRY_ID<<3) + 0x7;
            }
            else if (info->relocs[i].ord ==2) {
                xseg = (DS_ENTRY_ID<<3) + 0x7;    
            }
            
            do {
                chain = *(word*)(info->exec_base+offset);
                memcpy(info->exec_base+offset,&xseg,sizeof(uint32));
                //printf("chain %X %X\n",offset,chain);
                offset = chain;
            } while (chain != 0xFFFF);
            printf("internal ref %X %i\n",info->relocs[i].offset,info->relocs[i].ord);
        }
        //printf("reloc %s %i\n",name,info->relocs[i].idx);
    }
    
    printf("NE loaded\n");
    printf("Stubs base at %X\n",stub_area);
    
    
    return 0;
}

//pe
int load_exec(pe_info * info) {
    int start = 0;
    if (global_config_data.exec_mode == 1) {
        if (!peb) {
            build_peb(info);
            setup_tls();
            mmap((void*)0x7FFE0000,0x500, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_ANONYMOUS | MAP_PRIVATE,0,0);
            w_strcpy((wchar_t*)0x7FFE0030,L"C:\\WINDOWS");
            
            setup_syscalls(0x7FFE0300);
            mmap((void*)info->ex_opt_header->image_base,0x1000,PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE,0,0);
            memcpy((void*)info->ex_opt_header->image_base,info->base,0x1000);
            *(byte*)0x7FFE02D0 = 0x10;
            start = 1;
            
        }
    }
    
    pe_dir_entry * reloc = (pe_dir_entry*) (&info->ex_opt_header->base_reloc_table);
    mprot mprots[32];
    int mprots_sz = 0;
    log_info("Loading image...");
    int i;
    
    mmap((void*)info->ex_opt_header->image_base,0x1000,PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE,0,0);
    memcpy((void*)info->ex_opt_header->image_base,info->base,0x1000);
    for(i=0;i<info->sects_num;i++) {
        int prot = 0;
        if (info->sects[i]->characteristics & IMAGE_SCN_MEM_EXECUTE) {
            prot |= PROT_EXEC;
        }
        if (info->sects[i]->characteristics & IMAGE_SCN_MEM_READ) {
            prot |= PROT_READ;
        }
        if (info->sects[i]->characteristics & IMAGE_SCN_MEM_WRITE) {
            prot |= PROT_WRITE;
        }
        
        dword tprot = prot | PROT_WRITE; // enable write so we can write initially
        void * taddr = (void*)(info->sects[i]->vaddr+info->ex_opt_header->image_base);
        //printf("mmap %X\n",taddr);
        dword size = info->sects[i]->raw_data_size > info->sects[i]->vsize ? info->sects[i]->raw_data_size : info->sects[i]->vsize;
        void * addr = mmap(taddr,
                size, tprot, MAP_ANONYMOUS | MAP_PRIVATE, 
                0,0);
        
        if (!addr || addr != taddr) {
            log_error("Could not mmap memory at %X (%s) ",info->sects[i]->vaddr+info->ex_opt_header->image_base,strerror(errno));
            printf("fail\n");
            exit(0);
            return -1;
        }
        
        //printf("%X\n",addr);
        memset(addr,0,info->sects[i]->raw_data_size);
        if (!(info->sects[i]->characteristics & IMAGE_SCN_CNT_UNINITIALIZED_DATA ) && 
                !(info->sects[i]->characteristics &IMAGE_SCN_MEM_DISCARDABLE)) {
          //  printf("write on %X %X\n",addr,info->sects[i]->raw_data_size);
            memcpy(addr,info->raw + info->sects[i]->raw_data_ptr,info->sects[i]->raw_data_size);
        }
        mprots[mprots_sz].address = addr;
        mprots[mprots_sz].size = info->sects[i]->raw_data_size;
        mprots[mprots_sz].flags = prot;
        mprots_sz++;
        //mprotect(addr,info->sects[i]->raw_data_size,prot);
    }
    
    if (global_config_data.exec_mode == 1) {
        if (start) {
            pe_info * mp = lload("ntdll.dll");
            exit(0);
        }
    }
    FILE * fp = fopen("rr","wb");
   // printf("Total imports %i\n",info->import_table_sz);
    if (global_config_data.exec_mode == 0) {
        
        for(i=0;i<info->import_table_sz;i++) {
            //printf("%s %s ",info->import_table[i].name,info->import_table[i].libname);
           // printf("%X\n",info->import_table[i].vaddr+info->ex_opt_header->image_base);
            void * addr = sym_lookup(info->import_table[i].name);
            fwrite(&addr,1,4,fp);
            fflush(fp);
            if (addr) {
                
                *(dword*)(info->import_table[i].vaddr+info->ex_opt_header->image_base) = (dword)addr;
            }
            else {
                *(dword*)(info->import_table[i].vaddr+info->ex_opt_header->image_base) = (dword)build_func(pmaps_sz);
                sprintf(pmaps[pmaps_sz].libname,"%s %s",info->import_table[i].name,info->import_table[i].libname);
                pmaps[pmaps_sz].map = (info->import_table[i].vaddr+info->ex_opt_header->image_base);
                pmaps_sz++;

            }

        }
    }
    else if (global_config_data.exec_mode == 1) {
        
        if (ld_imports(info)) {
        
            for(i=0;i<info->import_table_sz;i++) {
              //  printf("%s %s\n",info->import_table[i].name,info->import_table[i].libname);
                pe_info * mp = lload(info->import_table[i].libname);

                if (!mp->export_table) {
                    printf("failed to import\n");
                    exit(0);
                }

                int j;
                for(j=0;j<mp->export_table_sz;j++) {
    //                printf("%i %s %s\n",i,mp->export_table[j].name,info->import_table[i].name);
                    if (!strcmp(mp->export_table[j].name,info->import_table[i].name)) {
                        if (mp->export_table[j].forward) {
                          //  printf("forward\n");
                            int i = l_lastindexof(mp->export_table[j].fwd_string,'.');
                            char mmx[256];
                            strcpy(mmx,mp->export_table[j].fwd_string);
                            *(mmx+i) = 0;

                         //   printf("%s %s\n",mmx,mmx+i+1);
                            pe_info * mtp;
                            if (!strcmp(mmx,"NTDLL")) {
                                mtp = lload("ntdll.dll");                         
                            }
                            else {
                                mtp = lload(mmx);
                            }

                            int k;
                            for(k=0;k<mtp->export_table_sz;k++) {
                                if (!strcmp(mtp->export_table[k].name,mmx+i+1)) {

                                    if (mtp->export_table[k].forward) {
                                        printf("nested forward\n");
                                        exit(0);
                                    }
                                    else {
                                        dword addr = mtp->export_table[k].vaddr+mp->ex_opt_header->image_base;
                                       // printf("export forward %X %s %s\n",addr,info->import_table[i].name,info->import_table[i].libname);
                                        //printf("%X\n",info->import_table[i].vaddr+info->ex_opt_header->image_base);
                                        *(dword*)(info->import_table[i].vaddr+info->ex_opt_header->image_base) = addr;
                                    }
                                    break;
                                }
                            }
                            if (k >= mtp->export_table_sz) {
                                printf("failed export\n");
                                exit(0);
                            }
                        }
                        else {

                            dword addr = mp->export_table[j].vaddr+mp->ex_opt_header->image_base;
                           // printf("export %X %s %s\n",addr,info->import_table[i].name,info->import_table[i].libname);
                           // printf("%X\n",info->import_table[i].vaddr+info->ex_opt_header->image_base);
                            *(dword*)(info->import_table[i].vaddr+info->ex_opt_header->image_base) = addr;
                        }
                        break;
                    }
                }
                if (j>=mp->export_table_sz) {
                    printf("failed export %s %s\n",info->import_table[i].name,info->import_table[i].libname);
                    exit(0);
                }
            }
        }
        printf("load complete\n");
        
    }
    
    for(i=0;i<mprots_sz;i++) {
      //  mprotect(mprots[i].address,mprots[i].size,mprots[i].flags);
    }
    
    if (global_config_data.exec_mode == 1) {
     //   printf("lib entry point for %s\n",info->name);
        dword lep = info->ex_opt_header->entry_point + info->ex_opt_header->image_base;
        char ppath[512];
        sprintf(ppath,"%s/ntdll.dll",SHELF,info->name);
        
#ifdef _DBG
        if (!fork_child && !strcmp(ppath,info->name)) {
//            struct sigaction sa;
//
//            memset(&sa, 0, sizeof (struct sigaction));
//            sigemptyset(&sa.sa_mask);
//            sa.sa_sigaction = ssbg;
//            sa.sa_flags = SA_SIGINFO;
//
//            sigaction(SIGTRAP, &sa, NULL);

            for(i=0;i<mprots_sz;i++) {
                mprotect(mprots[i].address,mprots[i].size,PROT_READ | PROT_WRITE | PROT_EXEC);
            }
        }
     //   *(byte*)lep = 0xCC;
        
        

#endif
        
        if (!strcmp(ppath,info->name)) {
            // ntdll has some special case init first
            init_ntdll(info);
        }
        //int rt = ((int(*)(dword,dword,dword))lep)(info->ex_opt_header->image_base,1,0);
#ifdef _DBG
        //exit(0);
#endif
       /* printf("load %i\n",rt);
        if (!rt) {
            printf("failed to load library\n");
            exit(0);
        }
        finish_lib_load(info);*/
    }
    printf("Image loaded %s\n",info->name);
    log_info("Image loaded %s\n",info->name);
    
    
    
}


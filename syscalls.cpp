#include "common.h"
#include "winm.h"
#include "fs.h"
#include <stdarg.h>
#include <time.h>
#include "exec.h"
#include "fsctl.h"

#define STATUS_WAIT_0                    ((DWORD   )0x00000000L)    
#define STATUS_ABANDONED_WAIT_0          ((DWORD   )0x00000080L)    
#define STATUS_USER_APC                  ((DWORD   )0x000000C0L)    
#define STATUS_TIMEOUT                   ((DWORD   )0x00000102L)    
#define STATUS_PENDING                   ((DWORD   )0x00000103L)    
#define DBG_EXCEPTION_HANDLED            ((DWORD   )0x00010001L)    
#define DBG_CONTINUE                     ((DWORD   )0x00010002L)    
#define STATUS_SEGMENT_NOTIFICATION      ((DWORD   )0x40000005L)    
#define DBG_TERMINATE_THREAD             ((DWORD   )0x40010003L)    
#define DBG_TERMINATE_PROCESS            ((DWORD   )0x40010004L)    
#define DBG_CONTROL_C                    ((DWORD   )0x40010005L)    
#define DBG_CONTROL_BREAK                ((DWORD   )0x40010008L)    
#define DBG_COMMAND_EXCEPTION            ((DWORD   )0x40010009L)    
#define STATUS_GUARD_PAGE_VIOLATION      ((DWORD   )0x80000001L)    
#define STATUS_DATATYPE_MISALIGNMENT     ((DWORD   )0x80000002L)    
#define STATUS_BREAKPOINT                ((DWORD   )0x80000003L)    
#define STATUS_SINGLE_STEP               ((DWORD   )0x80000004L)    
#define DBG_EXCEPTION_NOT_HANDLED        ((DWORD   )0x80010001L)    
#define STATUS_ACCESS_VIOLATION          ((DWORD   )0xC0000005L)    
#define STATUS_IN_PAGE_ERROR             ((DWORD   )0xC0000006L)    
#define STATUS_INVALID_HANDLE            ((DWORD   )0xC0000008L)    
#define STATUS_NO_MEMORY                 ((DWORD   )0xC0000017L)    
#define STATUS_ILLEGAL_INSTRUCTION       ((DWORD   )0xC000001DL)    
#define STATUS_NONCONTINUABLE_EXCEPTION  ((DWORD   )0xC0000025L)    
#define STATUS_INVALID_DISPOSITION       ((DWORD   )0xC0000026L)    
#define STATUS_ARRAY_BOUNDS_EXCEEDED     ((DWORD   )0xC000008CL)    
#define STATUS_FLOAT_DENORMAL_OPERAND    ((DWORD   )0xC000008DL)    
#define STATUS_FLOAT_DIVIDE_BY_ZERO      ((DWORD   )0xC000008EL)    
#define STATUS_FLOAT_INEXACT_RESULT      ((DWORD   )0xC000008FL)    
#define STATUS_FLOAT_INVALID_OPERATION   ((DWORD   )0xC0000090L)    
#define STATUS_FLOAT_OVERFLOW            ((DWORD   )0xC0000091L)    
#define STATUS_FLOAT_STACK_CHECK         ((DWORD   )0xC0000092L)    
#define STATUS_FLOAT_UNDERFLOW           ((DWORD   )0xC0000093L)    
#define STATUS_INTEGER_DIVIDE_BY_ZERO    ((DWORD   )0xC0000094L)    
#define STATUS_INTEGER_OVERFLOW          ((DWORD   )0xC0000095L)    
#define STATUS_PRIVILEGED_INSTRUCTION    ((DWORD   )0xC0000096L)    
#define STATUS_STACK_OVERFLOW            ((DWORD   )0xC00000FDL)    
#define STATUS_CONTROL_C_EXIT            ((DWORD   )0xC000013AL)    
#define STATUS_FLOAT_MULTIPLE_FAULTS     ((DWORD   )0xC00002B4L)    
#define STATUS_FLOAT_MULTIPLE_TRAPS      ((DWORD   )0xC00002B5L)    
#define STATUS_REG_NAT_CONSUMPTION       ((DWORD   )0xC00002C9L)    
//#if defined(STATUS_SUCCESS) || (_WIN32_WINNT > 0x0500) || (_WIN32_FUSION >= 0x0100) 
#define STATUS_SXS_EARLY_DEACTIVATION    ((DWORD   )0xC015000FL)    
#define STATUS_SXS_INVALID_DEACTIVATION  ((DWORD   )0xC0150010L)    
#define STATUS_OBJECT_NAME_NOT_FOUND 0xC0000034

#define NtAcceptConnectPort 0x0000
#define NtAccessCheck 0x0001
#define NtAccessCheckAndAuditAlarm 0x0002
#define NtAccessCheckByType 0x0003
#define NtAccessCheckByTypeAndAuditAlarm 0x0004
#define NtAccessCheckByTypeResultList 0x0005
#define NtAccessCheckByTypeResultListAndAuditAlarm 0x0006
#define NtAccessCheckByTypeResultListAndAuditAlarmByHandle 0x0007
#define NtAddAtom 0x0008
#define NtAddBootEntry 0x0009
#define NtAdjustGroupsToken 0x000a
#define NtAdjustPrivilegesToken 0x000b
#define NtAlertResumeThread 0x000c
#define NtAlertThread 0x000d
#define NtAllocateLocallyUniqueId 0x000e
#define NtAllocateUserPhysicalPages 0x000f
#define NtAllocateUuids 0x0010
#define NtAllocateVirtualMemory 0x0011
#define NtAreMappedFilesTheSame 0x0012
#define NtAssignProcessToJobObject 0x0013
#define NtCallbackReturn 0x0014
#define NtCancelDeviceWakeupRequest 0x0015
#define NtCancelIoFile 0x0016
#define NtCancelTimer 0x0017
#define NtClearEvent 0x0018
#define NtClose 0x0019
#define NtCloseObjectAuditAlarm 0x001a
#define NtCompactKeys 0x001b
#define NtCompareTokens 0x001c
#define NtCompleteConnectPort 0x001d
#define NtCompressKey 0x001e
#define NtConnectPort 0x001f
#define NtContinue 0x0020
#define NtCreateDebugObject 0x0021
#define NtCreateDirectoryObject 0x0022
#define NtCreateEvent 0x0023
#define NtCreateEventPair 0x0024
#define NtCreateFile 0x0025
#define NtCreateIoCompletion 0x0026
#define NtCreateJobObject 0x0027
#define NtCreateJobSet 0x0028
#define NtCreateKey 0x0029
#define NtCreateKeyedEvent 0x0117
#define NtCreateMailslotFile 0x002a
#define NtCreateMutant 0x002b
#define NtCreateNamedPipeFile 0x002c
#define NtCreatePagingFile 0x002d
#define NtCreatePort 0x002e
#define NtCreateProcess 0x002f
#define NtCreateProcessEx 0x0030
#define NtCreateProfile 0x0031
#define NtCreateSection 0x0032
#define NtCreateSemaphore 0x0033
#define NtCreateSymbolicLinkObject 0x0034
#define NtCreateThread 0x0035
#define NtCreateTimer 0x0036
#define NtCreateToken 0x0037
#define NtCreateWaitablePort 0x0038
#define NtDebugActiveProcess 0x0039
#define NtDebugContinue 0x003a
#define NtDelayExecution 0x003b
#define NtDeleteAtom 0x003c
#define NtDeleteBootEntry 0x003d
#define NtDeleteFile 0x003e
#define NtDeleteKey 0x003f
#define NtDeleteObjectAuditAlarm 0x0040
#define NtDeleteValueKey 0x0041
#define NtDeviceIoControlFile 0x0042
#define NtDisplayString 0x0043
#define NtDuplicateObject 0x0044
#define NtDuplicateToken 0x0045
#define NtEnumerateBootEntries 0x0046
#define NtEnumerateKey 0x0047
#define NtEnumerateSystemEnvironmentValuesEx 0x0048
#define NtEnumerateValueKey 0x0049
#define NtExtendSection 0x004a
#define NtFilterToken 0x004b
#define NtFindAtom 0x004c
#define NtFlushBuffersFile 0x004d
#define NtFlushInstructionCache 0x004e
#define NtFlushKey 0x004f
#define NtFlushVirtualMemory 0x0050
#define NtFlushWriteBuffer 0x0051
#define NtFreeUserPhysicalPages 0x0052
#define NtFreeVirtualMemory 0x0053
#define NtFsControlFile 0x0054
#define NtGetContextThread 0x0055
#define NtGetDevicePowerState 0x0056
#define NtGetPlugPlayEvent 0x0057
#define NtGetWriteWatch 0x0058
#define NtImpersonateAnonymousToken 0x0059
#define NtImpersonateClientOfPort 0x005a
#define NtImpersonateThread 0x005b
#define NtInitializeRegistry 0x005c
#define NtInitiatePowerAction 0x005d
#define NtIsProcessInJob 0x005e
#define NtIsSystemResumeAutomatic 0x005f
#define NtListenPort 0x0060
#define NtLoadDriver 0x0061
#define NtLoadKey 0x0062
#define NtLoadKey2 0x0063
#define NtLockFile 0x0064
#define NtLockProductActivationKeys 0x0065
#define NtLockRegistryKey 0x0066
#define NtLockVirtualMemory 0x0067
#define NtMakePermanentObject 0x0068
#define NtMakeTemporaryObject 0x0069
#define NtMapUserPhysicalPages 0x006a
#define NtMapUserPhysicalPagesScatter 0x006b
#define NtMapViewOfSection 0x006c
#define NtModifyBootEntry 0x006d
#define NtNotifyChangeDirectoryFile 0x006e
#define NtNotifyChangeKey 0x006f
#define NtNotifyChangeMultipleKeys 0x0070
#define NtOpenDirectoryObject 0x0071
#define NtOpenEvent 0x0072
#define NtOpenEventPair 0x0073
#define NtOpenFile 0x0074
#define NtOpenIoCompletion 0x0075
#define NtOpenJobObject 0x0076
#define NtOpenKey 0x0077
#define NtOpenKeyedEvent 0x0118
#define NtOpenMutant 0x0078
#define NtOpenObjectAuditAlarm 0x0079
#define NtOpenProcess 0x007a
#define NtOpenProcessToken 0x007b
#define NtOpenProcessTokenEx 0x007c
#define NtOpenSection 0x007d
#define NtOpenSemaphore 0x007e
#define NtOpenSymbolicLinkObject 0x007f
#define NtOpenThread 0x0080
#define NtOpenThreadToken 0x0081
#define NtOpenThreadTokenEx 0x0082
#define NtOpenTimer 0x0083
#define NtPlugPlayControl 0x0084
#define NtPowerInformation 0x0085
#define NtPrivilegeCheck 0x0086
#define NtPrivilegeObjectAuditAlarm 0x0087
#define NtPrivilegedServiceAuditAlarm 0x0088
#define NtProtectVirtualMemory 0x0089
#define NtPulseEvent 0x008a
#define NtQueryAttributesFile 0x008b
#define NtQueryBootEntryOrder 0x008c
#define NtQueryBootOptions 0x008d
#define NtQueryDebugFilterState 0x008e
#define NtQueryDefaultLocale 0x008f
#define NtQueryDefaultUILanguage 0x0090
#define NtQueryDirectoryFile 0x0091
#define NtQueryDirectoryObject 0x0092
#define NtQueryEaFile 0x0093
#define NtQueryEvent 0x0094
#define NtQueryFullAttributesFile 0x0095
#define NtQueryInformationAtom 0x0096
#define NtQueryInformationFile 0x0097
#define NtQueryInformationJobObject 0x0098
#define NtQueryInformationPort 0x0099
#define NtQueryInformationProcess 0x009a
#define NtQueryInformationThread 0x009b
#define NtQueryInformationToken 0x009c
#define NtQueryInstallUILanguage 0x009d
#define NtQueryIntervalProfile 0x009e
#define NtQueryIoCompletion 0x009f
#define NtQueryKey 0x00a0
#define NtQueryMultipleValueKey 0x00a1
#define NtQueryMutant 0x00a2
#define NtQueryObject 0x00a3
#define NtQueryOpenSubKeys 0x00a4
#define NtQueryPerformanceCounter 0x00a5
#define NtQueryPortInformationProcess 0x011b
#define NtQueryQuotaInformationFile 0x00a6
#define NtQuerySection 0x00a7
#define NtQuerySecurityObject 0x00a8
#define NtQuerySemaphore 0x00a9
#define NtQuerySymbolicLinkObject 0x00aa
#define NtQuerySystemEnvironmentValue 0x00ab
#define NtQuerySystemEnvironmentValueEx 0x00ac
#define NtQuerySystemInformation 0x00ad
#define NtQuerySystemTime 0x00ae
#define NtQueryTimer 0x00af
#define NtQueryTimerResolution 0x00b0
#define NtQueryValueKey 0x00b1
#define NtQueryVirtualMemory 0x00b2
#define NtQueryVolumeInformationFile 0x00b3
#define NtQueueApcThread 0x00b4
#define NtRaiseException 0x00b5
#define NtRaiseHardError 0x00b6
#define NtReadFile 0x00b7
#define NtReadFileScatter 0x00b8
#define NtReadRequestData 0x00b9
#define NtReadVirtualMemory 0x00ba
#define NtRegisterThreadTerminatePort 0x00bb
#define NtReleaseKeyedEvent 0x0119
#define NtReleaseMutant 0x00bc
#define NtReleaseSemaphore 0x00bd
#define NtRemoveIoCompletion 0x00be
#define NtRemoveProcessDebug 0x00bf
#define NtRenameKey 0x00c0
#define NtReplaceKey 0x00c1
#define NtReplyPort 0x00c2
#define NtReplyWaitReceivePort 0x00c3
#define NtReplyWaitReceivePortEx 0x00c4
#define NtReplyWaitReplyPort 0x00c5
#define NtRequestDeviceWakeup 0x00c6
#define NtRequestPort 0x00c7
#define NtRequestWaitReplyPort 0x00c8
#define NtRequestWakeupLatency 0x00c9
#define NtResetEvent 0x00ca
#define NtResetWriteWatch 0x00cb
#define NtRestoreKey 0x00cc
#define NtResumeProcess 0x00cd
#define NtResumeThread 0x00ce
#define NtSaveKey 0x00cf
#define NtSaveKeyEx 0x00d0
#define NtSaveMergedKeys 0x00d1
#define NtSecureConnectPort 0x00d2
#define NtSetBootEntryOrder 0x00d3
#define NtSetBootOptions 0x00d4
#define NtSetContextThread 0x00d5
#define NtSetDebugFilterState 0x00d6
#define NtSetDefaultHardErrorPort 0x00d7
#define NtSetDefaultLocale 0x00d8
#define NtSetDefaultUILanguage 0x00d9
#define NtSetEaFile 0x00da
#define NtSetEvent 0x00db
#define NtSetEventBoostPriority 0x00dc
#define NtSetHighEventPair 0x00dd
#define NtSetHighWaitLowEventPair 0x00de
#define NtSetInformationDebugObject 0x00df
#define NtSetInformationFile 0x00e0
#define NtSetInformationJobObject 0x00e1
#define NtSetInformationKey 0x00e2
#define NtSetInformationObject 0x00e3
#define NtSetInformationProcess 0x00e4
#define NtSetInformationThread 0x00e5
#define NtSetInformationToken 0x00e6
#define NtSetIntervalProfile 0x00e7
#define NtSetIoCompletion 0x00e8
#define NtSetLdtEntries 0x00e9
#define NtSetLowEventPair 0x00ea
#define NtSetLowWaitHighEventPair 0x00eb
#define NtSetQuotaInformationFile 0x00ec
#define NtSetSecurityObject 0x00ed
#define NtSetSystemEnvironmentValue 0x00ee
#define NtSetSystemEnvironmentValueEx 0x00ef
#define NtSetSystemInformation 0x00f0
#define NtSetSystemPowerState 0x00f1
#define NtSetSystemTime 0x00f2
#define NtSetThreadExecutionState 0x00f3
#define NtSetTimer 0x00f4
#define NtSetTimerResolution 0x00f5
#define NtSetUuidSeed 0x00f6
#define NtSetValueKey 0x00f7
#define NtSetVolumeInformationFile 0x00f8
#define NtShutdownSystem 0x00f9
#define NtSignalAndWaitForSingleObject 0x00fa
#define NtStartProfile 0x00fb
#define NtStopProfile 0x00fc
#define NtSuspendProcess 0x00fd
#define NtSuspendThread 0x00fe
#define NtSystemDebugControl 0x00ff
#define NtTerminateJobObject 0x0100
#define NtTerminateProcess 0x0101
#define NtTerminateThread 0x0102
#define NtTestAlert 0x0103
#define NtTraceEvent 0x0104
#define NtTranslateFilePath 0x0105
#define NtUnloadDriver 0x0106
#define NtUnloadKey 0x0107
#define NtUnloadKeyEx 0x0108
#define NtUnlockFile 0x0109
#define NtUnlockVirtualMemory 0x010a
#define NtUnmapViewOfSection 0x010b
#define NtVdmControl 0x010c
#define NtWaitForDebugEvent 0x010d
#define NtWaitForKeyedEvent 0x011a
#define NtWaitForMultipleObjects 0x010e
#define NtWaitForSingleObject 0x010f
#define NtWaitHighEventPair 0x0110
#define NtWaitLowEventPair 0x0111
#define NtWriteFile 0x0112
#define NtWriteFileGather 0x0113
#define NtWriteRequestData 0x0114
#define NtWriteVirtualMemory 0x0115
#define NtYieldExecution 0x0116

typedef HANDLE* PHANDLE;

typedef struct _SYSTEM_BASIC_INFORMATION {
    ULONG Reserved;
    ULONG TimerResolution;
    ULONG PageSize;
    ULONG NumberOfPhysicalPages;
    ULONG LowestPhysicalPageNumber;
    ULONG HighestPhysicalPageNumber;
    ULONG AllocationGranularity;
    ULONG MinimumUserModeAddress;
    ULONG MaximumUserModeAddress;
    ULONG ActiveProcessorsAffinityMask;
    char NumberOfProcessors;
} SYSTEM_BASIC_INFORMATION, *PSYSTEM_BASIC_INFORMATION;

extern "C" {
    
#define SS_RETURN(ss,rt) \
    *ssize = ss; \
    return rt;

typedef LONG NTSTATUS;
    
inline NTSTATUS _NtQueryPerformanceCounter(
    PLARGE_INTEGER PerformanceCounter,
    PLARGE_INTEGER PerformanceFrequency
) {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    PerformanceFrequency->QuadPart = 1000000000;
    PerformanceCounter->QuadPart = t.tv_sec;
    PerformanceCounter->QuadPart = PerformanceCounter->QuadPart*1000000000 + t.tv_sec;
    return 0;
    
}  

typedef HANDLE * PHANDLE;

typedef struct _OBJECT_ATTRIBUTES {
    ULONG           Length;
    HANDLE          RootDirectory;
    PUNICODE_STRING ObjectName;
    ULONG           Attributes;
    PVOID           SecurityDescriptor;
    PVOID           SecurityQualityOfService;
} OBJECT_ATTRIBUTES, *POBJECT_ATTRIBUTES;

NTSTATUS _NtQueryDebugFilterState(
    ULONG ComponentId,
    ULONG Level
    ) {
    return 0;
}

void reg_translate(POBJECT_ATTRIBUTES ObjectAttributes, char * keyname) {
    char * tx = wide_to_ansi((wchar_t *)ObjectAttributes->ObjectName->Buffer);
    
    printf("NtOpenKey %s \n",tx);
    
    strcpy(keyname,"COMPUTER\\");
    char * tk = keyname+strlen(keyname);
    tolower_str(tx);
    if (!strncmp(tx,"\\registry",9)) {
        tx = tx + 9;
    }
    if (!strncmp(tx,"\\machine",8)) {
        tx = tx + 8;
        strcpy(tk,"HKEY_LOCAL_MACHINE");
        tk+= strlen(tk);
        strcpy(tk,tx);
        
    }
    
    if (ObjectAttributes->RootDirectory) {
        tree_node * tn = (tree_node *)ObjectAttributes->RootDirectory;
        
        strcpy(tk,tn->name);
        tk+= strlen(tk);
        *tk++ = '\\';
        strcpy(tk,tx);
    }
}

NTSTATUS _NtOpenKey(
    PHANDLE            KeyHandle,
    DWORD              DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes
) {
    
    char keyname[512];
    reg_translate(ObjectAttributes,keyname);
    tree_node * tn = localRegistry->meta_search(keyname);
    printf("NtOpenKey %s %X\n",keyname,tn);
    if (!tn) {
        return STATUS_OBJECT_NAME_NOT_FOUND;
    }
    *KeyHandle = tn;
    return 0;
}

NTSTATUS _NtQueryVirtualMemory(
  HANDLE                   ProcessHandle,
  PVOID                    BaseAddress,
  DWORD                    MemoryInformationClass,
  PVOID                    MemoryInformation,
  SIZE_T                   MemoryInformationLength,
  ULONG*                   ReturnLength
) {
    if (ProcessHandle != (void*)-1) {
        printf("Unsupported NtQueryVirtualMemory - remote handle\n");
        exit(0);
    }
    printf("query at %X\n",BaseAddress);
    MEMORY_BASIC_INFORMATION * mb = (MEMORY_BASIC_INFORMATION*)MemoryInformation;
    mb->BaseAddress = BaseAddress;
    mb->Protect = PAGE_NOACCESS;
    if (ReturnLength) {
        *ReturnLength = sizeof(MEMORY_BASIC_INFORMATION);
    }
    return 0;
    
    
}



static dword events[1024];
static dword events_sz;

NTSTATUS _NtOpenKeyedEvent(PHANDLE KeyedEventHandle, DWORD DesiredAccess,
        POBJECT_ATTRIBUTES ObjectAttributes ) {
  //  printf("handle %X %X %ls\n",KeyedEventHandle,ObjectAttributes->ObjectName,ObjectAttributes->ObjectName->Buffer);
    
    *KeyedEventHandle = (HANDLE)(events+events_sz);
    
    events_sz++;
    return 0;
}
#define PAGE_READONLY          0x02     
#define PAGE_READWRITE         0x04     
#define PAGE_WRITECOPY         0x08     
#define PAGE_EXECUTE           0x10     
#define PAGE_EXECUTE_READ      0x20     
#define PAGE_EXECUTE_READWRITE 0x40     
#define PAGE_EXECUTE_WRITECOPY 0x80     



int prots2prots(dword p1) {
    switch (p1) {
        case PAGE_NOACCESS:
            return 0;
        case PAGE_READONLY:
            return PROT_READ;
        case PAGE_READWRITE:
            return PROT_READ | PROT_WRITE;
        case PAGE_EXECUTE:
            return PROT_EXEC;
        case PAGE_EXECUTE_READ:
            return PROT_EXEC | PROT_READ;
        case PAGE_EXECUTE_READWRITE:
            return PROT_READ | PROT_EXEC | PROT_WRITE;
    }
    return 0;
}


#ifdef _DBG
static FILE * fmmap = NULL;
#endif


NTSTATUS _NtAllocateVirtualMemory(
  HANDLE    ProcessHandle,
  PVOID     *BaseAddress,
  ULONG_PTR ZeroBits,
  SIZE_T*   RegionSize,
  ULONG     AllocationType,
  ULONG     Protect
) {
    if ((int)ProcessHandle != -1) {
        printf("Unsupported allocation\n");
        exit(0);
    }
    printf("Allocate %i at %X. ZeroBits %i\n",*RegionSize,*BaseAddress,ZeroBits);
#ifdef _DBG
    if (!fmmap) fmmap = fopen("mmap","rb");
    void * paddr;
    fread(&paddr,1,4,fmmap);
#endif    
    if (!*BaseAddress) {
#ifdef _DBG
        void * addr = mmap(paddr,*RegionSize,prots2prots(Protect),MAP_ANONYMOUS | MAP_PRIVATE ,0,0);
        printf("tried at %X, got %X\n",paddr,addr);
#else
        void * addr = mmap(0,*RegionSize,prots2prots(Protect),MAP_ANONYMOUS | MAP_PRIVATE ,0,0);
#endif        
        if (!addr) {
            printf("Fail to allocate properly\n");
            exit(0);
        }
        printf("alloc reply %X\n",addr);
        *BaseAddress = addr;
        return 0;
    }
    else {
        void * addr = mmap(*BaseAddress,*RegionSize,prots2prots(Protect),MAP_ANONYMOUS | MAP_PRIVATE,0,0);
        
        if (!addr) {
            printf("Fail to allocate properly\n");
            exit(0);
        }
        if (addr != *BaseAddress) {
            munmap(addr,*RegionSize);
            printf("relloc fail\n");
            
            addr = *BaseAddress;
        }
        printf("alloc reply %X\n",addr);
        *BaseAddress = addr;
        return 0;
    }
    exit(0);
    
}

typedef dword ACCESS_MASK;
typedef ULONG * PULONG;

#define KDLLS_SYMBOLIC_HANDLE ((HANDLE)2)

NTSTATUS WINAPI _NtQuerySymbolicLinkObject(
    HANDLE          LinkHandle,
    PUNICODE_STRING LinkTarget,
    PULONG          ReturnedLength
) {
    if (LinkHandle == KDLLS_SYMBOLIC_HANDLE) {
        memcpy(LinkTarget->Buffer,L"c:\\windows\\system32",38);
        LinkTarget->Length = 38;
        if (ReturnedLength) {
            *ReturnedLength = 38;
        }
        return 0;
    }
    
    exit(0);
}

NTSTATUS WINAPI _NtOpenSymbolicLinkObject(
    PHANDLE            LinkHandle,
    ACCESS_MASK        DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes
) {
    
    printf("%s\n",wide_to_ansi((wchar_t *)ObjectAttributes->ObjectName->Buffer));
    if (!w_strcmp(L"KnownDllPath",(wchar_t *)ObjectAttributes->ObjectName->Buffer)) {
        printf("known dll paths\n");
        *LinkHandle = (HANDLE)KDLLS_SYMBOLIC_HANDLE;
        return 0;
    }
    exit(0);
}


NTSTATUS _NtOpenDirectoryObject(
    PHANDLE            DirectoryHandle,
    ACCESS_MASK        DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes
) {
    printf("%s\n",wide_to_ansi((wchar_t *)ObjectAttributes->ObjectName->Buffer));
    *DirectoryHandle = (HANDLE)1;
    return 0;
    
}

typedef struct _IO_STATUS_BLOCK {
    union {
        NTSTATUS _Status;
        PVOID    Pointer;
    } DUMMYUNIONNAME;
    ULONG_PTR Information;
} IO_STATUS_BLOCK, *PIO_STATUS_BLOCK;

NTSTATUS _NtReadFile(
    HANDLE           FileHandle,
    HANDLE           Event,
    VOID *          ApcRoutine,
    PVOID            ApcContext,
    PIO_STATUS_BLOCK IoStatusBlock,
    PVOID            Buffer,
    ULONG            Length,
    PLARGE_INTEGER   ByteOffset,
    PULONG           Key
) {
    if (Event) {
        printf("Unsupported Read file operation\n");
        exit(0);
    }
    printf("Read %i on %X\n",Length,FileHandle);
    // FIXME deal with truncates in the open function
    FILE * fp = vfmap->get_phys_handle((tvfshandle*)FileHandle,"r+b");
    if (ByteOffset) {
        fseek(fp,ByteOffset->LowPart,SEEK_SET);
    }
    int rt = fread(Buffer,1,Length,fp);
    IoStatusBlock->Information = (ULONG_PTR)rt;
    
    return 0;
    
}

typedef struct _FILE_BASIC_INFORMATION {
    LARGE_INTEGER CreationTime;
    LARGE_INTEGER LastAccessTime;
    LARGE_INTEGER LastWriteTime;
    LARGE_INTEGER ChangeTime;
    ULONG         FileAttributes;
} FILE_BASIC_INFORMATION, *PFILE_BASIC_INFORMATION;

NTSTATUS _NtQueryAttributesFile(
    POBJECT_ATTRIBUTES      ObjectAttributes,
    PFILE_BASIC_INFORMATION FileInformation
) {
    char * name = wide_to_ansi((wchar_t *)ObjectAttributes->ObjectName->Buffer);
    if (!strcmp(name,"\\??\\C:\\WINDOWS\\system32\\WINMM.dll")) {
        FileInformation->CreationTime.QuadPart = 1;
        FileInformation->LastWriteTime.QuadPart = 1;
        FileInformation->LastAccessTime.QuadPart = 1;
        FileInformation->ChangeTime.QuadPart = 1;
        FileInformation->FileAttributes = 0;
        return 0;
    }
    printf("_NtQueryAttributesFile %s\n", name);
    return STATUS_OBJECT_NAME_NOT_FOUND;
    
}

#define SEC_TYPE_DLL 1
#define SEC_TYPE_NLS 2
#define SEC_TYPE_FILE 3

typedef struct _sec_handle {
    char name[512];
    int type;
    DWORD base;
    DWORD size;
    DWORD attr;
    void * virt;
    pe_info *pe;
} sec_handle;

sec_handle sec_handles[128] = {0,};
int sec_handles_sz = 0;

FILE * _getNLS(sec_handle * sh) {
    char nmap[256];
    strcpy(nmap,SHELF);
    char * t = nmap+strlen(nmap);
    *t++ = '/';

    strcpy(t,sh->name+15);
    t = nmap+strlen(nmap);
    strcpy(t,".nls");

    printf("%s\n",nmap);

    FILE * fp = fopen(nmap,"rb");
    return fp;
}


NTSTATUS _NtMapViewOfSection(
    HANDLE          SectionHandle,
    HANDLE          ProcessHandle,
    PVOID           *BaseAddress,
    ULONG_PTR       ZeroBits,
    SIZE_T          CommitSize,
    PLARGE_INTEGER  SectionOffset,
    PSIZE_T         ViewSize,
    DWORD           InheritDisposition,
    ULONG           AllocationType,
    ULONG           Win32Protect
) {
    char pmap[512];
    int idx = (int)SectionHandle-1;
    printf("map of section %X %X %X \n",idx,BaseAddress,Win32Protect);
    sec_handle * sh = sec_handles+idx;
    if (*ViewSize || SectionOffset) {
        printf("Unsupported NtMapViewOfSection1\n");
        exit(0);
    }
    if (sh->type == SEC_TYPE_DLL) {
        pe_info * pe = lload(sh->name);
        *BaseAddress = (void*)pe->ex_opt_header->image_base;
        *ViewSize = 0x100000;
        
    }
    else if (sh->type == SEC_TYPE_NLS) {
        printf("nls\n");
        FILE * fp = _getNLS(sh);
        if (!fp) {
            printf("invalid cause\n");
            exit(0);
            
        }
        fseek(fp,0,SEEK_END);
        long l = ftell(fp);
        fseek(fp,0,SEEK_SET);
        int tx = 0;
        *BaseAddress = malloc(l);
        byte * pos = (byte*)(*BaseAddress);
        int rt = 0;
        do {
            rt = fread(pos+tx,1,l,fp);
            tx += rt;
            
        } while (rt>0 && tx < l);


        fclose(fp);
        
    }
    else if (sh->type == SEC_TYPE_FILE) {        
        tvfshandle * h = (tvfshandle*)sh->virt;
        vfmap->get_real_path(h,pmap);
        
        pe_info * pe = lload2(sh->pe);
        *BaseAddress = (void*)pe->ex_opt_header->image_base;
        *ViewSize = 0x100000;
        
    }
    else {
        printf("Unsupported NtMapViewOfSection\n");
        exit(0);
    }
    
    sh->base = (dword)*BaseAddress;
    sh->size = *ViewSize;
    return 0;
}

NTSTATUS _NtOpenSection(
    PHANDLE            SectionHandle,
    ACCESS_MASK        DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes
) {
    char * name = wide_to_ansi((wchar_t *)ObjectAttributes->ObjectName->Buffer);
    *SectionHandle = (HANDLE)(sec_handles_sz+1);
    
    printf("_NtOpenSection %s %X %i\n", name,*SectionHandle);
    
    if (!strncmp(name,"WINMM",5)) {
        *SectionHandle = 0;
        printf("failing section winmm\n");

        return STATUS_OBJECT_NAME_NOT_FOUND;
    }
    
    
    
    printf("_NtOpenSection %s %X %i\n", name,*SectionHandle);
    strcpy(sec_handles[sec_handles_sz].name,name);
    tolower_str(sec_handles[sec_handles_sz].name);
    
    if (s_endswith(name,".dll")) {
        sec_handles[sec_handles_sz].type = SEC_TYPE_DLL;
    }
    else if(!strncmp(name,"\\NLS",3)) {
        sec_handles[sec_handles_sz].type = SEC_TYPE_NLS;
        
        FILE * fp = _getNLS(&sec_handles[sec_handles_sz]);
        
        if (!fp) {
            printf("STATUS_OBJECT_NAME_NOT_FOUND\n");
            return STATUS_OBJECT_NAME_NOT_FOUND;
        }
        
        fclose(fp);
    }
    else {
        printf("Unsupported NtOpenSection\n");
        exit(0);
    }
    
    sec_handles[sec_handles_sz].attr = DesiredAccess;
    
    sec_handles_sz++;
    
    return 0;
}

NTSTATUS _NtOpenFile(
    PHANDLE            FileHandle,
    ACCESS_MASK        DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes,
    PIO_STATUS_BLOCK   IoStatusBlock,
    ULONG              ShareAccess,
    ULONG              OpenOptions
) {
    char * name = wide_to_ansi((wchar_t *)ObjectAttributes->ObjectName->Buffer);
    printf("NtOpenFile %s\n",name);
    if (name[0] == '\\')  {
        if (name[1] == '?') {
            name = name+4;
        }
    }
    
    *FileHandle = vfmap->virtual_handle(name);
    printf("open file handle %X\n",*FileHandle);
            
    return 0;
}

typedef enum _FSINFOCLASS {
  FileFsVolumeInformation        ,
  FileFsLabelInformation         ,
  FileFsSizeInformation          ,
  FileFsDeviceInformation        ,
  FileFsAttributeInformation     ,
  FileFsControlInformation       ,
  FileFsFullSizeInformation      ,
  FileFsObjectIdInformation      ,
  FileFsDriverPathInformation    ,
  FileFsVolumeFlagsInformation   ,
  FileFsSectorSizeInformation    ,
  FileFsDataCopyInformation      ,
  FileFsMetadataSizeInformation  ,
  FileFsMaximumInformation
} FS_INFORMATION_CLASS, *PFS_INFORMATION_CLASS;

typedef struct _FILE_FS_ATTRIBUTE_INFORMATION {
    ULONG FileSystemAttributes;
    LONG  MaximumComponentNameLength;
    ULONG FileSystemNameLength;
    short FileSystemName[1];
} FILE_FS_ATTRIBUTE_INFORMATION, *PFILE_FS_ATTRIBUTE_INFORMATION;

NTSTATUS _NtQueryVolumeInformationFile(
  HANDLE               FileHandle,
  PIO_STATUS_BLOCK     IoStatusBlock,
  PVOID                FsInformation,
  ULONG                Length,
  DWORD FsInformationClass
) {
    printf("NtQueryVolumeInformationFile %X %X\n",FileHandle,FsInformationClass);
    switch (FsInformationClass) {
        case FileFsAttributeInformation: {
//            if (Length < sizeof(FILE_FS_ATTRIBUTE_INFORMATION)) {
//                printf("invalid len %i\n",Length);
//                return 0xC0000004;
//            }
            FILE_FS_ATTRIBUTE_INFORMATION * ff = (FILE_FS_ATTRIBUTE_INFORMATION*)FsInformation;
            ff->FileSystemAttributes = 0x1|0x2|0x4;
            ff->MaximumComponentNameLength = 0x20;
          //  ff->FileSystemNameLength = 0x80000;
           // ff->FileSystemName[0] = 0x303;
            
            IoStatusBlock->Information = (ULONG_PTR)sizeof(FILE_FS_ATTRIBUTE_INFORMATION);
            break;
        }
        default:
            printf("Unsupporrted NtQueryVolumeInformationFile\n");
            exit(0);
    }
    return 0;
}

NTSTATUS _NtFsControlFile(
    HANDLE           FileHandle,
    HANDLE           Event,
    VOID *           ApcRoutine,
    PVOID            ApcContext,
    PIO_STATUS_BLOCK IoStatusBlock,
    ULONG            FsControlCode,
    PVOID            InputBuffer,
    ULONG            InputBufferLength,
    PVOID            OutputBuffer,
    ULONG            OutputBufferLength
) {
    switch (FsControlCode) {
        case FSCTL_IS_VOLUME_MOUNTED:
            printf("is volume mounted??\n");
            IoStatusBlock->DUMMYUNIONNAME.Pointer = (PVOID)0;
            IoStatusBlock->Information = (ULONG_PTR)0;
            return 0;
    }
    exit(0);
}

NTSTATUS _NtCreateSection(
    PHANDLE            SectionHandle,
    ACCESS_MASK        DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes,
    PLARGE_INTEGER     MaximumSize,
    ULONG              SectionPageProtection,
    ULONG              AllocationAttributes,
    HANDLE             FileHandle
) {
    
    if (ObjectAttributes) {
        char * name = wide_to_ansi((wchar_t *)ObjectAttributes->ObjectName->Buffer);
        printf("_NtCreateSection %s\n",name);
        printf("Unsupported create section\n");
        exit(0);
    }
    
    printf("_NtCreateSection called. File handle %X\n",FileHandle);
    if (FileHandle) {
        if (((tvfshandle*)FileHandle)->type != DT_VIRT) {
            printf("Unsupported create section\n");
            exit(0);
        }
        *SectionHandle = (HANDLE)(sec_handles_sz+1);
        sec_handles[sec_handles_sz].type = SEC_TYPE_FILE;
        sec_handles[sec_handles_sz].virt = FileHandle;
        
        pe_info * info = (pe_info*)malloc(sizeof(pe_info));
        char pmap[512];
        vfmap->get_real_path((tvfshandle*)FileHandle,pmap);
        process_file(pmap,info);
        sec_handles[sec_handles_sz].pe = info;
        printf("Created at %X\n",*SectionHandle);
        return 0;
    }
    *SectionHandle = (HANDLE)0x35;
    return 0;
    
}

typedef struct _PORT_VIEW {
     ULONG Length;
     HANDLE SectionHandle;
     ULONG SectionOffset;
     SIZE_T ViewSize;
     PVOID ViewBase;
     PVOID ViewRemoteBase;
} PORT_VIEW, *PPORT_VIEW;

typedef struct _REMOTE_PORT_VIEW {
    ULONG Length;
    SIZE_T ViewSize;
    PVOID ViewBase;
} REMOTE_PORT_VIEW, *PREMOTE_PORT_VIEW;

typedef struct _OBJECT_DATA_INFORMATION {
    BOOLEAN                 InheritHandle;
    BOOLEAN                 ProtectFromClose;
} OBJECT_DATA_INFORMATION, *POBJECT_DATA_INFORMATION;


typedef struct _unk_conn_info {
    dword u1;
    dword u2;
    dword u3;
    dword u4;
    void * u5;
} unk_conn_info;

NTSTATUS  _NtSecureConnectPort(
    PHANDLE PortHandle,
    PUNICODE_STRING PortName,
    void* SecurityQos,
    PPORT_VIEW ClientView,
    void* ServerSid,
    PREMOTE_PORT_VIEW ServerView,
    PULONG MaxMessageLength,
    PVOID ConnectionInformation,
    PULONG ConnectionInformationLength
    ) {
    
    printf(" sec %X %X %X %X %X %X %X %X %X\n",*PortHandle,PortName,SecurityQos,ClientView,ServerSid,ServerView,MaxMessageLength,ConnectionInformation,ConnectionInformationLength);
    char * name = wide_to_ansi((wchar_t *)PortName->Buffer);
    printf("%s\n",name);
    unk_conn_info * uic = (unk_conn_info*)ConnectionInformation;
    uic->u5 = malloc(0x100);
    printf("u5 %X\n",uic->u5);
    
    *(dword*)((dword)uic->u5+4) = (dword)malloc(0x100);
    UNICODE_STRING * s1 = *(UNICODE_STRING**)((dword)uic->u5+4);
    void * ubase = (void*)s1;
    s1->Buffer = (WORD*)L"C:\\WINDOWS";
    s1->Length = s1->MaximumLength = _wcslen((wchar_t*)s1->Buffer)*2;
    s1++;
    s1->Buffer = (WORD*)L"C:\\WINDOWS\\SYSTEM32";
    s1->Length = s1->MaximumLength = _wcslen((wchar_t*)s1->Buffer)*2;
    s1++;
    s1->Buffer = (WORD*)L"\\BaseNamedObjects";
    s1->Length = s1->MaximumLength = _wcslen((wchar_t*)s1->Buffer)*2;
    
    dword ppos = (dword)ubase+0x22;
    printf("ppos %X\n",ppos);
    w_strcpy((wchar_t*)(ppos),L"Service Pack 3");
    
    ClientView->ViewBase = malloc(0x2000);

    
    *(dword*)ConnectionInformationLength = 0x20;
    *MaxMessageLength = 0xc8;
    ServerView->Length = 0;
    ClientView->Length = 0;
    *PortHandle = (HANDLE)0x36;
    return 0;
}

typedef enum _OBJECT_INFORMATION_CLASS {
    ObjectBasicInformation,
    ObjectNameInformation,
    ObjectTypeInformation,
    ObjectAllInformation,
    ObjectDataInformation
} OBJECT_INFORMATION_CLASS;

typedef struct _PUBLIC_OBJECT_BASIC_INFORMATION {
    ULONG       Attributes;
    ACCESS_MASK GrantedAccess;
    ULONG       HandleCount;
    ULONG       PointerCount;
    ULONG       Reserved[10];
} PUBLIC_OBJECT_BASIC_INFORMATION, *PPUBLIC_OBJECT_BASIC_INFORMATION;

NTSTATUS _NtQueryObject(
    HANDLE                   Handle,
    DWORD                    ObjectInformationClass,
    PVOID                    ObjectInformation,
    ULONG                    ObjectInformationLength,
    PULONG                   ReturnLength
) {
    printf("NtQueryObject %X %i\n",Handle,ObjectInformationClass);
    switch (ObjectInformationClass) {
        case 4: {
            OBJECT_DATA_INFORMATION * obi = (OBJECT_DATA_INFORMATION*)ObjectInformation;
            if (ReturnLength) *ReturnLength = sizeof(OBJECT_DATA_INFORMATION);
            obi->InheritHandle = 1;
            obi->ProtectFromClose = 1;
            return 0;
        }
        default:
            printf("Unsupported NtQueryObject\n");
            exit(0);
    }
    return 0;
}

NTSTATUS _NtSetInformationObject(
    HANDLE               ObjectHandle,
    DWORD ObjectInformationClass,
    PVOID                ObjectInformation,
    ULONG                Length
  ) {
    printf("%X %X\n",ObjectHandle,ObjectInformationClass);
    return 0;
}

typedef struct _LPC_MESSAGE {
  USHORT                  DataLength;
  USHORT                  Length;
  USHORT                  MessageType; //0x4
  USHORT                  DataInfoOffset;
  CLIENT_ID               ClientId; //0x8
  ULONG                   MessageId; //0x10
  ULONG                   CallbackId; //0x14
  

} LPC_MESSAGE, *PLPC_MESSAGE;

NTSTATUS _NtRequestWaitReplyPort(
    HANDLE               PortHandle,
    PLPC_MESSAGE         Request,
    PLPC_MESSAGE         IncomingReply 
  ) {
    printf("%X\n",Request->MessageType);
    
    byte * payload = (byte*)(IncomingReply+1);
    memset(payload,0,0x10);
    return 0;
}

NTSTATUS _NtRegisterThreadTerminatePort(
    HANDLE PortHandle
    ) {
    return 0;
}

typedef enum _KEY_VALUE_INFORMATION_CLASS {
  KeyValueBasicInformation           ,
  KeyValueFullInformation            ,
  KeyValuePartialInformation         ,
  KeyValueFullInformationAlign64     ,
  KeyValuePartialInformationAlign64  ,
  KeyValueLayerInformation           ,
  MaxKeyValueInfoClass
} KEY_VALUE_INFORMATION_CLASS;

typedef struct _KEY_VALUE_PARTIAL_INFORMATION {
  ULONG TitleIndex;
  ULONG Type;
  ULONG DataLength;
  UCHAR Data[1];
} KEY_VALUE_PARTIAL_INFORMATION, *PKEY_VALUE_PARTIAL_INFORMATION;

NTSTATUS _NtRaiseHardError(
   NTSTATUS             ErrorStatus,
   ULONG                NumberOfParameters,
   PUNICODE_STRING      UnicodeStringParameterMask ,
   PVOID*                Parameters,
   DWORD ResponseOption,
   DWORD Response 
  ) {
    printf("Total failure %X\n",ErrorStatus);
    exit(0);
}

NTSTATUS _NtQueryValueKey(
  HANDLE                      KeyHandle,
  PUNICODE_STRING             ValueName,
  DWORD                          KeyValueInformationClass,
  PVOID                       KeyValueInformation,
  ULONG                       Length,
  PULONG                      ResultLength
) {
    
    char * name = wide_to_ansi((wchar_t *)ValueName->Buffer);
    //printf("NtQueryValueKey %s %X\n",name,KeyValueInformationClass);
    
    switch (KeyValueInformationClass) {
        
        case KeyValuePartialInformation: {
            KEY_VALUE_PARTIAL_INFORMATION * kif = (KEY_VALUE_PARTIAL_INFORMATION*)KeyValueInformation;
            
            if (Length < sizeof(KEY_VALUE_PARTIAL_INFORMATION)) {
                printf("Error NtQueryValueKey\n");
                exit(0);
                // return STATUS_BUFFER_TOO_SMALL;
            }
            tree_node * tn = (tree_node*)KeyHandle;
            
            tree_node * tf = localRegistry->node_look_up(tn,name);
            if (!tf) {
               // printf("not found\n");
                return STATUS_OBJECT_NAME_NOT_FOUND;
            }
            if (tf->ltype == 0) {
                printf("Unsupported NtQueryValueKey\n");
                // not a leaf
                exit(0);
            }
            tree_leaf_node * tlf = (tree_leaf_node*)tf;
            kif->Type = tlf->type;
            int total_size = sizeof(KEY_VALUE_PARTIAL_INFORMATION);
            switch (kif->Type ) {
                case REG_SZ: {
                    char * str; 
                    int sz = localRegistry->leaf_get_data(tlf,(void**)&str);
                    
                    if (Length < sizeof(KEY_VALUE_PARTIAL_INFORMATION)+sz) {
                        printf("Error NtQueryValueKey\n");
                        exit(0);
                        // return STATUS_BUFFER_TOO_SMALL;
                    }
                    total_size += sz;
                    strncpy((char*)&kif->Data[0],str,sz);
                    break;
                }
                case REG_DWORD:
                    *(dword*)&kif->Data[0] = tlf->data[0];
                    break;
                default:
                    printf("Unsupported NtQueryValueKey\n");                
                    exit(0);
            }
            
            if (ResultLength) {
                *ResultLength = total_size;
            }
            
            
            return 0;
        }
            
        default:
            printf("Unsupported NtQueryValueKey\n");
            exit(0);
    }
    exit(0);
    
}

typedef DWORD* PDWORD;

NTSTATUS _NtQueryDefaultLocale(
    int              UserProfile,
    PDWORD               DefaultLocaleId
) {
    *DefaultLocaleId = 0x409;
    return 0;
}

typedef struct _SECTION_BASIC_INFORMATION {
  PVOID         Base;
  ULONG         Attributes;
  LARGE_INTEGER Size;
} SECTION_BASIC_INFORMATION;

typedef enum _SECTION_INFORMATION_CLASS {
    SectionBasicInformation,
    SectionImageInformation
} SECTION_INFORMATION_CLASS;

typedef struct _SECTION_IMAGE_INFORMATION
{
     PVOID TransferAddress;
     ULONG ZeroBits;
     ULONG MaximumStackSize;
     ULONG CommittedStackSize;
     ULONG SubSystemType; // 0x10
     union
     {
          struct
          {
               WORD SubSystemMinorVersion;
               WORD SubSystemMajorVersion;
          };
          ULONG SubSystemVersion;
     };
     ULONG GpValue;
     WORD ImageCharacteristics;
     WORD DllCharacteristics;
     //WORD Machine;
     char * str;  // 0x20
     ULONG unk1;
//     UCHAR ImageContainsCode;
//     UCHAR ImageFlags;
//     ULONG ComPlusNativeReady: 1;
//     ULONG ComPlusILOnly: 1;
//     ULONG ImageDynamicallyRelocated: 1;
//     ULONG Reserved: 5;
     ULONG LoaderFlags;
     ULONG ImageFileSize;
     ULONG CheckSum; // 0x30
} SECTION_IMAGE_INFORMATION, *PSECTION_IMAGE_INFORMATION;

NTSTATUS _NtQuerySection(
    HANDLE              SectionHandle,
    DWORD               InformationClass,
    PVOID               InformationBuffer,
    ULONG               InformationBufferSize,
    PULONG              ResultLength    
  ) {
    printf("query over %X %i\n",SectionHandle,InformationClass);
    int idx = (int)SectionHandle-1;
    sec_handle * sh = sec_handles+idx;
    
    if (idx < 0 || idx > 255) {
        printf("invalid\n");
        exit(0);
    }
    
    if (InformationClass == SectionBasicInformation) {
        SECTION_BASIC_INFORMATION * sbi = (SECTION_BASIC_INFORMATION*)InformationBuffer;
        sbi->Base = (void*)sh->base;        
        sbi->Size.LowPart = sh->size;
        sbi->Attributes = sh->attr;
        if (ResultLength) {
            *ResultLength= sizeof(SECTION_BASIC_INFORMATION);
        }
        
    }
    else if (InformationClass == SectionImageInformation) {
        SECTION_IMAGE_INFORMATION * sii = (SECTION_IMAGE_INFORMATION*) InformationBuffer; 
        
        if (sh->type == SEC_TYPE_FILE) {
            sii->MaximumStackSize = 0x40000;
            sii->MaximumStackSize = 0x1000;

            sii->SubSystemType = 2;
            sii->SubSystemVersion = 0x40000;
            sii->ImageCharacteristics = 0x210e;

            sii->TransferAddress = (void*)(sh->pe->ex_opt_header->image_base + sh->pe->ex_opt_header->entry_point);
            sii->ImageFileSize = sh->pe->ex_opt_header->size_of_image;
            
            
            return 0;
         
        }
        else {
            printf("Unsupported\n");
            exit(0);
        }
        if (ResultLength) {
             *ResultLength= sizeof(SECTION_IMAGE_INFORMATION);
        }
    }
    else {
        printf("Unsupported NtQuerySection\n");
        exit(0);
    }
    return 0;
}

NTSTATUS _NtProtectVirtualMemory(
    HANDLE              ProcessHandle,
    PVOID*              BaseAddress,
    PULONG              NumberOfBytesToProtect,
    ULONG               NewAccessProtection,
    PULONG              OldAccessProtection
  ) {
    printf("_NtProtectVirtualMemory %X %X\n",*BaseAddress,NewAccessProtection);
    *OldAccessProtection = 0x4;
    mprotect(*BaseAddress,*NumberOfBytesToProtect,PROT_EXEC | PROT_READ | PROT_WRITE);
    return 0;
}

NTSTATUS _NtOpenProcessToken(
  HANDLE               ProcessHandle,
  ACCESS_MASK          DesiredAccess,
  PHANDLE             TokenHandle
  ) {
    *TokenHandle = (HANDLE)0x100;
    return 0;
}

typedef struct _SID_IDENTIFIER_AUTHORITY {
  UCHAR Value[6];
} SID_IDENTIFIER_AUTHORITY, *PSID_IDENTIFIER_AUTHORITY;

typedef struct _SID {
  UCHAR                    Revision;
  UCHAR                    SubAuthorityCount;
  SID_IDENTIFIER_AUTHORITY IdentifierAuthority;
  ULONG                    SubAuthority[1];
} SID, *PISID;

typedef struct _SID_AND_ATTRIBUTES {
    PISID Sid;
    DWORD Attributes;
} SID_AND_ATTRIBUTES, *PSID_AND_ATTRIBUTES;

typedef struct _TOKEN_GROUPS {
    ULONG              GroupCount;
    SID_AND_ATTRIBUTES Groups[1];
} TOKEN_GROUPS, *PTOKEN_GROUPS;

typedef struct _TOKEN_USER {
  SID_AND_ATTRIBUTES User;
} TOKEN_USER, *PTOKEN_USER;

typedef struct _LUID {
  DWORD LowPart;
  LONG  HighPart;
} LUID, *PLUID;

typedef struct _TOKEN_STATISTICS {
    LUID                         TokenId;
    LUID                         AuthenticationId;
    LARGE_INTEGER                ExpirationTime;
    DWORD                       TokenType;
    DWORD                       ImpersonationLevel;
    ULONG                        DynamicCharged;
    ULONG                        DynamicAvailable;
    ULONG                        GroupCount;
    ULONG                        PrivilegeCount;
    LUID                         ModifiedId;
} TOKEN_STATISTICS, *PTOKEN_STATISTICS;

NTSTATUS _NtQueryInformationToken(
  HANDLE                  TokenHandle,
  DWORD                   TokenInformationClass,
  PVOID                   TokenInformation,
  ULONG                   TokenInformationLength,
  PULONG                  ReturnLength
) {
    printf("_NtQueryInformationToken %X %i\n",TokenHandle,TokenInformationClass);
    switch (TokenInformationClass) {
        case TokenUser: {
            SID_AND_ATTRIBUTES * st = (SID_AND_ATTRIBUTES*)TokenInformation;
            st->Sid = (SID*)calloc(1,sizeof(SID));
            st->Sid->SubAuthorityCount = 0;
            st->Attributes = 0;
            st->Sid->Revision = 01;
            st->Sid->IdentifierAuthority.Value[5] = 5;
            
            return 0;
        }
        case TokenStatistics: {
            TOKEN_STATISTICS * ts = (TOKEN_STATISTICS*)TokenInformation;
            memset(ts,0,sizeof(TOKEN_STATISTICS));
            return 0;
        }
            
        default:
            printf("Unsupported _NtQueryInformationToken\n");
            exit(0);
    }
    exit(0);
}

NTSTATUS _NtOpenThreadTokenEx(
  HANDLE      ThreadHandle,
  ACCESS_MASK DesiredAccess,
  DWORD     OpenAsSelf,
  ULONG       HandleAttributes,
  PHANDLE     TokenHandle
) {
    *TokenHandle = (HANDLE)0x200;
    return 0;
}

NTSTATUS _NtSetInformationProcess(
    HANDLE               ProcessHandle,
    DWORD  ProcessInformationClass,
    PVOID                ProcessInformation,
    ULONG                ProcessInformationLength 
  ) {
    printf("_NtSetInformationProcess %i %X\n",ProcessInformationClass,ProcessHandle);
    return 0;
}

NTSTATUS _NtCreateSemaphore(
  PHANDLE             SemaphoreHandle,
  ACCESS_MASK          DesiredAccess,
  POBJECT_ATTRIBUTES   ObjectAttributes,
  ULONG                InitialCount,
  ULONG                MaximumCount 
   ) {
    if (ObjectAttributes) {
        char * name = wide_to_ansi((wchar_t *)ObjectAttributes->ObjectName->Buffer);
        printf("create sempahore %s\n",name);
        exit(0);
    }
    printf("Create semaphore %X\n",MaximumCount);
    if (MaximumCount != 0x7FFFFFFF) {
        printf("Unsupported create semaphore\n");
        exit(0);
    }
    *SemaphoreHandle = create_semaphore(InitialCount);
    return 0;
}

NTSTATUS _NtCreateEvent(
  PHANDLE             EventHandle,
  ACCESS_MASK          DesiredAccess,
  POBJECT_ATTRIBUTES   ObjectAttributes,
  DWORD           EventType,
  DWORD              InitialState 
  ) {
    *EventHandle = create_event();
    return 0;
}

NTSTATUS _NtQueryTimerResolution( 
    PULONG MinimumResolution, 
    PULONG MaximumResolution, 
    PULONG ActualResolution
) {
    *MinimumResolution = *MaximumResolution = *ActualResolution = 100;
    return 0;
}

NTSTATUS _NtCreateKey(
    PHANDLE            KeyHandle,
    ACCESS_MASK        DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes,
    ULONG              TitleIndex,
    PUNICODE_STRING    Class,
    ULONG              CreateOptions,
    PULONG             Disposition
) {
    char keyname[512];
    reg_translate(ObjectAttributes,keyname);
    
    
    //char * cls = wide_to_ansi((wchar_t *)Class->Buffer);
    printf("_NtCreateKey %s \n",keyname);
    
    *KeyHandle = localRegistry->create_key(keyname);
    
    return 0;
}

dword WINAPI __isyscall_trap(dword reserved, dword * ssize, dword idx, ...) {
    va_list vl;
    va_start(vl,idx);
    printf("syscall trapped %X\n",idx);
    int proc;
    dword type;
    dword * out;
    SYSTEM_BASIC_INFORMATION * sbi;
    if (idx >= 0x1000) {
        return __gx_trap(reserved,ssize,idx,vl);
    }
    
    switch (idx) {
        case NtCreateKey: {
            GET_ARG(PHANDLE,KeyHandle);
            GET_ARG(ACCESS_MASK,DesiredAccess);
            GET_ARG(POBJECT_ATTRIBUTES,ObjectAttributes);
            GET_ARG(ULONG,TitleIndex);
            GET_ARG(PUNICODE_STRING,Class);
            GET_ARG(ULONG,CreateOptions);
            GET_ARG(PULONG,Disposition);
            return _NtCreateKey(KeyHandle,DesiredAccess,ObjectAttributes,TitleIndex,Class,CreateOptions,Disposition);
        }
        case NtQueryTimerResolution: {
            GET_ARG(PULONG,MinimumResolution);
            GET_ARG(PULONG,MaximumResolution);
            GET_ARG(PULONG,ActualResolution);
            return _NtQueryTimerResolution(MinimumResolution,MaximumResolution,ActualResolution);
        }
        case NtCreateEvent: {
            GET_ARG(PHANDLE,EventHandle);
            GET_ARG(ACCESS_MASK,DesiredAccess);
            GET_ARG(POBJECT_ATTRIBUTES,ObjectAttributes);
            GET_ARG(DWORD,EventType);
            GET_ARG(DWORD,InitialState);
            return _NtCreateEvent(EventHandle,DesiredAccess,ObjectAttributes,EventType,InitialState);
        }
        case NtCreateSemaphore: {
            GET_ARG(PHANDLE,SemaphoreHandle);
            GET_ARG(ACCESS_MASK,DesiredAccess);
            GET_ARG(POBJECT_ATTRIBUTES,ObjectAttributes);
            GET_ARG(ULONG,InitialCount);
            GET_ARG(ULONG,MaximumCount);
            return _NtCreateSemaphore(SemaphoreHandle,DesiredAccess,ObjectAttributes,InitialCount,MaximumCount);
        }
        case NtSetInformationProcess: {
            GET_ARG(HANDLE,ProcessHandle);
            GET_ARG(DWORD,ProcessInformationClass);
            GET_ARG(PVOID,ProcessInformation);
            GET_ARG(ULONG,ProcessInformationLength);
            return _NtSetInformationProcess(ProcessHandle,ProcessInformationClass,ProcessInformation,ProcessInformationLength);
        }
        case NtOpenThreadTokenEx: {
            GET_ARG(HANDLE,ThreadHandle);
            GET_ARG(ACCESS_MASK,DesiredAccess);
            GET_ARG(DWORD,OpenAsSelf);
            GET_ARG(ULONG,HandleAttributes);
            GET_ARG(PHANDLE,TokenHandle);
            return _NtOpenThreadTokenEx(ThreadHandle,DesiredAccess,OpenAsSelf,HandleAttributes,TokenHandle);
        }
        case NtQueryInformationToken: {
            GET_ARG(HANDLE,TokenHandle);
            GET_ARG(DWORD,TokenInformationClass);
            GET_ARG(PVOID,TokenInformation);
            GET_ARG(ULONG,TokenInformationLength);
            GET_ARG(PULONG,ReturnLength);
            return _NtQueryInformationToken(TokenHandle,TokenInformationClass,TokenInformation,TokenInformationLength,ReturnLength);
        }
        case NtOpenProcessToken: {
            GET_ARG(HANDLE,ProcessHandle);
            GET_ARG(ACCESS_MASK,DesiredAccess);
            GET_ARG(PHANDLE,TokenHandle);
            return _NtOpenProcessToken(ProcessHandle,DesiredAccess,TokenHandle);
        }
        case NtFlushInstructionCache: {
            FILE * fp = fopen ("/proc/sys/vm/drop_caches", "w"); 
            fprintf (fp, "3"); 
            fclose (fp);
            return 0;
        }
        case NtProtectVirtualMemory: {
            GET_ARG(HANDLE,ProcessHandle);
            GET_ARG(PVOID*,BaseAddress);
            GET_ARG(PULONG,NumberOfBytesToProtect);
            GET_ARG(ULONG,NewAccessProtection);
            GET_ARG(PULONG,OldAccessProtection);
            return _NtProtectVirtualMemory(ProcessHandle,BaseAddress,NumberOfBytesToProtect,NewAccessProtection,OldAccessProtection);
        }
        case NtRaiseHardError: {
            GET_ARG(NTSTATUS,ErrorStatus);
            GET_ARG(ULONG,NumberOfParameters);
            GET_ARG(PUNICODE_STRING,UnicodeStringParameterMask);
            GET_ARG(PVOID*,Parameters);
            GET_ARG(DWORD,ResponseOption);
            GET_ARG(DWORD,Response);
            return _NtRaiseHardError(ErrorStatus,NumberOfParameters,UnicodeStringParameterMask,Parameters,ResponseOption,Response);
        }
        case NtQuerySection: {
            GET_ARG(HANDLE,SectionHandle);
            GET_ARG(DWORD,InformationClass);
            GET_ARG(PVOID,InformationBuffer);
            GET_ARG(ULONG,InformationBufferSize);
            GET_ARG(PULONG,ResultLength);
            return _NtQuerySection(SectionHandle,InformationClass,InformationBuffer,InformationBufferSize,ResultLength);
        }
        case NtQueryDefaultLocale: {
            GET_ARG(int,              UserProfile);
            GET_ARG(PDWORD,               DefaultLocaleId);
            return _NtQueryDefaultLocale(UserProfile,DefaultLocaleId);
        }
        case NtQueryValueKey: {
            GET_ARG(HANDLE,KeyHandle);
            GET_ARG(PUNICODE_STRING,ValueName);
            GET_ARG(DWORD,KeyValueInformationClass);
            GET_ARG(PVOID,KeyValueInformation);
            GET_ARG(ULONG,Length);
            GET_ARG(PULONG,ResultLength);
            return _NtQueryValueKey(KeyHandle,ValueName,KeyValueInformationClass,KeyValueInformation,Length,ResultLength);
        }
        case NtRegisterThreadTerminatePort: {
            GET_ARG(HANDLE,PortHandle);
            return _NtRegisterThreadTerminatePort(PortHandle);
        }
        case NtRequestWaitReplyPort: {
            GET_ARG(HANDLE,PortHandle);
            GET_ARG(PLPC_MESSAGE,Request);
            GET_ARG(PLPC_MESSAGE,IncomingReply);
            return _NtRequestWaitReplyPort(PortHandle,Request,IncomingReply);
        }
        case NtSetInformationObject: {
            GET_ARG(HANDLE,ObjectHandle);
            GET_ARG(DWORD,ObjectInformationClass);
            GET_ARG(PVOID,ObjectInformation);
            GET_ARG(ULONG,Length);
            return _NtSetInformationObject(ObjectHandle,ObjectInformationClass,ObjectInformation,Length);
        }
        case NtQueryObject: {
            GET_ARG(HANDLE,Handle);
            GET_ARG(DWORD,ObjectInformationClass);
            GET_ARG(PVOID,ObjectInformation);
            GET_ARG(ULONG,ObjectInformationLength);
            GET_ARG(PULONG,ReturnLength);
            return _NtQueryObject(Handle,ObjectInformationClass,ObjectInformation,ObjectInformationLength,ReturnLength);
        }
        case NtSecureConnectPort: {
            GET_ARG(PHANDLE,PortHandle);
            GET_ARG(PUNICODE_STRING,PortName);
            GET_ARG(void*,SecurityQos);
            GET_ARG(PPORT_VIEW,ClientView);
            GET_ARG(void*,ServerSid);
            GET_ARG(PREMOTE_PORT_VIEW,ServerView);
            GET_ARG(PULONG,MaxMessageLength);
            GET_ARG(PVOID,ConnectionInformation);
            GET_ARG(PULONG,ConnectionInformationLength);
            return _NtSecureConnectPort(PortHandle,PortName,SecurityQos,ClientView,ServerSid,ServerView,MaxMessageLength,ConnectionInformation,ConnectionInformationLength);
        }
        case NtCreateSection: {
            GET_ARG(PHANDLE,SectionHandle);
            GET_ARG(ACCESS_MASK,DesiredAccess);
            GET_ARG(POBJECT_ATTRIBUTES,ObjectAttributes);
            GET_ARG(PLARGE_INTEGER,MaximumSize);
            GET_ARG(ULONG,SectionPageProtection);
            GET_ARG(ULONG,AllocationAttributes);
            GET_ARG(HANDLE,FileHandle);
            return _NtCreateSection(SectionHandle,DesiredAccess,ObjectAttributes,MaximumSize,SectionPageProtection,AllocationAttributes,FileHandle);
        }
        case NtFsControlFile: {
            GET_ARG(HANDLE,FileHandle);
            GET_ARG(HANDLE,Event);
            GET_ARG(VOID *,ApcRoutine);
            GET_ARG(PVOID,ApcContext);
            GET_ARG(PIO_STATUS_BLOCK,IoStatusBlock);
            GET_ARG(ULONG,FsControlCode);
            GET_ARG(PVOID,InputBuffer);
            GET_ARG(ULONG,InputBufferLength);
            GET_ARG(PVOID,OutputBuffer);
            GET_ARG(ULONG,OutputBufferLength);
            return _NtFsControlFile(FileHandle,Event,ApcRoutine,ApcContext,IoStatusBlock,FsControlCode,InputBuffer,InputBufferLength,OutputBuffer,OutputBufferLength);
        }
        case NtQueryVolumeInformationFile: {
            GET_ARG(HANDLE,               FileHandle);
            GET_ARG(PIO_STATUS_BLOCK,     IoStatusBlock);
            GET_ARG(PVOID,                FsInformation);
            GET_ARG(ULONG,                Length);
            GET_ARG(DWORD, FsInformationClass);
            return _NtQueryVolumeInformationFile(FileHandle,IoStatusBlock,FsInformation,Length,FsInformationClass);
        }
        case NtOpenFile: {
            GET_ARG(PHANDLE,            FileHandle);
            GET_ARG(ACCESS_MASK,        DesiredAccess);
            GET_ARG(POBJECT_ATTRIBUTES, ObjectAttributes);
            GET_ARG(PIO_STATUS_BLOCK,   IoStatusBlock);
            GET_ARG(ULONG,              ShareAccess);
            GET_ARG(ULONG,              OpenOptions);
            
            return _NtOpenFile(FileHandle,DesiredAccess,ObjectAttributes,IoStatusBlock,ShareAccess,OpenOptions);
        }
        case NtMapViewOfSection: {
            GET_ARG(HANDLE,          SectionHandle);
            GET_ARG(HANDLE,          ProcessHandle);
            GET_ARG(PVOID*,           BaseAddress);
            GET_ARG(ULONG_PTR,       ZeroBits);
            GET_ARG(SIZE_T,          CommitSize);
            GET_ARG(PLARGE_INTEGER,  SectionOffset);
            GET_ARG(PSIZE_T,         ViewSize);
            GET_ARG(DWORD, InheritDisposition);
            GET_ARG(ULONG,           AllocationType);
            GET_ARG(ULONG ,          Win32Protect);
            return _NtMapViewOfSection(SectionHandle,ProcessHandle,BaseAddress,ZeroBits,CommitSize,SectionOffset,ViewSize,InheritDisposition,AllocationType,Win32Protect);
        }
        case NtOpenSection: {
            GET_ARG(PHANDLE,           SectionHandle);
            GET_ARG(ACCESS_MASK,        DesiredAccess);
            GET_ARG(POBJECT_ATTRIBUTES, ObjectAttributes);
            return _NtOpenSection(SectionHandle,DesiredAccess,ObjectAttributes);
        }
        case NtQueryAttributesFile: {
            GET_ARG(POBJECT_ATTRIBUTES,      ObjectAttributes);
            GET_ARG(PFILE_BASIC_INFORMATION ,FileInformation);
            return _NtQueryAttributesFile(ObjectAttributes,FileInformation);
        }
        case NtReadFile: {
            GET_ARG(HANDLE,          FileHandle);
            GET_ARG(HANDLE,           Event);
            GET_ARG(VOID *,  ApcRoutine);
            GET_ARG(PVOID,            ApcContext);
            GET_ARG(PIO_STATUS_BLOCK, IoStatusBlock);
            GET_ARG(PVOID,            Buffer);
            GET_ARG(ULONG,            Length);
            GET_ARG(PLARGE_INTEGER,   ByteOffset);
            GET_ARG(PULONG,           Key);
            return _NtReadFile(FileHandle,Event,ApcRoutine,ApcContext,IoStatusBlock,Buffer,Length,ByteOffset,Key);
        }
        case NtClose: { // do nothing for now
            GET_ARG(HANDLE,Handle);
            printf("Close on %i\n",Handle);
            return 0;
        }
        case NtReleaseKeyedEvent: // check if anyone is waiting
            return 0;
        case NtQuerySymbolicLinkObject: {
            GET_ARG(HANDLE,            LinkHandle);
            GET_ARG(PUNICODE_STRING ,       LinkTarget);
            GET_ARG(PULONG ,ReturnedLength);
            return _NtQuerySymbolicLinkObject(LinkHandle,LinkTarget,ReturnedLength);
        }
        case NtOpenSymbolicLinkObject: {
            GET_ARG(PHANDLE,            LinkHandle);
            GET_ARG(ACCESS_MASK ,       DesiredAccess);
            GET_ARG(POBJECT_ATTRIBUTES ,ObjectAttributes);
            return _NtOpenSymbolicLinkObject(LinkHandle,DesiredAccess,ObjectAttributes);
        }
        case NtOpenDirectoryObject: {
            GET_ARG(PHANDLE,            DirectoryHandle);
            GET_ARG(ACCESS_MASK ,       DesiredAccess);
            GET_ARG(POBJECT_ATTRIBUTES ,ObjectAttributes);
            return _NtOpenDirectoryObject(DirectoryHandle,DesiredAccess,ObjectAttributes);
        }
        case NtAllocateVirtualMemory: {
            GET_ARG(HANDLE,    ProcessHandle);
            GET_ARG(PVOID     *,BaseAddress);
            GET_ARG(ULONG_PTR ,ZeroBits);
            GET_ARG(SIZE_T*,   RegionSize);
            GET_ARG(ULONG ,    AllocationType);
            GET_ARG(ULONG ,    Protect);
            return _NtAllocateVirtualMemory(ProcessHandle,BaseAddress,ZeroBits,RegionSize,AllocationType,Protect);
        }
        case NtTestAlert:
            return 0;
        case NtQueryDebugFilterState: {
            GET_ARG(ULONG,ComponentId);
            GET_ARG(ULONG,Level);
            return _NtQueryDebugFilterState(ComponentId,Level);
        }
            
        case NtOpenKey: {
            GET_ARG(PHANDLE,            KeyHandle);
            GET_ARG(DWORD,        DesiredAccess);
            GET_ARG(POBJECT_ATTRIBUTES, ObjectAttributes);
            return _NtOpenKey(KeyHandle,DesiredAccess,ObjectAttributes);
        }
        case NtQueryVirtualMemory: {
            
            GET_ARG(HANDLE,ProcessHandle);
            GET_ARG(PVOID,BaseAddress);
            GET_ARG(DWORD,MemoryInformationClass);
            GET_ARG(PVOID,MemoryInformation);
            GET_ARG(SIZE_T,MemoryInformationLength);
            GET_ARG(ULONG*,ReturnLength);
         
            return _NtQueryVirtualMemory(ProcessHandle,BaseAddress,MemoryInformationClass,MemoryInformation,MemoryInformationLength,ReturnLength);

        }
        case NtQueryPerformanceCounter: {
            PLARGE_INTEGER p1 = (PLARGE_INTEGER)va_arg(vl,PLARGE_INTEGER);
            PLARGE_INTEGER p2 = (PLARGE_INTEGER)va_arg(vl,PLARGE_INTEGER);
            return _NtQueryPerformanceCounter(p1,p2);
        }
            
        case NtOpenKeyedEvent: {
            
            PHANDLE ph = (PHANDLE)va_arg(vl,PHANDLE);
            DWORD d = (DWORD)va_arg(vl,DWORD);
            POBJECT_ATTRIBUTES oa = (POBJECT_ATTRIBUTES)va_arg(vl,POBJECT_ATTRIBUTES);
            return _NtOpenKeyedEvent(ph,d,oa);
        }
        case NtQueryInformationProcess: {
            proc = va_arg(vl,int);
            if (proc != -1) {
                printf("Unsupported syscall\n");
                exit(0);
            }
            type = va_arg(vl,dword);
            printf("NtQueryInformationProcess %X %X\n",proc,type);
            out = (dword*)va_arg(vl,dword*);
            
            *out = 1;
            ULONG nsize = va_arg(vl,ULONG);
            ULONG * osize = va_arg(vl,ULONG*);
            if (osize) *osize = sizeof(ULONG);
            return 0;
        }
        case NtQuerySystemInformation: {
            
            type = va_arg(vl,dword);
            printf("NtQuerySystemInformation %X\n",type);
            
            switch(type) {
                case 0x00: { // SystemBasicInformation
                    sbi = (SYSTEM_BASIC_INFORMATION*)va_arg(vl,SYSTEM_BASIC_INFORMATION*);
                    sbi->NumberOfProcessors = 1;
                    sbi->HighestPhysicalPageNumber = 4096*4096;
                    sbi->LowestPhysicalPageNumber = 4096;
                    sbi->PageSize = 4096;
                    sbi->MinimumUserModeAddress = 0x100000;
                    sbi->MaximumUserModeAddress = 0xFFFFFFFF;
                    sbi->ActiveProcessorsAffinityMask = 1;
                    return 0;
                    
                }
                case 0x32: //SystemRangeStartInformation
                    out = va_arg(vl,dword*);
                    *out = 0x1000;
                    return 0;
            }
            break;
        }
        default:
            printf("Unimplemented syscall \n");
            break;
    }
   exit(0);
}
}

#include "common.h"
#include <ctype.h>

char * toupper_str(char * str) {
    char * t = str;
    while (*t) {
        *t = toupper(*t);
        t++;
    }
    return str;
}

char * tolower_str(char * str) {
    char * t = str;
    while (*t) {
        *t = tolower(*t);
        t++;
    }
    return str;
}

char * tolower_str(char * str, char * dest) {
    char * t = str;
    char * t2 = dest;
    while (*t) {
        *t2 = tolower(*t);
        t++;
        t2++;
    }
    *t2 = 0;
    return dest;
}

char * l_substring(char * str, int st, int end) {
    if (st<0 || end<0) {
        log_error("Invalid substring");
        return NULL;
    }
    if (st == 0) {
        str[end] = 0;
    }
    else {
        char * t = str+st;
        char * t2 = str;
        
        while (*t) {
            *t2++ = *t++;
        }

        *(str+(end-st)) = 0;
    }
    
    return str;
}

int l_endswith(char * str, char * cmp) {
    int l1 = strlen(str);
    int l2 = strlen(cmp);
    if (l2>l1) {
        return 0;
    }
    char * t = str+(l1-1);
    char * t2 = cmp+(l2-1);
    while (t2>=cmp) {
        if (*t-- != *t2--) {
            return 0;
        }
    }
    return 1;
}

int l_lastindexof(char * str, char ch) {
    char * t = str;
    char * l = NULL;
    while (*t) {
        if (*t == ch) {
            l = t;
        }
        t++;
    }
    
    if (*l == ch) {
        return l-str;
    }
    
    return -1;
}

int l_strcicmp(char const *a, char const *b) {
    for (;; a++, b++) {
        int d = tolower((unsigned char)*a) - tolower((unsigned char)*b);
        if (d != 0 || !*a) {
            return d;
        }
    }
    return 0;
}

int str_cmp(void *a, void *b, void *h) {
    return strcmp((char*)a,(char*)b);
}

void w_replace_all(wchar_t * s1, wchar_t c, wchar_t c2) {
    
}
    
/**
 * Regular linux functions dont work with short wide so we build our own
 * @param s1
 * @return 
 */
int _wcslen(wchar_t * s1) {
    wchar_t * t = s1;
    while (*t)  t++;
    
    return (int)(t-s1);
}

static char wtoan[512];

int w_strcmp(wchar_t * s1, wchar_t *s2) {
    wchar_t * t1 = s1;
    wchar_t * t2 = s2;
    while (*t1 && *t2) {
        if (*t1 != *t2) {
            return *t2-*t1;
        }
        t1++;
        t2++;
    }
    return 0;
}

int wide_str(char * s1, wchar_t * s2) {
    char * t = s1;
    wchar_t * wt = s2;
    while (*t) {
        *wt++ = *t++;
    }
    *wt = *t;
    
    return (int)(t-s1);
}

int s_endswith(char * s1, char * match) {
    int len = strlen(s1);
    if (len == 0) {
        return 0;
    }
    int len2 = strlen(match);
    char * t = s1+len;
    char * t2 = match+len2;
    while (t > s1 && t2 > match) {
        if (*t != *t2) {
            return 0;
        }
        t--;
        t2--;
    }
    return 1;
}

char * wide_to_ansi(wchar_t * w, char * wtoan2) {
    wchar_t * wt = w;
    char * t = wtoan2;
    while (*wt) {
        *t = (char)((*wt)&0xFF);
        wt++;
        t++;
    }
    *t = 0;
    return wtoan2;
}

/**
 * Temporary buffer, shouldnt be used across threads
 */
char * wide_to_ansi(wchar_t * w) {
    wchar_t * wt = w;
    char * t = wtoan;
    while (*wt) {
        *t = (char)((*wt)&0xFF);
        wt++;
        t++;
    }
    *t = 0;
    return wtoan;
}

int w_strcpy(wchar_t * s1,wchar_t * s2) {
    wchar_t * wt1 = s1;
    wchar_t * wt2 = s2;
    
    while (*wt2) {
        *wt1++ = *wt2++;
    }
    *wt1 = *wt2;
    
    return (int)(wt2-s2);
}

wchar_t * w_join(wchar_t * targ,wchar_t * s1,char * s2) {
    int len = strlen(s2);
    int wlen = _wcslen(s1);
    
    memcpy(targ,s1,2*wlen);
    
    char * t = (char*)targ+2*wlen;
    printf("%X %X %X\n",t,targ,wlen);
    int i;
    for(i=0;i<len;i++) {
        *t++ = s2[i];
        *t++ = 0;
    }
    *t++ = 0;
    *t++ = 0;
    
    return targ;
}

int stricmp( char *str1, char *str2 ) {
    printf("Not implemented\n");
    exit(0);
}


int global_strcmp(void * s1, void *s2, void *h) {
    return strcmp((char*)s1,(char*)s2);
}
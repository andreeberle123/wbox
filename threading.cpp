#include <stdio.h>
#include <semaphore.h>
#include <sys/syscall.h>
#include <asm/ldt.h>
#include <asm/prctl.h>
#include <sys/prctl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <asm/ldt.h>
#include <pthread.h>
#include "common.h"

/**
 * The index we will share with our threads
 */
static dword proc_tls_entry = 0; 

/**
 * POSIX threads should be compatible with our custom TLS
 */
static pthread_mutex_t pmutex = PTHREAD_MUTEX_INITIALIZER;

extern struct _PEB * peb;





void setup_tls() {
    LOCK_MUTEX(pmutex);
    // all tls will be bound in fs
    unsigned int fsval = 0;
    struct user_desc udx;
    dword idx;
    int rt;
    memset(&udx,0,sizeof(udx));
    
    udx.entry_number = proc_tls_entry ? proc_tls_entry : -1;
    
    udx.limit = 128;
    
    //struct _TEB * tls_base = (struct _TEB*)malloc(sizeof(struct _TEB));
    struct _TEB * tls_base = (struct _TEB*)malloc(0xF00);
    tls_base->ProcessEnvironmentBlock = peb;
    log_debug("teb base at %X\n",tls_base);
    tls_base->NtTib.Self = (struct _NT_TIB *)tls_base;
    
    udx.base_addr = (dword)tls_base;
    
    udx.limit_in_pages = 1;
    udx.useable = 1;
    udx.seg_32bit = 1;
    
    *(dword*)((byte*)tls_base+0xBF8) = 0x20a0000; // unicode size
    
    wide_str("ntdll.dll",(wchar_t*)((byte*)tls_base+0xC00));
    
    *(dword*)((byte*)tls_base+0xBFC) = (dword)tls_base+0xC00;
    
    *(dword*)((byte*)tls_base+0xE0C) = 0x30000;
    
    rt = syscall(__NR_set_thread_area,&udx);
    
    if (rt < 0) {
        printf("Failed to set up TLS -- %s\n",strerror(errno));
        exit(0);
    }
    if (!proc_tls_entry) {
        proc_tls_entry = udx.entry_number;
    }
    idx = (udx.entry_number << 3) + 0x3;
    
    asm volatile(
        "xorl %%eax,%%eax;"
        "movl %0, %%eax;"
        "movw %%ax, %%fs;"
    :
    :"r"(idx)
	:"eax"
    );
    UNLOCK_MUTEX(pmutex);
}

typedef struct _isem {
    int type;
    sem_t sem;
}isem ;

typedef struct _ievt {
    int type;    
} evt;

void * create_semaphore(int c) {
    isem* sem = (isem*)malloc(sizeof(isem));
    sem_init(&sem->sem,0,c);
    sem->type = 1;
    
    return sem;
}

void * create_event() {
    evt * e = (evt*)malloc(sizeof(evt));
    e->type = 2;
    return e;
}
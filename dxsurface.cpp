#include <xcb/randr.h>

#include "common.h"
#include "winm.h"
#include "gl.h"
#include "xw.h"
#include "dx.h"

#include "sink.h"
void draw_test();

extern "C" {

int WINAPI _stAddAttachedSurface(struct _IDirectDrawSurface *_this, LPDIRECTDRAWSURFACE ds)  {
    printf("stAddAttachedSurface\n");exit(0);
}

int WINAPI _stAddOverlayDirtyRect(struct _IDirectDrawSurface *_this, LPRECT r)  {
    printf("stAddOverlayDirtyRect\n");exit(0);
}

int WINAPI _stBlt(struct _IDirectDrawSurface *_this, LPRECT r,LPDIRECTDRAWSURFACE ds, LPRECT r2,DWORD d, LPDDBLTFX t)  {
    printf("stBlt\n");exit(0);
}

int WINAPI _stBltBatch(struct _IDirectDrawSurface *_this, LPDDBLTBATCH, DWORD, DWORD )  {
    printf("stBltBatch\n");exit(0);
}

int WINAPI _stBltFast(struct _IDirectDrawSurface *_this, DWORD,DWORD,LPDIRECTDRAWSURFACE, LPRECT,DWORD)  {
    printf("stBltFast\n");exit(0);
}

int WINAPI _stDeleteAttachedSurface(struct _IDirectDrawSurface *_this, DWORD,LPDIRECTDRAWSURFACE)  {
    printf("stDeleteAttachedSurface\n");exit(0);
}

int WINAPI _stEnumAttachedSurfaces(struct _IDirectDrawSurface *_this, LPVOID,LPDDENUMSURFACESCALLBACK)  {
    printf("stEnumAttachedSurfaces\n");exit(0);
}

int WINAPI _stEnumOverlayZOrders(struct _IDirectDrawSurface *_this, DWORD,LPVOID,LPDDENUMSURFACESCALLBACK)  {
    printf("stEnumOverlayZOrders\n");exit(0);
}

int WINAPI _stFlip(struct _IDirectDrawSurface *_this, LPDIRECTDRAWSURFACE, DWORD)  {
    printf("stFlip\n");exit(0);
}

int WINAPI _stGetAttachedSurface(struct _IDirectDrawSurface *_this, LPDDSCAPS, LPDIRECTDRAWSURFACE  *)  {
    printf("stGetAttachedSurface\n");exit(0);
}

int WINAPI _stGetBltStatus(struct _IDirectDrawSurface *_this, DWORD)  {
    printf("stGetBltStatus\n");exit(0);
}

int WINAPI _stGetCaps(struct _IDirectDrawSurface *_this, LPDDSCAPS)  {
    printf("stGetCaps\n");exit(0);
}

int WINAPI _stGetClipper(struct _IDirectDrawSurface *_this, LPDIRECTDRAWCLIPPER *)  {
    printf("stGetClipper\n");exit(0);
}

int WINAPI _stGetColorKey(struct _IDirectDrawSurface *_this, DWORD, LPDDCOLORKEY)  {
    printf("stGetColorKey\n");exit(0);
}

int WINAPI _stGetDC(struct _IDirectDrawSurface *_this, HDC  *)  {
    printf("stGetDC\n");exit(0);
}

int WINAPI _stGetFlipStatus(struct _IDirectDrawSurface *_this, DWORD)  {
    printf("stGetFlipStatus\n");exit(0);
}

int WINAPI _stGetOverlayPosition(struct _IDirectDrawSurface *_this, LPLONG, LPLONG )  {
    printf("stGetOverlayPosition\n");exit(0);
}

int WINAPI _stGetPalette(struct _IDirectDrawSurface *_this, LPDIRECTDRAWPALETTE *)  {
    printf("stGetPalette\n");exit(0);
}

int WINAPI _stGetPixelFormat(struct _IDirectDrawSurface *_this, LPDDPIXELFORMAT)  {
    printf("stGetPixelFormat\n");exit(0);
}

#define SDDSD_ALL                0x007ff9eel

int WINAPI _stGetSurfaceDesc(struct _IDirectDrawSurface *_this, LPDDSURFACEDESC desc)  {
    
    
    struct _IDirectDrawSurface * t = *(struct _IDirectDrawSurface**)(_this);
    desc->dwSize =  sizeof(DDSURFACEDESC);
    desc->dwFlags = SDDSD_ALL;
    
    desc->dwWidth = t->sink->w;
    desc->dwHeight = t->sink->h;
    desc->lPitch = desc->dwWidth;
    
    desc->ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_3DDEVICE;
   // printf("stGetSurfaceDesc\n");exit(0);
    
    return DD_OK;
}

int WINAPI _stInitialize(struct _IDirectDrawSurface *_this, LPDIRECTDRAW, LPDDSURFACEDESC)  {
    struct _IDirectDrawSurface * t = *(struct _IDirectDrawSurface**)(_this);
    
    printf("stInitialize\n");exit(0);
}

int WINAPI _stIsLost(struct _IDirectDrawSurface *_this)  {
    printf("stIsLost\n");exit(0);
}

#define DDSD_LPSURFACE          0x00000800l

int WINAPI _stLock(struct _IDirectDrawSurface *_this, LPRECT r,LPDDSURFACEDESC d,DWORD d1,HANDLE h)  {
   // printf("stLock %i %i %X %X\n",d->lPitch,d->ddpfPixelFormat.dwSize,d->dwFlags,r);

    IDirectDrawSurface * indirect = *(IDirectDrawSurface**)_this;
    LOCK_MUTEX(indirect->mutex);
    gl_lock();
    indirect->locked = 1;
    UNLOCK_MUTEX(indirect->mutex);
    d->dwFlags |= DDSD_LPSURFACE;
    d->lPitch = d->dwWidth;
    
    d->lpSurface = indirect->sink->data;
   // printf("%X\n",d->lpSurface);
    //printf("%i %i %i %X\n",d->lPitch,d->dwWidth,d->dwHeight,d->lpSurface);
    
    
    _this->lock_desc = *d;
    
    return DD_OK;
}

int WINAPI _stReleaseDC(struct _IDirectDrawSurface *_this, HDC)  {
    printf("stReleaseDC\n");exit(0);
}

int WINAPI _stRestore(struct _IDirectDrawSurface *_this)  {
    printf("stRestore\n");exit(0);
}

int WINAPI _stSetClipper(struct _IDirectDrawSurface *_this, LPDIRECTDRAWCLIPPER)  {
    printf("stSetClipper\n");exit(0);
}

int WINAPI _stSetColorKey(struct _IDirectDrawSurface *_this, DWORD, LPDDCOLORKEY)  {
    printf("stSetColorKey\n");exit(0);
}

int WINAPI _stSetOverlayPosition(struct _IDirectDrawSurface *_this, LONG, LONG )  {
    printf("stSetOverlayPosition\n");exit(0);
}

int WINAPI _stSetPalette(struct _IDirectDrawSurface *_this, LPDIRECTDRAWPALETTE p)  {
    IDirectDrawPalette * ap = *(IDirectDrawPalette**)p;
    
    set_active_color(ap->internal_id);
    printf("stSetPalette %i\n",ap->internal_id);
    return DD_OK;
}


int a1 = 0;
int WINAPI _stUnlock(struct _IDirectDrawSurface *_this, LPVOID v)  {
   // printf("stUnlock %X\n",v);

    IDirectDrawSurface * indirect = *(IDirectDrawSurface**)_this;
    LOCK_MUTEX(indirect->mutex);
    if (indirect->locked) {
        draw_from_buffer_and_unlock((byte*)_this->lock_desc.lpSurface,
                _this->lock_desc.dwWidth,_this->lock_desc.dwHeight);
    }
    UNLOCK_MUTEX(indirect->mutex);
    return 0;
}

int WINAPI _stUpdateOverlay(struct _IDirectDrawSurface *_this, LPRECT, LPDIRECTDRAWSURFACE,LPRECT,DWORD, LPDDOVERLAYFX)  {
    printf("stUpdateOverlay\n");exit(0);
}

int WINAPI _stUpdateOverlayDisplay(struct _IDirectDrawSurface *_this, DWORD)  {
    printf("stUpdateOverlayDisplay\n");exit(0);
}

int WINAPI _stUpdateOverlayZOrder(struct _IDirectDrawSurface *_this, DWORD, LPDIRECTDRAWSURFACE)  {
    printf("stUpdateOverlayZOrder\n");exit(0);
}

/*ULONG*/ int WINAPI _stRelease(IDirectDraw *_this) {
   // printf("Release surface\n");
    IDirectDrawSurface * rp = *(IDirectDrawSurface**)_this;
    LOCK_MUTEX(rp->mutex);
    if (rp->locked) {
        gl_unlock();
    }
    UNLOCK_MUTEX(rp->mutex);
    return DD_OK;
}


/******************************************************************************/


IDirectDrawSurface _ref_draw_methods = {
    &QueryInterface, &AddRef, &_stRelease, &_stAddAttachedSurface,&_stAddOverlayDirtyRect,
    &_stBlt,&_stBltBatch,&_stBltFast,&_stDeleteAttachedSurface,&_stEnumAttachedSurfaces,
    &_stEnumOverlayZOrders,&_stFlip,&_stGetAttachedSurface,&_stGetBltStatus,&_stGetCaps,
    &_stGetClipper,&_stGetColorKey,&_stGetDC,&_stGetFlipStatus,&_stGetOverlayPosition,
    &_stGetPalette,&_stGetPixelFormat,&_stGetSurfaceDesc,&_stInitialize,&_stIsLost,
    &_stLock,&_stReleaseDC,&_stRestore,&_stSetClipper,&_stSetColorKey,&_stSetOverlayPosition,
    &_stSetPalette,&_stUnlock,&_stUpdateOverlay,&_stUpdateOverlayDisplay,&_stUpdateOverlayZOrder    
};


void dxs_fill_surface(LPDIRECTDRAWSURFACE ds) {
    *ds = _ref_draw_methods;
    
}

}
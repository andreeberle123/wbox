#include "fs.h"
#include "common.h"
static char * default_keys[] = {
    "HKEY_CLASSES_ROOT","HKEY_CURRENT_CONFIG","HKEY_CURRENT_USER","HKEY_CURRENT_USER_LOCAL_SETTINGS","HKEY_LOCAL_MACHINE",
    "HKEY_PERFORMANCE_DATA","HKEY_PERFORMANCE_NLSTEXT","HKEY_PERFORMANCE_TEXT","HKEY_USERS"
};

#define REGISTRY_ROOT_FILE          "/rnodes.r"
#define REGISTRY_LEAF_FILE          "/rleaf.r"

#define REGISTRY_STRING_FILE        "/rstrings.r"
#define REGISTRY_STRING_IDX_FILE    "/rstr_idx.r"




LocalRegistry::LocalRegistry() {
    string_gen_table = c_hash_create(4,256,0);
    string_gen_table->cmp_func = str_cmp;
    
    keys_mm  = c_hash_create(16,256,0);
    keys_mm->cmp_func = str_cmp;
    
    pthread_mutex_init(&storage_lock,0);
    pthread_mutex_init(&strings_lock,0);
    
    node_fp = fs_open_base(REGISTRY_ROOT_FILE,"r+b");

    if (1||!node_fp) {
        
        node_fp = fs_open_base(REGISTRY_ROOT_FILE,"w+b");
        string_fp = fs_open_base(REGISTRY_STRING_FILE,"w+b");
        string_idx_fp = fs_open_base(REGISTRY_STRING_IDX_FILE,"w+b");
        leaf_fp = fs_open_base(REGISTRY_LEAF_FILE,"w+b");
        
        node_header.first_empty = 0;
        node_header.first_valid = 0;
        node_header.next_node = HARD_NODE_BLOCK;
        
        leaf_header.first_empty = 0;
        leaf_header.first_valid = 0;
        node_header.next_node = HARD_NODE_BLOCK;
        
        memset(&string_header,0,sizeof(string_header));
        memset(sblocks,0,sizeof(sblocks));
        sblocks[0] = new block_elem;
        sblocks[0]->next = NULL;
        sblocks[0]->off = STRING_MMBLOCK;
        
        string_header.chunks[0] = STRING_MMBLOCK;
        string_header.first_non_chunked = STRING_MMBLOCK+STRING_MMBLOCK;
        fseek(string_idx_fp,STRING_MMBLOCK,SEEK_SET);
        int loff = -STRING_MMBLOCK;
        fwrite(&loff,1,sizeof(int),string_idx_fp);
        fflush(string_idx_fp);
        
        string_idx_header.first_empty = 0;
        string_idx_header.topidx = 1;
        
        string_indices = (dword*)calloc(512,sizeof(dword));
        string_indices_full_sz = 512;
        
        
        // no registry found, create it
        root = new_key("Computer",1);
        root->file_offset = HARD_NODE_BLOCK;
        
        store_key(root);
        
        create_default_keys();
        
    }
    else {    
        log_info("Rebuilding registry");
        string_fp = fs_open_base(REGISTRY_STRING_FILE,"r+b");
        string_idx_fp = fs_open_base(REGISTRY_STRING_IDX_FILE,"r+b");
        leaf_fp = fs_open_base(REGISTRY_LEAF_FILE,"r+b");
        
        memset(sblocks,0,sizeof(sblocks));
        build_from_file();
        
    }
    
}

dword round2power(dword val) {
    int fitsize = val;
    fitsize--;
    fitsize |= fitsize >> 1;
    fitsize |= fitsize >> 2;
    fitsize |= fitsize >> 4;
    fitsize |= fitsize >> 8;
    fitsize |= fitsize >> 16;
    fitsize++;
    
    if (fitsize < 16) {
        fitsize = 16;
    }
    return fitsize;
}

void LocalRegistry::build_string_blocks_from_file() {
    fread(&string_header,1,sizeof(string_header),string_fp);
    fseek(string_fp,0,SEEK_END);
    int fsize = ftell(string_fp);
    byte block[STRING_MMBLOCK];
    int i;
    for(i=0;i<5;i++) {
        if (string_header.chunks[i]) {
            printf("chunk %i %i\n",i,string_header.chunks[i]);
            int off = string_header.chunks[i];
            if (off >= fsize) {
                printf("beyond file\n");
                continue;
            }
            block_elem * t = sblocks[i];
            do {
                block_elem * b = new block_elem;
                b->next = NULL;
                b->off = off;
                if (!t) {
                    sblocks[i] = b;
                }
                else {
                    t->next = b;
                }
                t = b;
                fseek(string_fp,off,SEEK_SET);
                fread(block,1,STRING_MMBLOCK,string_fp);
                int size = *(int*)block;
                if (size > 0) {
                    printf("error in rebuilding string blocks - invalid size\n");
                    exit(0);
                }
                off = *((int*)block+1);                
                
            } while(off);
        }
    }
    
    
    // we ignore the first idx, zero is reserved for invalid values
    for(i=1;i<string_indices_sz;i++) {
        dword idoff = string_indices[i];
        fseek(string_fp,idoff,SEEK_SET);
        fread(block,1,STRING_MMBLOCK,string_fp);
        char * str = (char*)block+sizeof(dword);
        char * mt = (char*)malloc(strlen(str)+1);
        strcpy(mt,str);
        c_hash_put_k(DEF_STRING_HASH(str),mt,(void*)i,string_gen_table);
        log_debug("String added %i %s",idoff,str);
    }
    
    log_info("String blocks rebuilt");
}

typedef struct _qblock_elem {
    struct _qblock_elem * next;
    dword off;
    tree_node * parent;
} qblock_elem;

void LocalRegistry::build_from_file() {
    fread(&node_header,1,sizeof(node_header),node_fp);
    fread(&string_idx_header,1,sizeof(string_idx_header),string_idx_fp);
    int i;
    string_indices_full_sz = round2power(string_idx_header.topidx);
    string_indices = (dword*)calloc(string_indices_full_sz,sizeof(dword));
    string_indices_sz = 0;

    fseek(string_idx_fp,STRING_MMBLOCK,SEEK_SET);
    fread(string_indices,sizeof(dword),string_idx_header.topidx,string_idx_fp);
    string_indices_sz = string_idx_header.topidx;
    
    build_string_blocks_from_file();
    
    tree_leaf_node * tf;
    qblock_elem * queue = NULL;
    queue = new qblock_elem;
    queue->next = NULL;
    queue->off = node_header.first_valid;
    queue->parent = NULL;
    byte block[HARD_NODE_BLOCK];
    char tmpname[256];
    
    while (queue) {
        
        qblock_elem * t = queue;
        queue = queue->next;
        dword off = t->off;
        
        tree_node *parent = t->parent;
        delete t;
                
        fseek(node_fp,off,SEEK_SET);
        fread(block,1,HARD_NODE_BLOCK,node_fp);
        hard_tree_node * htn = (hard_tree_node*)block;
        int is_leaf = htn->stridx & 0x80000000;
        htn->stridx &= ~0x80000000;
        dword soff = string_indices[htn->stridx];
        
        fseek(string_fp,soff+sizeof(dword),SEEK_SET);
        fread(tmpname,1,256,string_fp);

        printf("new key %i %s %i %i %X\n",soff,tmpname,is_leaf,htn->children_size,off);
        
        tree_node * tn;
        if (!is_leaf) {
            tn = new_key(tmpname,0);
            tn->hfnode.stridx = htn->stridx;
            tn->file_offset = off;
        }
        else {
            tf = new_leaf(tmpname,0,0);
            tf->file_offset = off;
            tf->hf.stridx = htn->stridx;
            
            tn = (tree_node*)tf;
        }
        tn->ltype = is_leaf;
        
        if (!root) {
            root = tn;
        }
        else {            
            add_key(parent,tn,0);            
        }

        
        if (!is_leaf) {
            int children_size = htn->children_size;
            int guard = 0;
            int first_run = 1;
            dword * tt = (dword*)htn->data;
            
            for(i=0;i<children_size;i++) {
                off = *tt;
                if ((i == 4 && first_run) || (i!= guard && ((i-guard) % 7) == 0)) {                    
                    fseek(node_fp,off,SEEK_SET);
                    fread(block,1,HARD_NODE_BLOCK,node_fp);
                    tt = (dword*)block;
                    first_run = 0;
                    i--;
                    guard = i;
                }            
                else {
                    printf("queued from %i %i %i\n",off,i,children_size);
                    t = new qblock_elem;
                    t->next = queue;
                    t->off = off;
                    t->parent = tn;
                    queue = t;
                    tt++;
                }
            }
            
        }
        else {
            hard_tree_leaf * htf = (hard_tree_leaf *)htn;
            tf->type = htf->type;
            switch (htf->type) {
                case REG_SZ:
                    break;
                case REG_DWORD:
                    break;
            }
            
        }
    }
}

void LocalRegistry::set_leaf_data(tree_leaf_node * tf, dword type, void * data) {
    tf->type = type;
    dword kid;
    switch (type) {
        case REG_SZ:
            
            if (((char*)data)[0] == 0) {
                tf->data[0] = 0;
            }
            else {
          //      printf("new data str %s\n",data);
                kid = setup_string((char*)data);

                tf->data[0] = kid;
            }
            break;
        case REG_DWORD:
            tf->data[0] = *(dword*)data;
            break;
        default:
          //  printf("Non implemented reg key %u\n",type);
            ;
    }
    store_leaf(tf);
    fflush(leaf_fp);
}

tree_leaf_node * LocalRegistry::new_leaf(char * name, dword type, int new_off) {
    int key_id = setup_string(name);
    tree_leaf_node * tf = new tree_leaf_node;
    tf->ltype = 1;
    strcpy(tf->name,name);
    tf->hf.stridx = key_id;
    tf->type = type;
    
    if (new_off) {
        LOCK_MUTEX(storage_lock);
        tf->file_offset = next_free_node(1);
        UNLOCK_MUTEX(storage_lock);
    }
    
    return tf;
}

tree_node * LocalRegistry::new_key(char * name, int new_off) {
    int key_id = setup_string(name);
    tree_node * tn = new tree_node;
    tn->ltype = 0;
    tn->children = new struct _tree_node*[8];
    tn->children_size = 0;
    tn->children_full_size = 8;
    tn->parent = NULL;
    pthread_mutex_init(&tn->mutex,0);
    memset(tn->lnodes,0,sizeof(tn->lnodes));
    
    strcpy(tn->name,name);
    
    tn->hfnode.stridx = key_id;
    tn->hfnode.default_leaf = 0;
    
    if (new_off) {
        LOCK_MUTEX(storage_lock);
        tn->file_offset = next_free_node(0); 
        UNLOCK_MUTEX(storage_lock);
    }
    
    return tn;
}

dword LocalRegistry::next_free_node(int leaf) {
    
    tree_header * header;
    if (leaf) {
        header = &leaf_header;
    }
    else {
        header = & node_header;
    }
    
    dword off = header->first_empty;
    if (!off) {
        off = header->next_node;
        header->next_node += HARD_NODE_BLOCK;
    }
    else {
        // seek next empty
    }
    if (!header->first_valid) {
        header->first_valid = off;
    }

    fseek(node_fp,0,SEEK_SET);
    fwrite(header,1,sizeof(tree_header),node_fp);
    
    //log_debug("next node %X",off);
    return off;
}

int LocalRegistry::add_key(tree_node * parent, tree_node * child, int do_store) {
    int is_leaf = child->ltype;
    LOCK_MUTEX(parent->mutex);
    int i;
    
    for(i=0;i<parent->children_size;i++) {
        if (!strcmp(child->name,parent->children[i]->name)) {
            // already exists
            UNLOCK_MUTEX(parent->mutex);
            return -1;
        }
    }

    if (parent->children_size == parent->children_full_size) {
        // expand array
        struct _tree_node ** old = parent->children;
        parent->children_full_size *=2;
        parent->children = new struct _tree_node*[parent->children_full_size];
        memcpy(parent->children,old,parent->children_size*sizeof(struct _tree_node *));

        delete[] old;
    }
    
    parent->children[parent->children_size] = child;
    parent->children_size++;
    
    if (do_store) {
        store_key(parent);
        if (is_leaf) {
            store_leaf((tree_leaf_node *)child);
        }
        else {
            store_key(child);
        }
    }
    child->parent = parent;
    
    build_meta_path(child);
    
    
    UNLOCK_MUTEX(parent->mutex);
    
    fflush(node_fp);
    
    return 0;
    
}

void LocalRegistry::create_default_keys() {
    int i;

    for(i=0;i<sizeof(default_keys)/sizeof(char*);i++) {
        //printf("%s\n",default_keys[i]);
        tree_node * tn = new_key(default_keys[i],1);
        add_key(root,tn,1);
        
    }   
    
    
}

void LocalRegistry::store_leaf(tree_leaf_node * leaf) {
    LOCK_MUTEX(storage_lock);
    switch (leaf->type) {
        case REG_SZ:            
            leaf->hf.data1 = leaf->data[0];
            break;
        case REG_DWORD:
            leaf->hf.data1 = leaf->data[0];
            break;
        default:
           // printf("Non implemented reg key %u\n",leaf->type);
            ;
    }
    leaf->hf.type = leaf->type;
    
    fseek(leaf_fp,leaf->file_offset,SEEK_SET);
    fwrite(&leaf->hf,1,HARD_NODE_BLOCK,leaf_fp);
    UNLOCK_MUTEX(storage_lock);
}

void LocalRegistry::store_key(tree_node * key) {
    LOCK_MUTEX(storage_lock);
    //printf("soff %i\n",key->file_offset);
    
    
    byte block[HARD_NODE_BLOCK];
    hard_tree_node * hf = (hard_tree_node*)block;
    *hf = key->hfnode;
    hf->children_size = key->children_size;
    
    dword * t = (dword*)hf->data;
    //printf("t %i\n",(byte*)t-block);
    
    tree_node ** tt = key->children;
    int pernode = 4;
    int max = key->children_size > 4 ? 4 : key->children_size;
   // printf("%i\n",max);
    while (tt < key->children+max) {
       // printf("u %X %X\n",tt,key->children+max);
        *t++ = (*tt)->file_offset;
        tt++;
    }
    
    dword node;
    if (key->children_size > 4) {
        if (!key->lnodes[0]) {
            node = next_free_node(0);
            key->lnodes[0] = node;
        }
        else {
            node = key->lnodes[0];
        }
        *t++ = node;
    }    
    //printf("store at %i, %s and %i -- %i\n",key->file_offset,key->name,hf->children_size,hf->stridx);
    //printf("next node at %X\n",node);
    fseek(node_fp,key->file_offset,SEEK_SET);
    fwrite(block,1,HARD_NODE_BLOCK,node_fp);
    
    int left = key->children_size - 4;
    int j = 1;
    while (left > 0) {
        
        int max = left > 7 ? 7 : left;
        
        t = (dword*)block;
        tree_node ** guard = tt;
        while (tt < guard+max) {
            *t++ = (*tt)->file_offset;
            tt++;
        }
        fseek(node_fp,node,SEEK_SET);
        
        left -= max;
        if (left) {
            if (!key->lnodes[j]) {
                node = next_free_node(0);
                key->lnodes[j] = node;
            }
            else {
                node = key->lnodes[j];
            }
            j++;
            *t++ = node;
        }
        
        fwrite(block,1,HARD_NODE_BLOCK,node_fp);
    }
    
    
    UNLOCK_MUTEX(storage_lock);
}


void LocalRegistry::build_meta_path(tree_node * key) {
    char mpath[2048];
    memset(mpath,0,2048);
    char * t = mpath+2047;
    tree_node * okey = key;
    while (key) {
        int len = strlen(key->name);
        t -= len;
        
        if (t < mpath) {
            log_error("key path too large for meta");
            return;
        }
        memcpy(t,key->name,len);        
        
        if (key->parent) {
            t--;
            *t = '\\';        
        }

        key = key->parent;
    }

    toupper_str(t);
    char * mt = (char*)malloc(strlen(t)+1);
    strcpy(mt,t);
    log_debug("add metapath %s",mt);
    c_hash_put_k(DEF_STRING_HASH(mt),mt,(void*)okey,keys_mm);
   

    
}


static const int tab32[32] = {
     0,  0,  1, 10, 2, 21,  2, 29,
    3, 14, 16, 18, 22, 25,  3, 30,
     4, 12, 20, 28, 15, 17, 24,  7,
    19, 27, 23,  6, 26,  5,  4, 31};

dword LocalRegistry::string_h_lookup(int len) {
    if (len > 256) {
        return 0;
    }
    int fitsize = round2power(len);

    
   // printf("fitsize %i %i\n",fitsize,len);
    int iidx = fitsize >> 4;
    //printf("iidx %i %i\n",iidx,tab32[(dword)(iidx*0x07C4ACDD) >> 27]);
     
    iidx =  4 - tab32[iidx];
    
    //printf("init %i %i\n",iidx,string_header.chunks[0]);
    dword nsize = fitsize;
    do {
       // printf("chunk offs %i %i\n",iidx,string_header.chunks[iidx]);
        if (string_header.chunks[iidx]) {
            break;
        }
        else {
            nsize = nsize << 1;
            iidx--;
        }
    } while (1);
    
    byte block[STRING_MMBLOCK];
    dword loff = (sblocks[iidx]->off / STRING_MMBLOCK)*STRING_MMBLOCK;
    
    fseek(string_fp,loff,SEEK_SET);
    fread(block,1,STRING_MMBLOCK,string_fp);
    
    while (nsize > fitsize) {
        
        struct _block_elem * b = sblocks[iidx];
        sblocks[iidx] = sblocks[iidx]->next;
        if (iidx == 0 && !sblocks[iidx]) {
            // allocate next chunk for 256
            dword old_chunk = string_header.first_non_chunked;
            string_header.first_non_chunked = string_header.first_non_chunked +STRING_MMBLOCK;
            sblocks[iidx] = new block_elem;
            sblocks[iidx]->next = 0;
            sblocks[iidx]->off = old_chunk;

        }
        string_header.chunks[iidx] = sblocks[iidx] ? sblocks[iidx]->off : 0;
        
        int poff = sblocks[iidx+1] ? sblocks[iidx+1]->off : 0;
        int soff = b->off;
        
        b->next = sblocks[iidx+1];        
        b->off = soff + (nsize/2);
        sblocks[iidx+1] = b;
        

        b = new block_elem;      
        b->next = sblocks[iidx+1];
        b->off = soff;

        sblocks[iidx+1] = b;
        string_header.chunks[iidx+1] = b->off;
                
        iidx++;
        nsize /= 2;
        
        int p = nsize;
        
        *(int*)&block[soff-loff] = -p;
        *(int*)&block[(soff-loff)+ (nsize)] = -p;
        
        *(dword*)&block[(soff-loff)+sizeof(dword)] = soff + (nsize/2);
        *(dword*)&block[(soff-loff)+nsize+sizeof(dword)] = poff;
        
        if (iidx == 5) {
            log_error("Error invalid chunking\n");
            return 0;
        }
    }
    
    fseek(string_fp,loff,SEEK_SET);
    fwrite(block,1,STRING_MMBLOCK,string_fp);
    
    struct _block_elem * b = sblocks[iidx];
    sblocks[iidx] =  sblocks[iidx]->next;
    string_header.chunks[iidx] = sblocks[iidx] ? sblocks[iidx]->off : 0;
    dword off = b->off;
    delete b;
    
    return off;   
    
}

int LocalRegistry::free_string(char * str) {
    dword id;
    if (!(id = (dword) c_hash_get_k(DEF_STRING_HASH(str),str,string_gen_table)))  {
        log_error("Trying to delete invalid string");
        // not found
        return -1;
    }
    int len = strlen(str);
    int fitsize = round2power(len);
    
    
    LOCK_MUTEX(strings_lock);
    dword foff = string_indices[id];
    dword mset = foff % STRING_MMBLOCK;
    int parity = (mset/fitsize) % 2;
   // printf("parity %i %i\n",parity,mset/fitsize);
    
    byte block[STRING_MMBLOCK];
        
    fseek(string_fp,foff-mset,SEEK_SET);
    fread(block,1,STRING_MMBLOCK,string_fp);
    
    int deoff;
    
    if (parity) {
        deoff = mset-fitsize;
    }
    else {
        deoff = mset+fitsize;
    }
    
    int nval = *(int*)(block+(deoff));
    
    int nsize = fitsize;
    
    int iidx = fitsize >> 4;
     
    //log2    
    iidx =  4 - tab32[iidx];
    
    int blockoff = mset;
    
    if (nval < 0) {
        
        //printf("collapse\n");
        block_elem * t = sblocks[iidx];
        block_elem * t2 = NULL;
        
        while (t) {
            if (t->off == (deoff+(foff-mset))) {                
                if (!t2) {
                    sblocks[iidx] = sblocks[iidx]->next;
                }
                else {
                    t2->next = t->next;
                }
                string_header.chunks[iidx] = sblocks[iidx] ? sblocks[iidx]->off : 0;
                delete t;
                break;
            }
            t2 = t;
            t = t->next;
        }

        nsize *=2;
        iidx--;
        
        if (parity) {
            blockoff -= fitsize;
        }
        
    }
    
    
   // printf("rm %i\n",iidx);
    
    *(int*)(block+(blockoff)) = -nsize;
    *(int*)(block+(blockoff)+sizeof(dword)) = sblocks[iidx] ? sblocks[iidx]->off : 0;
    printf("next %i %i\n",nsize,sblocks[iidx]);
    struct _block_elem * b = new block_elem;
    b->next = sblocks[iidx];
    b->off = foff;
    sblocks[iidx] = b;
    string_header.chunks[iidx] = b->off;    
    
    //write headers and blocks
    
    fseek(string_idx_fp,STRING_MMBLOCK+(id*sizeof(dword)),SEEK_SET);
    fwrite(&string_idx_header.first_empty,1,sizeof(dword),string_idx_fp);
    
    fseek(string_idx_fp,0,SEEK_SET);
    fwrite(&string_idx_header,1,sizeof(string_idx_header),string_idx_fp);
    
    fseek(string_fp,0,SEEK_SET);
    fwrite(&string_header,1,sizeof(string_header),string_fp);
    
    fseek(string_fp,foff-mset,SEEK_SET);
    fwrite(block,1,STRING_MMBLOCK,string_fp);
    
    fflush(string_fp);
    fflush(string_idx_fp);
    
    string_idx_header.first_empty = id;
    
    UNLOCK_MUTEX(strings_lock);
    
    return 0;
}

dword LocalRegistry::setup_string(char * str) {
    
    dword id;
    if (id = (dword) c_hash_get_k(DEF_STRING_HASH(str),str,string_gen_table))  {
        // already exists
        return id;
    }
    //printf("new key %s\n",str);
    int len = strlen(str);
    LOCK_MUTEX(strings_lock);
    id = string_idx_header.first_empty;
    if (id == 0) {
        id = string_idx_header.topidx++;
    }
    if (id < string_idx_header.topidx) {
        // free it
        string_idx_header.first_empty = string_indices[id];        
    }
    else {
        string_idx_header.topidx++;
    }
    //printf("string id %i\n",id);
    c_hash_put_k(DEF_STRING_HASH(str),str,(void*)id,string_gen_table);
    
    dword foff = string_h_lookup(len+sizeof(dword));
    //printf("fseek %i\n",foff);
    // write block
    fseek(string_fp,foff,SEEK_SET);
    
    fwrite(&len,1,sizeof(dword),string_fp);
    fwrite(str,1,len+1,string_fp);
    //write updated header
    fseek(string_fp,0,SEEK_SET);
    fwrite(&string_header,1,sizeof(string_header),string_fp);
    
    // write indices file
    string_indices[id] = foff;
    fseek(string_idx_fp,0,SEEK_SET);
    fwrite(&string_idx_header,1,sizeof(string_idx_header),string_idx_fp);

    fseek(string_idx_fp,STRING_MMBLOCK+(id*sizeof(dword)),SEEK_SET);
    fwrite(&foff,1,sizeof(dword),string_idx_fp);
    
    fflush(string_idx_fp);
    fflush(string_fp);
    
    UNLOCK_MUTEX(strings_lock);
    
    return id;
}

int LocalRegistry::leaf_get_data(tree_leaf_node * tf, void ** p) {
    dword off;
    char * data;
    switch (tf->type) {
        case REG_SZ:
            off = tf->data[0];
            fseek(string_fp,string_indices[off],SEEK_SET);
            data = (char*)malloc(256);
            fread(data,1,256,string_fp);
            
            *p =(void*)(data+4);
            return strlen(data+4);
        case REG_DWORD:
            *p = (void*)tf->data[0];
            return sizeof(dword);
    }
    return 0;
}

tree_node * LocalRegistry::node_look_up(tree_node * tn, char * child) {
    int i;

    for(i=0;i<tn->children_size;i++) {
        
        if (!strcmp(tn->children[i]->name,child)) {
            return tn->children[i];
        }
    }
    return NULL;
}

tree_node * LocalRegistry::create_key(char * path) {
    char * t = path;
    char * ref = t;
    int done = 0;
    
    tree_node * tn = root;
    do {
        while (*t != '\\' && *t) {
            t++;
        }
        if (!*t) {
            done = 1;
        }
        *t = 0;
        
     //   printf("%s\n",ref);
        tree_node * child = node_look_up(tn,ref);
        if (!child) {
            child = new_key(ref,1);
            add_key(tn,child,1);
        }
        tn = child;
        t++;
        ref = t;
    } while (!done);
    
    return tn;
}

tree_node * LocalRegistry::meta_search(char * path) {
    toupper_str(path);
    return (tree_node*)c_hash_get_k(DEF_STRING_HASH(path),path,keys_mm);
}

void deescape(char * str) {
    char * t = str;
    while (*t) {
        while (*t && *t != '\\') {
            t++;
        }
        if (!*t) return;
        if (*(t+1) == '\\') {
            char * t2 = t;
            while (*t2) {
                *t2 = *(t2+1);
                t2++;
            }
        }
        t++;
    }
}

void LocalRegistry::process_reg_file(char * path) {
    FILE * fp = fopen(path,"r");
    log_debug("processing %s\n",path);
    printf("processing %s\n",path);
    char data[512];
    
    tree_node * tn = NULL;
    while (fgets(data,512,fp)) {
        //printf("data %s\n",data);
        if (data[0] == '[') {
            char * t = data;
            while (*t != ']') t++;
            *t = 0;
            tn = create_key(data+1);
           // printf("%s\n",tn->name);
            
            
        }
        else if (data[0] == '\"') {
            
            char * t = data+1;
            while (*t != '\"' && *t) {
                t++;
            }
            if (!*t) {
                printf("Failed to read file, bad format");
                return;
            }
            *t = 0;
           // printf("%s\n",data+1);
            tree_leaf_node *tf = new_leaf(data+1,0,1);
            t++;
            while (*t != '=' && *t) {
                t++;
            }
            t++;
            
            if (t[0] == '\"') {
                // string
                t++;
                char * ref = t;
                while (*t != '\"') {
                    t++;
                }
                *t = 0;
                deescape(ref);
                
                set_leaf_data(tf,REG_SZ,ref);
                
            }
            else if (!strncmp(t,"dword",5)) {
                // dword
                t = t+6;
                *(t+8) = 0;
                dword d = strtol(t,NULL,16);
                set_leaf_data(tf,REG_DWORD,&d);
            }
            else {
               // printf("unk data %s\n",t);
            }
           // printf("extracted key %s\n",tf->name);
            add_key(tn,(tree_node*)tf,1);
        }
    }
    fclose(fp);
}

void LocalRegistry::dump() {
    printf("Strings\n");
    int i;
    for(i=0;i<5;i++) {
        printf("%i ",string_header.chunks[i]);
    }
    printf("\n");
    printf("%i\n",string_header.first_non_chunked);
    
    printf("Strings idx\n");
    printf("topidx %i\n",string_idx_header.topidx);
    for(i=1;i<string_idx_header.topidx;i++) {
        printf("%i ",string_indices[i]);
    }
    printf("\n");
}
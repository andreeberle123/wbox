#include "winm.h"

#define MCI_OPEN                        0x0803
#define MCI_CLOSE                       0x0804
#define MCI_ESCAPE                      0x0805
#define MCI_PLAY                        0x0806
#define MCI_SEEK                        0x0807
#define MCI_STOP                        0x0808
#define MCI_PAUSE                       0x0809
#define MCI_INFO                        0x080A
#define MCI_GETDEVCAPS                  0x080B
#define MCI_SPIN                        0x080C
#define MCI_SET                         0x080D
#define MCI_STEP                        0x080E
#define MCI_RECORD                      0x080F
#define MCI_SYSINFO                     0x0810
#define MCI_BREAK                       0x0811
#define MCI_SAVE                        0x0813
#define MCI_STATUS                      0x0814
#define MCI_CUE                         0x0830
#define MCI_REALIZE                     0x0840
#define MCI_WINDOW                      0x0841
#define MCI_PUT                         0x0842
#define MCI_WHERE                       0x0843
#define MCI_FREEZE                      0x0844
#define MCI_UNFREEZE                    0x0845
#define MCI_LOAD                        0x0850
#define MCI_CUT                         0x0851
#define MCI_COPY                        0x0852
#define MCI_PASTE                       0x0853
#define MCI_UPDATE                      0x0854
#define MCI_RESUME                      0x0855
#define MCI_DELETE                      0x0856

/* constants for predefined MCI device types */
#define MCI_DEVTYPE_VCR                 513 /* (MCI_STRING_OFFSET + 1) */
#define MCI_DEVTYPE_VIDEODISC           514 /* (MCI_STRING_OFFSET + 2) */
#define MCI_DEVTYPE_OVERLAY             515 /* (MCI_STRING_OFFSET + 3) */
#define MCI_DEVTYPE_CD_AUDIO            516 /* (MCI_STRING_OFFSET + 4) */
#define MCI_DEVTYPE_DAT                 517 /* (MCI_STRING_OFFSET + 5) */
#define MCI_DEVTYPE_SCANNER             518 /* (MCI_STRING_OFFSET + 6) */
#define MCI_DEVTYPE_ANIMATION           519 /* (MCI_STRING_OFFSET + 7) */
#define MCI_DEVTYPE_DIGITAL_VIDEO       520 /* (MCI_STRING_OFFSET + 8) */
#define MCI_DEVTYPE_OTHER               521 /* (MCI_STRING_OFFSET + 9) */
#define MCI_DEVTYPE_WAVEFORM_AUDIO      522 /* (MCI_STRING_OFFSET + 10) */
#define MCI_DEVTYPE_SEQUENCER           523 /* (MCI_STRING_OFFSET + 11) */

extern "C" {
    
typedef struct {
    DWORD_PTR   dwCallback;
    MCIDEVICEID wDeviceID;
    LPCTSTR     lpstrDeviceType;
    LPCTSTR     lpstrElementName;
    LPCTSTR     lpstrAlias;
} MCI_OPEN_PARMS;

typedef struct {
  DWORD_PTR dwCallback;
  DWORD_PTR dwReturn;
  DWORD     dwItem;
  DWORD     dwTrack;
} MCI_STATUS_PARMS;

MCIERROR WINAPI mciSendCommandA(
   MCIDEVICEID IDDevice,
   UINT        uMsg,
   DWORD_PTR   fdwCommand,
   DWORD_PTR   dwParam
) {
    MCI_OPEN_PARMS * mci_open_params;
    MCI_STATUS_PARMS * mci_status_params;
    
    printf("mci command to %X %X %X %X\n",IDDevice,uMsg,fdwCommand,*dwParam);
    switch (uMsg) {
        case MCI_OPEN:
            mci_open_params = (MCI_OPEN_PARMS*)dwParam;
            printf("mci_open %i %X\n",mci_open_params->lpstrDeviceType,mci_open_params->dwCallback);
            // ignore multimedia for now
            mci_open_params->wDeviceID = 1;
            break;
        case MCI_SET:
            
            return 0;
        case MCI_STATUS:
            mci_status_params = (MCI_STATUS_PARMS*)dwParam;
            mci_status_params->dwReturn = (DWORD_PTR)1;
            printf("mci status %X %X\n",mci_status_params->dwItem,mci_status_params->dwTrack);
            return 0x1;   
        default:
            exit(0);
    }
    
    return 0;
}

}
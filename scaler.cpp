#include "common.h"
#include "sink.h"
#include "gl.h"

typedef struct _screen_cfg {
    int dw;
    int dh;
    int rw;
    int rh;
    int start_x;
    int start_y;
    int end_x;
    int end_y;
} screen_cfg;

screen_cfg current_screen_cfg;
    
int local_dx_display_modes_sz = 2;

ImageSink::ImageSink() {
    w = 0;
    h = 0;
}

void ImageSink::setDimensions(int iw, int ih) {
    w = iw;
    h = ih;
    
    data = (byte*)malloc(iw*ih);
    
    gl_push_buffer(data,w,h);
}

#include "dx.h"

DDSURFACEDESC local_dx_display_modes[] = {
    {sizeof(DDSURFACEDESC),DDSD_HEIGHT | DDSD_WIDTH | DDSD_REFRESHRATE | DDSD_PIXELFORMAT,
            480,640,0,0,60,0,0,0,{0,0},{0,0},{0,0},{0,0},
            {sizeof(DDPIXELFORMAT),0,0,8,0,0,0,0},0},
    {sizeof(DDSURFACEDESC),DDSD_HEIGHT | DDSD_WIDTH | DDSD_REFRESHRATE | DDSD_PIXELFORMAT,
            768,1024,0,0,60,0,0,0,{0,0},{0,0},{0,0},{0,0},
            {sizeof(DDPIXELFORMAT),0,0,8,0,0,0,0},0}
};




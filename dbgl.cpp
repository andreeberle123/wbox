#include "common.h"
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/mman.h>
#include "exec.h"
#include "lstring.h"
#include <dlfcn.h>
#include "winm.h"
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/types.h>



dword exp_fns[128];
int exp_fns_sz = 0;

void dbg_function(dword addr) {
    exp_fns[exp_fns_sz++] = addr;
}

static int fidx = 1;

void dbg_fn_start(dword addr, int pid, struct user_regs_struct * regs) {
    long l;
    int status;
    char fname[256];
    sprintf(fname,"lfn-%i",fidx++);
    FILE * fp = fopen(fname,"wb");
    for(;;) {
        unsigned int dr;
        fwrite(regs,1,sizeof(struct user_regs_struct),fp);
        //l = ptrace(PTRACE_PEEKUSER, pid, DR_OFFSET(7), &dr);
        l = ptrace(PTRACE_GETREGS, pid, NULL, regs);
       // printf("getregs %X %i %X\n",dr,l,regs.eip);
        if (l<0) break;

        byte ft = *(byte*)regs->eip;
        if (ft == 0xc2) {
            break;
        }

        l = ptrace(PT_STEP,pid,0,0);
       // printf("%i %s\n",l,strerror(errno));

        int rt = waitpid(pid,&status,0);
       // printf("waitpid %X %X %X\n",rt,status,pid);
       // if (cc == 10) break;
    }
    fclose(fp);
   
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   peloader.cpp
 * Author: z
 * 
 * Created on May 1, 2018, 12:32 AM
 */

#include "common.h"
#include "peloader.h"

#include "ne.h"

PEloader::PEloader() {


}




#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "c_hash.h"

#define BUF_SIZE 8192

byte ** ffbufs;

FILE * fptr;

pe_header * ex_header;
pe_opt_header32 * ex_opt_header;

dword sects_base;
pe_section_header ** sects;

pe_section_header * text;

it_inner_entry * import_table;
int import_table_sz;

pe_section_header * including_sect(dword vaddr) {
    int i;

    for (i = 0; i < ex_header->num_sections; i++) {

        if (vaddr >= sects[i]->vaddr && vaddr < (sects[i]->vaddr + sects[i]->raw_data_size)) {
            return sects[i];
        }
    }
    return NULL;
}

int memiszero(void * mm, int size) {
    unsigned char * t = (unsigned char*) mm;
    unsigned char * b = t;
    while (t < (b + size)) {
        if (*t) return 0;
        t++;
    }
    return 1;
}

dword vaddr_to_faddr(pe_section_header * sect, dword vaddr) {
    if (!sect) {
        printf("bad vaddr search\n");
        exit(0);
    }
    return ((vaddr - sect->vaddr) + sect->raw_data_ptr);
}

char * table_get_name(dword vaddr) {
    pe_section_header * sect = including_sect(vaddr);
    if (sect == 0) {
        return "";
    }
    dword faddr = vaddr_to_faddr(sect, vaddr);

    return (char*) (ffbufs[0] + faddr);
}



void process_export_table(pe_info * info) {
    pe_dir_entry * etable = (pe_dir_entry*) (&ex_opt_header->export_table);

    if (ex_opt_header->export_table == 0) {
        return;
    }
    //fprintf(fp_out, "image_base %X\n", ex_opt_header->image_base);
    pe_section_header* isect = including_sect(etable->addr);
    dword faddr = vaddr_to_faddr(isect, etable->addr);  
   
    export_directory_table * direntry = (export_directory_table*) (ffbufs[0] + faddr);
    faddr = vaddr_to_faddr(including_sect(direntry->export_table_rva), direntry->export_table_rva);
    dword * eebase = (dword*) (ffbufs[0] + faddr);
     
    faddr = vaddr_to_faddr(including_sect(direntry->name_pointer_rva), direntry->name_pointer_rva);
    dword * names = (dword*)(ffbufs[0] + faddr);
     
    export_entry * entries = (export_entry*)malloc(direntry->entry_table_addr * sizeof(export_entry));
     
    faddr = vaddr_to_faddr(including_sect(direntry->ord_table_rva), direntry->ord_table_rva);
    unsigned short * ords = (unsigned short*)(ffbufs[0] + faddr);
    
    int i;
    for(i=0;i<direntry->num_name_pointers;i++) {
     //   printf("%X\n",*ee);
        
        isect = including_sect(*names);
        if (!isect) {
            printf("failed to isect\n");
            exit(0);
        }
        faddr = vaddr_to_faddr(isect, *names);
        char * name = (char*)(ffbufs[0] + faddr);
        
        int ord = *(ords+i);
        //ord -= direntry->ordbase;
        
        
        //printf("%X %X %i %i\n",names,nbase,direntry->ordbase,*ords);
        dword *ee = eebase+ord;
        
        entries[i].name = name;
        if (*ee >= etable->addr && *ee < (etable->addr + etable->size)) {
            isect = including_sect(*ee);            
            faddr = vaddr_to_faddr(isect, *ee);
            
            //printf("forward %s %s\n",name,ffbufs[0]+faddr);
            entries[i].forward = 1;
            entries[i].fwd_string = (char*)(ffbufs[0]+faddr);
            
        }
        else {
            entries[i].forward = 0;
            entries[i].vaddr = *ee;
            //printf("export rva %X %s %i\n",*ee,name,*ords);
          //  exit(0);
            
        }
        names++;

        
    }
    
    info->export_table = entries;
    info->export_table_sz = direntry->entry_table_addr;
    
    log_debug("Total exports: %i\n",info->export_table_sz);

    //printf("exp %X %X\n", direntry->export_flags, direntry->name_rva);
}

void process_import_table() {
    log_debug("Processing import table");
    pe_dir_entry * itable = (pe_dir_entry*) (&ex_opt_header->import_table);

    if (ex_opt_header->import_table == 0) {
        import_table_sz = 0;
        return;
    }
    log_debug("Image base at %X\n", ex_opt_header->image_base);
    pe_section_header* isect = including_sect(itable->addr);

    dword faddr = vaddr_to_faddr(isect, itable->addr);
    idt * direntry = (idt*) (ffbufs[0] + faddr);

    it_inner_entry * t = import_table;

    while (!memiszero(direntry, sizeof (idt))) {
        int idx = 0;

        //printf("de %X %X\n",faddr,itable->addr);

        char * libname = table_get_name(direntry->name_rva);
        // printf("%s\n", table_get_name(direntry->name_rva));

        ilt * it;
        dword itentry = vaddr_to_faddr(isect, direntry->lookup_rva);
        it = (ilt*) (ffbufs[0] + itentry);
        printf("rva %X %X %s\n",direntry->iat_rva,itentry,libname);
        dword rvabase = direntry->lookup_rva;
        dword itable = direntry->iat_rva;

        while (it->name_idx) {
            if (it->name_idx & 0x80000000) {
                printf("ordinal\n");
                exit(0);
            }
            dword evaddr = itable;
            t->vaddr = evaddr;
            t->libname = libname;
            t->idx = idx;
            t->name = table_get_name(it->name_idx + 2);
            //printf("%X\n",ex_opt_header->image_base);
          //   printf("%X %X %s %s %X\n",ex_opt_header->image_base, it->name_idx,t->libname,t->name,evaddr);

            t++;
            it++;
            rvabase += 4;
            idx++;
            itable +=4;
        }
        direntry++;
    }
    
    import_table_sz = (int) (t - import_table);
    log_debug("Total imports: %i", import_table_sz);
}

static char * _pe_inner_read(dword base, dword size) {
    dword f_idx = base / BUF_SIZE;
    dword t_idx = f_idx;
    dword rd = 0;
    dword rd_sz;
    dword rd_base;

    while (size) {
        rd_sz = size > (BUF_SIZE - base) ? (BUF_SIZE - base) : size;
        rd_base = base - (base % BUF_SIZE);
        if (!ffbufs[t_idx]) {
            fseek(fptr, base, SEEK_SET);
            ffbufs[t_idx] = (unsigned char *) malloc(BUF_SIZE);
            fread(ffbufs[t_idx], 1, BUF_SIZE, fptr);
        }
        base += rd_sz;
        size -= rd_sz;
        t_idx++;
    }
}

pe_reloc_info * process_reloc_table() {
    pe_dir_entry * reloc = (pe_dir_entry*) (&ex_opt_header->base_reloc_table);
    //printf("reloc table at %X %X\n", reloc->addr, reloc->size);
    int pr = 0;
    int total = 0;
    while (pr < reloc->size) {
        pe_section_header * h = including_sect(reloc->addr);
        dword faddr = vaddr_to_faddr(h, reloc->addr + pr);
        pe_reloc_table_block * block = (pe_reloc_table_block*) (ffbufs[0] + faddr);
        //		printf("block %X %X\n", block->rva, block->block_size);
        pr += block->block_size;
        int nentries = (block->block_size - sizeof (pe_reloc_table_block)) / 2;
        total += nentries;
    }

    log_debug("Total %i relocs \n", total);
    pe_reloc_info * out = (pe_reloc_info*) malloc(total * sizeof (pe_reloc_info));
    pe_reloc_info * t = out;
    pr = 0;

    while (pr < reloc->size) {
        pe_section_header * h = including_sect(reloc->addr);
        dword faddr = vaddr_to_faddr(h, reloc->addr + pr);
        pe_reloc_table_block * block = (pe_reloc_table_block*) (ffbufs[0] + faddr);
        //		printf("block %X %X\n", block->rva, block->block_size);
        pr += block->block_size;
        int nentries = (block->block_size - sizeof (pe_reloc_table_block)) / 2;

        word * bl = (word*) (block + 1);
        int i;
        for (i = 0; i < nentries; i++) {
            int type = (*bl) >> 12;
            //if (type && type != 3) printf("type %i\n", type);
            bl++;
            dword offset = *bl & 0x0FFF;
            //printf("%X\n",offset+block->rva);
            t->rva = offset + block->rva;
            t->type = type;
            t++;
        }
    }

    return out;

}

int process_file(char * filename, pe_info * info) {
    log_debug("Loading file %s", filename);
    printf("Loading file %s\n",filename);
    dword fsz;
    dword header_off;
    int i;

    import_table = (it_inner_entry*) malloc(2048 * sizeof (it_inner_entry));
    fptr = fopen(filename, "rb");
    if (!fptr) {
        printf("File not found: %s\n", filename);
        return -1;
    }

    fseek(fptr, 0, SEEK_END);
    fsz = ftell(fptr);
    fseek(fptr, 0, SEEK_SET);

    ffbufs = (byte**) calloc(1, (fsz / BUF_SIZE) * sizeof (byte*));
    //ffbufs[0] = (unsigned char *)malloc(BUF_SIZE);
    //fread(ffbufs[0],1,BUF_SIZE,fptr);
    ffbufs[0] = (unsigned char *) malloc(fsz);
    int rt = fread(ffbufs[0], 1, fsz, fptr);

    memcpy(&header_off, ffbufs[0] + PE_SIG_BASE_OFF, sizeof (dword));
    
    if (ffbufs[0][header_off] == 'N' && ffbufs[0][header_off+1] == 'E') {
        
        return -2;
    }

    log_debug("File is PE\n");
    /* first 4 bytes are a signature, ignore them */
    header_off += sizeof (dword);
    ex_header = (pe_header*) (ffbufs[0] + header_off);
    printf("ex header at %X\n",header_off);
    ex_opt_header = (pe_opt_header32*) (ffbufs[0] + header_off + sizeof (pe_header));
    printf("ex opt at %X\n",header_off + sizeof (pe_header) );
    sects_base = header_off + sizeof (pe_header) + sizeof (pe_opt_header32);
    //printf("%X\n", sects_base);
    sects = (pe_section_header **) malloc(ex_header->num_sections * sizeof (pe_section_header*));

    for (i = 0; i < ex_header->num_sections; i++) {

        sects[i] = (pe_section_header*) (ffbufs[0]+(sects_base + i * sizeof (pe_section_header)));
        log_debug("%s %X %X %X %X", sects[i]->name, sects[i]->vaddr + ex_opt_header->image_base, sects[i]->raw_data_size, sects[i]->vsize,sects[i]->characteristics);
    }

    process_import_table();
    pe_reloc_info * ri = process_reloc_table();
    process_export_table(info);

    strcpy(info->name,filename);
    info->ex_header = ex_header;
    info->ex_opt_header = ex_opt_header;
    info->import_table = import_table;
    info->import_table_sz = import_table_sz;
    info->sects = sects;
    info->sects_base = sects_base;
    info->text = text;
    info->sects_num = ex_header->num_sections;
    info->raw = ffbufs[0];
    info->reloc_info = ri;
    info->base = ffbufs[0];
    
    log_debug("Entry point %X\n",info->ex_opt_header->entry_point);
    
    return 0;
}

dword get_entry_point() {
    //printf("%X\n",ex_opt_header->entry_point);
    return ex_opt_header->entry_point;
}

dword get_text_off(dword off) {
    return off - text->vaddr;
}

sect_info * get_text_section() {
    int i;
    sect_info * tex = (sect_info*) malloc(sizeof (sect_info));
    byte * tbuf;
    for (i = 0; i < ex_header->num_sections; i++) {
        if (!strcmp(sects[i]->name, ".text")) {
            text = sects[i];
            //printf("%X\n",sects[i]->vaddr);
            break;
        }
    }
    tbuf = (byte*) malloc(sects[i]->raw_data_size);
    fseek(fptr, sects[i]->raw_data_ptr, SEEK_SET);
    fread(tbuf, 1, sects[i]->raw_data_size, fptr);
    tex->data = tbuf;
    tex->ep = get_text_off(get_entry_point());
    tex->size = sects[i]->raw_data_size;
    tex->va = sects[i]->vaddr;
    tex->vsize = sects[i]->vsize;
    //printf("%X %X %X\n",sects[i]->raw_data_ptr,tex->ep,sects[i]->raw_data_ptr+tex->ep-text->vaddr);
    return tex;

}

void end_process() {
    free(sects);
    free(ffbufs);
    fclose(fptr);
}

//it_inner_entry * pe_get_call_target(dword vaddr) {
//    return (it_inner_entry*) c_hash_get_k(vaddr, (void*) vaddr, import_table);
//}





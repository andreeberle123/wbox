#include "common.h"

#include <string.h>
#include <stdlib.h>

#define LOCK_HASH(hash) \
    pthread_mutex_lock(&hash->mutex); \

#define UNLOCK_HASH(hash) \
    pthread_mutex_unlock(&hash->mutex); \
    
static void resize(c_hash * h);

/* rehash function, taken from java implementation (HashMap) */
int __c_rehash(int h) {
   h ^= (h >> 20) ^ (h >> 12);
   return h ^ (h >> 7) ^ (h >> 4);
}

/* default key compare, uses memcmp for ptr and uint32 cmp for uint32 keys */
int __c_hash_default_cmp(void * k1, void * k2, void * hash) {
    c_hash * h = (c_hash*)hash;
    if (k1 == k2) {
        return 0;
    }
    if (h->key_type == C_HASH_KEY_TYPE_PTR) {
        return memcmp(k1,k2,h->key_size);
    }
    return 1;
}

c_hash * c_hash_create(int bucket_size, uint32 st_size, uint32 hash_func(void *)) {
    if (st_size == 0)  {
        st_size = 2;
    }
    c_hash * hash = (c_hash*)malloc(sizeof(c_hash));
    pthread_mutex_init(&(hash->mutex),NULL);
    hash->num_buckets = st_size;
    hash->buckets = (c_hash_bucket*)calloc(1,BUCKET_FULL_SIZE(st_size,bucket_size));
    hash->bucket_size = bucket_size;
    hash->seq_st = NULL;
    hash->seq_end = NULL;
    hash->hash_func = hash_func;
    hash->num_entries = 0;
    hash->dirty = 0;
    hash->cmp_func = NULL;
    hash->key_size = 0;
    hash->locked = 0;

    return hash;   
}

c_hash * c_hash_create_pd(int bucket_size, uint32 st_size, uint32 hash_func(void *), uint32 ksize) {
    c_hash * hash = c_hash_create(bucket_size,st_size,hash_func);
    hash->key_type = C_HASH_KEY_TYPE_PTR;
    hash->cmp_func = __c_hash_default_cmp;
    
    return hash;
}

c_hash * c_hash_create_uintd(int bucket_size, uint32 st_size, uint32 hash_func(void *)) {
    c_hash * hash = c_hash_create(bucket_size,st_size,hash_func);
    hash->key_type = C_HASH_KEY_TYPE_UINT32;
    hash->cmp_func = __c_hash_default_cmp;    
            
    return hash;
}

c_hash * c_hash_create_psp(int bucket_size, uint32 st_size, uint32 hash_func(void *), 
        uint32 ksize, int cmp_func(void *, void *, void *)) {
    c_hash * hash = c_hash_create(bucket_size,st_size,hash_func);
    hash->cmp_func = cmp_func;
    hash->key_size = C_HASH_KEY_TYPE_PTR;
    hash->key_size = ksize;
    
    return hash;
}

void c_hash_clear(c_hash * h) {
    //MUTEX_LOCK(h->mutex);
    LOCK_HASH(h)
    h->num_entries = 0;
    h->seq_st = NULL;
    h->seq_end = NULL;
    c_hash_bucket * b = h->buckets;
    c_hash_bucket * bend = BUCKET_OFFSET(h,h->num_buckets);
    while (b < bend) {
        b->size = 0;
		b = (c_hash_bucket*)(((unsigned char*)b)+BUCKET_SIZE(h->bucket_size));
    }
    //MUTEX_UNLOCK(h->mutex);
    UNLOCK_HASH(h)
}

void c_hash_destroy(c_hash * h) {
    /* NOTE this behavior may cause problems if other threads have locked the mutex, use with care */
    //MUTEX_LOCK(h->mutex);
    LOCK_HASH(h)

    pthread_mutex_destroy(&(h->mutex));
    free(h->buckets);
    free(h);
}

uint32 c_hash_put(uint32 hash, void * data, c_hash * h) {
    return c_hash_put_k(hash,NULL,data,h);
}

uint32 c_hash_put_k(uint32 hash, void * key, void * data, c_hash * h) {
    hash = __c_rehash(hash);
    //MUTEX_LOCK(h->mutex);
    
    LOCK_HASH(h)
    if (hash == 0x147e) printf("%X %s\n",hash,key);
    c_hash_bucket * bckt = BUCKET_OFFSET(h,key_to_idx(hash,h->num_buckets));
    c_hash_entry * t = bckt->entries;
    while (t < (bckt->entries+bckt->size) ) {
        if (t->key == hash) {
            //log_debug("hash match %X %X %X",h->cmp_func,t->skey,h);
            if (!h->cmp_func || !key || (!h->cmp_func(t->skey,key,h))) {
                t->key = hash;
                t->data = data;
                //MUTEX_UNLOCK(h->mutex);
                UNLOCK_HASH(h)

                return 0;
            }
        }
        t++;
    }
    while (bckt->size >= h->bucket_size) {
        printf("%X %X %X %X\n",bckt->entries[0].key,bckt->entries[1].key,bckt->entries[2].key,bckt->entries[3].key);
        if (bckt->size >= C_HASH_MAX_SIZE) {
            //MUTEX_UNLOCK(h->mutex);
            UNLOCK_HASH(h)
            return C_HASH_MAX_SIZE_ERROR;
        }
        resize(h);
        bckt = BUCKET_OFFSET(h,key_to_idx(hash,h->num_buckets));
    }

    bckt->entries[bckt->size].key = hash;
    bckt->entries[bckt->size].data = data;
    bckt->entries[bckt->size].skey = key;
    bckt->entries[bckt->size].next = NULL;
    bckt->entries[bckt->size].prev = NULL;
    h->num_entries++;

    if (!h->dirty) {
        if (h->seq_st == NULL) {
            h->seq_end = h->seq_st = &(bckt->entries[bckt->size]);
        }
        else {
            h->seq_end->next = &(bckt->entries[bckt->size]);
            h->seq_end->next->prev = h->seq_end;
            h->seq_end = h->seq_end->next;
        }
    }
    bckt->size = bckt->size + 1;
    //MUTEX_UNLOCK(h->mutex);
    UNLOCK_HASH(h)
    
    return 0;
    
}

uint32 c_hash_put_h(void * key, void * data, c_hash * h) {
    return c_hash_put(h->hash_func(key),data,h);
}

void * c_hash_remove_h(void * key, c_hash * h) {
    return c_hash_remove(h->hash_func(key),h);
    
}

void * c_hash_remove(uint32 hash, c_hash * h) {
    return c_hash_remove_k(hash,NULL,h,1);
}

void * c_hash_remove_k(uint32 hash, void * key, c_hash * h, int rehash) {
    return c_hash_remove_kl(hash,key,h,rehash,1);
}

void * c_hash_remove_kl(uint32 hash, void * key, c_hash * h, int rehash, int lock) {
    if (rehash) {
        hash = __c_rehash(hash);
    }
    //MUTEX_LOCK(h->mutex);
    if (lock) {
        LOCK_HASH(h);
    }

    c_hash_bucket * bckt = BUCKET_OFFSET(h,key_to_idx(hash,h->num_buckets));
    c_hash_entry * t = bckt->entries;
    while (t < (bckt->entries+bckt->size) ) {
        if (t->key == hash) {
            if (!h->cmp_func || !key || (!h->cmp_func(t->skey,key,h))) {
                void * data = t->data;

                if(t->next)t->next->prev = t->prev;
                if(t->prev)t->prev->next = t->next;

                if (h->seq_st == t) {
                    h->seq_st = t->next;
                }
                if (h->seq_end == t) {
                    h->seq_end = t->prev;
                }
                //memcpy(t,t+1,(bckt->entries+bckt->size-t-1));
                memset(t,0,sizeof(c_hash_entry));
                /* TODO as we shift the data, the list becomes corrupt (!) */
                while ((t+1)<bckt->entries+bckt->size) {
                    *t = *(t+1);
                    t++;
                }
                bckt->size--;
                h->num_entries--;
                h->dirty = 1;

                //MUTEX_UNLOCK(h->mutex);
                if (lock) {
                    UNLOCK_HASH(h)
                }
                return data;
            }
        }
        t++;
    }

    //MUTEX_UNLOCK(h->mutex);
    if (lock) {
        UNLOCK_HASH(h)
    }
    return NULL;
}   

void * c_hash_get(uint32 hash, c_hash * h) {
    return c_hash_get_k(hash,NULL,h);
}

void * c_hash_get_k(uint32 hash, void * key, c_hash * h) {
    hash = __c_rehash(hash);
   
    //log_debug("take mutex %X",&h->mutex);
    //MUTEX_LOCK(h->mutex);
    LOCK_HASH(h)
    c_hash_bucket * bckt = BUCKET_OFFSET(h,key_to_idx(hash,h->num_buckets));
    c_hash_entry * t = bckt->entries;
    while (t < (bckt->entries+bckt->size) ) {
        if (t->key == hash) {
            if (!h->cmp_func || !key || (!h->cmp_func(t->skey,key,h))) {
                //MUTEX_UNLOCK(h->mutex);
                UNLOCK_HASH(h);
                return t->data;
            }
        }
        t++;
    }
    //MUTEX_UNLOCK(h->mutex);
    UNLOCK_HASH(h)
    return NULL;
}

int c_hash_contains(uint32 hash, c_hash * h) {
    //MUTEX_LOCK(h->mutex);
    int assess = (c_hash_get(hash,h) != NULL);
    //MUTEX_UNLOCK(h->mutex);
    return assess;
}

void * c_hash_geth(void * key, c_hash * h) {
    return c_hash_get(h->hash_func(key),h);
}

uint32 key_to_idx(uint32 key, uint32 len) {
    return key & (len-1);
}

static void resize(c_hash * h) {

    c_hash_bucket * old_buckets = h->buckets;
    c_hash_bucket * tmp = old_buckets;
    uint32 new_len = 2*h->num_buckets;

    log_debug("resize hash to %i, num_entries is %i",new_len,h->num_entries);
    h->buckets = (c_hash_bucket*)calloc(1,BUCKET_FULL_SIZE(new_len,h->bucket_size));

    while (tmp < (BUCKET_OFFSETH(old_buckets,h->bucket_size,h->num_buckets)) ) {
        c_hash_entry * e = &tmp->entries[0];
        while (e < (tmp->entries+tmp->size)) {
            c_hash_bucket * bckt = BUCKET_OFFSET(h,key_to_idx(e->key,new_len));
            bckt->entries[bckt->size].key = e->key;
            bckt->entries[bckt->size].data = e->data;
            bckt->entries[bckt->size].skey = e->skey;
            bckt->size = bckt->size + 1;
            e++;
        }
        tmp = BUCKET_INCREASE(tmp,h->bucket_size);
    }
    h->num_buckets = new_len;
    h->dirty = 1;
    free(old_buckets);
    
}

c_hash * c_deserialize_hash(c_serialized_hash * s_hash) {
    c_hash * h = c_hash_create(s_hash->bucket_size,s_hash->num_buckets*s_hash->bucket_size,0);
    return h;
}

uint32 c_hash_rebuild_list(c_hash * h) {
    uint32 i;
    //log_debug("rebuilding hash list");
    if (h->num_entries == 0) {
        h->seq_st = h->seq_end = NULL;
        return 0;
    }

    c_hash_entry * e = NULL;
    c_hash_entry * e2;

    c_hash_bucket * b = h->buckets;
    c_hash_bucket * bend = BUCKET_OFFSET(h, h->num_buckets);

    while (b < bend) {
        for (i = 0; i < b->size; i++) {
            if (!e) {
                e = &b->entries[i];
                e->prev = 0;
                h->seq_st = e;
            } else {
                e->next = &b->entries[i];
                e->next->prev = e;
                e2 = e;
                e = e->next;
            }
        }
        b = (c_hash_bucket*) (((unsigned char*) b) + BUCKET_SIZE(h->bucket_size));
    }
    if (!e) {
        h->seq_end = e2;
    } else {
        h->seq_end = e;
    }

    h->dirty = 0;

    return 0;
}

uint32 c_serialize_hash(c_serialized_hash * s_hash, c_hash * h) {
    s_hash->bucket_size = h->bucket_size;
    s_hash->num_buckets = h->num_buckets;
    s_hash->entries_num = h->num_entries;
 
/*
    c_hash_entry * t = h->seq_st;
    c_hash_entry_serial * ts = s_hash->entries;
    for(;;) {
        ts->key = t->key;
        ts->data = t->data;
        if(t == h->seq_end) {
            break;
        }
        t = t->next;
    }
*/
    
    return 0;
}

/*void c_hash_it_remove(c_hash_entry ** it, c_hash * h) {
    c_hash_entry * tmp = (*it)->next;
    c_hash_remove((*it)->key,h);
    *it = tmp;
    
}*/

/*
 * A hash function for strings. This algorithm is a classic from the literature.
 */
unsigned int str_djb2_hash(char *str) {
    unsigned int hash = 5381;
    int c;

	while ((c = *str++)) {
        hash = (hash * 33) ^ c;
	}

    return hash;
}

unsigned int str_djb2_hash2(char *str, int size) {
    unsigned int hash = 5381;
    int c;
    
    char * t = str;

	while (((str - t) < size) && (c = *str++)) {
        hash = (hash * 33) ^ c;
	}

    return hash;
}

#include "common.h"

typedef struct _earray {
    void * data;
    dword data_size;
    dword size;
    dword full_size;
} earray;

void eadd(earray * arr, void * el) {
    memcpy(((byte*)arr->data+arr->size*arr->data_size),el,arr->data_size);
}

template <class _Ty>
class ArrayList {
private:

public:
    
    ArrayList();
    _Ty operator[](int idx) {
        return get(idx);
    }

    void add(_Ty * key);
    _Ty *get(int idx);
    

};

template <class _Ty>
ArrayList<_Ty>::ArrayList() {
    
}

template <class _Ty>
_Ty *ArrayList<_Ty>::get(int idx) {
    return NULL;
}
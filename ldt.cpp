#include "common.h"

void assemble_local_ldt() {
    struct _TEB * ml = (struct _TEB*)calloc(1,sizeof(struct _TEB));
    struct user_desc udx;
    memset(&udx,0,sizeof(udx));
    
    udx.entry_number = 0;

    int ms;
    
    memset(&udx,0,sizeof(udx));
    
    udx.base_addr = (dword)ml;
    udx.entry_number = 0x0;
    udx.limit = sizeof(struct _TEB);
    udx.limit_in_pages = 1;
    ms = syscall(__NR_modify_ldt,1,&udx,sizeof(udx));
    
    printf("modify %i %s\n",ms, ms ? strerror(errno) : "");
    
    asm volatile(
        "movl $0x7, %%eax;"
        "movw %%ax, %%fs;"
    :::
    );
}
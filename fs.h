
#ifndef FS_H
#define FS_H

#include "common.h"
#include <sys/stat.h>



#define DT_VIRT 0x101

typedef struct _vfs_container {
    char path[512];
    char physical_path[512];
    int type;
} vfs_container;

typedef struct _tvfshandle {
    int type;
    vfs_container * vc;
    FILE * ph;
} tvfshandle;

typedef struct _local_find_file {
    int count;
    char base[256];
} local_find_file;

FILE * fs_open_base(char * path, char * mode);
tvfshandle * fs_look_up(char * expr, char * mode, struct stat * st, local_find_file * info);

FILE * fs_open_bpath_stat(char * path, char * mode, struct stat * st, int excl);
void init_fs();



tvfshandle * fs_open_bpath(char * path, char * mode, int excl);


class VirtualFileMap {
    c_hash * paths;
    char cbase[512];
    int cbase_sz;
    
    char cbase_full[512];
    int cbase_full_sz;
    void make_base();
public:
    VirtualFileMap();
    char * remap_from_absolute(char * path, char * dest);
    char * remap_from_current(char * path, char * dest);
    char * remap_from_rel(char * path, char * dest);
    tvfshandle * lookup_first(char * expr, local_find_file * info);
    void process_dir(char * path);
    void addPath(char * path);    
    tvfshandle * virtual_handle_local(char * opath);
    tvfshandle * virtual_handle(char * opath);
    tvfshandle* createNewFile(char * path);
    vfs_container* addFile(char * path,char * orig, int type);
    int get_real_path(tvfshandle * handle, char * path);
    FILE * get_phys_handle(tvfshandle * handle, char * mode);
    void get_emulated_address(tvfshandle * handle, char * dest);
    void matchFile(char * path);
    void add_fixed_virtuals();
    void free_handle(void * handle);
};

extern VirtualFileMap * vfmap;

#endif /* FS_H */


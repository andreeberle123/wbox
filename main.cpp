
#include <signal.h>
#include "common.h"
#include "peloader.h"
#include "exec.h"
#include "xw.h"
#include "gl.h"
#include "fs.h"
#include "ne.h"

void dbg_start(dword addr);

typedef struct _ljmpd {
    unsigned short int base;
    unsigned short int seg;
    
} __attribute__((packed))ljmpd;



int do_exec(char * fpath, char * args[], int argnum) {
    int rt;
    pe_info info;
    ne_file_info ne;
    int pe = 1;
    rt = process_file(fpath,&info);
    if (rt == -2) {
        pe = 0;
        process_file_ne(fpath,&ne);
    }
    else if (rt < 0) {
        return rt;
    }
    
    if (pe) {
        rt = load_exec(&info);
    }
    else {
        rt = load_exec_ne(&ne);
    }
    
    if (rt < 0) {
        return rt;
    }
    
    if (global_config_data.exec_mode == 1) {
        loader_init();
    }
    
    dword ep,cs,ds;
    if (pe) {
        ep = info.ex_opt_header->entry_point + info.ex_opt_header->image_base;
    }
    else {
        //ep = ne.entry+(dword)ne.map_base;
        ep = ne.entry;
        cs = ne.cs;
        ds = ne.ds;
    }
    
    log_debug("Start at %X",ep);
    printf("local ep at %X\n",ep);
    
    if (global_config_data.exec_mode == 0) {
        //assemble_local_ldt();

        setup_tls();
        
#ifdef _DBG
        dbg_start(ep);
#endif
    }
    
    if (pe) {

        __asm volatile (
            "jmp %0;"
        :: "r"(ep)
        :
        );
    }
    else {
        
        // Segment register is bits 0-1 -- ring, bit 2 ldt/gdt (0 gdt, 1 ldt)
        // other bits segnum
        
        cs = (cs<<3) + 0x7;
        ds = (ds<<3) + 0x7;
        ljmpd l;
        
        l.seg = cs;
        l.base = ep;
        printf("cs %X %X\n",cs,ep);
        
//        __asm volatile (
//            "ljmp *(%0);"
//        :: "r"(&l)
//        );
         __asm volatile (
            "movw %2,%%ds;"
            "movw %2,%%ss;"
            "movl $0xFC00,%%esp;"
            "push %0;"
            "push %1;"
            "retf;"
        :: "r"(cs),"r"(ep),"r"(ds)
        );
        
//        __asm volatile (
//            "ljmp $0x0100,$0x0;"
//        :::
//        );
    }
    
    return 0;
}

void show_usage() {
    printf("wrong parameters\n");
}

int process_args(int argc,char * argv[]) {
    int i;
    if (argc <= 1) {
        show_usage();
        return -1;
    }
    
    
    for(i=1;i<argc;i++) {
        if (*argv[i] != '-') {
            break;
        }
        if (!strcmp(argv[i],"--version")) {
            printf("version 0.0.1\n");
        }
        else {
            show_usage();
            return -1;
        }
        
    }
    
    init_config(argc, argv);
    init_fs();
    init_winm_system();

    //localRegistry->dump();
    localRegistry->process_reg_file("/home/z/eofs/eofs/SUNS/EMPEROR.REG");
    localRegistry->process_reg_file("/home/z/wbox/a1.reg");
    localRegistry->process_reg_file("d1.reg");
    
    do_exec(argv[1],argv+2, argc-2);
}

void xc_test();

void strap(int sig, siginfo_t * info, void * ucontext) {
    printf("trapped %i\n",info->si_signo);
    exit(0);
}

void tt() {
    struct sigaction sa;
//
    memset(&sa, 0, sizeof (struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = strap;
    sa.sa_flags = SA_SIGINFO;

    int i;
    //for(i=0;i<200;i++)
    sigaction(SIGINT, &sa, NULL);
}

int main(int argc, char * argv[]) {
//    tt();
//    
//    asm volatile (
//        "int $0x21;"
//    :::
//    );
//    pe_info px;
//    process_file("shelf/user32.dll",&px);
//    exit(0);
    printf("stdout at %X\n",&_IO_2_1_stdout_);
    printf("stdin at %X\n",&_IO_2_1_stdin_);
    printf("stderr at %X\n",&_IO_2_1_stderr_);
    
    //printf("%X\n",offsetof(pe_opt_header32,resource_table));
    //return 0;
   // xc_test();
    argv[1] = "/home/z/mt/tmp/SETUP.EXE";
    argc = 2;
   // init_gl(100,100,0,0,"A");    
    int rt;
    rt = process_args(argc,argv);
    if (rt < 0) {
        return rt;
    }
    
    
    return 0;
}


#ifndef WINM_H
#define WINM_H

#include "common.h"

class ImageSink;

#define SHELF "shelf"

#define HARD_NODE_BLOCK 32
#define HARD_NODE_CH_SIZE (5)

typedef unsigned char BOOL;
typedef unsigned long long UINT64;
typedef unsigned long ULONG_PTR;
typedef unsigned short USHORT;
typedef float FLOAT;
typedef long* LRESULT;
typedef unsigned int * WPARAM;
typedef long* LPARAM;


typedef void * HBRUSH;

typedef void * HCURSOR;
typedef void * HINSTANCE;
typedef void * HMENU;
typedef void * HICON;

typedef struct _tree_header {
    dword next_node;
    dword first_valid;
    dword first_empty;
} tree_header;

#define STRING_MMBLOCK  256

typedef struct _stringtable_header {
    dword chunks[5];
    dword first_non_chunked;
} stringtable_header;

typedef struct _stringindex_header {
    dword topidx;
    dword first_empty;
} stringindex_header;

typedef struct _stringindex_entry {
    dword offset;
} stringindex_enty;

typedef struct _hard_tree_node {
    dword stridx;
    dword default_leaf;
    dword children_size;
    char data[];
} __attribute__((packed)) hard_tree_node;

typedef struct _hard_tree_leaf {
    dword stridx;
    dword type;
    dword data1;
    dword data2;
    dword pad[16];
} __attribute__((packed)) hard_tree_leaf;

typedef struct _tree_node {
    int ltype;
    char name[256];
    dword file_offset;
    struct _tree_node * parent;
    struct _tree_node ** children;
    int children_size;
    int children_full_size;
    pthread_mutex_t mutex;
    dword lnodes[16];
    hard_tree_node hfnode;
} tree_node;

typedef struct _tree_leaf_node {
    int ltype;
    char name[256];
    int type;
    dword file_offset;
    hard_tree_leaf hf;
    dword data[2];
    
} tree_leaf_node;

typedef struct _block_elem {
    struct _block_elem * next;
    dword off;
} block_elem;



class LocalRegistry {
private:
    tree_node * root;
    void create_default_keys();
    pthread_mutex_t storage_lock;
    pthread_mutex_t strings_lock;
    tree_header node_header;
    tree_header leaf_header;
    stringtable_header string_header;
    stringindex_header string_idx_header;
    
    dword * string_indices = NULL;
    int string_indices_sz = 0;    
    int string_indices_full_sz = 0;
    
    struct _block_elem * sblocks[5];
    
    FILE * node_fp;
    FILE * leaf_fp;
    
    FILE * string_idx_fp;
    FILE * string_fp;
    /***************************************************************************
     * strings
     */
    c_hash * string_gen_table;
    c_hash * keys_mm;
    dword string_h_lookup(int len);
    dword next_free_node(int leaf);
    void build_from_file();
    void build_string_blocks_from_file();
    
    
    // c_hash * local_cache;
public:
    
    LocalRegistry();
    tree_node * create_key(char * path);
    int free_string(char * str);
    int add_key(tree_node * parent, tree_node * child, int do_store);
    tree_node * new_key(char * name, int new_off);
    void store_key(tree_node * key);
    tree_leaf_node * new_leaf(char * name, dword type, int new_off);

    void store_leaf(tree_leaf_node * leaf);
    dword setup_string(char * str);
    void build_meta_path(tree_node * key);
    tree_node * meta_search(char * path);
    void process_reg_file(char * path);
    void set_leaf_data(tree_leaf_node * tf, dword type, void * data);
    tree_node * node_look_up(tree_node * tn, char * child);
    int leaf_get_data(tree_leaf_node * tf, void ** p);
    
    void dump();
    
    
    
};

void init_winm_system();

extern LocalRegistry * localRegistry;

#define REG_NONE                    ( 0 )   // No value type
#define REG_SZ                      ( 1 )   // Unicode nul terminated string
#define REG_EXPAND_SZ               ( 2 )   // Unicode nul terminated string
                                            // (with environment variable references)
#define REG_BINARY                  ( 3 )   // Free form binary
#define REG_DWORD                   ( 4 )   // 32-bit number
#define REG_DWORD_LITTLE_ENDIAN     ( 4 )   // 32-bit number (same as REG_DWORD)
#define REG_DWORD_BIG_ENDIAN        ( 5 )   // 32-bit number
#define REG_LINK                    ( 6 )   // Symbolic Link (unicode)
#define REG_MULTI_SZ                ( 7 )   // Multiple Unicode strings
#define REG_RESOURCE_LIST           ( 8 )   // Resource list in the resource map
#define REG_FULL_RESOURCE_DESCRIPTOR ( 9 )  // Resource list in the hardware description
#define REG_RESOURCE_REQUIREMENTS_LIST ( 10 )
#define REG_QWORD                   ( 11 )  // 64-bit number
#define REG_QWORD_LITTLE_ENDIAN     ( 11 )  // 64-bit number (same as REG_QWORD)

typedef long LONG;

#define HMODULE void *
#define LPCTSTR char *
#define WINAPI __attribute__((stdcall))
typedef void * LPVOID;
typedef long *LPLONG;

typedef unsigned char BYTE;

typedef BYTE* LPBYTE;
#define LPSTR char *
#define LPCSTR char *
typedef unsigned int DWORD, *LPDWORD;
typedef void * HDC;
typedef void * HANDLE;
typedef unsigned int UINT;
typedef ULONG_PTR SIZE_T, *PSIZE_T;
#define LPCVOID const void *
typedef LONG HRESULT;
typedef void * HWND;
typedef word WORD;
typedef WORD* LPWORD;

typedef struct _FLOATING_SAVE_AREA {
    DWORD ControlWord;
    DWORD StatusWord;
    DWORD TagWord;
    DWORD ErrorOffset;
    DWORD ErrorSelector;
    DWORD DataOffset;
    DWORD DataSelector;
    BYTE RegisterArea[80];
    DWORD Spare0;
} FLOATING_SAVE_AREA;

typedef struct _SECURITY_ATTRIBUTES {
    DWORD  nLength;
    LPVOID lpSecurityDescriptor;
    BOOL   bInheritHandle;
} SECURITY_ATTRIBUTES, *PSECURITY_ATTRIBUTES, *LPSECURITY_ATTRIBUTES;

typedef struct _CONTEXT {
    DWORD ContextFlags;
    DWORD Dr0;
    DWORD Dr1;
    DWORD Dr2;
    DWORD Dr3;
    DWORD Dr6;
    DWORD Dr7;
    FLOATING_SAVE_AREA FloatSave;
    DWORD SegGs;
    DWORD SegFs;
    DWORD SegEs;
    DWORD SegDs;
    DWORD Edi;
    DWORD Esi;
    DWORD Ebx;
    DWORD Edx;
    DWORD Ecx;
    DWORD Eax;
    DWORD Ebp;
    DWORD Eip;
    DWORD SegCs; // MUST BE SANITIZED
    DWORD EFlags; // MUST BE SANITIZED
    DWORD Esp;
    DWORD SegSs;
    BYTE ExtendedRegisters[512];

} CONTEXT, *PCONTEXT;

extern "C" {

void init_dx_subsystem();
}

typedef void * PVOID;
typedef long long INT64;
typedef unsigned long ULONG;
typedef unsigned char UCHAR;
typedef void VOID;

typedef struct _UNICODE_STRING
{
     WORD Length;
     WORD MaximumLength;
     WORD * Buffer;
} UNICODE_STRING, *PUNICODE_STRING;

typedef struct _LIST_ENTRY {
    struct _LIST_ENTRY *Flink;
    struct _LIST_ENTRY *Blink;
} LIST_ENTRY, *PLIST_ENTRY, *PRLIST_ENTRY;

 	
typedef struct _STRING
{
     WORD Length;
     WORD MaximumLength;
     char * Buffer;
} STRING, *PSTRING;

typedef struct _CURDIR
{
     UNICODE_STRING DosPath;
     PVOID Handle;
} CURDIR, *PCURDIR;

typedef struct _RTL_DRIVE_LETTER_CURDIR
{
     WORD Flags;
     WORD Length;
     ULONG TimeStamp;
     STRING DosPath;
} RTL_DRIVE_LETTER_CURDIR, *PRTL_DRIVE_LETTER_CURDIR;



typedef struct _RTL_USER_PROCESS_PARAMETERS
{
    ULONG MaximumLength; //0
    ULONG Length;
    ULONG Flags;
    ULONG DebugFlags;
    PVOID ConsoleHandle; //0x10
    ULONG ConsoleFlags;
    PVOID StandardInput;
    PVOID StandardOutput;
    PVOID StandardError; //0x20
    CURDIR CurrentDirectory; // 0x24
    UNICODE_STRING DllPath; // 0x30
    //UNICODE_STRING ImagePathName; 
    WORD unk1; // 0x38
    WORD unk2; // 0x3A
    DWORD ImagePathOff; // 0x3c
    UNICODE_STRING CommandLine; //0x40
    PVOID Environment; //0x48
    ULONG StartingX; // 0x4c
    ULONG StartingY; // 0x50
    ULONG CountX; // 0x54
    ULONG CountY; // 0x58
    ULONG CountCharsX; // 0x5c
    ULONG CountCharsY; // 0x60
    ULONG FillAttribute; // 0x64
    ULONG WindowFlags; // 0x68
    ULONG ShowWindowFlags; // 0x6c
    UNICODE_STRING WindowTitle; // 0x70
    UNICODE_STRING DesktopInfo; // 0x7c
    UNICODE_STRING ShellInfo;
    UNICODE_STRING RuntimeData;
    RTL_DRIVE_LETTER_CURDIR CurrentDirectores[32];
     ULONG EnvironmentSize;
} __attribute__((packed)) RTL_USER_PROCESS_PARAMETERS, *PRTL_USER_PROCESS_PARAMETERS;

typedef unsigned char BOOLEAN;

typedef struct _PEB_LDR_DATA {
    ULONG Length;
    dword Initialized;
    HANDLE SsHandle;
    LIST_ENTRY InLoadOrderModuleList;
    LIST_ENTRY InMemoryOrderModuleList;
    LIST_ENTRY InInitializationOrderModuleList;
    PVOID EntryInProgress;
    BOOLEAN ShutdownInProgress;
    HANDLE ShutdownThreadId;
} __attribute__((aligned (4))) PEB_LDR_DATA, *PPEB_LDR_DATA;

typedef struct _LDR_DATA_TABLE_ENTRY
{
     LIST_ENTRY InLoadOrderLinks;
     LIST_ENTRY InMemoryOrderLinks;
     LIST_ENTRY InInitializationOrderLinks;
     PVOID DllBase;
     PVOID EntryPoint;
     ULONG SizeOfImage;
     UNICODE_STRING FullDllName;
     UNICODE_STRING BaseDllName;
     ULONG Flags;
     WORD LoadCount;
     WORD TlsIndex;
     union
     {
          LIST_ENTRY HashLinks;
          struct
          {
               PVOID SectionPointer;
               ULONG CheckSum;
          };
     };
     union
     {
          ULONG TimeDateStamp;
          PVOID LoadedImports;
     };
     void * EntryPointActivationContext;
     PVOID PatchInformation;
     LIST_ENTRY ForwarderLinks;
     LIST_ENTRY ServiceTagLinks;
     LIST_ENTRY StaticLinks;
} LDR_DATA_TABLE_ENTRY, *PLDR_DATA_TABLE_ENTRY;

struct _RTL_CRITICAL_SECTION;

typedef struct _RTL_CRITICAL_SECTION_DEBUG
{
     WORD Type;
     WORD CreatorBackTraceIndex;
     struct _RTL_CRITICAL_SECTION *CriticalSection;
     LIST_ENTRY ProcessLocksList;
     ULONG EntryCount;
     ULONG ContentionCount;
     ULONG Flags;
     WORD CreatorBackTraceIndexHigh;
     WORD SpareUSHORT;
} RTL_CRITICAL_SECTION_DEBUG, *PRTL_CRITICAL_SECTION_DEBUG;

typedef struct _RTL_CRITICAL_SECTION
{
     PRTL_CRITICAL_SECTION_DEBUG DebugInfo;
     LONG LockCount;
     LONG RecursionCount;
     PVOID OwningThread;
     PVOID LockSemaphore;
     ULONG SpinCount;
} RTL_CRITICAL_SECTION, *PRTL_CRITICAL_SECTION;

typedef struct _LARGE_INTEGER
{
     union
     {
          struct
          {
               ULONG LowPart;
               LONG HighPart;
          };
          INT64 QuadPart;
     };
} LARGE_INTEGER, *PLARGE_INTEGER;

typedef struct _ULARGE_INTEGER
{
     union
     {
          struct
          {
               ULONG LowPart;
               ULONG HighPart;
          };
          UINT64 QuadPart;
     };
} ULARGE_INTEGER, *PULARGE_INTEGER;

typedef struct _PEB_FREE_BLOCK
{
     struct _PEB_FREE_BLOCK * Next;
     ULONG Size;
} PEB_FREE_BLOCK, *PPEB_FREE_BLOCK;

typedef struct _PHEAP {
    dword unk;
    dword unk2;
    dword unk3;
    dword unk4;
    dword flags;
} PHEAP;

typedef struct _ACTIVATION_CONTEXT_DATA {
    ULONG Magic;
    ULONG HeaderSize;
    ULONG FormatVersion;
    ULONG TotalSize;
    ULONG DefaultTocOffset;
    ULONG ExtendedTocOffset;
    ULONG AssemblyRosterOffset;
    ULONG Flags;
} ACTIVATION_CONTEXT_DATA, *PACTIVATION_CONTEXT_DATA;

typedef struct _GUID {
    unsigned long Data1;
    unsigned short Data2;
    unsigned short Data3;
    unsigned char Data4[ 8 ];
} GUID;


typedef struct tagPOINT {
    LONG x;
    LONG y;
} POINT, *PPOINT, *LPPOINT;

typedef struct _PEB {
    UCHAR InheritedAddressSpace;
    UCHAR ReadImageFileExecOptions;
    UCHAR BeingDebugged;
    //UCHAR BitField;
    ULONG ImageUsesLargePages : 1; 
    ULONG IsProtectedProcess : 1;
    ULONG IsLegacyProcess : 1; 
    ULONG IsImageDynamicallyRelocated : 1;
    ULONG SpareBits : 4;
    PVOID Mutant; // 0x4
    PVOID ImageBaseAddress; //0x8
    PPEB_LDR_DATA Ldr; //0xc
    PRTL_USER_PROCESS_PARAMETERS ProcessParameters; //0x10
    PVOID SubSystemData;
    PHEAP *ProcessHeap; // 0x18
    PRTL_CRITICAL_SECTION FastPebLock;
    PVOID AtlThunkSListPtr; // 0x20
    PVOID IFEOKey;
    ULONG CrossProcessFlags;
    ULONG ProcessInJob : 1;
    ULONG ProcessInitializing : 1;
    ULONG ReservedBits0 : 30;
    
    

    union {
        PVOID KernelCallbackTable;
        PVOID UserSharedInfoPtr;
    } bb; // 0x30
    ULONG SystemReserved[1];
    ULONG SpareUlong;
    PPEB_FREE_BLOCK FreeList;
    ULONG TlsExpansionCounter; // 0x40
    PVOID TlsBitmap;
    ULONG TlsBitmapBits[2];
    PVOID ReadOnlySharedMemoryBase; //0x50
    PVOID HotpatchInformation; // 0x54
    BYTE * SomeValues1; // 0x58 -- this is wrong
    BYTE * SomeValues2; // 0x5c
    BYTE * SomeValues3; //0x60
    PVOID UnicodeCaseTableData; // 0x64
    ULONG NumberOfProcessors; // 0x68
    ULONG NtGlobalFlag; // 0x6c
    //LARGE_INTEGER CriticalSectionTimeout; // 0x70
    DWORD unk1; // 0x70
    DWORD unk2; // 0x74
    ULONG HeapSegmentReserve; // 0x78
    ULONG HeapSegmentCommit; //0x7c
    ULONG HeapDeCommitTotalFreeThreshold; //0x80
    ULONG HeapDeCommitFreeBlockThreshold; //0x84
    ULONG NumberOfHeaps; //0x88
    ULONG MaximumNumberOfHeaps; //0x8c
    VOID * * ProcessHeaps; //0x90
    PVOID GdiSharedHandleTable; //0x94
    PVOID ProcessStarterHelper;
    ULONG GdiDCAttributeList;
    PRTL_CRITICAL_SECTION LoaderLock;
    ULONG OSMajorVersion;
    ULONG OSMinorVersion;
    WORD OSBuildNumber;
    WORD OSCSDVersion;
    ULONG OSPlatformId;
    ULONG ImageSubsystem;
    ULONG ImageSubsystemMajorVersion;
    ULONG ImageSubsystemMinorVersion;
    ULONG ImageProcessAffinityMask;
    ULONG GdiHandleBuffer[34];
    PVOID PostProcessInitRoutine;
    PVOID TlsExpansionBitmap;
    ULONG TlsExpansionBitmapBits[32];
    ULONG SessionId;
    ULARGE_INTEGER AppCompatFlags;
    ULARGE_INTEGER AppCompatFlagsUser;
    PVOID pShimData;
    PVOID AppCompatInfo;
    UNICODE_STRING CSDVersion;
    void * ActivationContextData;
    void * ProcessAssemblyStorageMap;
    ACTIVATION_CONTEXT_DATA * SystemDefaultActivationContextData; // 0x200
    void * SystemAssemblyStorageMap; // 0x204
    ULONG MinimumStackCommit; // 0x208
    void * FlsCallback; // 0x20c
    LIST_ENTRY FlsListHead;
    PVOID FlsBitmap;
    ULONG FlsBitmapBits[4];
    ULONG FlsHighIndex;
    PVOID WerRegistrationData;
    PVOID WerShipAssertPtr; // 0x234
} __attribute__((packed)) PEB, *PPEB;

typedef struct _CLIENT_ID {
    void * UniqueProcess;
    void * UniqueThread;
} CLIENT_ID, *PCLIENT_ID;

typedef enum _EXCEPTION_DISPOSITION
{
    ExceptionContinueExecution = 0,
    ExceptionContinueSearch = 1,
    ExceptionNestedException = 2,
    ExceptionCollidedUnwind = 3
} EXCEPTION_DISPOSITION;

typedef struct _EXCEPTION_REGISTRATION_RECORD
{
     struct _EXCEPTION_REGISTRATION_RECORD* Next;
     EXCEPTION_DISPOSITION *Handler;
} EXCEPTION_REGISTRATION_RECORD, *PEXCEPTION_REGISTRATION_RECORD;

/* This structure is 56 bytes on x64, and 28 bytes on x86 */
struct _NT_TIB { //  x86  /  x64
    PEXCEPTION_REGISTRATION_RECORD ExceptionList; // 0x000 / 0x000
    void *StackBase; // 0x004 / 0x008
    void *StackLimit; // 0x008 / 0x010
    void *SubSystemTib; // 0x00c / 0x018

    union {
        void *FiberData; // 0x010 / 0x020
        uint32_t Version; // 0x010 / 0x020
    };
    void *ArbitraryUserPointer; // 0x014 / 0x028
    struct _NT_TIB *Self; // 0x018 / 0x030
} __attribute__((packed));

struct _TEB { //  x86  /  x64
    struct _NT_TIB NtTib; // 0x000 / 0x000
    void *EnvironmentPointer; // 0x01c / 0x038
    struct _CLIENT_ID ClientId; // 0x020 / 0x040
    void *ActiveRpcHandle; // 0x028 / 0x050
    void *ThreadLocalStoragePointer; // 0x02c / 0x058
    struct _PEB *ProcessEnvironmentBlock; // 0x030 / 0x060
    uint32_t LastErrorValue; // 0x034 / 0x068
    uint32_t CountOfOwnedCriticalSections; // 0x038 / 0x06c
    void *CsrClientThread; // 0x03c / 0x070
    void *Win32ThreadInfo; // 0x040 / 0x078
    uint32_t User32Reserved[26]; // 0x044 / 0x080
    uint32_t UserReserved[5]; // 0x0ac / 0x0e8
    void *WOW32Reserved; // 0x0c0 / 0x100?
    uint32_t CurrentLocale; // 0x0c4
    uint32_t FpSoftwareStatusRegister; // 0x0c8
    void *SystemReserved1[54]; // 0x0cc
    uint32_t ExceptionCode; // 0x1a4
    //  struct _ACTIVATION_CONTEXT_STACK ActivationContextStack; // 0x1a8
    //uint8_t SpareBytes1[24]; // 0x1bc
    //struct _GDI_TEB_BATCH           GdiTebBatch;                  // 0x1d4
} __attribute__((packed));

extern "C" {
dword WINAPI __isyscall_trap(dword reserved, dword * ssize,dword idx, ...);
dword __syscall_trap(...);    
}

void setup_tls();
void * create_semaphore(int c);
void * create_event();

typedef enum _TOKEN_INFORMATION_CLASS {
  TokenUser                            =1 ,
  TokenGroups                           ,
  TokenPrivileges                       ,
  TokenOwner                            ,
  TokenPrimaryGroup                     ,
  TokenDefaultDacl                      ,
  TokenSource                           ,
  TokenType                             ,
  TokenImpersonationLevel               ,
  TokenStatistics                       ,
  TokenRestrictedSids                   ,
  TokenSessionId                        ,
  TokenGroupsAndPrivileges              ,
  TokenSessionReference                 ,
  TokenSandBoxInert                     ,
  TokenAuditPolicy                      ,
  TokenOrigin                           ,
  TokenElevationType                    ,
  TokenLinkedToken                      ,
  TokenElevation                        ,
  TokenHasRestrictions                  ,
  TokenAccessInformation                ,
  TokenVirtualizationAllowed            ,
  TokenVirtualizationEnabled            ,
  TokenIntegrityLevel                   ,
  TokenUIAccess                         ,
  TokenMandatoryPolicy                  ,
  TokenLogonSid                         ,
  TokenIsAppContainer                   ,
  TokenCapabilities                     ,
  TokenAppContainerSid                  ,
  TokenAppContainerNumber               ,
  TokenUserClaimAttributes              ,
  TokenDeviceClaimAttributes            ,
  TokenRestrictedUserClaimAttributes    ,
  TokenRestrictedDeviceClaimAttributes  ,
  TokenDeviceGroups                     ,
  TokenRestrictedDeviceGroups           ,
  TokenSecurityAttributes               ,
  TokenIsRestricted                     ,
  TokenProcessTrustLevel                ,
  TokenPrivateNameSpace                 ,
  TokenSingletonAttributes              ,
  TokenBnoIsolation                     ,
  TokenChildProcessFlags                ,
  MaxTokenInfoClass
} TOKEN_INFORMATION_CLASS, *PTOKEN_INFORMATION_CLASS;

dword WINAPI __gx_trap(dword reserved, dword * ssize, dword idx, va_list vl);
#define GET_ARG(h,p) h p = ( h )va_arg(vl, h );

void * build_gdi_table();

typedef short SHORT;

#define VK_LBUTTON        0x01
#define VK_RBUTTON        0x02
#define VK_CANCEL         0x03
#define VK_MBUTTON        0x04    /* NOT contiguous with L & RBUTTON */

#if(_WIN32_WINNT >= 0x0500)
#define VK_XBUTTON1       0x05    /* NOT contiguous with L & RBUTTON */
#define VK_XBUTTON2       0x06    /* NOT contiguous with L & RBUTTON */
#endif /* _WIN32_WINNT >= 0x0500 */

/*
 * 0x07 : unassigned
 */

#define VK_BACK           0x08
#define VK_TAB            0x09

/*
 * 0x0A - 0x0B : reserved
 */

#define VK_CLEAR          0x0C
#define VK_RETURN         0x0D

#define VK_SHIFT          0x10
#define VK_CONTROL        0x11
#define VK_MENU           0x12
#define VK_PAUSE          0x13
#define VK_CAPITAL        0x14 // caps lock

#define VK_KANA           0x15
#define VK_HANGEUL        0x15  // wtf
#define VK_HANGUL         0x15
#define VK_JUNJA          0x17
#define VK_FINAL          0x18
#define VK_HANJA          0x19
#define VK_KANJI          0x19

#define VK_ESCAPE         0x1B

#define VK_CONVERT        0x1C
#define VK_NONCONVERT     0x1D
#define VK_ACCEPT         0x1E
#define VK_MODECHANGE     0x1F

#define VK_SPACE          0x20
#define VK_PRIOR          0x21
#define VK_NEXT           0x22
#define VK_END            0x23
#define VK_HOME           0x24
#define VK_LEFT           0x25
#define VK_UP             0x26
#define VK_RIGHT          0x27
#define VK_DOWN           0x28
#define VK_SELECT         0x29
#define VK_PRINT          0x2A
#define VK_EXECUTE        0x2B
#define VK_SNAPSHOT       0x2C
#define VK_INSERT         0x2D
#define VK_DELETE         0x2E
#define VK_HELP           0x2F

/*
 * VK_0 - VK_9 are the same as ASCII '0' - '9' (0x30 - 0x39)
 * 0x40 : unassigned
 * VK_A - VK_Z are the same as ASCII 'A' - 'Z' (0x41 - 0x5A)
 */

#define VK_LWIN           0x5B
#define VK_RWIN           0x5C
#define VK_APPS           0x5D

/*
 * 0x5E : reserved
 */

#define VK_SLEEP          0x5F

#define VK_NUMPAD0        0x60
#define VK_NUMPAD1        0x61
#define VK_NUMPAD2        0x62
#define VK_NUMPAD3        0x63
#define VK_NUMPAD4        0x64
#define VK_NUMPAD5        0x65
#define VK_NUMPAD6        0x66
#define VK_NUMPAD7        0x67
#define VK_NUMPAD8        0x68
#define VK_NUMPAD9        0x69
#define VK_MULTIPLY       0x6A
#define VK_ADD            0x6B
#define VK_SEPARATOR      0x6C
#define VK_SUBTRACT       0x6D
#define VK_DECIMAL        0x6E
#define VK_DIVIDE         0x6F
#define VK_F1             0x70
#define VK_F2             0x71
#define VK_F3             0x72
#define VK_F4             0x73
#define VK_F5             0x74
#define VK_F6             0x75
#define VK_F7             0x76
#define VK_F8             0x77
#define VK_F9             0x78
#define VK_F10            0x79
#define VK_F11            0x7A
#define VK_F12            0x7B
#define VK_F13            0x7C
#define VK_F14            0x7D
#define VK_F15            0x7E
#define VK_F16            0x7F
#define VK_F17            0x80
#define VK_F18            0x81
#define VK_F19            0x82
#define VK_F20            0x83
#define VK_F21            0x84
#define VK_F22            0x85
#define VK_F23            0x86
#define VK_F24            0x87

/*
 * 0x88 - 0x8F : unassigned
 */

#define VK_NUMLOCK        0x90
#define VK_SCROLL         0x91

/*
 * NEC PC-9800 kbd definitions
 */
#define VK_OEM_NEC_EQUAL  0x92   // '=' key on numpad

/*
 * Fujitsu/OASYS kbd definitions
 */
#define VK_OEM_FJ_JISHO   0x92   // 'Dictionary' key
#define VK_OEM_FJ_MASSHOU 0x93   // 'Unregister word' key
#define VK_OEM_FJ_TOUROKU 0x94   // 'Register word' key
#define VK_OEM_FJ_LOYA    0x95   // 'Left OYAYUBI' key
#define VK_OEM_FJ_ROYA    0x96   // 'Right OYAYUBI' key

/*
 * 0x97 - 0x9F : unassigned
 */

/*
 * VK_L* & VK_R* - left and right Alt, Ctrl and Shift virtual keys.
 * Used only as parameters to GetAsyncKeyState() and GetKeyState().
 * No other API or message will distinguish left and right keys in this way.
 */
#define VK_LSHIFT         0xA0
#define VK_RSHIFT         0xA1
#define VK_LCONTROL       0xA2
#define VK_RCONTROL       0xA3
#define VK_LMENU          0xA4
#define VK_RMENU          0xA5

#define VK_BROWSER_BACK        0xA6
#define VK_BROWSER_FORWARD     0xA7
#define VK_BROWSER_REFRESH     0xA8
#define VK_BROWSER_STOP        0xA9
#define VK_BROWSER_SEARCH      0xAA
#define VK_BROWSER_FAVORITES   0xAB
#define VK_BROWSER_HOME        0xAC

#define VK_VOLUME_MUTE         0xAD
#define VK_VOLUME_DOWN         0xAE
#define VK_VOLUME_UP           0xAF
#define VK_MEDIA_NEXT_TRACK    0xB0
#define VK_MEDIA_PREV_TRACK    0xB1
#define VK_MEDIA_STOP          0xB2
#define VK_MEDIA_PLAY_PAUSE    0xB3
#define VK_LAUNCH_MAIL         0xB4
#define VK_LAUNCH_MEDIA_SELECT 0xB5
#define VK_LAUNCH_APP1         0xB6
#define VK_LAUNCH_APP2         0xB7



/*
 * 0xB8 - 0xB9 : reserved
 */

#define VK_OEM_1          0xBA   // ';:' for US
#define VK_OEM_PLUS       0xBB   // '+' any country
#define VK_OEM_COMMA      0xBC   // ',' any country
#define VK_OEM_MINUS      0xBD   // '-' any country
#define VK_OEM_PERIOD     0xBE   // '.' any country
#define VK_OEM_2          0xBF   // '/?' for US
#define VK_OEM_3          0xC0   // '`~' for US

/*
 * 0xC1 - 0xD7 : reserved
 */

/*
 * 0xD8 - 0xDA : unassigned
 */

#define VK_OEM_4          0xDB  //  '[{' for US
#define VK_OEM_5          0xDC  //  '\|' for US
#define VK_OEM_6          0xDD  //  ']}' for US
#define VK_OEM_7          0xDE  //  ''"' for US
#define VK_OEM_8          0xDF

/*
 * 0xE0 : reserved
 */

/*
 * Various extended or enhanced keyboards
 */
#define VK_OEM_AX         0xE1  //  'AX' key on Japanese AX kbd
#define VK_OEM_102        0xE2  //  "<>" or "\|" on RT 102-key kbd.
#define VK_ICO_HELP       0xE3  //  Help key on ICO
#define VK_ICO_00         0xE4  //  00 key on ICO


#define VK_PROCESSKEY     0xE5


#define VK_ICO_CLEAR      0xE6



#define VK_PACKET         0xE7


/*
 * 0xE8 : unassigned
 */

#define VK_OEM_RESET      0xE9
#define VK_OEM_JUMP       0xEA
#define VK_OEM_PA1        0xEB
#define VK_OEM_PA2        0xEC
#define VK_OEM_PA3        0xED
#define VK_OEM_WSCTRL     0xEE
#define VK_OEM_CUSEL      0xEF
#define VK_OEM_ATTN       0xF0
#define VK_OEM_FINISH     0xF1
#define VK_OEM_COPY       0xF2
#define VK_OEM_AUTO       0xF3
#define VK_OEM_ENLW       0xF4
#define VK_OEM_BACKTAB    0xF5

#define VK_ATTN           0xF6
#define VK_CRSEL          0xF7
#define VK_EXSEL          0xF8
#define VK_EREOF          0xF9
#define VK_PLAY           0xFA
#define VK_ZOOM           0xFB
#define VK_NONAME         0xFC
#define VK_PA1            0xFD
#define VK_OEM_CLEAR      0xFE

extern "C" {
void init_keys();
}

typedef struct _EXCEPTION_POINTERS {
    struct _EXCEPTION_POINTERS * ExceptionRecord;
    PCONTEXT ContextRecord;
} EXCEPTION_POINTERS, *PEXCEPTION_POINTERS;

typedef LONG(WINAPI *PTOP_LEVEL_EXCEPTION_FILTER)(
        struct _EXCEPTION_POINTERS *ExceptionInfo
        );

typedef PTOP_LEVEL_EXCEPTION_FILTER LPTOP_LEVEL_EXCEPTION_FILTER;

typedef struct _MEMORY_BASIC_INFORMATION {
    PVOID BaseAddress;
    PVOID AllocationBase;
    DWORD AllocationProtect;
    SIZE_T RegionSize;
    DWORD State;
    DWORD Protect;
    DWORD Type;
} MEMORY_BASIC_INFORMATION, *PMEMORY_BASIC_INFORMATION;

#define PAGE_READONLY 0x02
#define PAGE_READWRITE 0x04
#define PAGE_EXECUTE 0x10
#define PAGE_EXECUTE_READ 0x20
#define PAGE_EXECUTE_READWRITE 0x40
#define PAGE_GUARD 0x100
#define PAGE_NOACCESS 0x01
#define PAGE_NOCACHE 0x200

typedef struct _dx_context {
    HWND hwnd;
} dx_context;

#define WM_NULL                         0x0000
#define WM_CREATE                       0x0001
#define WM_DESTROY                      0x0002
#define WM_MOVE                         0x0003
#define WM_SIZE                         0x0005

#define WM_ACTIVATE                     0x0006
/*
 * WM_ACTIVATE state values
 */
#define     WA_INACTIVE     0
#define     WA_ACTIVE       1
#define     WA_CLICKACTIVE  2

#define WM_SETFOCUS                     0x0007
#define WM_KILLFOCUS                    0x0008
#define WM_ENABLE                       0x000A
#define WM_SETREDRAW                    0x000B
#define WM_SETTEXT                      0x000C
#define WM_GETTEXT                      0x000D
#define WM_GETTEXTLENGTH                0x000E
#define WM_PAINT                        0x000F
#define WM_CLOSE                        0x0010

#define WM_QUERYENDSESSION              0x0011
#define WM_QUERYOPEN                    0x0013
#define WM_ENDSESSION                   0x0016

#define WM_QUIT                         0x0012
#define WM_ERASEBKGND                   0x0014
#define WM_SYSCOLORCHANGE               0x0015
#define WM_SHOWWINDOW                   0x0018
#define WM_WININICHANGE                 0x001A

#define WM_SETTINGCHANGE                WM_WININICHANGE



#define WM_DEVMODECHANGE                0x001B
#define WM_ACTIVATEAPP                  0x001C
#define WM_FONTCHANGE                   0x001D
#define WM_TIMECHANGE                   0x001E
#define WM_CANCELMODE                   0x001F
#define WM_SETCURSOR                    0x0020
#define WM_MOUSEACTIVATE                0x0021
#define WM_CHILDACTIVATE                0x0022
#define WM_QUEUESYNC                    0x0023

#define WM_GETMINMAXINFO                0x0024
/*
 * Struct pointed to by WM_GETMINMAXINFO lParam
 */
typedef struct tagMINMAXINFO {
    POINT ptReserved;
    POINT ptMaxSize;
    POINT ptMaxPosition;
    POINT ptMinTrackSize;
    POINT ptMaxTrackSize;
} MINMAXINFO, *PMINMAXINFO, *LPMINMAXINFO;

#define WM_PAINTICON                    0x0026
#define WM_ICONERASEBKGND               0x0027
#define WM_NEXTDLGCTL                   0x0028
#define WM_SPOOLERSTATUS                0x002A
#define WM_DRAWITEM                     0x002B
#define WM_MEASUREITEM                  0x002C
#define WM_DELETEITEM                   0x002D
#define WM_VKEYTOITEM                   0x002E
#define WM_CHARTOITEM                   0x002F
#define WM_SETFONT                      0x0030
#define WM_GETFONT                      0x0031
#define WM_SETHOTKEY                    0x0032
#define WM_GETHOTKEY                    0x0033
#define WM_QUERYDRAGICON                0x0037
#define WM_COMPAREITEM                  0x0039


#define WM_GETOBJECT                    0x003D


#define WM_COMPACTING                   0x0041
#define WM_COMMNOTIFY                   0x0044  /* no longer suported */
#define WM_WINDOWPOSCHANGING            0x0046
#define WM_WINDOWPOSCHANGED             0x0047

#define WM_POWER                        0x0048

#define WM_COPYDATA                     0x004A
#define WM_CANCELJOURNAL                0x004B



#define WM_NOTIFY                       0x004E
#define WM_INPUTLANGCHANGEREQUEST       0x0050
#define WM_INPUTLANGCHANGE              0x0051
#define WM_TCARD                        0x0052
#define WM_HELP                         0x0053
#define WM_USERCHANGED                  0x0054
#define WM_NOTIFYFORMAT                 0x0055

#define NFR_ANSI                             1
#define NFR_UNICODE                          2
#define NF_QUERY                             3
#define NF_REQUERY                           4

#define WM_CONTEXTMENU                  0x007B
#define WM_STYLECHANGING                0x007C
#define WM_STYLECHANGED                 0x007D
#define WM_DISPLAYCHANGE                0x007E
#define WM_GETICON                      0x007F
#define WM_SETICON                      0x0080


#define WM_NCCREATE                     0x0081
#define WM_NCDESTROY                    0x0082
#define WM_NCCALCSIZE                   0x0083
#define WM_NCHITTEST                    0x0084
#define WM_NCPAINT                      0x0085
#define WM_NCACTIVATE                   0x0086
#define WM_GETDLGCODE                   0x0087

#define WM_SYNCPAINT                    0x0088

#define WM_NCMOUSEMOVE                  0x00A0
#define WM_NCLBUTTONDOWN                0x00A1
#define WM_NCLBUTTONUP                  0x00A2
#define WM_NCLBUTTONDBLCLK              0x00A3
#define WM_NCRBUTTONDOWN                0x00A4
#define WM_NCRBUTTONUP                  0x00A5
#define WM_NCRBUTTONDBLCLK              0x00A6
#define WM_NCMBUTTONDOWN                0x00A7
#define WM_NCMBUTTONUP                  0x00A8
#define WM_NCMBUTTONDBLCLK              0x00A9




#define WM_NCXBUTTONDOWN                0x00AB
#define WM_NCXBUTTONUP                  0x00AC
#define WM_NCXBUTTONDBLCLK              0x00AD




#define WM_INPUT_DEVICE_CHANGE          0x00FE



#define WM_INPUT                        0x00FF


#define WM_KEYFIRST                     0x0100
#define WM_KEYDOWN                      0x0100
#define WM_KEYUP                        0x0101
#define WM_CHAR                         0x0102
#define WM_DEADCHAR                     0x0103
#define WM_SYSKEYDOWN                   0x0104
#define WM_SYSKEYUP                     0x0105
#define WM_SYSCHAR                      0x0106
#define WM_SYSDEADCHAR                  0x0107

#define WM_KEYLAST                      0x0108



#define WM_IME_STARTCOMPOSITION         0x010D
#define WM_IME_ENDCOMPOSITION           0x010E
#define WM_IME_COMPOSITION              0x010F
#define WM_IME_KEYLAST                  0x010F


#define WM_INITDIALOG                   0x0110
#define WM_COMMAND                      0x0111
#define WM_SYSCOMMAND                   0x0112
#define WM_TIMER                        0x0113
#define WM_HSCROLL                      0x0114
#define WM_VSCROLL                      0x0115
#define WM_INITMENU                     0x0116
#define WM_INITMENUPOPUP                0x0117
#define WM_MENUSELECT                   0x011F
#define WM_MENUCHAR                     0x0120
#define WM_ENTERIDLE                    0x0121


#define WM_MENURBUTTONUP                0x0122
#define WM_MENUDRAG                     0x0123
#define WM_MENUGETOBJECT                0x0124
#define WM_UNINITMENUPOPUP              0x0125
#define WM_MENUCOMMAND                  0x0126


#define WM_CHANGEUISTATE                0x0127
#define WM_UPDATEUISTATE                0x0128
#define WM_QUERYUISTATE                 0x0129

/*
 * LOWORD(wParam) values in WM_*UISTATE*
 */
#define UIS_SET                         1
#define UIS_CLEAR                       2
#define UIS_INITIALIZE                  3

/*
 * HIWORD(wParam) values in WM_*UISTATE*
 */
#define UISF_HIDEFOCUS                  0x1
#define UISF_HIDEACCEL                  0x2

#define UISF_ACTIVE                     0x4

#define WM_CTLCOLORMSGBOX               0x0132
#define WM_CTLCOLOREDIT                 0x0133
#define WM_CTLCOLORLISTBOX              0x0134
#define WM_CTLCOLORBTN                  0x0135
#define WM_CTLCOLORDLG                  0x0136
#define WM_CTLCOLORSCROLLBAR            0x0137
#define WM_CTLCOLORSTATIC               0x0138
#define MN_GETHMENU                     0x01E1

#define WM_MOUSEFIRST                   0x0200
#define WM_MOUSEMOVE                    0x0200
#define WM_LBUTTONDOWN                  0x0201
#define WM_LBUTTONUP                    0x0202
#define WM_LBUTTONDBLCLK                0x0203
#define WM_RBUTTONDOWN                  0x0204
#define WM_RBUTTONUP                    0x0205
#define WM_RBUTTONDBLCLK                0x0206
#define WM_MBUTTONDOWN                  0x0207
#define WM_MBUTTONUP                    0x0208
#define WM_MBUTTONDBLCLK                0x0209

#define WM_MOUSEWHEEL                   0x020A


#define WM_XBUTTONDOWN                  0x020B
#define WM_XBUTTONUP                    0x020C
#define WM_XBUTTONDBLCLK                0x020D


#define WM_MOUSEHWHEEL                  0x020E


#if (_WIN32_WINNT >= 0x0600)
#define WM_MOUSELAST                    0x020E
#elif (_WIN32_WINNT >= 0x0500)
#define WM_MOUSELAST                    0x020D
#elif (_WIN32_WINNT >= 0x0400) || (_WIN32_WINDOWS > 0x0400)
#define WM_MOUSELAST                    0x020A
#else
#define WM_MOUSELAST                    0x0209
#endif /* (_WIN32_WINNT >= 0x0600) */



/* Value for rolling one detent */
#define WHEEL_DELTA                     120
#define GET_WHEEL_DELTA_WPARAM(wParam)  ((short)HIWORD(wParam))

/* Setting to scroll one page for SPI_GET/SETWHEELSCROLLLINES */
#define WHEEL_PAGESCROLL                (UINT_MAX)



#define GET_KEYSTATE_WPARAM(wParam)     (LOWORD(wParam))
#define GET_NCHITTEST_WPARAM(wParam)    ((short)LOWORD(wParam))
#define GET_XBUTTON_WPARAM(wParam)      (HIWORD(wParam))

/* XButton values are WORD flags */
#define XBUTTON1      0x0001
#define XBUTTON2      0x0002
/* Were there to be an XBUTTON3, its value would be 0x0004 */


#define WM_PARENTNOTIFY                 0x0210
#define WM_ENTERMENULOOP                0x0211
#define WM_EXITMENULOOP                 0x0212


#define WM_NEXTMENU                     0x0213
#define WM_SIZING                       0x0214
#define WM_CAPTURECHANGED               0x0215
#define WM_MOVING                       0x0216





#define WM_POWERBROADCAST               0x0218


#define PBT_APMQUERYSUSPEND             0x0000
#define PBT_APMQUERYSTANDBY             0x0001

#define PBT_APMQUERYSUSPENDFAILED       0x0002
#define PBT_APMQUERYSTANDBYFAILED       0x0003

#define PBT_APMSUSPEND                  0x0004
#define PBT_APMSTANDBY                  0x0005

#define PBT_APMRESUMECRITICAL           0x0006
#define PBT_APMRESUMESUSPEND            0x0007
#define PBT_APMRESUMESTANDBY            0x0008

#define PBTF_APMRESUMEFROMFAILURE       0x00000001

#define PBT_APMBATTERYLOW               0x0009
#define PBT_APMPOWERSTATUSCHANGE        0x000A

#define PBT_APMOEMEVENT                 0x000B


#define PBT_APMRESUMEAUTOMATIC          0x0012


#define PBT_POWERSETTINGCHANGE          0x8013
typedef struct {
    GUID PowerSetting;
    DWORD DataLength;
    UCHAR Data[1];
} POWERBROADCAST_SETTING, *PPOWERBROADCAST_SETTING;



#define WM_DEVICECHANGE                 0x0219


#define WM_MDICREATE                    0x0220
#define WM_MDIDESTROY                   0x0221
#define WM_MDIACTIVATE                  0x0222
#define WM_MDIRESTORE                   0x0223
#define WM_MDINEXT                      0x0224
#define WM_MDIMAXIMIZE                  0x0225
#define WM_MDITILE                      0x0226
#define WM_MDICASCADE                   0x0227
#define WM_MDIICONARRANGE               0x0228
#define WM_MDIGETACTIVE                 0x0229


#define WM_MDISETMENU                   0x0230
#define WM_ENTERSIZEMOVE                0x0231
#define WM_EXITSIZEMOVE                 0x0232
#define WM_DROPFILES                    0x0233
#define WM_MDIREFRESHMENU               0x0234



#define WM_IME_SETCONTEXT               0x0281
#define WM_IME_NOTIFY                   0x0282
#define WM_IME_CONTROL                  0x0283
#define WM_IME_COMPOSITIONFULL          0x0284
#define WM_IME_SELECT                   0x0285
#define WM_IME_CHAR                     0x0286

#define WM_IME_REQUEST                  0x0288

#define WM_IME_KEYDOWN                  0x0290
#define WM_IME_KEYUP                    0x0291



#define WM_MOUSEHOVER                   0x02A1
#define WM_MOUSELEAVE                   0x02A3

#define WM_NCMOUSEHOVER                 0x02A0
#define WM_NCMOUSELEAVE                 0x02A2


#define WM_WTSSESSION_CHANGE            0x02B1

#define WM_TABLET_FIRST                 0x02c0
#define WM_TABLET_LAST                  0x02df


#define WM_CUT                          0x0300
#define WM_COPY                         0x0301
#define WM_PASTE                        0x0302
#define WM_CLEAR                        0x0303
#define WM_UNDO                         0x0304
#define WM_RENDERFORMAT                 0x0305
#define WM_RENDERALLFORMATS             0x0306
#define WM_DESTROYCLIPBOARD             0x0307
#define WM_DRAWCLIPBOARD                0x0308
#define WM_PAINTCLIPBOARD               0x0309
#define WM_VSCROLLCLIPBOARD             0x030A
#define WM_SIZECLIPBOARD                0x030B
#define WM_ASKCBFORMATNAME              0x030C
#define WM_CHANGECBCHAIN                0x030D
#define WM_HSCROLLCLIPBOARD             0x030E
#define WM_QUERYNEWPALETTE              0x030F
#define WM_PALETTEISCHANGING            0x0310
#define WM_PALETTECHANGED               0x0311
#define WM_HOTKEY                       0x0312


#define WM_PRINT                        0x0317
#define WM_PRINTCLIENT                  0x0318
#define WM_APPCOMMAND                   0x0319
#define WM_THEMECHANGED                 0x031A
#define WM_CLIPBOARDUPDATE              0x031D
#define WM_DWMCOMPOSITIONCHANGED        0x031E
#define WM_DWMNCRENDERINGCHANGED        0x031F
#define WM_DWMCOLORIZATIONCOLORCHANGED  0x0320
#define WM_DWMWINDOWMAXIMIZEDCHANGE     0x0321

#define WM_GETTITLEBARINFOEX            0x033F


#define WM_HANDHELDFIRST                0x0358
#define WM_HANDHELDLAST                 0x035F

#define WM_AFXFIRST                     0x0360
#define WM_AFXLAST                      0x037F


#define WM_PENWINFIRST                  0x0380
#define WM_PENWINLAST                   0x038F



#define WM_APP                          0x8000

#define SWP_NOSIZE          0x0001
#define SWP_NOMOVE          0x0002
#define SWP_NOZORDER        0x0004
#define SWP_NOREDRAW        0x0008
#define SWP_NOACTIVATE      0x0010
#define SWP_FRAMECHANGED    0x0020  /* The frame changed: send WM_NCCALCSIZE */
#define SWP_SHOWWINDOW      0x0040
#define SWP_HIDEWINDOW      0x0080
#define SWP_NOCOPYBITS      0x0100
#define SWP_NOOWNERZORDER   0x0200  /* Don't do owner Z ordering */
#define SWP_NOSENDCHANGING  0x0400  /* Don't send WM_WINDOWPOSCHANGING */

typedef LRESULT  (WINAPI * WNDPROC)(HWND, UINT, WPARAM, LPARAM);

typedef struct tagWINDOWPOS {
  HWND hwnd;
  HWND hwndInsertAfter;
  int  x;
  int  y;
  int  cx;
  int  cy;
  UINT flags;
} WINDOWPOS, *LPWINDOWPOS, *PWINDOWPOS;



typedef struct tagCREATESTRUCT {
    LPVOID    lpCreateParams;
    HINSTANCE hInstance;
    HMENU     hMenu;
    HWND      hwndParent;
    int       cy;
    int       cx;
    int       y;
    int       x;
    LONG      style;
    LPCTSTR   lpszName;
    LPCTSTR   lpszClass;
    DWORD     dwExStyle;
} CREATESTRUCT, *LPCREATESTRUCT;


typedef struct tagWNDCLASS {
    UINT      style;
    WNDPROC   lpfnWndProc;
    int       cbClsExtra;
    int       cbWndExtra;
    HINSTANCE hInstance;
    HICON     hIcon;
    HCURSOR   hCursor;
    HBRUSH    hbrBackground;
    LPCTSTR   lpszMenuName;
    LPCTSTR   lpszClassName;
    uint32 reserved;
} WNDCLASS, *PWNDCLASS;

struct _innerwin;

typedef struct tagMSG {
    HWND   hwnd;
    UINT   message;
    WPARAM wParam;
    LPARAM lParam;
    DWORD  time;
    POINT  pt;
} MSG, *PMSG, *LPMSG;

typedef struct _wmsg_queue {
    struct _wmsg_queue * next;
    MSG msg;
    int processable;
    struct _innerwin * iw;
    struct timespec time;
} wmsg_queue;

typedef struct _innerwin {
    dword type;
    WNDCLASS *wclass;
    int x,y,w,h;
    char wname[1024];
    void * lparam;
    void * hwnd;
    int desired_w;
    int desired_h;
//    Window * xWindow;
    
    wmsg_queue * wmsg_queue_h;
    wmsg_queue * wmsg_queue_t;

    pthread_mutex_t wmsg_queue_mutex;
    pthread_cond_t wmsg_queue_cond;
} innerwin;

#define STATE_UP 2
#define STATE_DOWN 1

extern "C" {
int wmm_set_desired_res(int w, int h, HWND wd);
int wmm_resize_window(int w, int h);

int wmm_dimension(ImageSink * sink, HWND hwnd);
void w_mouse_move(int x, int y, int rx, int ry, void * arg);
void w_mouse_button(int b, int state, int x, int y, int rx, int ry, void * arg);
void w_key_event(int key, int ascii, int state, int x, int y, int mods, void * arg);
}

int xc_resize_window(int w, int h);
#define INVALID_HANDLE_VALUE ((HANDLE)(long)-1)

typedef DWORD   MCIERROR;       /* error return code, 0 means no error */
typedef UINT    MCIDEVICEID;
typedef unsigned long *DWORD_PTR;

#define MK_LBUTTON          0x0001
#define MK_RBUTTON          0x0002
#define MK_SHIFT            0x0004
#define MK_CONTROL          0x0008
#define MK_MBUTTON          0x0010
#define MK_XBUTTON1         0x0020
#define MK_XBUTTON2         0x0040

#define WF_PMODE 	0x0001
#define WF_CPU286 	0x0002
#define	WF_CPU386	0x0004
#define	WF_CPU486 	0x0008
#define	WF_STANDARD	0x0010
#define	WF_WIN286 	0x0010
#define	WF_ENHANCED	0x0020
#define	WF_WIN386	0x0020
#define	WF_CPU086	0x0040
#define	WF_CPU186	0x0080
#define	WF_LARGEFRAME	0x0100
#define	WF_SMALLFRAME	0x0200
#define	WF_80x87	0x0400
#define	WF_PAGING	0x0800
#define	WF_HASCPUID     0x2000
#define	WF_WIN32WOW     0x4000
#define	WF_WLO          0x8000

typedef WORD ATOM;

extern "C" {
int process_msg_queue(MSG * msg, int excl, void * win);
ATOM WINAPI RegisterClassA(
    const WNDCLASS *lpWndClass
);
}
#endif /* WINM_H */


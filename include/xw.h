#ifndef XW_H
#define XW_H

#include <xcb/xcb.h>
#include <xcb/randr.h>
#include <X11/Xlib-xcb.h> 

void create_window(int x, int y, int w, int h, void * arg);
xcb_randr_screen_size_t * x_enum_screen_types(int* size);
int xc_get_mouse_pos(int * rx, int * ry, int * wx, int * wy);
int xc_root_to_window(int x, int y, int * wx, int * wy);
void gl_from_x(Display * display, xcb_window_t window, GLXFBConfig fb_config) ;
void xc_fullscreen();

void get_current_res(int * w, int * h, char * output);

#endif /* XW_H */


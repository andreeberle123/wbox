/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   gl.h
 * Author: z
 *
 * Created on February 16, 2018, 2:18 PM
 */

#ifndef GL_H
#define GL_H
#include <SDL.h>
/*#ifdef __cplusplus
extern "C" {
#endif*/

int init_gl(int w, int h, int stx, int sty, char * title);
void _lock_gl();
void _unlock_gl();


class Renderer {
public:
    Renderer(int w, int h, int stx, int sty);
    
};


#include "dx.h"
void draw_from_buffer_and_unlock(byte * buf, int x, int y);
void set_active_color(int id);
int set_colors(LPPALETTEENTRY entries, int start, int size, int id);
int color_id(int r, int g, int b);
void flush_palette(int idx);
void gl_push_buffer(byte * buf, int x, int y);
void gl_lock();
void gl_unlock();

/*
#ifdef __cplusplus
}
#endif*/

#endif /* GL_H */


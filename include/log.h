
#ifndef _C_LOGGER_H_
#define _C_LOGGER_H_

#include "common.h"

#define LOG_THREAD_HASH         ((unsigned)pthread_self()%87541)

void log_data(char *file, unsigned int line, int level, char *category,
              const char *data, ...) __attribute__((format(printf, 5, 6)));

#define log_debug(data, arg...) \
    log_data(__FILE__, __LINE__, 4, "DEBUG", data, ## arg)

#define log_info(data, arg...) \
    log_data(__FILE__, __LINE__, 3, "INFO", data, ## arg)

#define log_error(data, arg...) \
    log_data(__FILE__, __LINE__, 1, "ERROR", data, ## arg)

#define log_warn(data, arg...) \
    log_data(__FILE__, __LINE__, 2, "WARN", data, ## arg)

void log_array(byte *array, int size);



#endif

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lstring.h
 * Author: z
 *
 * Created on May 5, 2018, 4:17 PM
 */

#ifndef LSTRING_H
#define LSTRING_H

int s_endswith(char * s1, char * match);
int wide_str(char * s1, wchar_t * s2);
char * toupper_str(char * str);
char * tolower_str(char * str);
char * tolower_str(char * str, char * dest);
char * l_substring(char * str, int st, int end);
int l_lastindexof(char * str, char ch);
int l_strcicmp(char const *a, char const *b);
int l_endswith(char * str, char * cmp);
int str_cmp(void *a, void *b, void *h);
wchar_t * w_join(wchar_t * targ,wchar_t * s1,char * s2);
char * wide_to_ansi(wchar_t * w);
char * wide_to_ansi(wchar_t * w, char * wtoan2);
int w_strcpy(wchar_t * s1,wchar_t * s2);
int _wcslen(wchar_t * s1);
int w_strcmp(wchar_t * s1, wchar_t *s2);
int stricmp( char *str1, char *str2 );
int global_strcmp(void * s1, void *s2, void *h);

#endif /* LSTRING_H */


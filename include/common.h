#ifndef COMMON_H
#define COMMON_H

#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

//#define GLEW_STATIC
//#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glx.h>
//#include <GL/glxew.h>

#include "lstring.h"

typedef unsigned int uint32;
typedef unsigned long long uint64;
typedef unsigned char byte;

typedef unsigned int dword;
typedef unsigned short word;

#define LOCK_MUTEX(mutex) pthread_mutex_lock(&mutex)
#define UNLOCK_MUTEX(mutex) pthread_mutex_unlock(&mutex)
#define LOCK_MUTEX_P(mutex) pthread_mutex_lock(mutex)
#define UNLOCK_MUTEX_P(mutex) pthread_mutex_unlock(mutex)
#define WAIT_CONDITION(cond,mutex) pthread_cond_wait(&cond,&mutex)
#define WAIT_CONDITION_TIMED(cond,mutex, ts) pthread_cond_timedwait(&cond,&mutex, &ts)
#define WAIT_CONDITION_TIMED_P(cond,mutex, ts) pthread_cond_timedwait(cond,mutex, &ts)
#define SIGNAL(cond) pthread_cond_signal(&cond)
#define BROADCAST(cond) pthread_cond_broadcast(&cond)

#define WAIT_CONDITION_P(cond,mutex) pthread_cond_wait(cond,mutex)
#define SIGNAL_P(cond) pthread_cond_signal(cond)

//#include "clock.h"
#include "log.h"
//#include "item.h"
//#include "geo.h"
//#include "mathf.h"
#include "c_hash.h"
#include <wchar.h>
#include <sys/types.h>
#include <sys/mman.h>

#include <sys/syscall.h>
#include <asm/ldt.h>
#include <asm/prctl.h>
#include <sys/prctl.h>
#include <linux/unistd.h>
#include <asm/ldt.h>
#include "winm.h"

enum scale_mode_ {
    NO_SCALE,
    SCALE_KEEP_ASPECT,
    SCALE_STRETCH
};

typedef struct _global_config {
    char cmdline_remapped_q[512];
    char cmdline_remapped[512];
    char cmdline[512];
    char cmdex[512];
    wchar_t cmdline_w[512];
    wchar_t cmdline_w_q[512];
    char emu_base_dir[512];
    char base_path[512];
    char cur_dir[512];
    int base_path_sz;
    int emu_base_dir_sz;
    int exec_mode;
    char base_path_remapped[512];
    int use_nt_debug;
    int use_full_vmap;
    // gfx
    int scale_mode;
    int full_screen;
    
} global_config;

extern global_config global_config_data;
void init_config(int argc, char * argv[]);

void assemble_local_ldt();

void * sym_lookup(char * name);
extern "C" void time_diff(struct timespec *then, struct timespec *now, struct timespec *diff);

#endif /* COMMON_H */


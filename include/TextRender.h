#ifndef _TEXTRENDER_H_
#define _TEXTRENDER_H_

#include "common.h"
#include <ft2build.h>
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>
#include <freetype/ftoutln.h>
#include <freetype/fttrigon.h>

#define TEX_SIZE_8  0
#define TEX_SIZE_10  1
#define TEX_SIZE_14  2

#define TEX_SIZE_8_H  14
#define TEX_SIZE_10_H   18
#define TEX_SIZE_14_H   22

class TextRender;

TextRender * TR_getInstance();

class TextRender {
public:
    GLuint font_tex[3][128];
    GLuint list_base[3];
    int font_heights[64];
    int base_sizes[3];
    int py, px;
    float depth;
    float color[3];

    void setColor(float r, float g, float b) {
        color[0] = r;
        color[1] = g;
        color[2] = b;
    }
    void setPos(int x, int y);
    void setPos(int x, int y, float z);
    FT_Face face;
    FT_Library library;
    TextRender();
    void renderText(char * tex, int size);
    void createDisplayLists();
    int getFontHeight(int id);
    FT_Error loadFont(char * name);
};

#endif
#ifndef DX_H
#define DX_H

#define FAR

#include "winm.h"

extern "C" {
    
typedef struct tagPALETTEENTRY {
    BYTE        peRed;
    BYTE        peGreen;
    BYTE        peBlue;
    BYTE        peFlags;
} PALETTEENTRY, *PPALETTEENTRY, *LPPALETTEENTRY;

typedef struct _IDirectDrawPalette *LPDIRECTDRAWPALETTE;
    
typedef struct IUnknown {
/*HRESULT*/ int WINAPI(*QueryInterface)(struct IDirectDraw *_this, /*REFIID*/ int riid, void* ppvObj);
/*ULONG*/ int WINAPI(*AddRef)(struct IDirectDraw *_this);
/*ULONG*/ int WINAPI(*Release)(struct IDirectDraw *_this);

/*HRESULT WINAPI QueryInterface(_COM_Outptr_ Q** pp)
{
    return QueryInterface(__uuidof(Q), (void **)pp);
}*/


} IUnknown;

/*
 * This bit is reserved. It should not be specified.
 */
#define DDSCAPS_RESERVED1                       0x00000001l

/*
 * Indicates that this surface contains alpha-only information.
 * (To determine if a surface is RGBA/YUVA, the pixel format must be
 * interrogated.)
 */
#define DDSCAPS_ALPHA                           0x00000002l

/*
 * Indicates that this surface is a backbuffer.  It is generally
 * set by CreateSurface when the DDSCAPS_FLIP capability bit is set.
 * It indicates that this surface is THE back buffer of a surface
 * flipping structure.  DirectDraw supports N surfaces in a
 * surface flipping structure.  Only the surface that immediately
 * precedeces the DDSCAPS_FRONTBUFFER has this capability bit set.
 * The other surfaces are identified as back buffers by the presence
 * of the DDSCAPS_FLIP capability, their attachment order, and the
 * absence of the DDSCAPS_FRONTBUFFER and DDSCAPS_BACKBUFFER
 * capabilities.  The bit is sent to CreateSurface when a standalone
 * back buffer is being created.  This surface could be attached to
 * a front buffer and/or back buffers to form a flipping surface
 * structure after the CreateSurface call.  See AddAttachments for
 * a detailed description of the behaviors in this case.
 */
#define DDSCAPS_BACKBUFFER                      0x00000004l

/*
 * Indicates a complex surface structure is being described.  A
 * complex surface structure results in the creation of more than
 * one surface.  The additional surfaces are attached to the root
 * surface.  The complex structure can only be destroyed by
 * destroying the root.
 */
#define DDSCAPS_COMPLEX                         0x00000008l

/*
 * Indicates that this surface is a part of a surface flipping structure.
 * When it is passed to CreateSurface the DDSCAPS_FRONTBUFFER and
 * DDSCAP_BACKBUFFER bits are not set.  They are set by CreateSurface
 * on the resulting creations.  The dwBackBufferCount field in the
 * DDSURFACEDESC structure must be set to at least 1 in order for
 * the CreateSurface call to succeed.  The DDSCAPS_COMPLEX capability
 * must always be set with creating multiple surfaces through CreateSurface.
 */
#define DDSCAPS_FLIP                            0x00000010l

/*
 * Indicates that this surface is THE front buffer of a surface flipping
 * structure.  It is generally set by CreateSurface when the DDSCAPS_FLIP
 * capability bit is set.
 * If this capability is sent to CreateSurface then a standalonw front buffer
 * is created.  This surface will not have the DDSCAPS_FLIP capability.
 * It can be attached to other back buffers to form a flipping structure.
 * See AddAttachments for a detailed description of the behaviors in this
 * case.
 */
#define DDSCAPS_FRONTBUFFER                     0x00000020l

/*
 * Indicates that this surface is any offscreen surface that is not an overlay,
 * texture, zbuffer, front buffer, back buffer, or alpha surface.  It is used
 * to identify plain vanilla surfaces.
 */
#define DDSCAPS_OFFSCREENPLAIN                  0x00000040l

/*
 * Indicates that this surface is an overlay.  It may or may not be directly visible
 * depending on whether or not it is currently being overlayed onto the primary
 * surface.  DDSCAPS_VISIBLE can be used to determine whether or not it is being
 * overlayed at the moment.
 */
#define DDSCAPS_OVERLAY                         0x00000080l

/*
 * Indicates that unique DirectDrawPalette objects can be created and
 * attached to this surface.
 */
#define DDSCAPS_PALETTE                         0x00000100l

/*
 * Indicates that this surface is the primary surface.  The primary
 * surface represents what the user is seeing at the moment.
 */
#define DDSCAPS_PRIMARYSURFACE                  0x00000200l


/*
 * This flag used to be DDSCAPS_PRIMARYSURFACELEFT, which is now
 * obsolete.
 */
#define DDSCAPS_RESERVED3               0x00000400l

/*
 * Indicates that this surface memory was allocated in system memory
 */
#define DDSCAPS_SYSTEMMEMORY                    0x00000800l

/*
 * Indicates that this surface can be used as a 3D texture.  It does not
 * indicate whether or not the surface is being used for that purpose.
 */
#define DDSCAPS_TEXTURE                         0x00001000l

/*
 * Indicates that a surface may be a destination for 3D rendering.  This
 * bit must be set in order to query for a Direct3D Device Interface
 * from this surface.
 */
#define DDSCAPS_3DDEVICE                        0x00002000l

/*
 * Indicates that this surface exists in video memory.
 */
#define DDSCAPS_VIDEOMEMORY                     0x00004000l

/*
 * Indicates that changes made to this surface are immediately visible.
 * It is always set for the primary surface and is set for overlays while
 * they are being overlayed and texture maps while they are being textured.
 */
#define DDSCAPS_VISIBLE                         0x00008000l

/*
 * Indicates that only writes are permitted to the surface.  Read accesses
 * from the surface may or may not generate a protection fault, but the
 * results of a read from this surface will not be meaningful.  READ ONLY.
 */
#define DDSCAPS_WRITEONLY                       0x00010000l

/*
 * Indicates that this surface is a z buffer. A z buffer does not contain
 * displayable information.  Instead it contains bit depth information that is
 * used to determine which pixels are visible and which are obscured.
 */
#define DDSCAPS_ZBUFFER                         0x00020000l

/*
 * Indicates surface will have a DC associated long term
 */
#define DDSCAPS_OWNDC                           0x00040000l

/*
 * Indicates surface should be able to receive live video
 */
#define DDSCAPS_LIVEVIDEO                       0x00080000l

/*
 * Indicates surface should be able to have a stream decompressed
 * to it by the hardware.
 */
#define DDSCAPS_HWCODEC                         0x00100000l

/*
 * Surface is a ModeX surface.
 *
 */
#define DDSCAPS_MODEX                           0x00200000l

/*
 * Indicates surface is one level of a mip-map. This surface will
 * be attached to other DDSCAPS_MIPMAP surfaces to form the mip-map.
 * This can be done explicitly, by creating a number of surfaces and
 * attaching them with AddAttachedSurface or by implicitly by CreateSurface.
 * If this bit is set then DDSCAPS_TEXTURE must also be set.
 */
#define DDSCAPS_MIPMAP                          0x00400000l

/*
 * This bit is reserved. It should not be specified.
 */
#define DDSCAPS_RESERVED2                       0x00800000l


/*
 * Indicates that memory for the surface is not allocated until the surface
 * is loaded (via the Direct3D texture Load() function).
 */
#define DDSCAPS_ALLOCONLOAD                     0x04000000l

/*
 * Indicates that the surface will recieve data from a video port.
 */
#define DDSCAPS_VIDEOPORT                       0x08000000l

/*
 * Indicates that a video memory surface is resident in true, local video
 * memory rather than non-local video memory. If this flag is specified then
 * so must DDSCAPS_VIDEOMEMORY. This flag is mutually exclusive with
 * DDSCAPS_NONLOCALVIDMEM.
 */
#define DDSCAPS_LOCALVIDMEM                     0x10000000l

/*
 * Indicates that a video memory surface is resident in non-local video
 * memory rather than true, local video memory. If this flag is specified
 * then so must DDSCAPS_VIDEOMEMORY. This flag is mutually exclusive with
 * DDSCAPS_LOCALVIDMEM.
 */
#define DDSCAPS_NONLOCALVIDMEM                  0x20000000l

/*
 * Indicates that this surface is a standard VGA mode surface, and not a
 * ModeX surface. (This flag will never be set in combination with the
 * DDSCAPS_MODEX flag).
 */
#define DDSCAPS_STANDARDVGAMODE                 0x40000000l

/*
 * Indicates that this surface will be an optimized surface. This flag is
 * currently only valid in conjunction with the DDSCAPS_TEXTURE flag. The surface
 * will be created without any underlying video memory until loaded.
 */
#define DDSCAPS_OPTIMIZED                       0x80000000l

struct _IDirectDrawSurface;
typedef _IDirectDrawSurface IDirectDrawSurface,DIRECTDRAWSURFACE,*LPDIRECTDRAWSURFACE;

typedef /*HRESULT*/ int WINAPI(*tQueryInterface)(struct IDirectDraw *_this, /*REFIID*/ int riid, void* ppvObj);
typedef /*ULONG*/ int WINAPI(*tAddRef)(struct IDirectDraw *_this);
typedef /*ULONG*/ int WINAPI(*tRelease)(struct IDirectDraw *_this);

typedef struct tagRECT
{
    LONG    left;
    LONG    top;
    LONG    right;
    LONG    bottom;
} RECT, *PRECT, *NPRECT, *LPRECT;


/*
 * Exclusive mode owner will be responsible for the entire primary surface.
 * GDI can be ignored. used with DD
 */
#define DDSCL_FULLSCREEN                        0x00000001l

/*
 * allow CTRL_ALT_DEL to work while in fullscreen exclusive mode
 */
#define DDSCL_ALLOWREBOOT                       0x00000002l

/*
 * prevents DDRAW from modifying the application window.
 * prevents DDRAW from minimize/restore the application window on activation.
 */
#define DDSCL_NOWINDOWCHANGES                   0x00000004l

/*
 * app wants to work as a regular Windows application
 */
#define DDSCL_NORMAL                            0x00000008l

/*
 * app wants exclusive access
 */
#define DDSCL_EXCLUSIVE                         0x00000010l


/*
 * app can deal with non-windows display modes
 */
#define DDSCL_ALLOWMODEX                        0x00000040l

/*
 * this window will receive the focus messages
 */
#define DDSCL_SETFOCUSWINDOW                    0x00000080l

/*
 * this window is associated with the DDRAW object and will
 * cover the screen in fullscreen mode
 */
#define DDSCL_SETDEVICEWINDOW                   0x00000100l

/*
 * app wants DDRAW to create a window to be associated with the
 * DDRAW object
 */
#define DDSCL_CREATEDEVICEWINDOW                0x00000200l

/*
 * App explicitly asks DDRAW/D3D to be multithread safe. This makes D3D
 * take the global crtisec more frequently.
 */
#define DDSCL_MULTITHREADED                     0x00000400l

/*
 * App specifies that it would like to keep the FPU set up for optimal Direct3D
 * performance (single precision and exceptions disabled) so Direct3D
 * does not need to explicitly set the FPU each time. This is assumed by
 * default in DirectX 7. See also DDSCL_FPUPRESERVE
 */
#define DDSCL_FPUSETUP                          0x00000800l

/*
 * App specifies that it needs either double precision FPU or FPU exceptions
 * enabled. This makes Direct3D explicitly set the FPU state eah time it is
 * called. Setting the flag will reduce Direct3D performance. The flag is
 * assumed by default in DirectX 6 and earlier. See also DDSCL_FPUSETUP
 */
#define DDSCL_FPUPRESERVE                          0x00001000l

typedef struct _DDCOLORKEY {
    DWORD dwColorSpaceLowValue; // low boundary of color space that is to
    // be treated as Color Key, inclusive
    DWORD dwColorSpaceHighValue; // high boundary of color space that is
    // to be treated as Color Key, inclusive
} DDCOLORKEY;

#define DUMMYUNIONNAMEN(n) u##n

typedef struct _DDPIXELFORMAT {
    DWORD dwSize; // size of structure
    DWORD dwFlags; // pixel format flags
    DWORD dwFourCC; // (FOURCC code)

    union {
        DWORD dwRGBBitCount; // how many bits per pixel
        DWORD dwYUVBitCount; // how many bits per pixel
        DWORD dwZBufferBitDepth; // how many total bits/pixel in z buffer (including any stencil bits)
        DWORD dwAlphaBitDepth; // how many bits for alpha channels
        DWORD dwLuminanceBitCount; // how many bits per pixel
        DWORD dwBumpBitCount; // how many bits per "buxel", total
        DWORD dwPrivateFormatBitCount; // Bits per pixel of private driver formats. Only valid in texture
        // format list and if DDPF_D3DFORMAT is set
    } DUMMYUNIONNAMEN(1);

    union {
        DWORD dwRBitMask; // mask for red bit
        DWORD dwYBitMask; // mask for Y bits
        DWORD dwStencilBitDepth; // how many stencil bits (note: dwZBufferBitDepth-dwStencilBitDepth is total Z-only bits)
        DWORD dwLuminanceBitMask; // mask for luminance bits
        DWORD dwBumpDuBitMask; // mask for bump map U delta bits
        DWORD dwOperations; // DDPF_D3DFORMAT Operations
    } DUMMYUNIONNAMEN(2);

    union {
        DWORD dwGBitMask; // mask for green bits
        DWORD dwUBitMask; // mask for U bits
        DWORD dwZBitMask; // mask for Z bits
        DWORD dwBumpDvBitMask; // mask for bump map V delta bits

        struct {
            WORD wFlipMSTypes; // Multisample methods supported via flip for this D3DFORMAT
            WORD wBltMSTypes; // Multisample methods supported via blt for this D3DFORMAT
        } MultiSampleCaps;

    } DUMMYUNIONNAMEN(3);

    union {
        DWORD dwBBitMask; // mask for blue bits
        DWORD dwVBitMask; // mask for V bits
        DWORD dwStencilBitMask; // mask for stencil bits
        DWORD dwBumpLuminanceBitMask; // mask for luminance in bump map
    } DUMMYUNIONNAMEN(4);

    union {
        DWORD dwRGBAlphaBitMask; // mask for alpha channel
        DWORD dwYUVAlphaBitMask; // mask for alpha channel
        DWORD dwLuminanceAlphaBitMask; // mask for alpha channel
        DWORD dwRGBZBitMask; // mask for Z channel
        DWORD dwYUVZBitMask; // mask for Z channel
    } DUMMYUNIONNAMEN(5);
} DDPIXELFORMAT;

typedef struct _DDSURFACEDESC DDSURFACEDESC, * LPDDSURFACEDESC;

typedef HRESULT WINAPI ( * LPDDENUMMODESCALLBACK)(LPDDSURFACEDESC, LPVOID);

typedef /*HRESULT*/ int WINAPI(*tCompact)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tCreateClipper)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tCreatePalette)(struct IDirectDraw *_this, DWORD i, LPPALETTEENTRY e, LPDIRECTDRAWPALETTE * dp, IUnknown * n);
typedef /*HRESULT*/ int WINAPI(*tCreateSurface)(struct IDirectDraw *_this, LPDDSURFACEDESC desc, LPDIRECTDRAWSURFACE *ds, IUnknown *);
typedef /*HRESULT*/ int WINAPI(*tDuplicateSurface)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tEnumDisplayModes)(struct IDirectDraw *_this, DWORD dwFlags, 
        HRESULT* lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK lpEnumModesCallback);
typedef /*HRESULT*/ int WINAPI(*tEnumSurfaces)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tFlipToGDISurface)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tGetCaps)(struct IDirectDraw *_this, int a, int b);
typedef /*HRESULT*/ int WINAPI(*tGetDisplayMode)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tGetFourCCCodes)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tGetGDISurface)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tGetMonitorFrequency)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tGetScanLine)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tGetVerticalBlankStatus)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tInitialize)(struct IDirectDraw *_this, int i);
typedef /*HRESULT*/ int WINAPI(*tRestoreDisplayMode)(struct IDirectDraw *_this);
typedef /*HRESULT*/ int WINAPI(*tSetCooperativeLevel)(struct IDirectDraw *_this, HWND a, DWORD b);
typedef /*HRESULT*/ int WINAPI(*tSetDisplayMode)(struct IDirectDraw *_this, int i,int,int);
typedef /*HRESULT*/ int WINAPI(*tWaitForVerticalBlank)(struct IDirectDraw *_this, int i);

typedef struct IDirectDraw {
    tQueryInterface * QueryInterface;
    tAddRef * AddRef;
    tRelease * Release;
    tCompact * Compact;
    tCreateClipper * CreateClipper; // 0x10
    tCreatePalette * CreatePalette;
    tCreateSurface * CreateSurface;
    tDuplicateSurface * DuplicateSurface;
    tEnumDisplayModes * EnumDisplayModes; // 0x20
    tEnumSurfaces * EnumSurfaces;
    tFlipToGDISurface * FlipToGDISurface;
    tGetCaps * GetCaps;
    tGetDisplayMode * GetDisplayMode; // 0x30
    tGetFourCCCodes * GetFourCCCodes;
    tGetGDISurface * GetGDISurface;
    tGetMonitorFrequency * GetMonitorFrequency;
    tGetScanLine * GetScanLine; //0x40
    tGetVerticalBlankStatus * GetVerticalBlankStatus;
    tInitialize * Initialize;
    tRestoreDisplayMode * RestoreDisplayMode;
    tSetCooperativeLevel * SetCooperativeLevel; // 0x50
    tSetDisplayMode * SetDisplayMode;
    tWaitForVerticalBlank * WaitForVerticalBlank;
    // opaque data
    dx_context dcontext;
} IDirectDraw, *LPDIRECTDRAW;

typedef struct IDirectDrawl {
    tQueryInterface QueryInterface;
    tAddRef AddRef;
    tRelease Release;
    tCompact Compact;
    tCreateClipper CreateClipper; // 0x10
    tCreatePalette CreatePalette;
    tCreateSurface CreateSurface;
    tDuplicateSurface DuplicateSurface;
    tEnumDisplayModes EnumDisplayModes; // 0x20
    tEnumSurfaces EnumSurfaces;
    tFlipToGDISurface FlipToGDISurface;
    tGetCaps GetCaps;
    tGetDisplayMode GetDisplayMode; // 0x30
    tGetFourCCCodes GetFourCCCodes;
    tGetGDISurface GetGDISurface;
    tGetMonitorFrequency GetMonitorFrequency;
    tGetScanLine GetScanLine; // 0x40
    tGetVerticalBlankStatus GetVerticalBlankStatus;
    tInitialize Initialize;
    tRestoreDisplayMode RestoreDisplayMode;
    tSetCooperativeLevel SetCooperativeLevel; // 0x50
    tSetDisplayMode SetDisplayMode;
    tWaitForVerticalBlank WaitForVerticalBlank;
} IDirectDrawl, *LPDIRECTDRAWl;







typedef struct _DDSCAPS {
    DWORD dwCaps; // capabilities of surface wanted
} DDSCAPS;

#define DD_OK 0

struct _DDSURFACEDESC {
    DWORD               dwSize;                 // size of the DDSURFACEDESC structure
    DWORD               dwFlags;                // determines what fields are valid
    DWORD               dwHeight;               // height of surface to be created
    DWORD               dwWidth;                // width of input surface
    
    LONG            lPitch;                 // distance to start of next line (return value only)
        
    DWORD               dwBackBufferCount;      // number of back buffers requested

    DWORD           dwRefreshRate;          // refresh rate (used when display mode is described)

    DWORD               dwAlphaBitDepth;        // depth of alpha buffer requested
    DWORD               dwReserved;             // reserved
    LPVOID              lpSurface;              // 0x24
    DDCOLORKEY          ddckCKDestOverlay;      // 0x28
    DDCOLORKEY          ddckCKDestBlt;          // 0x30
    DDCOLORKEY          ddckCKSrcOverlay;       // 0x38
    DDCOLORKEY          ddckCKSrcBlt;           // 0x40
    DDPIXELFORMAT       ddpfPixelFormat;        // 0x48
    DDSCAPS             ddsCaps;                // direct draw surface capabilities
};

/*
struct _DDSURFACEDESC {
    DWORD dwSize; 
    DWORD dwFlags;
    DWORD dwHeight;
    DWORD dwWidth; // 0xc
    LONG lPitch; // 0x10
    LONG lXPitch;
    DWORD dwBackBufferCount;
    DWORD dwRefreshRate; // 0x1c
    LPVOID lpSurface; // 0x20
    DDCOLORKEY ddckCKDestOverlay; // 0x24
    DDCOLORKEY ddckCKDestBlt; // 0x2C
    DDCOLORKEY ddckCKSrcOverlay; // 0x34
    DDCOLORKEY ddckCKSrcBlt;  // 0x3C
    DDPIXELFORMAT ddpfPixelFormat; //0x44
    DDSCAPS ddsCaps;
    DWORD dwSurfaceSize;
} ;*/

typedef struct _DDOVERLAYFX
{
    DWORD       dwSize;                         // size of structure
    DWORD       dwAlphaEdgeBlendBitDepth;       // Bit depth used to specify constant for alpha edge blend
    DWORD       dwAlphaEdgeBlend;               // Constant to use as alpha for edge blend
    DWORD       dwReserved;
    DWORD       dwAlphaDestConstBitDepth;       // Bit depth used to specify alpha constant for destination
    union
    {
        DWORD   dwAlphaDestConst;               // Constant to use as alpha channel for dest
        LPDIRECTDRAWSURFACE lpDDSAlphaDest;     // Surface to use as alpha channel for dest
    } DUMMYUNIONNAMEN(1);
    DWORD       dwAlphaSrcConstBitDepth;        // Bit depth used to specify alpha constant for source
    union
    {
        DWORD   dwAlphaSrcConst;                // Constant to use as alpha channel for src
        LPDIRECTDRAWSURFACE lpDDSAlphaSrc;      // Surface to use as alpha channel for src
    } DUMMYUNIONNAMEN(2);
    DDCOLORKEY  dckDestColorkey;                // DestColorkey override
    DDCOLORKEY  dckSrcColorkey;                 // DestColorkey override
    DWORD       dwDDFX;                         // Overlay FX
    DWORD       dwFlags;                        // flags
} DDOVERLAYFX;


typedef DDOVERLAYFX FAR *LPDDOVERLAYFX;

typedef struct _DDBLTFX DDBLTFX, *LPDDBLTFX;
typedef struct _DDBLTBATCH DDBLTBATCH;
typedef DDBLTBATCH FAR * LPDDBLTBATCH;
typedef DDPIXELFORMAT FAR* LPDDPIXELFORMAT;

typedef DDSCAPS FAR* LPDDSCAPS;

typedef struct _IDirectDrawClipper IDirectDrawClipper,*LPDIRECTDRAWCLIPPER;

typedef DDCOLORKEY FAR* LPDDCOLORKEY;




typedef LONG WINAPI(*LPDDENUMSURFACESCALLBACK)(LPDIRECTDRAWSURFACE, LPDDSURFACEDESC, LPVOID);

typedef /*ULONG*/ int WINAPI(*stQueryInterface)(struct _IDirectDrawSurface *_this, LPVOID);


typedef /*ULONG*/ int WINAPI(*stAddRef)(struct _IDirectDrawSurface *_this);
typedef /*ULONG*/ int WINAPI(*stRelease)(struct _IDirectDrawSurface *_this);
/*** IDirectDrawSurface methods ***/
typedef /*ULONG*/ int WINAPI(*stAddAttachedSurface)(struct _IDirectDrawSurface *_this, LPDIRECTDRAWSURFACE) ;
typedef /*ULONG*/ int WINAPI(*stAddOverlayDirtyRect)(struct _IDirectDrawSurface *_this, LPRECT) ;
typedef /*ULONG*/ int WINAPI(*stBlt)(struct _IDirectDrawSurface *_this, LPRECT,LPDIRECTDRAWSURFACE, LPRECT,DWORD, LPDDBLTFX) ;
typedef /*ULONG*/ int WINAPI(*stBltBatch)(struct _IDirectDrawSurface *_this, LPDDBLTBATCH, DWORD, DWORD ) ;
typedef /*ULONG*/ int WINAPI(*stBltFast)(struct _IDirectDrawSurface *_this, DWORD,DWORD,LPDIRECTDRAWSURFACE, LPRECT,DWORD) ;
typedef /*ULONG*/ int WINAPI(*stDeleteAttachedSurface)(struct _IDirectDrawSurface *_this, DWORD,LPDIRECTDRAWSURFACE) ;
typedef /*ULONG*/ int WINAPI(*stEnumAttachedSurfaces)(struct _IDirectDrawSurface *_this, LPVOID,LPDDENUMSURFACESCALLBACK) ;
typedef /*ULONG*/ int WINAPI(*stEnumOverlayZOrders)(struct _IDirectDrawSurface *_this, DWORD,LPVOID,LPDDENUMSURFACESCALLBACK) ;
typedef /*ULONG*/ int WINAPI(*stFlip)(struct _IDirectDrawSurface *_this, LPDIRECTDRAWSURFACE, DWORD) ;
typedef /*ULONG*/ int WINAPI(*stGetAttachedSurface)(struct _IDirectDrawSurface *_this, LPDDSCAPS, LPDIRECTDRAWSURFACE  *) ;
typedef /*ULONG*/ int WINAPI(*stGetBltStatus)(struct _IDirectDrawSurface *_this, DWORD) ;
typedef /*ULONG*/ int WINAPI(*stGetCaps)(struct _IDirectDrawSurface *_this, LPDDSCAPS) ;
typedef /*ULONG*/ int WINAPI(*stGetClipper)(struct _IDirectDrawSurface *_this, LPDIRECTDRAWCLIPPER *) ;
typedef /*ULONG*/ int WINAPI(*stGetColorKey)(struct _IDirectDrawSurface *_this, DWORD, LPDDCOLORKEY) ;
typedef /*ULONG*/ int WINAPI(*stGetDC)(struct _IDirectDrawSurface *_this, HDC  *) ;
typedef /*ULONG*/ int WINAPI(*stGetFlipStatus)(struct _IDirectDrawSurface *_this, DWORD) ;
typedef /*ULONG*/ int WINAPI(*stGetOverlayPosition)(struct _IDirectDrawSurface *_this, LPLONG, LPLONG ) ;
typedef /*ULONG*/ int WINAPI(*stGetPalette)(struct _IDirectDrawSurface *_this, LPDIRECTDRAWPALETTE *) ;
typedef /*ULONG*/ int WINAPI(*stGetPixelFormat)(struct _IDirectDrawSurface *_this, LPDDPIXELFORMAT) ;
typedef /*ULONG*/ int WINAPI(*stGetSurfaceDesc)(struct _IDirectDrawSurface *_this, LPDDSURFACEDESC) ;
typedef /*ULONG*/ int WINAPI(*stInitialize)(struct _IDirectDrawSurface *_this, LPDIRECTDRAW, LPDDSURFACEDESC) ;
typedef /*ULONG*/ int WINAPI(*stIsLost)(struct _IDirectDrawSurface *_this) ;
typedef /*ULONG*/ int WINAPI(*stLock)(struct _IDirectDrawSurface *_this, LPRECT,LPDDSURFACEDESC,DWORD,HANDLE) ;
typedef /*ULONG*/ int WINAPI(*stReleaseDC)(struct _IDirectDrawSurface *_this, HDC) ;
typedef /*ULONG*/ int WINAPI(*stRestore)(struct _IDirectDrawSurface *_this) ;
typedef /*ULONG*/ int WINAPI(*stSetClipper)(struct _IDirectDrawSurface *_this, LPDIRECTDRAWCLIPPER) ;
typedef /*ULONG*/ int WINAPI(*stSetColorKey)(struct _IDirectDrawSurface *_this, DWORD, LPDDCOLORKEY) ;
typedef /*ULONG*/ int WINAPI(*stSetOverlayPosition)(struct _IDirectDrawSurface *_this, LONG, LONG ) ;
typedef /*ULONG*/ int WINAPI(*stSetPalette)(struct _IDirectDrawSurface *_this, LPDIRECTDRAWPALETTE) ;
typedef /*ULONG*/ int WINAPI(*stUnlock)(struct _IDirectDrawSurface *_this, LPVOID) ;
typedef /*ULONG*/ int WINAPI(*stUpdateOverlay)(struct _IDirectDrawSurface *_this, LPRECT, LPDIRECTDRAWSURFACE,LPRECT,DWORD, LPDDOVERLAYFX) ;
typedef /*ULONG*/ int WINAPI(*stUpdateOverlayDisplay)(struct _IDirectDrawSurface *_this, DWORD) ;
typedef /*ULONG*/ int WINAPI(*stUpdateOverlayZOrder)(struct _IDirectDrawSurface *_this, DWORD, LPDIRECTDRAWSURFACE) ;

class ImageSink;

struct _IDirectDrawSurface {
    /*** IUnknown methods ***/
    tQueryInterface QueryInterface; // 0x0
    tAddRef AddRef;
    tRelease Release;
    /*** IDirectDrawSurface methods ***/
    stAddAttachedSurface AddAttachedSurface;
    stAddOverlayDirtyRect AddOverlayDirtyRect; // 0x10
    stBlt Blt;
    stBltBatch BltBatch;
    stBltFast BltFast;
    stDeleteAttachedSurface DeleteAttachedSurface; // 0x20
    stEnumAttachedSurfaces EnumAttachedSurfaces;
    stEnumOverlayZOrders EnumOverlayZOrders;
    stFlip Flip;
    stGetAttachedSurface GetAttachedSurface; // 0x30
    stGetBltStatus GetBltStatus;
    stGetCaps GetCaps;
    stGetClipper GetClipper;
    stGetColorKey GetColorKey; // 0x40
    stGetDC GetDC;
    stGetFlipStatus GetFlipStatus;
    stGetOverlayPosition GetOverlayPosition;
    stGetPalette GetPalette; // 0x50
    stGetPixelFormat GetPixelFormat;
    stGetSurfaceDesc GetSurfaceDesc;
    stInitialize Initialize;
    stIsLost IsLost; // 0x60
    stLock Lock;
    stReleaseDC ReleaseDC;
    stRestore Restore;
    stSetClipper SetClipper; //0x70
    stSetColorKey SetColorKey;
    stSetOverlayPosition SetOverlayPosition;
    stSetPalette SetPalette;
    stUnlock Unlock;
    stUpdateOverlay UpdateOverlay;
    stUpdateOverlayDisplay UpdateOverlayDisplay;
    stUpdateOverlayZOrder UpdateOverlayZOrder;
    // opaque
    IDirectDraw * ddraw;
    ImageSink * sink;
    DDSURFACEDESC lock_desc;
    pthread_mutex_t mutex;
    int locked;
} ;

struct _DDBLTFX
{
    DWORD       dwSize;                         // size of structure
    DWORD       dwDDFX;                         // FX operations
    DWORD       dwROP;                          // Win32 raster operations
    DWORD       dwDDROP;                        // Raster operations new for DirectDraw
    DWORD       dwRotationAngle;                // Rotation angle for blt
    DWORD       dwZBufferOpCode;                // ZBuffer compares
    DWORD       dwZBufferLow;                   // Low limit of Z buffer
    DWORD       dwZBufferHigh;                  // High limit of Z buffer
    DWORD       dwZBufferBaseDest;              // Destination base value
    DWORD       dwZDestConstBitDepth;           // Bit depth used to specify Z constant for destination
    union
    {
        DWORD   dwZDestConst;                   // Constant to use as Z buffer for dest
        LPDIRECTDRAWSURFACE lpDDSZBufferDest;   // Surface to use as Z buffer for dest
    } DUMMYUNIONNAMEN(1);
    DWORD       dwZSrcConstBitDepth;            // Bit depth used to specify Z constant for source
    union
    {
        DWORD   dwZSrcConst;                    // Constant to use as Z buffer for src
        LPDIRECTDRAWSURFACE lpDDSZBufferSrc;    // Surface to use as Z buffer for src
    } DUMMYUNIONNAMEN(2);
    DWORD       dwAlphaEdgeBlendBitDepth;       // Bit depth used to specify constant for alpha edge blend
    DWORD       dwAlphaEdgeBlend;               // Alpha for edge blending
    DWORD       dwReserved;
    DWORD       dwAlphaDestConstBitDepth;       // Bit depth used to specify alpha constant for destination
    union
    {
        DWORD   dwAlphaDestConst;               // Constant to use as Alpha Channel
        LPDIRECTDRAWSURFACE lpDDSAlphaDest;     // Surface to use as Alpha Channel
    } DUMMYUNIONNAMEN(3);
    DWORD       dwAlphaSrcConstBitDepth;        // Bit depth used to specify alpha constant for source
    union
    {
        DWORD   dwAlphaSrcConst;                // Constant to use as Alpha Channel
        LPDIRECTDRAWSURFACE lpDDSAlphaSrc;      // Surface to use as Alpha Channel
    } DUMMYUNIONNAMEN(4);
    union
    {
        DWORD   dwFillColor;                    // color in RGB or Palettized
        DWORD   dwFillDepth;                    // depth value for z-buffer
        DWORD   dwFillPixel;                    // pixel value for RGBA or RGBZ
        LPDIRECTDRAWSURFACE lpDDSPattern;       // Surface to use as pattern
    } DUMMYUNIONNAMEN(5);
    DDCOLORKEY  ddckDestColorkey;               // DestColorkey override
    DDCOLORKEY  ddckSrcColorkey;                // SrcColorkey override
} ;

typedef long *LPLONG;



typedef struct _IDirectDrawPalette {
    /*** IUnknown methods ***/
    tQueryInterface QueryInterface;
    tAddRef AddRef;
    tRelease Release;
    /*** IDirectDrawPalette methods ***/
    int (*GetCaps)(struct _IDirectDrawPalette * _this, LPDWORD) ;
    int (*GetEntries)(struct _IDirectDrawPalette * _this, DWORD,DWORD,DWORD,LPPALETTEENTRY) ;
    int (*Initialize)(struct _IDirectDrawPalette * _this, LPDIRECTDRAW, DWORD, LPPALETTEENTRY) ;
    int (*SetEntries)(struct _IDirectDrawPalette * _this, DWORD,DWORD,DWORD,LPPALETTEENTRY) ;
    
    // opaque
    int internal_id;
} IDirectDrawPalette;

typedef struct _RGNDATAHEADER {
    DWORD   dwSize;
    DWORD   iType;
    DWORD   nCount;
    DWORD   nRgnSize;
    RECT    rcBound;
} RGNDATAHEADER, *PRGNDATAHEADER;

typedef struct _RGNDATA {
    RGNDATAHEADER   rdh;
    char            Buffer[1];
} RGNDATA, *PRGNDATA, *NPRGNDATA, FAR *LPRGNDATA;


struct _IDirectDrawClipper {
    /*** IUnknown methods ***/
    tQueryInterface QueryInterface;
    tAddRef AddRef;
    tRelease Release;
    /*** IDirectDrawClipper methods ***/
    int GetClipList(struct _IDirectDrawClipper * _this, LPRECT, LPRGNDATA, LPDWORD) ;
    int GetHWnd(struct _IDirectDrawClipper * _this, HWND FAR *) ;
    int Initialize(struct _IDirectDrawClipper * _this, LPDIRECTDRAW, DWORD) ;
    int IsClipListChanged(struct _IDirectDrawClipper * _this, BOOL FAR *) ;
    int SetClipList(struct _IDirectDrawClipper * _this, LPRGNDATA,DWORD) ;
    int SetHWnd(struct _IDirectDrawClipper * _this, DWORD, HWND ) ;
};



struct _DDBLTBATCH
{
    LPRECT              lprDest;
    LPDIRECTDRAWSURFACE lpDDSSrc;
    LPRECT              lprSrc;
    DWORD               dwFlags;
    LPDDBLTFX           lpDDBltFx;
} ;

void dxs_fill_surface(LPDIRECTDRAWSURFACE ds);

int WINAPI QueryInterface(IDirectDraw *_this, /*REFIID*/ int riid, void* ppvObj);
int WINAPI AddRef(IDirectDraw *_this);
int WINAPI Release(IDirectDraw *_this);

typedef struct tWAVEFORMATEX
{
    WORD        wFormatTag;         /* format type */
    WORD        nChannels;          /* number of channels (i.e. mono, stereo...) */
    DWORD       nSamplesPerSec;     /* sample rate */
    DWORD       nAvgBytesPerSec;    /* for buffer estimation */
    WORD        nBlockAlign;        /* block size of data */
    WORD        wBitsPerSample;     /* number of bits per sample of mono data */
    WORD        cbSize;             /* the count in bytes of the size of */
                                    /* extra information (after cbSize) */
} WAVEFORMATEX, *PWAVEFORMATEX, *NPWAVEFORMATEX, *LPWAVEFORMATEX;

typedef struct _DSBUFFERDESC
 {
    DWORD           dwSize;
    DWORD           dwFlags;
    DWORD           dwBufferBytes;
    DWORD           dwReserved;
    LPWAVEFORMATEX  lpwfxFormat;
    GUID            guid3DAlgorithm;
 
} DSBUFFERDESC, *LPDSBUFFERDESC;
 
typedef const DSBUFFERDESC *LPCDSBUFFERDESC;
typedef struct IDirectSound * LPDIRECTSOUND;

typedef struct _DSBCAPS
{
    DWORD           dwSize;
    DWORD           dwFlags;
    DWORD           dwBufferBytes;
    DWORD           dwUnlockTransferRate;
    DWORD           dwPlayCpuOverhead;
} DSBCAPS, *LPDSBCAPS;

typedef struct _DSCAPS
{
    DWORD           dwSize;
    DWORD           dwFlags;
    DWORD           dwMinSecondarySampleRate;
    DWORD           dwMaxSecondarySampleRate;
    DWORD           dwPrimaryBuffers;
    DWORD           dwMaxHwMixingAllBuffers;
    DWORD           dwMaxHwMixingStaticBuffers;
    DWORD           dwMaxHwMixingStreamingBuffers;
    DWORD           dwFreeHwMixingAllBuffers;
    DWORD           dwFreeHwMixingStaticBuffers;
    DWORD           dwFreeHwMixingStreamingBuffers;
    DWORD           dwMaxHw3DAllBuffers;
    DWORD           dwMaxHw3DStaticBuffers;
    DWORD           dwMaxHw3DStreamingBuffers;
    DWORD           dwFreeHw3DAllBuffers;
    DWORD           dwFreeHw3DStaticBuffers;
    DWORD           dwFreeHw3DStreamingBuffers;
    DWORD           dwTotalHwMemBytes;
    DWORD           dwFreeHwMemBytes;
    DWORD           dwMaxContigFreeHwMemBytes;
    DWORD           dwUnlockTransferRateHwBuffers;
    DWORD           dwPlayCpuOverheadSwBuffers;
    DWORD           dwReserved1;
    DWORD           dwReserved2;
} DSCAPS, *LPDSCAPS;

typedef const WAVEFORMATEX FAR *LPCWAVEFORMATEX;

typedef /*HRESULT*/ int WINAPI(*tGetCaps3)              (struct IDirectSoundBuffer * _this, LPDSBCAPS) ;
typedef /*HRESULT*/ int WINAPI(*tGetCurrentPosition)   (struct IDirectSoundBuffer * _this, LPDWORD, LPDWORD) ;
typedef /*HRESULT*/ int WINAPI(*tGetFormat)            (struct IDirectSoundBuffer * _this, LPWAVEFORMATEX, DWORD, LPDWORD) ;
typedef /*HRESULT*/ int WINAPI(*tGetVolume)            (struct IDirectSoundBuffer * _this, LPLONG) ;
typedef /*HRESULT*/ int WINAPI(*tGetPan)               (struct IDirectSoundBuffer * _this, LPLONG) ;
typedef /*HRESULT*/ int WINAPI(*tGetFrequency)         (struct IDirectSoundBuffer * _this, LPDWORD) ;
typedef /*HRESULT*/ int WINAPI(*tGetStatus)            (struct IDirectSoundBuffer * _this, LPDWORD) ;
typedef /*HRESULT*/ int WINAPI(*tInitialize2)           (struct IDirectSoundBuffer * _this, LPDIRECTSOUND, LPCDSBUFFERDESC) ;
typedef /*HRESULT*/ int WINAPI(*tLock)                 (struct IDirectSoundBuffer * _this, DWORD, DWORD, LPVOID *, LPDWORD, LPVOID *, LPDWORD, DWORD) ;
typedef /*HRESULT*/ int WINAPI(*tPlay)                 (struct IDirectSoundBuffer * _this, DWORD, DWORD, DWORD) ;
typedef /*HRESULT*/ int WINAPI(*tSetCurrentPosition)   (struct IDirectSoundBuffer * _this, DWORD) ;
typedef /*HRESULT*/ int WINAPI(*tSetFormat)            (struct IDirectSoundBuffer * _this, LPCWAVEFORMATEX) ;
typedef /*HRESULT*/ int WINAPI(*tSetVolume)            (struct IDirectSoundBuffer * _this, LONG) ;
typedef /*HRESULT*/ int WINAPI(*tSetPan)               (struct IDirectSoundBuffer * _this, LONG) ;
typedef /*HRESULT*/ int WINAPI(*tSetFrequency)         (struct IDirectSoundBuffer * _this, DWORD) ;
typedef /*HRESULT*/ int WINAPI(*tStop)                 (struct IDirectSoundBuffer * _this) ;
typedef /*HRESULT*/ int WINAPI(*tUnlock)               (struct IDirectSoundBuffer * _this, LPVOID, DWORD, LPVOID, DWORD) ;
typedef /*HRESULT*/ int WINAPI(*tRestore)              (struct IDirectSoundBuffer * _this) ;

struct IDirectSoundBuffer {
    // IUnknown methods
    tQueryInterface QueryInterface;
    tAddRef AddRef;
    tRelease Release;
    
    tGetCaps3 GetCaps;
    tGetCurrentPosition GetCurrentPosition;
    tGetFormat GetFormat;
    tGetVolume GetVolume;
    tGetPan GetPan;
    tGetFrequency GetFrequency;
    tGetStatus GetStatus;
    tInitialize2 Initialize;
    tLock Lock;
    tPlay Play;
    tSetCurrentPosition SetCurrentPosition;
    tSetFormat SetFormat;
    tSetVolume SetVolume;
    tSetPan SetPan;
    tSetFrequency SetFrequency;
    tStop Stop;
    tUnlock Unlock;
    tRestore Restore;
    
};

typedef struct IDirectSoundBuffer *LPDIRECTSOUNDBUFFER;
typedef const GUID *LPCGUID;

struct IDirectSound;
typedef /*HRESULT*/ int WINAPI(*tCreateSoundBuffer)(struct IDirectSound *_this, LPCDSBUFFERDESC desc,
        LPDIRECTSOUNDBUFFER * sbuffer, void * unk);
typedef /*HRESULT*/ int WINAPI(*tGetCaps2)              (struct IDirectSound *_this, LPDSCAPS) ;
typedef /*HRESULT*/ int WINAPI(*tDuplicateSoundBuffer) (struct IDirectSound *_this, LPDIRECTSOUNDBUFFER, LPDIRECTSOUNDBUFFER *) ;
typedef /*HRESULT*/ int WINAPI(*tSetCooperativeLevel2)  (struct IDirectSound *_this, HWND, DWORD) ;
typedef /*HRESULT*/ int WINAPI(*tCompact2)              (struct IDirectSound *_this) ;
typedef /*HRESULT*/ int WINAPI(*tGetSpeakerConfig)     (struct IDirectSound *_this, LPDWORD) ;
typedef /*HRESULT*/ int WINAPI(*tSetSpeakerConfig)     (struct IDirectSound *_this, DWORD) ;
typedef /*HRESULT*/ int WINAPI(*tInitialize3)           (struct IDirectSound *_this, LPCGUID) ;

struct IDirectSound {   
    // IUnknown methods
    tQueryInterface QueryInterface;
    tAddRef AddRef;
    tRelease Release;

    // IDirectSound methods
    tCreateSoundBuffer CreateSoundBuffer;
    tGetCaps2 GetCaps;
    tDuplicateSoundBuffer DuplicateSoundBuffer;
    tSetCooperativeLevel2 SetCooperativeLevel;
    tCompact2 Compact;
    tGetSpeakerConfig GetSpeakerConfig;
    tSetSpeakerConfig SetSpeakerConfig;
    tInitialize3 Initialize;     
 };
 

#define DDSD_CAPS               0x00000001l     // default

/*
 * dwHeight field is valid.
 */
#define DDSD_HEIGHT             0x00000002l

/*
 * dwWidth field is valid.
 */
#define DDSD_WIDTH              0x00000004l

/*
 * lPitch is valid.
 */
#define DDSD_PITCH              0x00000008l

/*
 * dwBackBufferCount is valid.
 */
#define DDSD_BACKBUFFERCOUNT    0x00000020l

/*
 * dwZBufferBitDepth is valid.  (shouldnt be used in DDSURFACEDESC2)
 */
#define DDSD_ZBUFFERBITDEPTH    0x00000040l

/*
 * dwAlphaBitDepth is valid.
 */
#define DDSD_ALPHABITDEPTH      0x00000080l


/*
 * lpSurface is valid.
 */
#define DDSD_LPSURFACE          0x00000800l

/*
 * ddpfPixelFormat is valid.
 */
#define DDSD_PIXELFORMAT        0x00001000l

/*
 * ddckCKDestOverlay is valid.
 */
#define DDSD_CKDESTOVERLAY      0x00002000l

/*
 * ddckCKDestBlt is valid.
 */
#define DDSD_CKDESTBLT          0x00004000l

/*
 * ddckCKSrcOverlay is valid.
 */
#define DDSD_CKSRCOVERLAY       0x00008000l

/*
 * ddckCKSrcBlt is valid.
 */
#define DDSD_CKSRCBLT           0x00010000l

/*
 * dwMipMapCount is valid.
 */
#define DDSD_MIPMAPCOUNT        0x00020000l

 /*
  * dwRefreshRate is valid
  */
#define DDSD_REFRESHRATE        0x00040000l

/*
 * dwLinearSize is valid
 */
#define DDSD_LINEARSIZE         0x00080000l

/*
 * dwTextureStage is valid
 */
#define DDSD_TEXTURESTAGE       0x00100000l
/*
 * dwFVF is valid
 */
#define DDSD_FVF                0x00200000l
/*
 * dwSrcVBHandle is valid
 */
#define DDSD_SRCVBHANDLE        0x00400000l

/*
 * dwDepth is valid
 */
#define DDSD_DEPTH              0x00800000l

/*
 * All input fields are valid.
 */
#define DDSD_ALL                0x00fff9eel

typedef struct {
    DWORD        dwSize;
    DWORD        dwFlags;
    DWORD        dwMonitorFrequency;
    DDSURFACEDESC    dsdSurfaceDesc;
} DDMODEDESC, * LPDDMODEDESC; 




}


#endif /* DX_H */


#ifndef _C_HASH_H_
#define _C_HASH_H_

#include "common.h"
/**
 * Generic hash functions. 
 * NOTE there is some problem with this implementation, poor hashing functions
 * may cause the table to grow too quickly, which must be fixed.
 * TODO change buckets to sorted array and implement binary search
 */


#define BUCKET_ST_SIZE          (sizeof(uint32))
#define BUCKET_FULL_SIZE(s,b)   (s*(BUCKET_ST_SIZE + (b)*sizeof(c_hash_entry)))
#define BUCKET_SIZE(b)          (BUCKET_ST_SIZE+(b)*sizeof(c_hash_entry))
#define BUCKET_INCREASE(t,b)    (c_hash_bucket*)((unsigned char *)t+BUCKET_SIZE(b))

#define BUCKET_OFFSET(h, o)     (c_hash_bucket*)((unsigned char *)h->buckets+((o)*BUCKET_SIZE(h->bucket_size)))
#define BUCKET_OFFSETH(hs, hb, o) (c_hash_bucket*)((unsigned char *)hs+((o)*BUCKET_SIZE(hb)))

#define C_HASH_KEY_TYPE_PTR             1
#define C_HASH_KEY_TYPE_UINT32          2

// 32 mb for now
#define C_HASH_MAX_SIZE 33554432

#define C_HASH_MAX_SIZE_ERROR -1

typedef struct _c_hash_entry_serial {
    uint32 key;
    unsigned char data[];
} c_hash_entry_serial;

typedef struct _c_hash_entry {
    uint32 key;
    void * skey;
    void * data;
    struct _c_hash_entry * next;
    struct _c_hash_entry * prev;
} c_hash_entry;

typedef struct _c_hash_bucket {
	uint32 size;
	c_hash_entry entries[];	
} c_hash_bucket;


typedef struct _c_hash {
    /* Hash mutex. Currently, any operation over the hash will lock it. This may be improved
        with a mutex in each bucket, locking only operations over the particular areas */
    pthread_mutex_t mutex;
    uint32 bucket_size;
    uint32 num_buckets;
    uint32 num_entries;
    c_hash_bucket * buckets;
    uint32 (*hash_func)(void *);
    c_hash_entry * seq_st;
    c_hash_entry * seq_end;
    int (*cmp_func)(void *, void *, void *);
    uint32 key_size;
    uint32 key_type;
    int dirty;
    int locked;
} c_hash;

typedef struct _c_serialized_hash {
    uint32 bucket_size;
    uint32 num_buckets;
    uint32 entries_num;
    c_hash_entry_serial entries[];
} c_serialized_hash;

#define C_HASH_REMOVE_IT(it,h) \
    int tobreak = (it == h->seq_end); \
    c_hash_entry * tmp = (it)->next; \
    c_hash_remove_kl((it)->key,NULL,h,0,0); \
    if (tobreak) { \
        break; \
    } \
    it = tmp; \
    continue;
    
#define C_HASH_ITERATE_L(it,h,code) \
    LOCK_HASH(h); \
    { \
    if (h->dirty) { \
		c_hash_rebuild_list(h); \
	} \
    c_hash_entry * it = h->seq_st; \
    if (it) { \
        for(;;) { \
        	code \
            if (it == h->seq_end) { \
                break; \
            } \
            it = it->next; \
        } \
    }\
    } \
    UNLOCK_HASH(h); \


#define C_HASH_ITERATE(it,h,code) \
    {\
    if (h->dirty) { \
		c_hash_rebuild_list(h); \
	} \
    c_hash_entry * it = h->seq_st; \
    if (it) { \
        for(;;) { \
            code \
            if (it == h->seq_end) { \
                break; \
            } \
            it = it->next; \
        } \
    }\
    } \
    

uint32 c_hash_rebuild_list(c_hash * h);
void c_hash_it_remove(c_hash_entry ** it, c_hash * h);
void * c_hash_remove(uint32 keyh, c_hash * h);
void * c_hash_remove_k(uint32 keyh, void * key, c_hash * h, int rehash);
void * c_hash_remove_kl(uint32 hash, void * key, c_hash * h, int rehash, int lock);
uint32 key_to_idx(uint32 key, uint32 len);
int c_hash_contains(uint32 hash, c_hash * h);
void * c_hash_get(uint32 hash, c_hash * h);
void * c_hash_get_k(uint32 hash, void * key, c_hash * h);
uint32 c_hash_put(uint32 hash, void * data, c_hash * h);
uint32 c_hash_put_h(void * key, void * data, c_hash * h);
uint32 c_hash_put_k(uint32 hash, void * key, void * data, c_hash * h);
void * c_hash_geth(void * key, c_hash * h);

c_hash * c_hash_create(int bucket_size, uint32 st_size, uint32 hash_func(void *));
c_hash * c_hash_create_pd(int bucket_size, uint32 st_size, uint32 hash_func(void *), uint32 ksize);
c_hash * c_hash_create_uintd(int bucket_size, uint32 st_size, uint32 hash_func(void *));
c_hash * c_hash_create_psp(int bucket_size, uint32 st_size, uint32 hash_func(void *), uint32 ksize, int cmp_func(void *, void *, void *));
void * c_hash_remove_h(void * key, c_hash * h);

//#define C_LOCK_HASH(h) LOCK_MUTEX(h->mutex)
//#define C_UNLOCK_HASH(h) UNLOCK_MUTEX(h->mutex)
#define C_HASH_SERIAL_SZ(h,s) (sizeof(c_serialized_hash)+h->num_entries*(sizeof(uint32)+s))

/* The serializer will provide an EMPTY array of data in the blob, to be
    filled by the caller. */
uint32 c_serialize_hash(c_serialized_hash * s_hash, c_hash * h);
void c_hash_clear(c_hash * h);
void c_hash_destroy(c_hash * h);
c_hash * c_deserialize_hash(c_serialized_hash * s_hash);
unsigned int str_djb2_hash(char *str);
unsigned int str_djb2_hash2(char *str, int size);

#define DEF_STRING_HASH(s)	str_djb2_hash((s))
#define DEF_ARRAY_HASH(a,s) str_djb2_hash2(((char*)a),(s))

#endif

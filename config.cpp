
#include "common.h"

global_config global_config_data = {0,};

static void defaults() {
    sprintf(global_config_data.emu_base_dir,"/etc/ntbox");
    global_config_data.emu_base_dir_sz  = strlen(global_config_data.emu_base_dir);
    global_config_data.exec_mode = 0;
    global_config_data.use_nt_debug = 0;
    global_config_data.use_full_vmap = 1;
    
    global_config_data.scale_mode = SCALE_KEEP_ASPECT;
    global_config_data.full_screen = 1;
}

char * name_only(char * cmd) {
    char * t = cmd;
    char * p = NULL;
    char * vp = NULL;

    while (*t) {
        
        if (*t == '/')  {
            vp = p;
            p=t;
        }
        t++;
    }
    
    if (vp) {
        return vp+1;
    }
    
    return p+1;
}

void init_config(int argc, char * argv[]) {
    defaults();
    
    strcpy(global_config_data.cmdex,argv[1]);
    
    char * t = global_config_data.cmdline;
    
    strcpy(t,name_only(argv[1]));
    t += strlen(t);

    *t = ' ';
    
    int i;
    for(i=2;i<argc;i++) {
        strcpy(t,argv[i]);
        t += strlen(t);
        *t = ' ';
    }
    *t = 0;
    
    byte * px = (byte*)global_config_data.cmdline_w;
    
    for(i=0;i<strlen(global_config_data.cmdline);i++) {
        px[i*2] = global_config_data.cmdline[i];
        px[i*2+1] = 0;
        
    }
    px[i*2] = 0;
    px[i*2+1] = 0;
    
    sprintf(global_config_data.cmdline_remapped,"C:\\%s",global_config_data.cmdline);
    t = global_config_data.cmdline_remapped;
    while (*t) {
        if (*t == '/') *t = '\\';
        t++;
    }
    
    strcpy(global_config_data.base_path_remapped,global_config_data.cmdline_remapped);
    l_substring(global_config_data.base_path_remapped,0,l_lastindexof(global_config_data.base_path_remapped,'\\'));
    
    strcpy(global_config_data.base_path,argv[1]);
    
    l_substring(global_config_data.base_path,0,l_lastindexof(global_config_data.base_path,'/'));
    strcpy(global_config_data.cur_dir,global_config_data.base_path);
    l_substring(global_config_data.base_path,0,l_lastindexof(global_config_data.base_path,'/'));
    
    global_config_data.base_path_sz = strlen(global_config_data.base_path);
    
    sprintf(global_config_data.cmdline_remapped_q,"\"%s\"",global_config_data.cmdline_remapped);
    
    global_config_data.cmdline_w_q[0] = '\"';
    global_config_data.cmdline_w_q[1] = 'C';
    global_config_data.cmdline_w_q[2] = ':';
    global_config_data.cmdline_w_q[3] = '\\';
    
    w_strcpy(global_config_data.cmdline_w_q+4,global_config_data.cmdline_w);
    
    
    
    int len = _wcslen(global_config_data.cmdline_w_q);
    
    global_config_data.cmdline_w_q[len] = '\"';
    global_config_data.cmdline_w_q[len+1] = 0;
    
    len = _wcslen(global_config_data.cmdline_w_q);
    
    for(i=0;i<len;i++) {
        if (global_config_data.cmdline_w_q[i] == '/') {
            global_config_data.cmdline_w_q[i] = '\\';
        }
    }
    
    
    log_info("Command line %s",global_config_data.cmdline);
    log_info("Command line quoted %s",global_config_data.cmdline_remapped_q);
    log_info("Base path %s",global_config_data.base_path);
    log_info("Remapped current dir %s",global_config_data.base_path_remapped);

    

}


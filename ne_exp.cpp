#include "ne.h"
#include "c_hash.h"

typedef struct _ne_exp_table_entry {
    uint32 idx;
} ne_exp_table_entry;

static int nmods = 1;
static char local_mod_names[16][256];
static c_hash * local_mod_table = NULL;
static uint32 nfns_idx = 1;
static FILE * ftable = NULL;

int get_global_fn_idx(char * module, int ord) {
    int i;
    for(i=0;i<nmods;i++) {
        if (!strcmp(local_mod_names[i],module)) {
            int iidx = (i<<16)+ord;
            int ix = (int)c_hash_get_k(iidx,(void*)iidx,local_mod_table);
            if (!ix) {
                printf("ERROR: Invalid ord in global table\n");
                exit(0);
            }
            return ix;
        }
    }
    printf("ERROR: Invalid ord in global table\n");
    exit(0);
}

int exp_parse(char * path, ne_exp_info ** infos, int *i_size, char * mod_name) {
    if (!local_mod_table) {
        local_mod_table = c_hash_create(16,256,0);
    }
    if (!ftable) {
        ftable = fopen("gtable","wb");
    }
    
    int local_mod = nmods++;    
    strcpy(local_mod_names[local_mod],mod_name);
    local_mod = (local_mod<<16);
    FILE * fx = fopen(path,"rb");
    if (!fx) {
        printf("Failure\n");
        exit(0);
    }
    fseek(fx,0,SEEK_END);
    int size = ftell(fx);
    fseek(fx,0,SEEK_SET);
    byte * data = (byte*)malloc(size);
    size = fread(data,1,size,fx);
    fclose(fx);
    
    printf("NE File %s %i\n",path,size);
    uint32 header_off = *(uint32*)(data+0x3c);
    NE_header * header = (NE_header*)(data+(header_off));
    
    byte * tt = (byte*)header+header->EntryTableOffset;
    byte * end = tt+header->EntryTableLength;
    
    int i;
    for(;tt<end;) {
        int nentries = *tt;
        tt++;
        for(i=0;i<nentries;i++) {
            int type = *tt;

            tt++;
            if (type == 0) {
                continue;
            }
            if (type == 0xff) {
                tt++;
                tt += 5;

            }
            else {
                tt++;
                tt +=2;
                
            }
        }
    }
    
    *infos = (ne_exp_info*)malloc(1024*sizeof(ne_exp_info));
    
    tt = data+header->OffStartNonResTab;
    end = tt+header->NoResNamesTabSiz;
    printf("%X\n",tt-data);
    int cc = 0;
    char name[128];
    while (tt<end) {
        int size = *tt;
        if (size > 64) {
            printf("ERROR:too large exp symbol\n");
            exit(0);
        }
        tt++;
        memcpy(name,tt,size);
        
        name[size] = 0;
        
        tt += size;
        int ord = *(uint16_t*)tt;
        int midx = local_mod + ord;
        int global_idx = nfns_idx++;
        c_hash_put_k(midx,(void*)midx,(void*)global_idx,local_mod_table);
        ne_exp_info * ex = &(*infos)[ord];
        strcpy(ex->name,name);
        
        fprintf(ftable,"%s %s %X %X\n",mod_name,name,midx,global_idx);
        printf("%s %i\n",ex->name,ord);
        ex->nsize = size;
        ex->ord = ord;
        tt += 2;
        cc++;
        ex++;
        if (ord >= 1024) {
            printf("ERROR: too many exports\n");
            exit(0);
        }
       // break;
    }

    
    *i_size = 1024;
    
}

void finish_exports() {
    fclose(ftable);
}


#include "ne.h"
#include "peloader.h"
#include "c_hash.h"

int vashift = 0;


#define NE_DATA "ne/"

int process_imports(byte * data, NE_header * header, int dsize) {
    uint16_t * imports = (uint16_t*)((byte*)header+header->ModRefTable);
    int i;
    byte * symbols = (byte*)header+header->ImportNameTable;
    char name[256];
    for(i=0;i<header->ModRefs;i++) {
        uint16_t off = *imports;
        printf("%X\n",off);
        byte * symbol = symbols+off;
        imports++;
        int size = *symbol;
        symbol++;
        memcpy(name,symbol,size);
        name[size] = 0;
        printf("%s\n",name);
    }
    
}

typedef struct _ne_base_info {
    ne_exp_info *infos;
    int info_size;
    char fname[128];
    char mname[128];
} ne_base_info;

static ne_base_info * ne_infos = NULL;
static int ne_base_info_sz = 0;

static c_hash * local_exp_symbols = NULL;




static void load_exp_symbols() {
    local_exp_symbols = c_hash_create(8,1024,0);
    local_exp_symbols->cmp_func = global_strcmp;
    
    FILE * fx = fopen("pmods","r");
    char fname[512];
    if (!fx) {
        printf("ERROR: pmods not found, 16 bits loading unavailable\n");
        exit(0);
    }
    
    int cc;
    char line[1024];
    while (fgets(line,1024,fx)) {
        int sz = strlen(line);
        if (sz == 1) {
            continue;
        }

        cc++;
    }
    
    fseek(fx,0,SEEK_SET);
    
    while (fgets(line,1024,fx)) {
        int sz = strlen(line);
        if (sz == 1) {
            continue;
        }
        line[sz-1] = 0;
        pe_info info;
        sprintf(fname,"shelf/%s",line);
        process_file(fname,&info);
        printf("load from %s\n",fname);
        
        int i;
        for(i=0;i<info.export_table_sz;i++) {
            
            char * name = (char*)malloc(strlen(info.export_table[i].name)+1);
            tolower_str(info.export_table[i].name,name);
            //printf("%s %s\n",info.export_table[i].name,name);
            if (c_hash_get_k(DEF_STRING_HASH(name),name,local_exp_symbols)) {
                printf("Repeated entry %s\n",name);
                exit(0);
            }
            c_hash_put_k(DEF_STRING_HASH(name),name,info.export_table[i].name,local_exp_symbols);
            
            
        }
    }
    
}


int get_ne_symbol(char * module, int ordinal, char * fn) {
    char * fni;
    char lower[256];
    
    int i;
    for(i=0;i<ne_base_info_sz;i++) {
        if (!strcmp(ne_infos[i].mname,module)) {
            int j;
           // printf("ifn %s\n",ne_infos[i].infos[ordinal].name);
            tolower_str(ne_infos[i].infos[ordinal].name,lower);
            
            if (!strcmp("localalloc",lower)) {
                strcpy(fn,"localalloc16");
                return 1;
            }
            if (!strcmp("__winflags",lower)) {
                // this isnt a function
                strcpy(fn,lower);
                return 3;
            }
            if (lower[0] == 0) {
                printf("Bad ordinal? %s %X %s\n",module,ordinal,ne_infos[i].infos[ordinal].name );
                exit(0);
            }
            
            int sz = strlen(lower);
            // do we have it as auxiliary?
            lower[sz] = '1';
            lower[sz+1] = '6';
            lower[sz+2] = 0;
            void * sym = sym_lookup(lower);
            
            if (sym) {
                strcpy(fn,lower);
                return 2;
            }
            
            lower[sz] = 0;
            void * ret = c_hash_get_k(DEF_STRING_HASH(lower),lower,local_exp_symbols);
           // printf("from hash %X\n",ret);
            if (ret) {
                // we have pe32 equivalent
                strcpy(fn,(char*)ret);
                return 1;
            }
            
            lower[sz] = 'a';
            lower[sz+1] = 0;

            ret = c_hash_get_k(DEF_STRING_HASH(lower),lower,local_exp_symbols);
            if (ret) {
                // we have pe32 equivalent (ANSI string)
                strcpy(fn,(char*)ret);
                return 1;
            }
           // printf("No pe equivalent %s\n",ne_infos[i].infos[ordinal].name);
            
            printf("ERROR: Could not emulate 16 bits %s %s, aborting...\n",module,ne_infos[i].infos[ordinal].name);
            exit(0);
        }
    }
    printf("Invalid import %s -- %i\n",module,ordinal);
    exit(0);
}

void load_ne_infos() {
    load_exp_symbols();
    
    
    FILE * fx = fopen("mmods","r");
    if (!fx) {
        printf("ERROR: mmods not found, 16 bits loading unavailable\n");
        exit(0);
    }
    int cc = 0;

    char line[1024];
    while (fgets(line,1024,fx)) {
        int sz = strlen(line);
        if (sz == 1) {
            continue;
        }

        cc++;
    }
    fseek(fx,0,SEEK_SET);
    
    ne_infos = (ne_base_info*)calloc(cc,sizeof(ne_base_info));
    ne_base_info * ii = ne_infos;
    ne_base_info_sz = cc;
    while (fgets(line,1024,fx)) {
        int sz = strlen(line);
        if (sz == 1) {
            continue;
        }
        line[strlen(line)-1] = 0;
        char * t = line;
        char * t2 = NULL;
        while (*t) {
            if (*t == ',') {
                *t = 0;
                t2 = t+1;
                break;
            }
            t++;
        }
        if (!t2) {
            printf("Invalid entry on mmods\n");
            exit(0);
        }
        
        char path[256];
        strcpy(path,NE_DATA);
        strcpy(path+strlen(NE_DATA),t2);
        printf("%s\n",path);
        exp_parse(path,&ii->infos,&ii->info_size,line);
        strcpy(ii->fname,t2);
        strcpy(ii->mname,line);
        ii++;
        
    }
    
    fclose(fx);
    finish_exports();
    
    
}

int process_file_ne(char * path, ne_file_info * ne) {
    if (!ne_infos) {
        load_ne_infos();
    }
    FILE * fx = fopen(path,"rb");
    if (!fx) {
        printf("Failure\n");
        exit(0);
    }
    fseek(fx,0,SEEK_END);
    int size = ftell(fx);
    fseek(fx,0,SEEK_SET);
    byte * data = (byte*)malloc(size);
    size = fread(data,1,size,fx);
    fclose(fx);
    
    printf("NE File %s %i\n",path,size);
    uint32 header_off = *(uint32*)(data+0x3c);
    NE_header * header = (NE_header*)(data+(header_off));
    process_imports(data,header,size);
    
    printf("offs %X %X\n",header->ImportNameTable,header->EntryTableOffset);
    printf("str off %X\n",header->ImportNameTable+header_off);
    int i;
    res_table * rt = (res_table*)(header->ResTableOffset+data+header_off);
    vashift = 1<<rt->align;
    ne->vashift = vashift;
    printf("valign %X\n",vashift);
    printf("%X\n",header->ResTableOffset+header_off);
    
    TYPEINFO * t = (TYPEINFO*)rt->infos;
    for(;;) {
        if (!t->rtTypeID) break;
        printf("type %X\n",t->rtTypeID);
        int off = t->rtResourceCount*sizeof(NAMEINFO);
        NAMEINFO * nx = t->rtNameInfo;
        for(i=0;i<t->rtResourceCount;i++) {
            printf("nx %X %i\n",nx->rnOffset*vashift,nx->rnLength*vashift);
            nx++;
        }
        t = t+1;
        t = (TYPEINFO*)nx;
        printf("off %i\n",off);
        
//        NE_entry * ne = (NE_entry*)t;
//        printf("%X %X\n",ne->entries,ne->type);
//        if (ne->type == 0xFF) {
//            NE_movable_segment * sa = (NE_movable_segment*)ne->data;
//            printf("%X %X %X\n",sa->seg_off);
//            
//            t = t+sizeof(NE_entry)+sizeof(NE_movable_segment);
//        }
//        else {
//            t = t+sizeof(NE_entry);
//        }
    }
    byte * tt = (byte*)t+2;
    printf("tt at %X\n",tt-data);
    while (*tt) {
        int size = *tt;
        char mm[256];
        memcpy(mm,tt+1,size);
        mm[size] = 0;
        printf("info %s %i\n",mm,size);
        tt+= size+1;
    }
    
    segment * ss = (segment*)(header->SegTableOffset+data+header_off);
    int numsegs = header->SegCount;
    printf("entry %X\n",header->EntryPoint);
    ne->entry = header->EntryPoint & 0xFFFF;
    printf("count %i\n",numsegs);
    printf("mpos %X %X\n",(byte*)ss-data,*(WORD*)ss);
    
    short * its = (short*)(data+header->ModRefTable+header_off);
    printf("its %i %X %i\n",*its,header->ModRefTable+header_off,header->ModRefs);
    ne->import_table = (ne_import_info*)malloc(sizeof(ne_import_info)*header->ModRefs);
    ne_import_info * import = ne->import_table;
    for(i=0;i<header->ModRefs;i++) {
        char nname[256];
        byte * nn = (data+header->ImportNameTable+header_off+its[i]);
        memcpy(import->module,nn+1,*nn);
        import->module[*nn] = 0;      
        import++;
    }
    ne->import_table_size = header->ModRefs;
    
    for(i=0;i<numsegs;i++) {
        printf("segment %X %X flags %X\n",ss->offset*vashift,ss->length,ss->flags);
        ne->segments[i] = ss;
        
        if (ss->flags & 0x100) {
            // reloc info
            byte * tx = (byte*)data+ss->offset*vashift+ss->length;
            
            int numrelocs = *(uint16_t*)tx;
            printf("reloc %X %X %X %X\n",*(uint16_t*)tx,ss->offset*vashift,tx-(byte*)data,numrelocs);
            int j;
            tx +=2;
            ne_import_reloc * tt;
            if (numrelocs > 0) {
                ne->relocs = (ne_import_reloc*)malloc(sizeof(ne_import_reloc)*numrelocs);               
                tt = (ne_import_reloc*)ne->relocs;
                ne->relocs_size = numrelocs;
            }
             
            for(j=0;j<numrelocs;j++) {
                byte source = *tx++;
                byte flags = (*tx++) & 0x3;
                uint16_t offset = *(uint16_t*)tx;
                
                tx += 2;
                printf("src %X %X %X %X\n",source,flags,offset,tx-(byte*)data);
                if (flags == 1) {
                    //printf("import ordinal %X %X\n",*(uint16_t*)tx,*(uint16_t*)(tx+2));
                    tt->idx = (*(uint16_t*)tx)-1;
                    
                    tt->ord = *(uint16_t*)(tx+2);
                    tt->offset = offset;
                    tt->type = RELOC_TYPE_ORD + source;
                    tt++;
                }
                else if (flags == 3) {
                    printf("unsupported reloc\n");
                    exit(0);
                }
                else if (flags == 2) {
                    printf("Unsupported import\n");
                    exit(0);
                }
                else if (flags == 0) {
                    printf("Internal ref %X %X\n",*tx,*(uint16_t*)(tx+2));
                   // exit(0);
                    tt->offset = offset;
                    tt->ord = *tx;
                    tt->type = RELOC_TYPE_INTERNAL + source;
                    tt++;
                }
                else {
                    printf("Unknown ref\n");
                    exit(0);
                }
                tx +=4;
                
            }
            
        }
        ss++;        
    }
    ne->seg_num = numsegs;
    
    ne->data = data;
    ne->dsize = size;
    
}
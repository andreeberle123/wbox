/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   peloader.h
 * Author: z
 *
 * Created on May 1, 2018, 12:32 AM
 */

#ifndef PELOADER_H
#define PELOADER_H

class PEloader {
public:
    PEloader();
    
private:

};

#include "common.h"

#define PE_SIG_BASE_OFF  0x3c

typedef struct _pe_header {
    word machine;
    word num_sections;
    dword timestamp; //0x4
    dword symbol_table; // 0x8
    dword num_symbols; // 0xc
    word optheader_size;
    word characteristics;
} __attribute__((packed)) pe_header;

typedef struct _pe_dir_entry {
    dword addr;
    dword size;
} __attribute__((packed)) pe_dir_entry;

typedef struct _sect_info {
    byte * data;
    unsigned int size;
    unsigned int ep;
    unsigned int va;
    unsigned int vsize;
} __attribute__((packed)) sect_info;

typedef struct _import_table_internal_entry {
    char * name;
    dword idx;
    dword vaddr;
    char * libname;
} __attribute__((packed)) it_inner_entry;

typedef struct _pe_opt_header32 {
    word magic;
    byte maj_linker_version;
    byte min_linker_version;
    dword size_of_code;
    dword size_of_initdata; // 0x8
    dword size_of_uninitdata; // 0xc
    dword entry_point; // 0x10
    dword code_base;
    dword data_base;
    dword image_base;
    dword sect_align; // 0x20
    dword file_align;
    word maj_os_ver;
    word min_os_ver;
    word maj_img_ver;
    word min_img_ver;
    word maj_subsys_ver; // 0x30
    word min_subsys_ver;
    dword win32_ver; // 0x34
    dword size_of_image;
    dword size_of_headers;
    dword checksum; // 0x40
    word subsys;
    word dll_charac;
    dword size_of_stack_res;
    dword size_of_stack_commit;
    dword size_of_heap_res;
    dword size_of_heap_commit; //0x50
    dword loader_flags;
    dword num_rvas;
    long long export_table; // 0x60
    long long import_table;
    long long resource_table; // 0x70
    long long exception_table;
    long long cert_table;
    long long base_reloc_table;
    long long debug;
    long long arch;
    long long global_ptr;
    long long tls_table;
    long long load_cfg_table;
    long long bound_import;
    long long iat;
    long long delay_descriptor;
    long long clr_header;
    long long reserved; /* always zero */


} __attribute__((packed)) pe_opt_header32;

typedef struct _pe_section_header {
    char name[8];
    dword vsize; 
    dword vaddr;
    dword raw_data_size;
    dword raw_data_ptr;
    dword reloc_ptr;
    dword ln_ptr;
    word reloc_num;
    word ln_num;
    dword characteristics;

} __attribute__((packed)) pe_section_header;

typedef struct _import_directory_table {
    dword lookup_rva;
    dword tstamp;
    dword fwd_chain;
    dword name_rva;
    dword iat_rva;
} __attribute__((packed)) idt;

typedef struct _export_entry {
    char * name;
    int forward;
    char * fwd_string;
    dword vaddr;
} export_entry;

typedef struct _export_directory_table{
    dword export_flags;
    dword tstamp;
    word majorversion;
    word mintorvesion;
    dword name_rva;
    dword ordbase;
    dword entry_table_addr;
    dword num_name_pointers;
    dword export_table_rva;
    dword name_pointer_rva;
    dword ord_table_rva;		
} __attribute__((packed)) export_directory_table;

#define PE_IS_ORDINAL(i) (i&0x80000000)
#define PE_NAME_IDX(i)  (i&0x7FFFFFFF)

typedef struct _import_lookup_table {
    dword name_idx;
} __attribute__((packed)) ilt;


#pragma warning( disable : 4200 )

typedef struct _hint_name_table {
    unsigned short hint;
    char name[];
} __attribute__((packed)) hnt;

typedef struct _pe_reloc_info {
    dword rva;
    dword type;
} __attribute__((packed)) pe_reloc_info;

typedef struct _pe_info {
    char name[256];
    pe_header * ex_header;
    pe_opt_header32 * ex_opt_header;

    dword sects_base;
    pe_section_header ** sects;
    int sects_num;

    pe_section_header * text;

    it_inner_entry * import_table;
    int import_table_sz;
    byte *raw;
    pe_reloc_info * reloc_info;
    export_entry * export_table;
    int export_table_sz;
    byte * base;
} pe_info;

typedef struct _pe_reloc_table_block {
    dword rva;
    dword block_size;
} __attribute__((packed)) pe_reloc_table_block;



int process_file(char * filename, pe_info * info);

#define IMAGE_SCN_MEM_EXECUTE 0x20000000
#define IMAGE_SCN_MEM_READ  0x40000000
#define IMAGE_SCN_MEM_WRITE 0x80000000
#define IMAGE_SCN_CNT_UNINITIALIZED_DATA  0x00000080
#define IMAGE_SCN_MEM_DISCARDABLE 0x02000000 


#endif /* PELOADER_H */

